package com.edmi.view;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.menu.ControllerTableFacturacion;
import com.edmi.reports.ReportNotaCredito;
import java.util.Date;
import javax.swing.JOptionPane;
import com.edmi.util.Util;

public class DialogNotaCreditoDebito extends javax.swing.JDialog {
    private static int row, opcion;

    public DialogNotaCreditoDebito(java.awt.Frame parent, boolean modal, int opcion) {
        super(parent, modal);
        initComponents();
        DialogNotaCreditoDebito.opcion = opcion;
        setPreferences();
    }

    private void setPreferences() {
        this.setResizable(false);
        this.dcFechaEmision.setDate(new Date());
        this.txtDescuento.setVisible(false);
        this.lblDescuento.setVisible(false);
        this.txtMotivo.setEditable(false);
        this.txtIdCabecera.setVisible(false);

        Util.restriccionLetras(txtDescuento);

        if (opcion == 1) {
            cboTipoOperacion.addItem("ANULACION DE OPERACION");
            cboTipoOperacion.addItem("DEVOLUCION POR ITEM");
            cboTipoOperacion.addItem("DESCUENTO GLOBAL");
        } else if (opcion == 2) {
            cboTipoOperacion.addItem("INTERESES POR MORA");
            cboTipoOperacion.addItem("AUMENTO EN EL VALOR");
            cboTipoOperacion.addItem("PENALIDAD");
        }
    }

    public static int getRow() {
        return DialogNotaCreditoDebito.row;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cboTipoOperacion = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        txtCodigoCliente = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtTipoDocumento = new javax.swing.JTextField();
        txtNumeroDocumento = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtRazonSocial = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtSerieComprobante = new javax.swing.JTextField();
        txtNumeroComprobante = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        dcFechaEmision = new com.toedter.calendar.JDateChooser();
        jLabel9 = new javax.swing.JLabel();
        txtCodigoNota = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaProductosNotaCredito = new javax.swing.JTable();
        txtSumaTotal = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtIGV = new javax.swing.JTextField();
        lblDescuento = new javax.swing.JLabel();
        txtDescuento = new javax.swing.JTextField();
        btnProcesar = new javax.swing.JButton();
        txtFechaComprobante = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtMotivo = new javax.swing.JTextField();
        txtIdCabecera = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Nota de Crédito");

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Tipo de operación:");

        cboTipoOperacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cboTipoOperacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --" }));
        cboTipoOperacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTipoOperacionActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setText("Código cliente:");

        txtCodigoCliente.setEditable(false);
        txtCodigoCliente.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText("Tipo/Nro. Documento:");

        txtTipoDocumento.setEditable(false);
        txtTipoDocumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtNumeroDocumento.setEditable(false);
        txtNumeroDocumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel4.setText("Razon social:");

        txtRazonSocial.setEditable(false);
        txtRazonSocial.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel5.setText("Serie doc. afectado:");

        jLabel6.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel6.setText("Nro. doc. afectado:");

        txtSerieComprobante.setEditable(false);
        txtSerieComprobante.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtNumeroComprobante.setEditable(false);
        txtNumeroComprobante.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel7.setText("Fecha documento:");

        jLabel8.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel8.setText("Fecha emsión:");

        dcFechaEmision.setDateFormatString("yyyy-MM-dd");
        dcFechaEmision.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel9.setText("Código nota crédito:");

        txtCodigoNota.setEditable(false);
        txtCodigoNota.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        tablaProductosNotaCredito.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tablaProductosNotaCredito.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "COD. PRODUCTO", "NOMBRE", "CANTIDAD", "P. UNIDAD (S/)", "SUBTOTAL (S/)", "UNIDAD MEDIDA"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaProductosNotaCredito.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaProductosNotaCredito.getTableHeader().setReorderingAllowed(false);
        tablaProductosNotaCredito.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaProductosNotaCreditoMouseClicked(evt);
            }
        });
        tablaProductosNotaCredito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaProductosNotaCreditoKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tablaProductosNotaCredito);
        if (tablaProductosNotaCredito.getColumnModel().getColumnCount() > 0) {
            tablaProductosNotaCredito.getColumnModel().getColumn(0).setPreferredWidth(150);
            tablaProductosNotaCredito.getColumnModel().getColumn(1).setPreferredWidth(300);
            tablaProductosNotaCredito.getColumnModel().getColumn(2).setPreferredWidth(80);
            tablaProductosNotaCredito.getColumnModel().getColumn(3).setPreferredWidth(120);
            tablaProductosNotaCredito.getColumnModel().getColumn(4).setPreferredWidth(120);
            tablaProductosNotaCredito.getColumnModel().getColumn(5).setPreferredWidth(120);
        }

        txtSumaTotal.setEditable(false);
        txtSumaTotal.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel11.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel11.setText("Suma total: S/.");

        jLabel12.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel12.setText("IGV: S/.");

        txtIGV.setEditable(false);
        txtIGV.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        lblDescuento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblDescuento.setText("Descuento: S/.");

        txtDescuento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        btnProcesar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnProcesar.setText("PROCESAR");
        btnProcesar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnProcesarActionPerformed(evt);
            }
        });

        txtFechaComprobante.setEditable(false);
        txtFechaComprobante.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel10.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel10.setText("Motivo:");

        txtMotivo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtIdCabecera.setText("jTextField1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jLabel11)
                        .addGap(2, 2, 2)
                        .addComponent(txtSumaTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIGV, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblDescuento)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDescuento, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnProcesar))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 710, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(26, 26, 26)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel1)
                                        .addComponent(jLabel2)
                                        .addComponent(jLabel9))
                                    .addGap(26, 26, 26))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addContainerGap()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel3)
                                        .addComponent(jLabel10))
                                    .addGap(18, 18, 18)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtCodigoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(cboTipoOperacion, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGap(1, 1, 1)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(txtTipoDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(txtNumeroDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(txtRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addGap(26, 26, 26)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel5)
                                            .addGap(18, 18, 18)
                                            .addComponent(txtSerieComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel6)
                                            .addGap(24, 24, 24)
                                            .addComponent(txtNumeroComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabel8)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel7)
                                            .addGap(24, 24, 24)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(dcFechaEmision, javax.swing.GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
                                                .addComponent(txtFechaComprobante)))
                                        .addComponent(txtIdCabecera, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addComponent(txtCodigoNota, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtMotivo)))))
                .addContainerGap(24, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCodigoNota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIdCabecera, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtFechaComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(cboTipoOperacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtCodigoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(dcFechaEmision, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtSerieComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtTipoDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtNumeroDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtNumeroComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtRazonSocial)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtMotivo))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIGV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSumaTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDescuento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblDescuento, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnProcesar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnProcesarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnProcesarActionPerformed
        ReportNotaCredito report = new ReportNotaCredito();

        if (this.cboTipoOperacion.getSelectedIndex() <= 0) {
            JOptionPane.showMessageDialog(this, "Debe seleccionar el tipo de operación.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (opcion == 1) {
            if (this.cboTipoOperacion.getSelectedIndex() == 3 && this.txtDescuento.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "El campo 'descuento' es requerido.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }
        } else if (opcion == 2) {
            if (this.cboTipoOperacion.getSelectedIndex() == 1 && this.txtDescuento.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Debe ingresar el interes de mora.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }
        }

        int op = JOptionPane.showConfirmDialog(this, "¿Desea generar el comprobante?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if (op == JOptionPane.YES_OPTION) {
            report.enviarDatosNota(opcion, Long.parseLong(ControllerTableFacturacion.getIdCabecera()), this);
        }
    }//GEN-LAST:event_btnProcesarActionPerformed

    private void cboTipoOperacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTipoOperacionActionPerformed
        if (opcion == 1) {
            switch (this.cboTipoOperacion.getSelectedIndex()) {
                case 0:
                    this.txtMotivo.setText("");
                    this.txtMotivo.setEditable(false);

                    this.txtDescuento.setVisible(false);
                    this.lblDescuento.setVisible(false);
                    break;
                case 1:
                    this.txtMotivo.setText("");
                    this.txtMotivo.setEditable(true);

                    this.txtDescuento.setVisible(false);
                    this.lblDescuento.setVisible(false);
                    break;
                case 2:
                    this.txtMotivo.setText("DEVOLUCION POR PRODUCTO");
                    this.txtMotivo.setEditable(true);

                    this.lblDescuento.setText("Codigo:");
                    this.lblDescuento.setVisible(true);

                    this.txtDescuento.setVisible(true);
                    this.txtDescuento.setText(this.tablaProductosNotaCredito.getValueAt(row, 0).toString());
                    this.txtDescuento.setEditable(false);
                    break;
                case 3:
                    this.txtMotivo.setText("DESCUENTO GLOBAL");
                    this.txtMotivo.setEditable(true);

                    this.lblDescuento.setText("Descuento: S/.");
                    this.lblDescuento.setVisible(true);

                    this.txtDescuento.setText("");
                    this.txtDescuento.setEditable(true);
                    this.txtDescuento.setVisible(true);
            }
        } else if (opcion == 2) {
            this.tablaProductosNotaCredito.removeColumn(this.tablaProductosNotaCredito.getColumnModel().getColumn(0));

            switch (this.cboTipoOperacion.getSelectedIndex()) {
                case 0:
                    this.txtMotivo.setText("");
                    this.txtMotivo.setEditable(false);

                    this.txtDescuento.setVisible(false);
                    this.lblDescuento.setVisible(false);
                    break;
                case 1:
                    this.txtMotivo.setText("INTERESES POR MORA");
                    this.txtMotivo.setEditable(false);

                    this.txtDescuento.setText("");
                    this.txtDescuento.setEditable(true);
                    this.txtDescuento.setVisible(true);

                    this.lblDescuento.setText("Costo: S/.");
                    this.lblDescuento.setVisible(true);
                    break;
                case 2:
                    this.txtMotivo.setText("AUMENTO EN EL VALOR");
                    this.txtMotivo.setEditable(false);

                    this.lblDescuento.setText("");
                    this.lblDescuento.setVisible(false);

                    this.txtDescuento.setVisible(false);
                    this.txtDescuento.setEditable(false);
                    break;

                case 3:
                    this.txtMotivo.setText("PENALIDAD");
                    this.txtMotivo.setEditable(true);

                    this.lblDescuento.setText("");
                    this.lblDescuento.setVisible(true);

                    this.txtDescuento.setText("");
                    this.txtDescuento.setEditable(true);
                    this.txtDescuento.setVisible(true);
            }
        }
    }//GEN-LAST:event_cboTipoOperacionActionPerformed

    private void tablaProductosNotaCreditoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaProductosNotaCreditoMouseClicked
        if (evt.getClickCount() == 1) {
            if (opcion == 1) {
                if(this.cboTipoOperacion.getSelectedIndex() == 3){
                    this.txtDescuento.setText("");
                }else{
                    row = this.tablaProductosNotaCredito.getSelectedRow();
                    this.txtDescuento.setText(this.tablaProductosNotaCredito.getValueAt(row, 0).toString());
                }
            } else if (opcion == 2) {
                this.txtDescuento.setText("");
            }
        }
    }//GEN-LAST:event_tablaProductosNotaCreditoMouseClicked

    private void tablaProductosNotaCreditoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaProductosNotaCreditoKeyReleased
        double precioTotal = 0.00D;
        for (int i = 0; i < this.tablaProductosNotaCredito.getRowCount(); i++) {
            precioTotal += Double.parseDouble(this.tablaProductosNotaCredito.getValueAt(i, 4).toString());
        }

        this.txtSumaTotal.setText(Util.decimalRound(precioTotal));
    }//GEN-LAST:event_tablaProductosNotaCreditoKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnProcesar;
    public javax.swing.JComboBox<String> cboTipoOperacion;
    public com.toedter.calendar.JDateChooser dcFechaEmision;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    public static javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblDescuento;
    public javax.swing.JTable tablaProductosNotaCredito;
    public javax.swing.JTextField txtCodigoCliente;
    public javax.swing.JTextField txtCodigoNota;
    public javax.swing.JTextField txtDescuento;
    public javax.swing.JTextField txtFechaComprobante;
    public javax.swing.JTextField txtIGV;
    public javax.swing.JTextField txtIdCabecera;
    public javax.swing.JTextField txtMotivo;
    public javax.swing.JTextField txtNumeroComprobante;
    public javax.swing.JTextField txtNumeroDocumento;
    public javax.swing.JTextField txtRazonSocial;
    public javax.swing.JTextField txtSerieComprobante;
    public javax.swing.JTextField txtSumaTotal;
    public javax.swing.JTextField txtTipoDocumento;
    // End of variables declaration//GEN-END:variables
}
