package com.edmi.view.panel;

import com.edmi.model.Consumo;
import com.edmi.repository.imp.ConsumoRepositoryImp;
import static com.edmi.util.MessageSystem.SYS_MSG;
import java.util.List;
import com.edmi.util.Util;
import com.edmi.view.DialogReportFiles;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public final class PanelReportVentas extends javax.swing.JPanel {
    private static PanelReportVentas instance = null;
    private ConsumoRepositoryImp repositoryConsumo = null;
    private String opcion = "1";
    private String hojaExcel = "TODOS LOS CLIENTES";
    private String nombreArchivo = "";

    private PanelReportVentas() {
        initComponents();
        repositoryConsumo = new ConsumoRepositoryImp();
        mostrarConsumos();
        txtFechaInicio.setEnabled(false);
        txtFechaFinal.setEnabled(false);
        btnBuscarFecha.setEnabled(false);
        nombreArchivo = "TV_".concat(Util.getDateFormat(new Date()));
        PanelReportVentas.tablaConsumoReport.getColumnModel().getColumn(1).setMaxWidth(0);
        PanelReportVentas.tablaConsumoReport.getColumnModel().getColumn(1).setMinWidth(0);
        PanelReportVentas.tablaConsumoReport.getColumnModel().getColumn(1).setPreferredWidth(0);
    }

    public static PanelReportVentas getInstance() {
        if (instance == null) {
            instance = new PanelReportVentas();
        }
        return instance;
    }
    
    private void almacenarReporte() {
        String path = DialogReportFiles.obtenerRutaArchiReporPdfXml()[0];
        boolean res = generareReporte(path + File.separator + nombreArchivo + ".xlsx");
        if (res) {
            JOptionPane.showMessageDialog(this, "Exportacion generada con exito\n\n"
                    + "RUTA: " + path + File.separator + nombreArchivo, SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(this, "No se pudo generar el reporte,\n\ncontacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }

    private boolean generareReporte(String sheetName) {
        List<Consumo> consumos = null;
        switch (opcion) {
            case "1":
                consumos = repositoryConsumo.findAll();
                break;

            case "2":
                consumos = repositoryConsumo.findAllWhere("", "SI");
                break;
                
            case "3":
                consumos = repositoryConsumo.findAllDate(txtFechaInicio.getText().trim(), txtFechaFinal.getText().trim());
                break;
        }
        
        boolean response = false;
        Workbook book = new XSSFWorkbook();
        Sheet sheet = book.createSheet(this.hojaExcel);
        
        try {
            File ruta = new File("design" + File.separator + "logo inicio");
            String[] img = ruta.list();
            String logo = ruta.getAbsolutePath() + File.separator + img[0];

            InputStream is = new FileInputStream(logo);
            byte[] bytes = IOUtils.toByteArray(is);
            int imgIndex = book.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG); //Agrega la imagen y su tipo de imagen.
            is.close();

            //Agregamos la imagen al archivo.
            CreationHelper help = book.getCreationHelper();
            Drawing draw = sheet.createDrawingPatriarch(); //Para crear la imagen.

            ClientAnchor anchor = help.createClientAnchor(); //Sacamos el ancho de la imagen para colocarlo de forma correcta.
            anchor.setCol1(0); //Posicion columna.
            anchor.setRow1(1); //Posicion fila.
            Picture pict = draw.createPicture(anchor, imgIndex); //Aparece la imagen.
            pict.resize(1, 5); //Indicamos el tamaño, de donde comenzara y cuanto espacio utilizara.

        } catch (IOException e) {
            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, e);
        }

        CellStyle estiloTitulo = book.createCellStyle(); //Para crear el diseño del titulo.
        estiloTitulo.setAlignment(HorizontalAlignment.CENTER); //Para centrar el titulo.
        estiloTitulo.setVerticalAlignment(VerticalAlignment.CENTER);

        org.apache.poi.ss.usermodel.Font fontTitulo = book.createFont(); //Problemas de compatibilidad con el itextPdf no se podia importar.

        fontTitulo.setFontName("Arial");
        fontTitulo.setBold(true);
        fontTitulo.setFontHeightInPoints((short) 22); //tamaño de letra
        estiloTitulo.setFont(fontTitulo); //agregamos los cambios.
        Row filaTitulo = sheet.createRow(2); //Creamos la fila para el titulo
        Cell celdaTitulo = filaTitulo.createCell(1); //Posicion columna
        celdaTitulo.setCellStyle(estiloTitulo); //Agregamos el estilo
        celdaTitulo.setCellValue("REPORTE DE VENTAS");

        sheet.addMergedRegion(new CellRangeAddress(2, 2, 1, 3));
        
        sheet.setColumnWidth(0, 6000);
        sheet.setColumnWidth(1, 12000);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 4000);
        sheet.setColumnWidth(4, 6000);
        sheet.setColumnWidth(5, 7000);
        sheet.setColumnWidth(6, 7000);
        sheet.setColumnWidth(7, 4000);
        sheet.setColumnWidth(8, 4000);

        CellStyle estiloTablaTitulos = book.createCellStyle(); //Para crear el diseño del titulo.
        estiloTablaTitulos.setAlignment(HorizontalAlignment.CENTER); //Para centrar el titulo.
        estiloTablaTitulos.setVerticalAlignment(VerticalAlignment.CENTER);

        String cabecera[] = new String[]{"CODIGO PRODUCTO", "NOMBRE PRODCUTO", "CANTIDAD", "GRATUITO", "FECHA VENTA", "PRECIO UNITARIO (S/)",
            "IMPORTE TOTAL (S/)", "IGV (S/)", "ICBPER (S/)"};

        CellStyle estiloTablaCabezera = book.createCellStyle();
        estiloTablaCabezera.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        estiloTablaCabezera.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        estiloTablaCabezera.setBorderBottom(BorderStyle.THIN);
        estiloTablaCabezera.setBorderLeft(BorderStyle.THIN);
        estiloTablaCabezera.setBorderRight(BorderStyle.THIN);
        estiloTablaCabezera.setBorderTop(BorderStyle.THIN);

        org.apache.poi.ss.usermodel.Font font = book.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setFontHeightInPoints((short) 12);
        font.setColor(IndexedColors.WHITE.getIndex());
        estiloTablaCabezera.setFont(font);

        Row row = sheet.createRow(7);

        for (int i = 0; i < cabecera.length; i++) {
            Cell celdaCabecera = row.createCell(i);
            celdaCabecera.setCellStyle(estiloTablaCabezera);
            celdaCabecera.setCellValue(cabecera[i]);
        }
        
        CellStyle estiloTablaDatos = book.createCellStyle();
        estiloTablaDatos.setBorderBottom(BorderStyle.THIN);
        estiloTablaDatos.setBorderLeft(BorderStyle.THIN);
        estiloTablaDatos.setBorderRight(BorderStyle.THIN);
        estiloTablaDatos.setBorderTop(BorderStyle.THIN);        

        int nroRow = 8;
        for (Consumo c : consumos) {
            Row fila = sheet.createRow(nroRow);
            
            Cell celdaInformacion0 = fila.createCell(0);
            celdaInformacion0.setCellValue(c.getCodProducto());
            celdaInformacion0.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion1 = fila.createCell(1);
            celdaInformacion1.setCellValue(c.getNombreProducto());
            celdaInformacion1.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion2 = fila.createCell(2);
            celdaInformacion2.setCellValue(c.getCantidad());
            celdaInformacion2.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion3 = fila.createCell(3);
            celdaInformacion3.setCellValue(c.getGratuito());
            celdaInformacion3.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion4 = fila.createCell(4);
            celdaInformacion4.setCellValue(c.getFechaVenta());
            celdaInformacion4.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion5 = fila.createCell(5);
            celdaInformacion5.setCellValue(c.getPrecioUnitario());
            celdaInformacion5.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion6 = fila.createCell(6);
            celdaInformacion6.setCellValue(c.getImporteTotal());
            celdaInformacion6.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion7 = fila.createCell(7);
            celdaInformacion7.setCellValue(c.getTributoIgv());
            celdaInformacion7.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion8 = fila.createCell(8);
            celdaInformacion8.setCellValue(c.getTributoIcbper());
            celdaInformacion8.setCellStyle(estiloTablaDatos);

            nroRow++;
        }

        try {
            try ( FileOutputStream out = new FileOutputStream(sheetName)) {
                book.write(out);
                response = true;
            }
        } catch (IOException ex) {
            Logger.getLogger(PanelReportProduct.class.getName()).log(Level.SEVERE, null, ex);
            response = false;
        }
        
        try {
            book.close();
        } catch (IOException ex) {
            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    private void generarPDF() {
        Document documento = new Document();
        documento.setMargins(24f, 24f, 24f, 24f);

        String path = DialogReportFiles.obtenerRutaArchiReporPdfXml()[0];

        try {
            PdfWriter.getInstance(documento, new FileOutputStream(path + File.separator + nombreArchivo + ".pdf"));
            documento.open();
        } catch (DocumentException | HeadlessException | FileNotFoundException e) {
            JOptionPane.showMessageDialog(this, "No se pudo generar el reporte,\n\ncontacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        List<Consumo> consumos = null;
        switch (opcion) {
            case "1":
                consumos = repositoryConsumo.findAll();
                break;

            case "2":
                consumos = repositoryConsumo.findAllWhere("gratuito", "SI");
                break;
                
            case "3":
                consumos = repositoryConsumo.findAllDate(txtFechaInicio.getText().trim(), txtFechaFinal.getText().trim());
                break;
        }

        try {
            documento.setPageSize(PageSize.A2.rotate());
            documento.open();

            BaseFont fuenteUser = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, true);
            Font tituloUser = new Font(fuenteUser, 20f, Font.BOLD, BaseColor.BLACK);

            BaseFont fuenteUser1 = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, true);
            Font celdaTitulo = new Font(fuenteUser1, 15f, Font.BOLD, BaseColor.RED);

            var titulo = new Paragraph(hojaExcel + "\n\n", tituloUser);
            titulo.setAlignment(Paragraph.ALIGN_CENTER);
            documento.add(titulo);

            PdfPTable tablapdf = new PdfPTable(new float[]{75f, 75f, 75f, 75f, 75f, 75f, 75f, 75f, 75f});
            tablapdf.setWidthPercentage(100f);
            tablapdf.addCell(new Phrase("CODIGO PRODUCTOCODIGO PRODUCTO", celdaTitulo));
            tablapdf.addCell(new Phrase("NOMBRE PRODCUTO", celdaTitulo));
            tablapdf.addCell(new Phrase("CANTIDAD", celdaTitulo));
            tablapdf.addCell(new Phrase("GRATUITO", celdaTitulo));
            tablapdf.addCell(new Phrase("FECHA VENTA", celdaTitulo));
            tablapdf.addCell(new Phrase("PRECIO UNITARIO (S/)", celdaTitulo));
            tablapdf.addCell(new Phrase("IMPORTE TOTAL (S/)", celdaTitulo));
            tablapdf.addCell(new Phrase("IGV (S/)", celdaTitulo));
            tablapdf.addCell(new Phrase("ICBPER (S/)", celdaTitulo));

            for (Consumo c : consumos) {
                tablapdf.addCell(c.getCodProducto());
                tablapdf.addCell(c.getNombreProducto());
                tablapdf.addCell(c.getCantidad().toString());
                tablapdf.addCell(c.getGratuito());
                tablapdf.addCell(c.getFechaVenta());
                tablapdf.addCell(c.getPrecioUnitario().toString());
                tablapdf.addCell(c.getImporteTotal().toString());
                tablapdf.addCell(c.getTributoIgv().toString());
                tablapdf.addCell(c.getTributoIcbper().toString());
            }
            documento.add(tablapdf);
            documento.close();

            JOptionPane.showMessageDialog(this, "Exportación generada con exito\n\n"
                    + "RUTA: " + path + File.separator + nombreArchivo, SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } catch (DocumentException | IOException ex) {
            JOptionPane.showMessageDialog(this, "Error al exportar archivo, intente nuevamente.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }

    public void mostrarConsumos() {
        clearTableProducts();
        
        List<Consumo> consumos = null;
        switch (opcion) {
            case "1":
                consumos = repositoryConsumo.findAll();
                break;

            case "2":
                System.out.println("opcion =" + opcion);
                consumos = repositoryConsumo.findAllWhere("gratuito", "SI");
                break;
                
            case "3":
                consumos = repositoryConsumo.findAllDate(txtFechaInicio.getText().trim(), txtFechaFinal.getText().trim());
                break;
        }
        
        DefaultTableModel model = (DefaultTableModel) tablaConsumoReport.getModel();
        for (Consumo c : consumos) {
            model.addRow(new Object[]{
                c.getCodProducto(), c.getCodReserva(), c.getNombreProducto(), c.getCantidad(), c.getGratuito(),
                c.getFechaVenta(), c.getPrecioUnitario() <= 0.0D ? "0.00" : c.getPrecioUnitario(),
                c.getImporteTotal() <= 0.0D ? "0.00" : c.getImporteTotal(),
                c.getTributoIgv(), c.getTributoIcbper(), c.getDescuento() <= 0.00D ? "0.00" : c.getDescuento()
            });
        }
        tablaConsumoReport.setModel(model);
    }

    public void clearTableProducts() {
        DefaultTableModel model = (DefaultTableModel) tablaConsumoReport.getModel();
        for (int i = tablaConsumoReport.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaConsumoReport = new javax.swing.JTable();
        rbTodo = new javax.swing.JRadioButton();
        rbGratuito = new javax.swing.JRadioButton();
        rbFechaVenta = new javax.swing.JRadioButton();
        btnExportarExcel = new javax.swing.JButton();
        btnExportarPDF = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        txtFechaInicio = new javax.swing.JTextField();
        txtFechaFinal = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnBuscarFecha = new javax.swing.JButton();

        tablaConsumoReport.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tablaConsumoReport.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "COD. PRODUCTO", "ID_RESERVA", "NOMBRE", "CANTIDAD", "GRATUITO", "FECHA VENTA", "P. UNIDAD (S/)", "SUBTOTAL (S/)", "IGV", "ICBPER"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, true, true
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaConsumoReport.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaConsumoReport.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tablaConsumoReport);
        if (tablaConsumoReport.getColumnModel().getColumnCount() > 0) {
            tablaConsumoReport.getColumnModel().getColumn(0).setPreferredWidth(130);
            tablaConsumoReport.getColumnModel().getColumn(1).setPreferredWidth(120);
            tablaConsumoReport.getColumnModel().getColumn(2).setPreferredWidth(300);
            tablaConsumoReport.getColumnModel().getColumn(5).setPreferredWidth(150);
            tablaConsumoReport.getColumnModel().getColumn(6).setPreferredWidth(120);
            tablaConsumoReport.getColumnModel().getColumn(7).setPreferredWidth(120);
            tablaConsumoReport.getColumnModel().getColumn(8).setPreferredWidth(100);
            tablaConsumoReport.getColumnModel().getColumn(9).setPreferredWidth(100);
        }

        buttonGroup1.add(rbTodo);
        rbTodo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbTodo.setSelected(true);
        rbTodo.setText("TODO");
        rbTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbTodoActionPerformed(evt);
            }
        });

        buttonGroup1.add(rbGratuito);
        rbGratuito.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbGratuito.setText("GRATUITO");
        rbGratuito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbGratuitoActionPerformed(evt);
            }
        });

        buttonGroup1.add(rbFechaVenta);
        rbFechaVenta.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbFechaVenta.setText("FECHA VENTA");
        rbFechaVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbFechaVentaActionPerformed(evt);
            }
        });

        btnExportarExcel.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnExportarExcel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_microsoft_excel_26px.png"))); // NOI18N
        btnExportarExcel.setText("EXPORTAR EXCEL");
        btnExportarExcel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExportarExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarExcelActionPerformed(evt);
            }
        });

        btnExportarPDF.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnExportarPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_pdf_30px.png"))); // NOI18N
        btnExportarPDF.setText("EXPORTAR PDF");
        btnExportarPDF.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExportarPDF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarPDFActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel1.setText("REPORTE DE VENTAS");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        txtFechaInicio.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtFechaFinal.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setText("Inicio:");

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText("Fin:");

        btnBuscarFecha.setText("jButton1");
        btnBuscarFecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarFechaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(rbFechaVenta)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtFechaFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnBuscarFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(rbTodo)
                                .addGap(18, 18, 18)
                                .addComponent(rbGratuito)))
                        .addGap(17, 17, 17)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnExportarExcel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnExportarPDF, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 892, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rbGratuito)
                            .addComponent(rbTodo))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rbFechaVenta)
                            .addComponent(txtFechaInicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFechaFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBuscarFecha)))
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(btnExportarExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnExportarPDF, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 291, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void rbTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbTodoActionPerformed
        opcion = "1";
        hojaExcel = "TODOS LAS VENTAS";
        nombreArchivo = "TV_".concat(Util.getDateFormat(new Date()));

        this.rbTodo.setSelected(true);
        this.rbGratuito.setSelected(false);
        this.rbFechaVenta.setSelected(false);
        this.txtFechaInicio.setEnabled(false);
        this.txtFechaFinal.setEnabled(false);
        this.btnBuscarFecha.setEnabled(false);
        this.txtFechaInicio.setText("");
        this.txtFechaFinal.setText("");
        
        mostrarConsumos();
    }//GEN-LAST:event_rbTodoActionPerformed

    private void rbGratuitoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbGratuitoActionPerformed
        opcion = "2";
        hojaExcel = "VENTAS GRATUITAS";
        nombreArchivo = "VG_".concat(Util.getDateFormat(new Date()));

        this.txtFechaInicio.setEnabled(false);
        this.txtFechaFinal.setEnabled(false);
        this.btnBuscarFecha.setEnabled(false);
        this.txtFechaInicio.setText("");
        this.txtFechaFinal.setText("");
        
        mostrarConsumos();
    }//GEN-LAST:event_rbGratuitoActionPerformed

    private void rbFechaVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbFechaVentaActionPerformed
        opcion = "3";
        hojaExcel = "VENTAS POR FECHA";
        nombreArchivo = "VF_".concat(Util.getDateFormat(new Date()));

        this.txtFechaInicio.setEnabled(true);
        this.txtFechaFinal.setEnabled(true);
        this.btnBuscarFecha.setEnabled(true);
    }//GEN-LAST:event_rbFechaVentaActionPerformed

    private void btnBuscarFechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarFechaActionPerformed
        mostrarConsumos();
    }//GEN-LAST:event_btnBuscarFechaActionPerformed

    private void btnExportarExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarExcelActionPerformed
        almacenarReporte();
    }//GEN-LAST:event_btnExportarExcelActionPerformed

    private void btnExportarPDFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarPDFActionPerformed
        generarPDF();
    }//GEN-LAST:event_btnExportarPDFActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscarFecha;
    private javax.swing.JButton btnExportarExcel;
    private javax.swing.JButton btnExportarPDF;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    public static javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JRadioButton rbFechaVenta;
    private javax.swing.JRadioButton rbGratuito;
    private javax.swing.JRadioButton rbTodo;
    public static javax.swing.JTable tablaConsumoReport;
    private javax.swing.JTextField txtFechaFinal;
    private javax.swing.JTextField txtFechaInicio;
    // End of variables declaration//GEN-END:variables
}
