package com.edmi.view.widget;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

public class MenuTableProduct {
    private static MenuTableProduct instance;
    private JPopupMenu menu;
    public JMenuItem itemUpdateStock;
    public JMenuItem itemUpdateProduct;
    public JMenuItem itemDeleteProduct;
    
    private MenuTableProduct(){}
    
    public static MenuTableProduct getInstance(){
        if(instance == null){
            instance = new MenuTableProduct();
        }
        return instance;
    }
    
    public void initMenuProduct(JTable table){
        menu = new JPopupMenu();
        itemUpdateStock = new JMenuItem("Actualizar STOCK");
        itemUpdateProduct = new JMenuItem("Modificar producto");
        itemDeleteProduct = new JMenuItem("Eliminar producto");
        
        menu.add(itemUpdateStock);
        menu.add(itemUpdateProduct);
        menu.add(itemDeleteProduct);
        table.setComponentPopupMenu(menu);
    }
}
