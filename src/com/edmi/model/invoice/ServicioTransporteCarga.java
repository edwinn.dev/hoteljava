package com.edmi.model.invoice;

public class ServicioTransporteCarga {
    private String idLineaServicio;
    private String detViaje;
    private String ubiOrigen;
    private String dirOrigen;
    private String ubiDestino;
    private String dirDestino;
    private String tipReferencialServicio;
    private String varReferencialServicio;
    private String tipCargaEfectiva;
    private String varCargaEfectiva;
    private String tipCargaUtil;
    private String varCargaUtil;

    public String getIdLineaServicio() {
        return idLineaServicio;
    }

    public void setIdLineaServicio(String idLineaServicio) {
        this.idLineaServicio = idLineaServicio;
    }

    public String getDetViaje() {
        return detViaje;
    }

    public void setDetViaje(String detViaje) {
        this.detViaje = detViaje;
    }

    public String getUbiOrigen() {
        return ubiOrigen;
    }

    public void setUbiOrigen(String ubiOrigen) {
        this.ubiOrigen = ubiOrigen;
    }

    public String getDirOrigen() {
        return dirOrigen;
    }

    public void setDirOrigen(String dirOrigen) {
        this.dirOrigen = dirOrigen;
    }

    public String getUbiDestino() {
        return ubiDestino;
    }

    public void setUbiDestino(String ubiDestino) {
        this.ubiDestino = ubiDestino;
    }

    public String getDirDestino() {
        return dirDestino;
    }

    public void setDirDestino(String dirDestino) {
        this.dirDestino = dirDestino;
    }

    public String getTipReferencialServicio() {
        return tipReferencialServicio;
    }

    public void setTipReferencialServicio(String tipReferencialServicio) {
        this.tipReferencialServicio = tipReferencialServicio;
    }

    public String getVarReferencialServicio() {
        return varReferencialServicio;
    }

    public void setVarReferencialServicio(String varReferencialServicio) {
        this.varReferencialServicio = varReferencialServicio;
    }

    public String getTipCargaEfectiva() {
        return tipCargaEfectiva;
    }

    public void setTipCargaEfectiva(String tipCargaEfectiva) {
        this.tipCargaEfectiva = tipCargaEfectiva;
    }

    public String getVarCargaEfectiva() {
        return varCargaEfectiva;
    }

    public void setVarCargaEfectiva(String varCargaEfectiva) {
        this.varCargaEfectiva = varCargaEfectiva;
    }

    public String getTipCargaUtil() {
        return tipCargaUtil;
    }

    public void setTipCargaUtil(String tipCargaUtil) {
        this.tipCargaUtil = tipCargaUtil;
    }

    public String getVarCargaUtil() {
        return varCargaUtil;
    }

    public void setVarCargaUtil(String varCargaUtil) {
        this.varCargaUtil = varCargaUtil;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ServicioTransporteCarga{idLineaServicio=").append(idLineaServicio);
        sb.append(", detViaje=").append(detViaje);
        sb.append(", ubiOrigen=").append(ubiOrigen);
        sb.append(", dirOrigen=").append(dirOrigen);
        sb.append(", ubiDestino=").append(ubiDestino);
        sb.append(", dirDestino=").append(dirDestino);
        sb.append(", tipReferencialServicio=").append(tipReferencialServicio);
        sb.append(", varReferencialServicio=").append(varReferencialServicio);
        sb.append(", tipCargaEfectiva=").append(tipCargaEfectiva);
        sb.append(", varCargaEfectiva=").append(varCargaEfectiva);
        sb.append(", tipCargaUtil=").append(tipCargaUtil);
        sb.append(", varCargaUtil=").append(varCargaUtil);
        sb.append('}');
        return sb.toString();
    }
}
