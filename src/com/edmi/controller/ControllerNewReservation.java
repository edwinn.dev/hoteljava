package com.edmi.controller;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.panel.ControllerReservation;
import com.edmi.model.Cliente;
import com.edmi.model.Habitacion;
import com.edmi.model.Reservacion;
import com.edmi.model.Trabajador;
import com.edmi.repository.RepositoryEmployee;
import com.edmi.repository.RepositoryReservation;
import com.edmi.repository.RepositoryRoom;
import com.edmi.repository.imp.HabitacionRepositoryImp;
import com.edmi.repository.imp.ReservaRepositoryImp;
import com.edmi.repository.imp.TrabajadorRepositoryImp;
import com.edmi.view.AddDataReserva;
import com.edmi.view.DialogSearchClient;
import com.edmi.view.DialogSearchRoom;
import com.edmi.view.ModalNewReservation;
import com.edmi.util.MessageView;
import com.edmi.util.Util;
import com.edmi.view.MainPanelServices;
import com.edmi.view.ModalNewClient;
import com.edmi.view.panel.PanelReportReserva;
import com.edmi.view.panel.PanelReservation;
import com.edmi.view.widget.CardRoom;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ControllerNewReservation implements ActionListener {
    private final ImageIcon ocupado = new ImageIcon(getClass().getResource("/com/edmi/images/sleeping_bed.png"));
    private final ImageIcon disponible = new ImageIcon(getClass().getResource("/com/edmi/images/icon_double_bed.png"));
    private ModalNewReservation modal = null;
    private MainPanelServices mainFrame = null;
    private RepositoryReservation<Reservacion> repositoryReserva = null;
    private ControllerReservation controllerReservation = null;
    private RepositoryRoom<Habitacion> repositoryRoom = null;
    
    public ControllerNewReservation(ModalNewReservation modal, MainPanelServices mainFrame, ControllerReservation controllerReservation){
        this.modal = modal;
        this.mainFrame = mainFrame;
        this.repositoryReserva = new ReservaRepositoryImp();
        this.controllerReservation = controllerReservation;
        setListener();
    }
    
    private void setListener(){
        modal.btnAgregarFecha.addActionListener(this);
        modal.btnBuscarCliente.addActionListener(this);
        modal.btnBuscarHabitacion.addActionListener(this);
        modal.btnNuevoCliente.addActionListener(this);
        modal.btnRegistrarReserva.addActionListener(this);
        modal.chkFechaActual.addActionListener(this);
        modal.btnModificar.addActionListener(this);
    }
    
    public void loadDataFromDataBase(){
        loadDefaultData();
        RepositoryEmployee<Trabajador> employeeRepository = new TrabajadorRepositoryImp();
        Trabajador employee = employeeRepository.findById(mainFrame.getCodigoUsuario());
        modal.txtCodTrabajador.setText(employee.getCodTrabajador());
        modal.txtNombreTrabajador.setText(Util.getUserNamesConcatened(employee.getNombre(), employee.getApellidoPaterno(), employee.getApellidoMaterno()));
    }
    
    public void initViewNewReservation(){
        this.modal.setModal(true);
        this.modal.setVisible(true);
        this.modal.setLocationRelativeTo(mainFrame);
    }
    
    private void loadDefaultData(){
        modal.btnAgregarFecha.setEnabled(false);
        String fechaIngreso = Util.getDateTimeFormat(new Date());
        modal.txtFechaHoraIngreso.setValue(fechaIngreso);
        String salida = Util.getDateTimeReservation(fechaIngreso, 0, 12, 00);
        modal.txtFechaHoraSalida.setValue(salida);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == modal.btnBuscarCliente){
            setDataClient();
        }else if(e.getSource() == modal.btnBuscarHabitacion){
            setDataRoom();
        }else if(e.getSource() == modal.btnRegistrarReserva){
            setDataForReserva();
        }else if(e.getSource() == modal.btnAgregarFecha){
            if(modal.txtCostoAlquiler.getText().isEmpty()){
                JOptionPane.showMessageDialog(modal, MessageView.MROOM_NO_SELECTED, SYS_MSG, JOptionPane.WARNING_MESSAGE);
                return;
            }
            addDateForReserva();
        }else if(e.getSource() == modal.chkFechaActual){
            setDateNow();
        }
        
        if (e.getSource() == modal.btnNuevoCliente) {
            ModalNewClient modalClient = new ModalNewClient(modal, true);
            ControllerCliente control = new ControllerCliente(mainFrame, modalClient);
            modalClient.btnModificarCliente.setEnabled(false);
            modalClient.setVisible(true);
            if(!modalClient.txtNroDoc.getText().trim().isEmpty()){
                control.mostrarDatosClienteEnReservacion(modal);
            }
        }
        
        if(e.getSource() == modal.btnModificar){
            modificarReserva();
        }
    }
    
    private void setDataClient(){
        DialogSearchClient form = new DialogSearchClient(modal, true);
        form.setModal(true);
        form.setVisible(true);
        
        //Si se ha seleccionado una fila en el formulario de busqueda de cliente, devuelve los datos de ese registro.
        if(form.isSelectedRow()){
            Cliente client = form.getClient();
            modal.txtTipoDoc.setValue(client.getTipoDocumento());
            modal.txtNroDoc.setValue(client.getNroDocumento());
            modal.txtNombres.setValue(Util.getUserNamesConcatened(client.getNombre(), client.getApellidoPaterno(), client.getApellidoMaterno()));
            modal.txtCodigoCliente.setValue(client.getCodCliente());
            modal.txtTelefono.setValue(client.getNroTelefono());
            modal.txtEmail.setValue(client.getEmail());
            form.dispose();
        }
    }
    
    private void setDataForReserva(){
        String codigoTrabajador = modal.txtCodTrabajador.getText().toUpperCase().trim();
        String codigoCliente = modal.txtCodigoCliente.getText().toUpperCase().trim();
        String codHabitacion = modal.txtCodigoHabitacion.getText().trim();
        String nroHabitacion = modal.txtNroHabitacion.getText().trim();
        
        if(codigoCliente.isEmpty()){
            JOptionPane.showMessageDialog(mainFrame, "Seleccione un cliente.", SYS_MSG, JOptionPane.ERROR_MESSAGE); return;
        }
        
        if(codHabitacion.isEmpty()){
            JOptionPane.showMessageDialog(mainFrame, "Seleccione un habitación.", SYS_MSG, JOptionPane.ERROR_MESSAGE); return;
        }
        
        if(codigoTrabajador.isEmpty()){
            JOptionPane.showMessageDialog(mainFrame, "Ud. no puede realizar reservas\nContacte con el administrador\n"
                    + "o reinicie el programa.", SYS_MSG, JOptionPane.ERROR_MESSAGE); return;
        }

        int option = JOptionPane.showConfirmDialog(modal, MessageView.MODAL_RESERVA_QUESTION_CREATE, SYS_MSG, JOptionPane.YES_NO_OPTION);
        if(option == JOptionPane.YES_OPTION){
            createReserva(codigoTrabajador, codigoCliente, codHabitacion, nroHabitacion);
        }
    }
    
    private void createReserva(String codEmployee, String codClient, String codHabitacion, String nroHabitacion){
        Reservacion reserva = new Reservacion();
        
        Integer cantidadHoras = Integer.parseInt(modal.lblCantidadHoras.getText());
        Integer cantidadMinutos = Integer.parseInt(modal.lblCantidadMinutos.getText());
        //Integer tiempoReserva = Util.convetHoursToMinutes(cantidadHoras, cantidadMinutos);
        
        reserva.setCodTrabajador(codEmployee);
        reserva.setCodCliente(codClient);
        reserva.setCodHabitacion(codHabitacion);
        reserva.setFechaIngreso(modal.txtFechaHoraIngreso.getValue().toString());
        reserva.setFechaSalida(modal.txtFechaHoraSalida.getValue().toString());
        reserva.setImporteReserva(Double.parseDouble(modal.txtCostoAlquiler.getText()));
        reserva.setImporteProductos(0.0D);
        reserva.setImporteTotal(Double.parseDouble(modal.txtCostoAlquiler.getText()));
        reserva.setTiempoReserva(cantidadHoras+":"+String.format("%2s", cantidadMinutos).replace(" ", "0"));
        reserva.setMagnitudReserva(Util.getMagnitudeTimeReserva());
        reserva.setEstado("1");
        reserva.setAuxNroRoom(nroHabitacion);
        reserva.setObservacion(modal.txtObservacion.getText().isEmpty() ? "-" : modal.txtObservacion.getText());
 
        int response = repositoryReserva.create(reserva);
        if(response >= 1){
            JOptionPane.showMessageDialog(modal, "Reserva registrada.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            updateRoom(nroHabitacion, "OCUPADO");
            modal.dispose();
            showReservas();
            showRooms();
            PanelReportReserva.getInstance().showReservas();
            return;
        }
        JOptionPane.showMessageDialog(modal,"Error al registrar la reserva\nVerifique que los datos esten correctos.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
    }
    
    private void modificarReserva(){
        int option = JOptionPane.showConfirmDialog(modal, "¿Está seguro de actualizar esta reservación?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if(option == JOptionPane.YES_OPTION){
            Reservacion reserva = new Reservacion();
        
            Integer cantidadHoras = Integer.parseInt(modal.lblCantidadHoras.getText());
            Integer cantidadMinutos = Integer.parseInt(modal.lblCantidadMinutos.getText());
            String habitacionNueva = modal.txtNroHabitacion.getText().trim();
            String habitacionMod = modal.txtNroHabitacionMod.getText().trim();

            reserva.setCodReserva(Integer.parseInt(modal.txtIdReserva.getText().trim()));
            reserva.setCodHabitacion(modal.txtCodigoHabitacion.getText().trim());
            reserva.setFechaIngreso(modal.txtFechaHoraIngreso.getValue().toString());
            reserva.setFechaSalida(modal.txtFechaHoraSalida.getValue().toString());
            reserva.setImporteReserva(Double.parseDouble(modal.txtCostoAlquiler.getText()));
            reserva.setImporteProductos(Double.parseDouble(modal.txtImporteProductos.getText().trim()));
            reserva.setImporteTotal(Double.parseDouble(modal.txtCostoAlquiler.getText()) + Double.parseDouble(modal.txtImporteProductos.getText().trim()));
            reserva.setTiempoReserva(cantidadHoras+":"+String.format("%2s", cantidadMinutos).replace(" ", "0"));
            reserva.setMagnitudReserva(Util.getMagnitudeTimeReserva());
            reserva.setEstado("1");
            reserva.setObservacion(modal.txtObservacion.getText().isEmpty() ? "-" : modal.txtObservacion.getText());

            if(habitacionMod.equalsIgnoreCase(habitacionNueva)){
                reserva.setAuxNroRoom(habitacionMod);
                int response = repositoryReserva.update(reserva);
                if(response >= 1){
                    JOptionPane.showMessageDialog(modal, "Reserva actualizada.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                    modal.dispose();
                    showReservas();
                    showRooms();
                    PanelReportReserva.getInstance().showReservas();
                    return;
                }
                JOptionPane.showMessageDialog(modal,"Error al modificar la reserva\n"
                        + "Verifique que los datos esten correctos.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }else{
                reserva.setAuxNroRoom(habitacionNueva);
                int response = repositoryReserva.update(reserva);
                if(response >= 1){
                    JOptionPane.showMessageDialog(modal, "Reserva actualizada.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                    updateRoom(habitacionNueva, "OCUPADO");
                    updateRoom(habitacionMod, "DISPONIBLE");
                    modal.dispose();
                    showReservas();
                    showRooms();
                    PanelReportReserva.getInstance().showReservas();
                    return;
                }
                JOptionPane.showMessageDialog(modal,"Error al modificar la reserva\n"
                        + "Verifique que los datos esten correctos.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    private void updateRoom(String nroHabitacion, String estado){
        repositoryRoom = new HabitacionRepositoryImp();
        int response = repositoryRoom.updateState(nroHabitacion, estado);
        if(response < 0){
           JOptionPane.showMessageDialog(modal, "Error al actualizar estado de la habitación.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void setDataRoom(){
        DialogSearchRoom form = new DialogSearchRoom(modal);
        form.setVisible(true);
    }
    
    private void addDateForReserva(){
        modal.chkFechaActual.setSelected(false);
        AddDataReserva form = new AddDataReserva(modal, true);
        form.txtAddCostoTotal.setText(modal.txtCostoAlquiler.getText());
        
        form.btnAceptar.addActionListener((ActionEvent e) -> {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            String dateString = formatter.format(AddDataReserva.chooserIngreso.getDate());
            
            String hora = AddDataReserva.spinnerHoras.getValue().toString().trim();
            String minutos = AddDataReserva.spinnerMinutos.getValue().toString().trim();
            String tiempo = Util.concatTime(hora, minutos);//12:45:00
            
            String fechaIngreso = Util.getDateTimeConcat(dateString, tiempo);
            long cantidadHoras = Long.parseLong(AddDataReserva.txtAddCantidadHoras.getValue().toString().trim());
            long cantidadMinutos = Long.parseLong(AddDataReserva.txtAddCantidadMinutos.getValue().toString().trim());
            
            String salida = Util.getDateTimeReservation(fechaIngreso, 0, cantidadHoras, cantidadMinutos);
            modal.txtFechaHoraIngreso.setValue(fechaIngreso);
            modal.txtFechaHoraSalida.setValue(String.valueOf(salida));
            modal.lblCantidadHoras.setText(String.valueOf(cantidadHoras));
            modal.lblCantidadMinutos.setText(String.valueOf(cantidadMinutos));
            form.dispose();
        });
        form.setVisible(true);
    }
    
    private void setDateNow(){
        if(modal.chkFechaActual.isSelected()){
            modal.btnAgregarFecha.setEnabled(true);
            modal.txtFechaHoraIngreso.setValue("");
            modal.txtFechaHoraSalida.setValue("");
        }else{
            modal.btnAgregarFecha.setEnabled(false);
            loadDefaultData();
        }
    }
    
    private void showRooms(){
        RepositoryRoom<Habitacion> repositoryRooms = new HabitacionRepositoryImp();
        List<Habitacion> rooms = repositoryRooms.findAll();
        mainFrame.panelRooms.removeAll();
        int columns = 5;
        
        GridLayout layout = new GridLayout((columns - 1), columns, 15, 15);
        if(rooms.size() <= 4 || rooms.size() <= 8){
            layout = new GridLayout(3, 8, 15,15);
        }
        mainFrame.panelRooms.setLayout(layout);

        rooms.stream().map(room -> {
            CardRoom card = new CardRoom(mainFrame);
            card.txtImagenHabitacion.setIcon(room.getEstado().equalsIgnoreCase("ocupado") ? ocupado : disponible);
            card.txtCodigoHabitacion.setText(room.getCodhaHabitacion());
            card.txtNumeroHabitacion.setText(String.valueOf(room.getNroHabitacion()));
            card.txtEstadoHabitacion.setText(room.getEstado());
            card.txtTipoHabitacion.setText(room.getTipoHabitacion());
            card.txtNroPlanta.setText(String.valueOf(room.getNroPlanta()));
            card.txtCostoHabitacion.setText(String.valueOf(room.getCostoAlquiler()));
            return card;
        }).forEachOrdered(panel -> {
            mainFrame.panelRooms.add(panel);
        });
        mainFrame.repaint();
    }
    
    public void showReservas(){
        PanelReservation panelReservation = PanelReservation.getInstance();
        clearTable(panelReservation.tablaReserva);
        List<Reservacion> reservaciones = repositoryReserva.findAll();
        DefaultTableModel modelReserva = (DefaultTableModel) panelReservation.tablaReserva.getModel();
        reservaciones.forEach((r) -> {
            modelReserva.addRow(new Object[]{
                r.getCodReserva(), r.getCodCliente(), r.getCodTrabajador(), r.getAuxNroRoom(), r.getNombreCliente(), r.getNombreTrabajador(),
                r.getFechaIngreso(), r.getFechaSalida(),
                r.getImporteReserva(), r.getImporteProductos() > 0.0D ? r.getImporteProductos() : "0.00",
                r.getImporteTotal(), String.valueOf(r.getTiempoReserva()).concat(" ").concat( r.getMagnitudReserva()),
                r.getEstado(), r.getObservacion(), 
            });
        });
        panelReservation.tablaReserva.setModel(modelReserva);
    }
    
    private void clearTable(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }
}
