package com.edmi.controller.menu;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.ControllerNewProduct;
import com.edmi.controller.ControllerUpdateStock;
import com.edmi.model.Product;
import com.edmi.repository.RepositoryProduct;
import com.edmi.repository.imp.ProductRepositoryImp;
import com.edmi.util.MiRenderer;
import com.edmi.view.DialogUpdateStock;
import com.edmi.view.MainPanelServices;
import com.edmi.view.ModalNewProduct;
import com.edmi.view.panel.PanelShopping;
import com.edmi.view.widget.MenuTableProduct;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControllerTableProduct implements ActionListener, MouseListener{
    private PanelShopping panelShopping = null;
    private MenuTableProduct menuTableProduct = null;
    private String codProduct;
    private MainPanelServices mainFrame = null;
    private int row = -1;
    
    public ControllerTableProduct(PanelShopping panelShopping, MainPanelServices mainFrame){
        this.panelShopping = panelShopping;
        this.mainFrame = mainFrame;
        this.menuTableProduct = MenuTableProduct.getInstance();
        this.menuTableProduct.initMenuProduct(PanelShopping.tableProducts);
        setListener();
    }
    
    private void setListener(){
        PanelShopping.tableProducts.addMouseListener(this);
        
        this.menuTableProduct.itemUpdateStock.addActionListener(this);
        this.menuTableProduct.itemUpdateProduct.addActionListener(this);
        this.menuTableProduct.itemDeleteProduct.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.menuTableProduct.itemUpdateStock){
            if(row == -1){
                JOptionPane.showMessageDialog(mainFrame, "Seleccione un registro\n para continuar.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }else{
                DialogUpdateStock dialogUpdateStock = new DialogUpdateStock(mainFrame, true);
                ControllerUpdateStock controllerUpdateStock = new ControllerUpdateStock(dialogUpdateStock, this.panelShopping);
                controllerUpdateStock.loadDataUpdateStock(codProduct);
                dialogUpdateStock.setVisible(true);
            }
            return;
        }
        
        if(e.getSource() == this.menuTableProduct.itemUpdateProduct){
            if(row == -1){
                JOptionPane.showMessageDialog(mainFrame, "Seleccione un registro\n para continuar.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }else{
                ModalNewProduct modalNewProduct = new ModalNewProduct(mainFrame,true);
                modalNewProduct.setTitle("Modificar producto");
                ControllerNewProduct controllerNewProduct = new ControllerNewProduct(modalNewProduct, this.panelShopping);
                controllerNewProduct.loadDataFromUpdateProduct(codProduct);
                modalNewProduct.btnRegistrarProducto.setEnabled(false);
                modalNewProduct.setVisible(true);
            }
            return;
        }
        
        if(e.getSource() == this.menuTableProduct.itemDeleteProduct){
            deleteProduct();
        }
    }
    
    private void deleteProduct(){
        RepositoryProduct<Product> repositoryProduct = new ProductRepositoryImp();
        int rs = JOptionPane.showConfirmDialog(mainFrame, "¿Está seguro de eliminar\n"
                + "este producto?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if(rs == JOptionPane.YES_OPTION){
            int response = repositoryProduct.delete(codProduct, "-1");
            if(response <= 0){
                JOptionPane.showMessageDialog(mainFrame, "No se puede eleminar este producto.", SYS_MSG, JOptionPane.ERROR_MESSAGE); return;
            }
            JOptionPane.showMessageDialog(mainFrame, "Producto eliminado.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            showRefreshTableProducts();
        }
    }

    private void showRefreshTableProducts(){
        RepositoryProduct<Product> repositoryProduct = new ProductRepositoryImp();
        clearTableProducts();
        DefaultTableModel modelProduct = (DefaultTableModel) PanelShopping.tableProducts.getModel();
        List<Product> products = repositoryProduct.findAll();
        products.forEach((p) -> {
            modelProduct.addRow(new Object[]{
                p.getCodProducto(), p.getNombreProducto(), p.getMarca(), p.getNombreCategoria(), p.getCantidadStock(),
                p.getFechaVenc(), p.getFechaReg(), "-", p.getPrecioCompra(), 
                p.getPrecioVenta(),  p.getMontoIgv(), p.getMontoIcbper(), p.getDescripcion()
            });
        });
        PanelShopping.tableProducts.setDefaultRenderer(Object.class, new MiRenderer());
        PanelShopping.tableProducts.setModel(modelProduct);
    }
    
    private void clearTableProducts(){
        DefaultTableModel model = (DefaultTableModel) PanelShopping.tableProducts.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        row = PanelShopping.tableProducts.getSelectedRow();
        codProduct = PanelShopping.tableProducts.getValueAt(row, 0).toString();
    }

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}
}
