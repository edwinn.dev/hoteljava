package com.edmi.model;

public class CategoriaProducto {
    private String codCategoria;
    private String nombreCategoria;
    private String descripcion;
    
    public CategoriaProducto(){}

    public CategoriaProducto(String codCategoria, String nombreCategoria, String descripcion) {
        this.codCategoria = codCategoria;
        this.nombreCategoria = nombreCategoria;
        this.descripcion = descripcion;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria.toUpperCase();
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion.toUpperCase();
    }

    public String getCodCategoria() {
        return codCategoria;
    }

    public void setCodCategoria(String codCategoria) {
        this.codCategoria = codCategoria.toUpperCase();
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CategoriaProducto{nombreCategoria=").append(nombreCategoria);
        sb.append(", descripcion=").append(descripcion);
        sb.append('}');
        return sb.toString();
    }
}
