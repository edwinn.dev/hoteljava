package com.edmi.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionDb {
    private static String url;
    private static String user;
    private static String pass;
    private static Connection connection;

    public static synchronized Connection getInstance() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            if (connection == null) {
                connection = DriverManager.getConnection(url, user, pass);
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConnectionDb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }

    public static void setDatosConnection(String host, String puerto, String basedatos, String user, String pass) {
        ConnectionDb.url = "jdbc:mysql://" + host + ":" + puerto + "/" + basedatos + "?useSSL=false&useTimezone=true&serverTimezone=UTC&allowPublicKeyRetrieval=true";
        ConnectionDb.user = user;
        ConnectionDb.pass = pass;
    }

    public static void setConnection(Connection connection) {
        ConnectionDb.connection = connection;
    }
}
