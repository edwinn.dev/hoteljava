package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.database.query.Query;
import com.edmi.model.Email;
import com.edmi.repository.RepositoryEmail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EmailRepository implements RepositoryEmail<Email> {

    private Connection getConnection() throws SQLException {
        return getInstance();
    }

    @Override
    public Email getConfigEmail() {
        Email e = new Email();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CONFIGEMAIL_FIND_ALL)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    e.setCorreoEmpresa(rs.getString("correo_empresa"));
                    e.setPassword(rs.getString("password"));
                    e.setHost(rs.getString("host"));
                    e.setPuerto(rs.getString("puerto"));
                    e.setAsunto(rs.getString("asunto"));
                    e.setMensaje(rs.getString("mensaje"));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmailRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return e;
    }

    @Override
    public int updateConfgEmail(Email email) {
        int op = 0;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CONFIGEMAIL_UPDATE)) {
            ps.setString(1, "EMAIL_1");
            ps.setString(2, email.getHost());
            ps.setString(3, email.getPuerto());
            ps.setString(4, email.getCorreoEmpresa());
            ps.setString(5, email.getPassword());
            ps.setString(6, email.getAsunto());
            ps.setString(7, email.getMensaje());
            op = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EmailRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return op;
    }

    @Override
    public int insertConfigEmail(Email email) {
        int op = 0;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CONFIGEMAIL_INSERT)) {
            ps.setString(1, "EMAIL_1");
            ps.setString(2, email.getHost());
            ps.setString(3, email.getPuerto());
            ps.setString(4, email.getCorreoEmpresa());
            ps.setString(5, email.getPassword());
            ps.setString(6, email.getAsunto());
            ps.setString(7, email.getMensaje());
            op = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(EmailRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return op;
    }
}
