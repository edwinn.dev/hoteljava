package com.edmi.repository;

import java.util.List;

public interface RepositoryClient<T> {
    List<T> findAllEstado(String estado);
    List<T> findAll();
    List<T> findAllWhere(String condition, String value, String estado);
    T findById(Object id);
    T findByNroDocu(Object nro);
    String findAllTipeDocu(Object tipoDocu);
    List<String> findAllDocuDescripciones();
    int create(T client);
    int update(T client);
    int state(Object id, Object estado);
}
