package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.repository.RepositoryProduct;
import com.edmi.model.Product;
import com.edmi.database.query.Query;
import java.sql.*;
import java.util.*;
import java.util.logging.*;

public class ProductRepositoryImp implements RepositoryProduct<Product> {
    
    private Connection getConnection()throws SQLException{
        return getInstance();
    }

    @Override
    public List<Product> findAll() {
        List<Product> products = new ArrayList<>();
        try(PreparedStatement ps = getConnection().prepareStatement(Query.SQL_PROD_FIND_ALL);
            ResultSet rs = ps.executeQuery()){
            
            while(rs.next()){
                Product p = createProduct(rs);
                products.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return products;
    }
    
    public List<String> findAllCategorias() {
        List<String> categorias = new ArrayList<>();
        try(PreparedStatement ps = getConnection().prepareStatement(Query.SQL_PROD_CATEGORIAS);
            ResultSet rs = ps.executeQuery()){
            
            while(rs.next()){
                Product p = new Product();
                String cate = rs.getString("nombre_categoria");
                categorias.add(cate);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return categorias;
    }
    
    @Override
    public List<Product> findAllCodeProduct() {
        List<Product> products = new ArrayList<>();
        try(PreparedStatement ps = getConnection().prepareStatement(Query.SQL_PROD_FIND_ALL_CODE);
            ResultSet rs = ps.executeQuery()){
            
            while(rs.next()){
                Product p = createProduct(rs);
                products.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return products;
    }

    @Override
    public List<Product> findAllWhere(String condition, String value) {
        List<Product> products = new ArrayList<>();
        try(PreparedStatement ps = getConnection().prepareStatement(Query.SQL_PROD_FIND_ALL_WHERE)){
            ps.setString(1, value);
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()){
                    Product p = createProduct(rs);
                    products.add(p);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return products;
    }
    
    public List<Product> filtroDeProducto(String condition, String value) {
        String sql = "SELECT p.*, cat.nombre_categoria, um.descripcion as unidad_medida "
            .concat("FROM producto p INNER JOIN categoria_producto cat ON p.cod_categoria = cat.cod_categoria ")
            .concat("INNER JOIN unidad_medida um on p.cod_unidadmedida = um.cod_unidad WHERE " + condition + " LIKE '" + value + "%' AND p.estado != '-1'  ORDER BY p.cod_producto");
        List<Product> products = new ArrayList<>();
        try(PreparedStatement ps = getConnection().prepareStatement(sql)){
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()){
                    Product p = createProduct(rs);
                    products.add(p);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return products;
    }
    
    @Override
    public Product findById(Object id) {
        Product p = new Product();
        try(PreparedStatement st = getConnection().prepareStatement(Query.SQL_PROD_FIND_BY_ID)) {
            st.setString(1, id.toString());
            try (ResultSet rs = st.executeQuery()) {
                if(rs.next()){
                    p = createProduct(rs);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return p;
    }

    @Override
    public int create(Product product) {
        int response = 0;
        try(PreparedStatement st = getConnection().prepareStatement(Query.SQL_PROD_CREATE)) {
            st.setString(1, product.getCodProducto());
            st.setString(2, product.getCodCategoria());
            st.setString(3, product.getCodUnidadMedida());
            st.setString(4, product.getNombreProducto());
            st.setDouble(5, product.getCantidadStock());
            st.setDouble(6, product.getPrecioCompra());
            st.setDouble(7, product.getPrecioVenta());
            st.setString(8, product.getFechaReg());
            st.setString(9, product.getFechaVenc());
            st.setDouble(10, product.getCantidadSalida());
            st.setString(11, product.getEstado());
            st.setString(12, product.getDescripcion());
            st.setString(13, "-");
            st.setString(14, product.getTipoAfectacionIgv());
            st.setDouble(15, product.getMontoIgv());
            st.setString(16, product.getTributoIcbper());
            st.setDouble(17, product.getMontoIcbper());
            st.setString(18, product.getMarca());
            response = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }
    
    @Override
    public int update(Product product) {
        int response = 0;
        try(PreparedStatement st = getConnection().prepareStatement(Query.SQL_PROD_UPDATE)) {
            st.setString(1, product.getCodCategoria());
            st.setString(2, product.getCodUnidadMedida());
            st.setString(3, product.getNombreProducto());
            st.setDouble(4, product.getCantidadStock());
            st.setDouble(5, product.getPrecioCompra());
            st.setDouble(6, product.getPrecioVenta());
            st.setString(7, product.getFechaReg());
            st.setString(8, product.getFechaVenc());
            //st.setDouble(9, product.getCantidadSalida());
            st.setString(9, product.getEstado());
            st.setString(10, product.getDescripcion());
            st.setString(11, "-");
            st.setString(12, product.getTipoAfectacionIgv());
            st.setDouble(13, product.getMontoIgv());
            st.setString(14, product.getTributoIcbper());
            st.setDouble(15, product.getMontoIcbper());
            st.setString(16, product.getMarca());
            st.setString(17, product.getCodProducto());
            response = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }
    
    @Override
    public int updateStock(Object id, Double value) {
        int response = 0;
        try(PreparedStatement ps = getConnection().prepareStatement(Query.SQL_PROD_UPDATE_STOCK)) {
            ps.setDouble(1, value);
            ps.setString(2, id.toString());
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    @Override
    public int delete(Object id, Object value) {
        int response = 0;
        try(PreparedStatement st = getConnection().prepareStatement(Query.SQL_PROD_DELETE)) {
            st.setString(1, value.toString());
            st.setString(2, id.toString());
            response = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }
    
    private Product createProduct(ResultSet rs) throws SQLException{
        Product p = new Product();
        p.setCodProducto(rs.getString("cod_producto"));
        p.setCodCategoria(rs.getString("cod_categoria"));
        p.setCodUnidadMedida(rs.getString("cod_unidadmedida"));
        p.setNombreProducto(rs.getString("nombre_producto"));
        p.setCantidadStock(rs.getDouble("cantidad_stock"));
        p.setPrecioCompra(rs.getDouble("precio_compra"));
        p.setPrecioVenta(rs.getDouble("precio_venta"));
        p.setFechaReg(rs.getString("fecha_registro"));
        p.setFechaVenc(rs.getString("fecha_vencimiento"));
        p.setCantidadSalida(rs.getDouble("cantidad_salida"));
        p.setEstado(rs.getString("estado"));
        p.setDescripcion(rs.getString("descripcion"));
        p.setNombreCategoria(rs.getString("nombre_categoria"));
        p.setNombreUnidadMedida(rs.getString("unidad_medida"));
        p.setCodSunat(rs.getString("cod_sunat"));
        p.setTipoAfectacionIgv(rs.getString("tipo_igv"));
        p.setMontoIgv(rs.getDouble("mto_igv"));
        p.setTributoIcbper(rs.getString("icbper"));
        p.setMontoIcbper(rs.getDouble("mto_icbper"));
        p.setMarca(rs.getString("marca"));
        return p;
    }
}
