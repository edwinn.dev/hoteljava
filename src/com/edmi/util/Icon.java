package com.edmi.util;

import javax.swing.ImageIcon;

public class Icon {
    public final ImageIcon MENU_REGISTRAR_PAGO = new ImageIcon(getClass().getResource("/com/edmi/images/pay.png"));
    public static final ImageIcon MENU_VER_DETALLE = new ImageIcon("/com/edmi/images/sleeping_bed.png");
    public static final ImageIcon MENU_CANCELAR_RESERVA = new ImageIcon("/com/edmi/images/sleeping_bed.png");
}
