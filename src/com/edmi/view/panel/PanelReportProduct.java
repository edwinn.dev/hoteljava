package com.edmi.view.panel;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.model.Product;
import com.edmi.repository.imp.ProductRepositoryImp;
import com.edmi.util.MiRenderer;
import com.edmi.util.Util;
import com.edmi.view.DialogReportFiles;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public final class PanelReportProduct extends javax.swing.JPanel {

    private static PanelReportProduct instance = null;
    private ProductRepositoryImp repositoryProduct = null;
    private String opcion = "1";
    private String hojaExcel = "TODOS LOS PRODUCTOS";
    private String nombreArchivo = "";

    private PanelReportProduct() {
        initComponents();
        this.repositoryProduct = new ProductRepositoryImp();
        showRefreshTableProducts();
        loadCategory();
        this.rbTodo.setSelected(true);
        this.cboCategoria.setEnabled(false);
        this.txtStock.setEnabled(false);
        this.txtFechaVencimiento.setEnabled(false);
        this.txtFechaIngreso.setEnabled(false);
        nombreArchivo = "TP_".concat(Util.getDateFormat(new Date()));
    }

    public static PanelReportProduct getInstance() {
        if (instance == null) {
            instance = new PanelReportProduct();
        }
        return instance;
    }

    private void almacenarReporte() {
        String path = DialogReportFiles.obtenerRutaArchiReporPdfXml()[0];
        boolean res = generareReporte(path + File.separator + nombreArchivo + ".xlsx");
        if (res) {
            JOptionPane.showMessageDialog(this, "Exportacion generada con exito\n\n"
                    + "RUTA: " + path + File.separator + nombreArchivo, SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(this, "No se pudo generar el reporte,\n\ncontacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }

    private boolean generareReporte(String sheetName) {
        List<Product> productos = null;
        switch (opcion) {
            case "1":
                productos = repositoryProduct.findAll();
                break;

            case "2":
                productos = repositoryProduct.filtroDeProducto("p.cantidad_stock", this.txtStock.getText().trim());
                break;

            case "3":
                String categoria = cboCategoria.getSelectedIndex() == 0 ? "" : cboCategoria.getSelectedItem().toString();
                productos = repositoryProduct.filtroDeProducto("cat.nombre_categoria", categoria);
                break;

            case "4":
                productos = repositoryProduct.filtroDeProducto("p.fecha_vencimiento", txtFechaVencimiento.getText().trim());
                break;

            case "5":
                productos = repositoryProduct.filtroDeProducto("p.fecha_registro", txtFechaIngreso.getText().trim());
                break;
        }

        boolean response = false;
        Workbook book = new XSSFWorkbook();
        Sheet sheet = book.createSheet(this.hojaExcel);

        try {
            File ruta = new File("design" + File.separator + "logo inicio");
            String[] img = ruta.list();
            String logo = ruta.getAbsolutePath() + File.separator + img[0];

            InputStream is = new FileInputStream(logo);
            byte[] bytes = IOUtils.toByteArray(is);
            int imgIndex = book.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG); //Agrega la imagen y su tipo de imagen.
            is.close();

            //Agregamos la imagen al archivo.
            CreationHelper help = book.getCreationHelper();
            Drawing draw = sheet.createDrawingPatriarch(); //Para crear la imagen.

            ClientAnchor anchor = help.createClientAnchor(); //Sacamos el ancho de la imagen para colocarlo de forma correcta.
            anchor.setCol1(0); //Posicion columna.
            anchor.setRow1(1); //Posicion fila.
            Picture pict = draw.createPicture(anchor, imgIndex); //Aparece la imagen.
            pict.resize(1, 5); //Indicamos el tamaño, de donde comenzara y cuanto espacio utilizara.

        } catch (IOException e) {
            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, e);
        }

        CellStyle estiloTitulo = book.createCellStyle(); //Para crear el diseño del titulo.
        estiloTitulo.setAlignment(HorizontalAlignment.CENTER); //Para centrar el titulo.
        estiloTitulo.setVerticalAlignment(VerticalAlignment.CENTER);

        org.apache.poi.ss.usermodel.Font fontTitulo = book.createFont(); //Problemas de compatibilidad con el itextPdf no se podia importar.

        fontTitulo.setFontName("Arial");
        fontTitulo.setBold(true);
        fontTitulo.setFontHeightInPoints((short) 22); //tamaño de letra
        estiloTitulo.setFont(fontTitulo); //agregamos los cambios.
        Row filaTitulo = sheet.createRow(2); //Creamos la fila para el titulo
        Cell celdaTitulo = filaTitulo.createCell(1); //Posicion columna
        celdaTitulo.setCellStyle(estiloTitulo); //Agregamos el estilo
        celdaTitulo.setCellValue("REPORTE DE PRODUCTOS");

        sheet.addMergedRegion(new CellRangeAddress(2, 2, 1, 3));

        sheet.setColumnWidth(0, 7000);
        sheet.setColumnWidth(1, 12000);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 6000);
        sheet.setColumnWidth(4, 7000);
        sheet.setColumnWidth(5, 6000);
        sheet.setColumnWidth(6, 7000);
        sheet.setColumnWidth(7, 6000);
        sheet.setColumnWidth(8, 6000);
        sheet.setColumnWidth(9, 10000);
        sheet.setColumnWidth(10, 6000);
        sheet.setColumnWidth(11, 6000);
        sheet.setColumnWidth(12, 6000);

        CellStyle estiloTablaTitulos = book.createCellStyle(); //Para crear el diseño del titulo.
        estiloTablaTitulos.setAlignment(HorizontalAlignment.CENTER); //Para centrar el titulo.
        estiloTablaTitulos.setVerticalAlignment(VerticalAlignment.CENTER);

        String cabecera[] = new String[]{"CODIGO PRODUCTO", "NOMBRE PRODUCTO", "STOCK", "CATEGORIA", "PRECIO COMPRA (S/)", "PRECIO VENTA (S/)",
            "FECHA VENCIMIENTO", "FECHA REGISTRO", "PROVEEDOR", "DESCRIPCION", "MONTO IGV (S/)", "MONTO ICBPER (S/)", "MARCA"};

        CellStyle estiloTablaCabezera = book.createCellStyle();
        estiloTablaCabezera.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        estiloTablaCabezera.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        estiloTablaCabezera.setBorderBottom(BorderStyle.THIN);
        estiloTablaCabezera.setBorderLeft(BorderStyle.THIN);
        estiloTablaCabezera.setBorderRight(BorderStyle.THIN);
        estiloTablaCabezera.setBorderTop(BorderStyle.THIN);

        org.apache.poi.ss.usermodel.Font font = book.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setFontHeightInPoints((short) 12);
        font.setColor(IndexedColors.WHITE.getIndex());
        estiloTablaCabezera.setFont(font);

        Row row = sheet.createRow(7);

        for (int i = 0; i < cabecera.length; i++) {
            Cell celdaCabecera = row.createCell(i);
            celdaCabecera.setCellStyle(estiloTablaCabezera);
            celdaCabecera.setCellValue(cabecera[i]);
        }

        int nroRow = 8;
        for (Product p : productos) {
            Row fila = sheet.createRow(nroRow);

            if (p.getCantidadStock() < 5) {
                CellStyle estiloAdvertenciaStock = book.createCellStyle();
                estiloAdvertenciaStock.setFillForegroundColor(IndexedColors.RED.getIndex());
                estiloAdvertenciaStock.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                estiloAdvertenciaStock.setBorderBottom(BorderStyle.THIN);
                estiloAdvertenciaStock.setBorderLeft(BorderStyle.THIN);
                estiloAdvertenciaStock.setBorderRight(BorderStyle.THIN);
                estiloAdvertenciaStock.setBorderTop(BorderStyle.THIN);
                
                Cell c0 = fila.createCell(0);
                c0.setCellValue(p.getCodProducto());
                c0.setCellStyle(estiloAdvertenciaStock);

                Cell c1 = fila.createCell(1);
                c1.setCellValue(p.getNombreProducto());
                c1.setCellStyle(estiloAdvertenciaStock);

                Cell c2 = fila.createCell(2);
                c2.setCellValue(p.getCantidadStock());
                c2.setCellStyle(estiloAdvertenciaStock);

                Cell c3 = fila.createCell(3);
                c3.setCellValue(p.getNombreCategoria());
                c3.setCellStyle(estiloAdvertenciaStock);

                Cell c4 = fila.createCell(4);
                c4.setCellValue(p.getPrecioCompra());
                c4.setCellStyle(estiloAdvertenciaStock);

                Cell c5 = fila.createCell(5);
                c5.setCellValue(p.getPrecioVenta());
                c5.setCellStyle(estiloAdvertenciaStock);

                Cell c6 = fila.createCell(6);
                c6.setCellValue(p.getFechaVenc());
                c6.setCellStyle(estiloAdvertenciaStock);

                Cell c7 = fila.createCell(7);
                c7.setCellValue(p.getFechaReg());
                c7.setCellStyle(estiloAdvertenciaStock);

                Cell c8 = fila.createCell(8);
                c8.setCellValue("-");
                c8.setCellStyle(estiloAdvertenciaStock);

                Cell c9 = fila.createCell(9);
                c9.setCellValue(p.getDescripcion());
                c9.setCellStyle(estiloAdvertenciaStock);

                Cell c10 = fila.createCell(10);
                c10.setCellValue(p.getMontoIgv());
                c10.setCellStyle(estiloAdvertenciaStock);

                Cell c11 = fila.createCell(11);
                c11.setCellValue(p.getMontoIcbper());
                c11.setCellStyle(estiloAdvertenciaStock);

                Cell c12 = fila.createCell(12);
                c12.setCellValue(p.getMarca());
                c12.setCellStyle(estiloAdvertenciaStock);
            } else {
                
                CellStyle estiloTablaDatos = book.createCellStyle();
                estiloTablaDatos.setBorderBottom(BorderStyle.THIN);
                estiloTablaDatos.setBorderLeft(BorderStyle.THIN);
                estiloTablaDatos.setBorderRight(BorderStyle.THIN);
                estiloTablaDatos.setBorderTop(BorderStyle.THIN);

                Cell c0 = fila.createCell(0);
                c0.setCellValue(p.getCodProducto());
                c0.setCellStyle(estiloTablaDatos);

                Cell c1 = fila.createCell(1);
                c1.setCellValue(p.getNombreProducto());
                c1.setCellStyle(estiloTablaDatos);

                Cell c2 = fila.createCell(2);
                c2.setCellValue(p.getCantidadStock());
                c2.setCellStyle(estiloTablaDatos);

                Cell c3 = fila.createCell(3);
                c3.setCellValue(p.getNombreCategoria());
                c3.setCellStyle(estiloTablaDatos);

                Cell c4 = fila.createCell(4);
                c4.setCellValue(p.getPrecioCompra());
                c4.setCellStyle(estiloTablaDatos);

                Cell c5 = fila.createCell(5);
                c5.setCellValue(p.getPrecioVenta());
                c5.setCellStyle(estiloTablaDatos);

                Cell c6 = fila.createCell(6);
                c6.setCellValue(p.getFechaVenc());
                c6.setCellStyle(estiloTablaDatos);

                Cell c7 = fila.createCell(7);
                c7.setCellValue(p.getFechaReg());
                c7.setCellStyle(estiloTablaDatos);

                Cell c8 = fila.createCell(8);
                c8.setCellValue("-");
                c8.setCellStyle(estiloTablaDatos);

                Cell c9 = fila.createCell(9);
                c9.setCellValue(p.getDescripcion());
                c9.setCellStyle(estiloTablaDatos);

                Cell c10 = fila.createCell(10);
                c10.setCellValue(p.getMontoIgv());
                c10.setCellStyle(estiloTablaDatos);

                Cell c11 = fila.createCell(11);
                c11.setCellValue(p.getMontoIcbper());
                c11.setCellStyle(estiloTablaDatos);

                Cell c12 = fila.createCell(12);
                c12.setCellValue(p.getMarca());
                c12.setCellStyle(estiloTablaDatos);
            }

            nroRow++;
        }

        try {
            try ( FileOutputStream out = new FileOutputStream(sheetName)) {
                book.write(out);
                response = true;
            }
        } catch (IOException ex) {
            Logger.getLogger(PanelReportProduct.class.getName()).log(Level.SEVERE, null, ex);
            response = false;
        }

        try {
            book.close();
        } catch (IOException ex) {
            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    private void generarPDF() {
        Document documento = new Document();
        documento.setMargins(24f, 24f, 24f, 24f);

        String path = DialogReportFiles.obtenerRutaArchiReporPdfXml()[0];

        try {
//            String ruta = System.getProperty("user.home");
            PdfWriter.getInstance(documento, new FileOutputStream(path + File.separator + nombreArchivo + ".pdf"));
            documento.open();
        } catch (DocumentException | HeadlessException | FileNotFoundException e) {
            JOptionPane.showMessageDialog(this, "No se pudo generar el reporte,\n\ncontacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        List<Product> productos = null;
        switch (opcion) {
            case "1":
                productos = repositoryProduct.findAll();
                break;

            case "2":
                productos = repositoryProduct.filtroDeProducto("p.cantidad_stock", this.txtStock.getText().trim());
                break;

            case "3":
                String categoria = cboCategoria.getSelectedIndex() == 0 ? "" : cboCategoria.getSelectedItem().toString();
                productos = repositoryProduct.filtroDeProducto("cat.nombre_categoria", categoria);
                break;

            case "4":
                productos = repositoryProduct.filtroDeProducto("p.fecha_vencimiento", txtFechaVencimiento.getText().trim());
                break;

            case "5":
                productos = repositoryProduct.filtroDeProducto("p.fecha_registro", txtFechaIngreso.getText().trim());
                break;
        }

        try {
            documento.setPageSize(PageSize.A2.rotate());
            documento.open();

            BaseFont fuentePro = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, true);
            Font tituloUser = new Font(fuentePro, 20f, Font.BOLD, BaseColor.BLACK);

            BaseFont fuentePro1 = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, true);
            Font celdaTitulo = new Font(fuentePro1, 15f, Font.BOLD, BaseColor.RED);

            var titulo = new Paragraph(hojaExcel + "\n\n", tituloUser);
            titulo.setAlignment(Paragraph.ALIGN_CENTER);
            documento.add(titulo);

            PdfPTable tablapdf = new PdfPTable(new float[]{75f, 75f, 75f, 75f, 75f, 75f, 75f, 75f, 75f, 75f, 75f, 75f, 75f});
            tablapdf.setWidthPercentage(100f);
            tablapdf.addCell(new Phrase("CODIGO PRODUCTO", celdaTitulo));
            tablapdf.addCell(new Phrase("NOMBRE PRODUCTO", celdaTitulo));
            tablapdf.addCell(new Phrase("STOCK", celdaTitulo));
            tablapdf.addCell(new Phrase("CATEGORIA", celdaTitulo));
            tablapdf.addCell(new Phrase("PRECIO COMPRA (S/)", celdaTitulo));
            tablapdf.addCell(new Phrase("PRECIO VENTA (S/)", celdaTitulo));
            tablapdf.addCell(new Phrase("FECHA VENCIMIENTO", celdaTitulo));
            tablapdf.addCell(new Phrase("FECHA REGISTRO", celdaTitulo));
            tablapdf.addCell(new Phrase("PROVEEDOR", celdaTitulo));
            tablapdf.addCell(new Phrase("DESCRIPCION", celdaTitulo));
            tablapdf.addCell(new Phrase("MONTO IGV", celdaTitulo));
            tablapdf.addCell(new Phrase("MONTO ICBPER (S/)", celdaTitulo));
            tablapdf.addCell(new Phrase("MARCA", celdaTitulo));

            for (Product p : productos) {
                tablapdf.addCell(p.getCodProducto());
                tablapdf.addCell(p.getNombreProducto());
                tablapdf.addCell(p.getCantidadStock().toString());
                tablapdf.addCell(p.getNombreCategoria());
                tablapdf.addCell(p.getPrecioCompra().toString());
                tablapdf.addCell(p.getPrecioVenta().toString());
                tablapdf.addCell(p.getFechaVenc());
                tablapdf.addCell(p.getFechaReg());
                tablapdf.addCell("-");
                tablapdf.addCell(p.getDescripcion());
                tablapdf.addCell(p.getMontoIgv().toString());
                tablapdf.addCell(p.getMontoIcbper().toString());
                tablapdf.addCell(p.getMarca());
            }
            documento.add(tablapdf);
            documento.close();

            JOptionPane.showMessageDialog(this, "Exportación generada con exito\n\n"
                    + "RUTA: " + path + File.separator + nombreArchivo, SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } catch (DocumentException | IOException ex) {
            JOptionPane.showMessageDialog(this, "Error al exportar archivo, intente nuevamente.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }

    public void showRefreshTableProducts() {
        clearTableProducts();
        DefaultTableModel modelProduct = (DefaultTableModel) tablaProductosReportes.getModel();

        List<Product> productos = null;
        switch (opcion) {
            case "1":
                productos = repositoryProduct.findAll();
                break;

            case "2":
                productos = repositoryProduct.filtroDeProducto("p.cantidad_stock", this.txtStock.getText().trim());
                break;

            case "3":
                String categoria = cboCategoria.getSelectedIndex() == 0 ? "" : cboCategoria.getSelectedItem().toString();
                productos = repositoryProduct.filtroDeProducto("cat.nombre_categoria", categoria);
                break;

            case "4":
                productos = repositoryProduct.filtroDeProducto("p.fecha_vencimiento", txtFechaVencimiento.getText().trim());
                break;

            case "5":
                productos = repositoryProduct.filtroDeProducto("p.fecha_registro", txtFechaIngreso.getText().trim());
                break;
        }

        productos.forEach((p) -> {
            modelProduct.addRow(new Object[]{
                p.getCodProducto(), p.getNombreProducto(), p.getMarca(), p.getNombreCategoria(), p.getCantidadStock(),
                p.getFechaVenc(), p.getFechaReg(), "-", p.getPrecioCompra(),
                p.getPrecioVenta(), p.getMontoIgv(), p.getMontoIcbper(), p.getDescripcion()
            });
        });
        tablaProductosReportes.setDefaultRenderer(Object.class, new MiRenderer());
        tablaProductosReportes.setModel(modelProduct);
    }

    private void clearTableProducts() {
        DefaultTableModel model = (DefaultTableModel) tablaProductosReportes.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    public void loadCategory() {
        List<String> categories = repositoryProduct.findAllCategorias();
        this.cboCategoria.removeAllItems();
        this.cboCategoria.addItem("-- SELECCIONAR --");
        for (String c : categories) {
            this.cboCategoria.addItem(c);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        btnReporteExcel = new javax.swing.JButton();
        btnReportePDF = new javax.swing.JButton();
        rbTodo = new javax.swing.JRadioButton();
        rbCategoria = new javax.swing.JRadioButton();
        rbStock = new javax.swing.JRadioButton();
        rbFechaVencimiento = new javax.swing.JRadioButton();
        jSeparator1 = new javax.swing.JSeparator();
        txtFechaVencimiento = new javax.swing.JTextField();
        cboCategoria = new javax.swing.JComboBox<>();
        txtStock = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaProductosReportes = new javax.swing.JTable();
        txtFechaIngreso = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        btnReporteExcel.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnReporteExcel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_microsoft_excel_26px.png"))); // NOI18N
        btnReporteExcel.setText("EXPORTAR EXCEL");
        btnReporteExcel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnReporteExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReporteExcelActionPerformed(evt);
            }
        });

        btnReportePDF.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnReportePDF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_pdf_30px.png"))); // NOI18N
        btnReportePDF.setText("EXPORTAR PDF");
        btnReportePDF.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnReportePDF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportePDFActionPerformed(evt);
            }
        });

        rbTodo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbTodo.setText("TODO");
        rbTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbTodoActionPerformed(evt);
            }
        });

        rbCategoria.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbCategoria.setText("CATEGORIA");
        rbCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbCategoriaActionPerformed(evt);
            }
        });

        rbStock.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbStock.setText("STOCK ");
        rbStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbStockActionPerformed(evt);
            }
        });

        rbFechaVencimiento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbFechaVencimiento.setText("FECHA");
        rbFechaVencimiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbFechaVencimientoActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        txtFechaVencimiento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFechaVencimientoKeyReleased(evt);
            }
        });

        cboCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCategoriaActionPerformed(evt);
            }
        });

        txtStock.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtStockKeyReleased(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel1.setText("REPORTE DE PRODUCTOS");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        tablaProductosReportes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tablaProductosReportes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "NOMBRE DE PRODUCTO", "MARCA", "CATEGORIA", "STOCK", "FECHA VENCIMIENTO", "FECHA REG.", "PROVEEDOR", "P. COMPRA (S/)", "P. VENTA (S/)", "MONTO IGV", "MONTO ICBPER", "DESCRIPCION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaProductosReportes.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaProductosReportes.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tablaProductosReportes);
        if (tablaProductosReportes.getColumnModel().getColumnCount() > 0) {
            tablaProductosReportes.getColumnModel().getColumn(0).setPreferredWidth(120);
            tablaProductosReportes.getColumnModel().getColumn(1).setPreferredWidth(300);
            tablaProductosReportes.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablaProductosReportes.getColumnModel().getColumn(3).setPreferredWidth(200);
            tablaProductosReportes.getColumnModel().getColumn(5).setPreferredWidth(150);
            tablaProductosReportes.getColumnModel().getColumn(6).setPreferredWidth(150);
            tablaProductosReportes.getColumnModel().getColumn(7).setPreferredWidth(300);
            tablaProductosReportes.getColumnModel().getColumn(8).setPreferredWidth(150);
            tablaProductosReportes.getColumnModel().getColumn(9).setPreferredWidth(150);
            tablaProductosReportes.getColumnModel().getColumn(10).setPreferredWidth(150);
            tablaProductosReportes.getColumnModel().getColumn(11).setPreferredWidth(150);
            tablaProductosReportes.getColumnModel().getColumn(12).setPreferredWidth(250);
        }

        txtFechaIngreso.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFechaIngresoKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setText("Ingreso:");

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText("Vencimiento:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbCategoria)
                            .addComponent(rbTodo)
                            .addComponent(rbStock))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtStock, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(139, 139, 139)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(rbFechaVencimiento)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(21, 21, 21)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel2)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(txtFechaIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel3)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(txtFechaVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                            .addComponent(cboCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(34, 34, 34)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnReportePDF, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnReporteExcel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 104, Short.MAX_VALUE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(btnReporteExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnReportePDF, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(rbTodo)
                                            .addComponent(rbFechaVencimiento))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(rbStock)
                                            .addComponent(txtStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(25, 25, 25)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel2)
                                            .addComponent(txtFechaIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel3)
                                            .addComponent(txtFechaVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cboCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(rbCategoria)))
                            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 283, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void rbTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbTodoActionPerformed
        opcion = "1";
        hojaExcel = "TODOS LOS PRODUCTOS";
        nombreArchivo = "TP_".concat(Util.getDateFormat(new Date()));

        this.rbTodo.setSelected(true);
        this.rbStock.setSelected(false);
        this.rbCategoria.setSelected(false);
        this.rbFechaVencimiento.setSelected(false);
        this.txtStock.setEnabled(false);
        this.txtFechaVencimiento.setEnabled(false);
        this.txtFechaIngreso.setEnabled(false);
        this.cboCategoria.setEnabled(false);
        this.cboCategoria.setSelectedIndex(0);
        this.txtFechaVencimiento.setText("");
        this.txtStock.setText("");
    }//GEN-LAST:event_rbTodoActionPerformed

    private void rbStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbStockActionPerformed
        opcion = "2";
        hojaExcel = "STOCK DE LOS PRODUCTOS";
        nombreArchivo = "SP_".concat(Util.getDateFormat(new Date()));

        this.rbStock.setSelected(true);
        this.rbTodo.setSelected(false);
        this.rbCategoria.setSelected(false);
        this.rbFechaVencimiento.setSelected(false);
        this.txtStock.setEnabled(true);
        this.txtFechaVencimiento.setEnabled(false);
        this.txtFechaIngreso.setEnabled(false);
        this.cboCategoria.setEnabled(false);
        this.cboCategoria.setSelectedIndex(0);
        this.txtFechaVencimiento.setText("");
    }//GEN-LAST:event_rbStockActionPerformed

    private void rbCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbCategoriaActionPerformed
        opcion = "3";
        hojaExcel = "CATEGORIA DE LOS PRODUCTOS";
        nombreArchivo = "CP_".concat(Util.getDateFormat(new Date()));

        this.rbCategoria.setSelected(true);
        this.rbTodo.setSelected(false);
        this.rbStock.setSelected(false);
        this.rbFechaVencimiento.setSelected(false);
        this.cboCategoria.setEnabled(true);
        this.txtStock.setEnabled(false);
        this.txtFechaVencimiento.setEnabled(false);
        this.txtFechaIngreso.setEnabled(false);
        this.txtFechaVencimiento.setText("");
        this.txtStock.setText("");
    }//GEN-LAST:event_rbCategoriaActionPerformed

    private void rbFechaVencimientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbFechaVencimientoActionPerformed
        this.rbFechaVencimiento.setSelected(true);
        this.rbTodo.setSelected(false);
        this.rbStock.setSelected(false);
        this.rbCategoria.setSelected(false);
        this.txtStock.setEnabled(false);
        this.txtFechaVencimiento.setEnabled(true);
        this.txtFechaIngreso.setEnabled(true);
        this.cboCategoria.setEnabled(true);
        this.cboCategoria.setSelectedIndex(0);
        this.txtStock.setText("");
    }//GEN-LAST:event_rbFechaVencimientoActionPerformed

    private void txtStockKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStockKeyReleased
        showRefreshTableProducts();
    }//GEN-LAST:event_txtStockKeyReleased

    private void txtFechaVencimientoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFechaVencimientoKeyReleased
        opcion = "4";
        hojaExcel = "PRODUCTOS POR FECHA DE VENCIMIENTO";
        nombreArchivo = "PFV_".concat(Util.getDateFormat(new Date()));
        showRefreshTableProducts();
    }//GEN-LAST:event_txtFechaVencimientoKeyReleased

    private void cboCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCategoriaActionPerformed
        showRefreshTableProducts();
    }//GEN-LAST:event_cboCategoriaActionPerformed

    private void btnReporteExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReporteExcelActionPerformed
        almacenarReporte();
    }//GEN-LAST:event_btnReporteExcelActionPerformed

    private void btnReportePDFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportePDFActionPerformed
        generarPDF();
    }//GEN-LAST:event_btnReportePDFActionPerformed

    private void txtFechaIngresoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFechaIngresoKeyReleased
        opcion = "5";
        hojaExcel = "PRODUCTOS POR FECHA DE REGISTRO";
        nombreArchivo = "PFR_".concat(Util.getDateFormat(new Date()));
        showRefreshTableProducts();
    }//GEN-LAST:event_txtFechaIngresoKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnReporteExcel;
    private javax.swing.JButton btnReportePDF;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cboCategoria;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JRadioButton rbCategoria;
    private javax.swing.JRadioButton rbFechaVencimiento;
    private javax.swing.JRadioButton rbStock;
    private javax.swing.JRadioButton rbTodo;
    public javax.swing.JTable tablaProductosReportes;
    private javax.swing.JTextField txtFechaIngreso;
    private javax.swing.JTextField txtFechaVencimiento;
    private javax.swing.JTextField txtStock;
    // End of variables declaration//GEN-END:variables
}
