package com.edmi.repository;

import com.edmi.model.Habitacion;
import java.util.List;

public interface RepositoryRoom<T> {
    List<T> findAll();
    List<Habitacion> findAllDesc();
    List<Habitacion> findAllEstado(String estado);
    List<Habitacion> findAllEstado(String estado, String state);
    List<T> findAllAvailable();
    List<T> findAllWhere(String condition, String value, String estado);
    List<Habitacion> findAllWhereFiltrosReserva(String condition, String value, String estado);
    int state(Object id, Object estado);
    String findAllTipeRom(Object tipoHab);
    T findById(Object id);
    List<T> findByIdBuscar(Object id);
    int create(T room);
    int update(T room);
    int updateState(String room, String condition);
    int delete(Object id);
}
