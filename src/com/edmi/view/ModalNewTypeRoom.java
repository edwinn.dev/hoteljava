package com.edmi.view;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.repository.imp.TipoHabitacionRepositoryImp;
import com.edmi.repository.RepositoryProduct;
import com.edmi.util.MessageView;
import com.edmi.model.TipoHabitacion;
import com.edmi.util.Util;
import java.awt.Component;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;

public class ModalNewTypeRoom extends JDialog {
    private RepositoryProduct<TipoHabitacion> typeRoomRepository;
    private ModalNewRoom modal = null;

    public ModalNewTypeRoom(ModalNewRoom parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.modal = parent;
        setPreferences();
    }
    
    private void setPreferences() {
        this.setModal(true);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        
        Util.restriccionLetras(txtCapacidad);
        Util.restriccionNumeros(txtTipo);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtTipo = new javax.swing.JFormattedTextField();
        txtCapacidad = new javax.swing.JFormattedTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescripcion = new javax.swing.JTextArea();
        btnRegistrarTipo = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Nuevo Tipo de Habitacion");

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Tipo de habitación:");

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setText("Capacidad (personas):");

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText("Descripción:");

        txtTipo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtCapacidad.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtDescripcion.setColumns(20);
        txtDescripcion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtDescripcion.setRows(5);
        jScrollPane1.setViewportView(txtDescripcion);

        btnRegistrarTipo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnRegistrarTipo.setText("REGISTRAR");
        btnRegistrarTipo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRegistrarTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarTipoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCapacidad, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 390, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 10, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnRegistrarTipo)
                .addGap(167, 167, 167))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCapacidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnRegistrarTipo, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRegistrarTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarTipoActionPerformed
        typeRoomRepository = new TipoHabitacionRepositoryImp();
        
        if(isFieldsInvalid()){
            JOptionPane.showMessageDialog(this, MessageView.MODAL_TYPE_ROOM_VALIDATE); return;
        }
        
        String pattern = txtTipo.getText().concat("TYPEROOM");
        String code = pattern.replace(" ", "").substring(0, 6).toUpperCase();
        
        TipoHabitacion type = new TipoHabitacion();
        type.setCodTipo("TH-".concat(code));
        type.setCapacidad(Integer.parseInt(txtCapacidad.getText()));
        type.setTipoHabitacion(txtTipo.getText());
        type.setDescripcion(txtDescripcion.getText());
        
        int response = typeRoomRepository.create(type);
        if(response <= 0){
            JOptionPane.showMessageDialog(this, "Estos datos ya fueron registrados.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE); return;
        }
        JOptionPane.showMessageDialog(this, "Tipo de habitacion registrada",  SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        modal.loadRoomType(modal);
        clearTextField();
        this.dispose();
    }//GEN-LAST:event_btnRegistrarTipoActionPerformed

    private boolean isFieldsInvalid(){
        boolean invalid = txtCapacidad.getText().isEmpty() || txtTipo.getText().isEmpty();
        return invalid;
    }
    
    private void clearTextField(){
        Component[] components = getContentPane().getComponents();
        for(Component c : components){
            if(c instanceof JFormattedTextField){
                ((JFormattedTextField) c).setValue("");
            }
        }
        txtDescripcion.setText("");
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRegistrarTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JFormattedTextField txtCapacidad;
    private javax.swing.JTextArea txtDescripcion;
    private javax.swing.JFormattedTextField txtTipo;
    // End of variables declaration//GEN-END:variables
}
