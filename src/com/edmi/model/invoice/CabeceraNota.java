package com.edmi.model.invoice;

public class CabeceraNota {
    private String tipOperacion;
    private String fecEmision;
    private String horEmision;
    private String codLocalEmisor;
    private String tipDocUsuario;
    private String numDocUsuario;
    private String rznSocialUsuario;
    private String tipMoneda;
    private String codMotivo;
    private String desMotivo;
    private String tipDocAfectado;
    private String numDocAfectado;
    private String sumTotTributos;
    private String sumTotValVenta;
    private String sumPrecioVenta;
    private String sumDescTotal;
    private String sumOtrosCargos;
    private String sumTotalAnticipos;
    private String sumImpVenta;
    private String ublVersionId;
    private String customizationId;

    public String getTipOperacion() {
        return tipOperacion;
    }

    public void setTipOperacion(String tipOperacion) {
        this.tipOperacion = tipOperacion;
    }

    public String getFecEmision() {
        return fecEmision;
    }

    public void setFecEmision(String fecEmision) {
        this.fecEmision = fecEmision;
    }

    public String getHorEmision() {
        return horEmision;
    }

    public void setHorEmision(String horEmision) {
        this.horEmision = horEmision;
    }

    public String getCodLocalEmisor() {
        return codLocalEmisor;
    }

    public void setCodLocalEmisor(String codLocalEmisor) {
        this.codLocalEmisor = codLocalEmisor;
    }

    public String getTipDocUsuario() {
        return tipDocUsuario;
    }

    public void setTipDocUsuario(String tipDocUsuario) {
        this.tipDocUsuario = tipDocUsuario;
    }

    public String getNumDocUsuario() {
        return numDocUsuario;
    }

    public void setNumDocUsuario(String numDocUsuario) {
        this.numDocUsuario = numDocUsuario;
    }

    public String getRznSocialUsuario() {
        return rznSocialUsuario;
    }

    public void setRznSocialUsuario(String rznSocialUsuario) {
        this.rznSocialUsuario = rznSocialUsuario;
    }

    public String getTipMoneda() {
        return tipMoneda;
    }

    public void setTipMoneda(String tipMoneda) {
        this.tipMoneda = tipMoneda;
    }

    public String getCodMotivo() {
        return codMotivo;
    }

    public void setCodMotivo(String codMotivo) {
        this.codMotivo = codMotivo;
    }

    public String getDesMotivo() {
        return desMotivo;
    }

    public void setDesMotivo(String desMotivo) {
        this.desMotivo = desMotivo;
    }

    public String getTipDocAfectado() {
        return tipDocAfectado;
    }

    public void setTipDocAfectado(String tipDocAfectado) {
        this.tipDocAfectado = tipDocAfectado;
    }

    public String getNumDocAfectado() {
        return numDocAfectado;
    }

    public void setNumDocAfectado(String numDocAfectado) {
        this.numDocAfectado = numDocAfectado;
    }

    public String getSumTotTributos() {
        return sumTotTributos;
    }

    public void setSumTotTributos(String sumTotTributos) {
        this.sumTotTributos = sumTotTributos;
    }

    public String getSumTotValVenta() {
        return sumTotValVenta;
    }

    public void setSumTotValVenta(String sumTotValVenta) {
        this.sumTotValVenta = sumTotValVenta;
    }

    public String getSumPrecioVenta() {
        return sumPrecioVenta;
    }

    public void setSumPrecioVenta(String sumPrecioVenta) {
        this.sumPrecioVenta = sumPrecioVenta;
    }

    public String getSumDescTotal() {
        return sumDescTotal;
    }

    public void setSumDescTotal(String sumDescTotal) {
        this.sumDescTotal = sumDescTotal;
    }

    public String getSumOtrosCargos() {
        return sumOtrosCargos;
    }

    public void setSumOtrosCargos(String sumOtrosCargos) {
        this.sumOtrosCargos = sumOtrosCargos;
    }

    public String getSumTotalAnticipos() {
        return sumTotalAnticipos;
    }

    public void setSumTotalAnticipos(String sumTotalAnticipos) {
        this.sumTotalAnticipos = sumTotalAnticipos;
    }

    public String getSumImpVenta() {
        return sumImpVenta;
    }

    public void setSumImpVenta(String sumImpVenta) {
        this.sumImpVenta = sumImpVenta;
    }

    public String getUblVersionId() {
        return ublVersionId;
    }

    public void setUblVersionId(String ublVersionId) {
        this.ublVersionId = ublVersionId;
    }

    public String getCustomizationId() {
        return customizationId;
    }

    public void setCustomizationId(String customizationId) {
        this.customizationId = customizationId;
    }
    
}
