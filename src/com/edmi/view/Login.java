package com.edmi.view;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.ControllerConexionBD;
import com.edmi.controller.ControllerConfigCompany;
import com.edmi.controller.ControllerEmployee;
import com.edmi.model.Trabajador;
import com.edmi.repository.RepositoryEmployee;
import com.edmi.repository.imp.TrabajadorRepositoryImp;
import com.edmi.util.EncriptacionPasswordAES;
import com.edmi.util.TextPrompt;
import com.edmi.util.Util;
import java.awt.HeadlessException;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

public class Login extends javax.swing.JFrame {
    private Trabajador trabajador = null;
    private MainPanelServices mainFrame = null;
    private ControllerConexionBD contolConexion = null;
    ConfigConexionBD confiModal = null;
    private static final Logger LOGGER = Logger.getLogger(Login.class);
    private static String cargo;

    public Login() {
        initComponents();
        showLoginForm();
        loadData();
        LOGGER.info("testeando");
        btnConfigBD.setOpaque(false);
        btnConfigBD.setContentAreaFilled(false);
        btnConfigBD.setBorderPainted(false);
    }

    private void showLoginForm() {
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jEImagePanel1 = new LIB.JEImagePanel();
        jPanelTransparente1 = new LIB.JPanelTransparente();
        txtPassMuestra = new javax.swing.JTextField();
        txtUsuario = new javax.swing.JTextField();
        checkMostrar = new javax.swing.JCheckBox();
        txtPassOculto = new javax.swing.JPasswordField();
        btnIniciarSesion = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnConfigBD = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jEImagePanel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/img_login.jpeg"))); // NOI18N

        jPanelTransparente1.setColorPrimario(new java.awt.Color(0, 0, 0));
        jPanelTransparente1.setColorSecundario(new java.awt.Color(0, 0, 0));
        jPanelTransparente1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtPassMuestra.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        txtPassMuestra.setForeground(new java.awt.Color(255, 255, 255));
        txtPassMuestra.setBorder(null);
        txtPassMuestra.setOpaque(false);
        jPanelTransparente1.add(txtPassMuestra, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 140, 210, 30));

        txtUsuario.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        txtUsuario.setForeground(new java.awt.Color(255, 255, 255));
        txtUsuario.setBorder(null);
        txtUsuario.setOpaque(false);
        jPanelTransparente1.add(txtUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 90, 210, 30));

        checkMostrar.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        checkMostrar.setForeground(new java.awt.Color(255, 255, 255));
        checkMostrar.setText("Mostrar");
        checkMostrar.setOpaque(false);
        checkMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkMostrarActionPerformed(evt);
            }
        });
        jPanelTransparente1.add(checkMostrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 200, -1, -1));

        txtPassOculto.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        txtPassOculto.setForeground(new java.awt.Color(255, 255, 255));
        txtPassOculto.setBorder(null);
        txtPassOculto.setOpaque(false);
        jPanelTransparente1.add(txtPassOculto, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 140, 210, 30));

        btnIniciarSesion.setFont(new java.awt.Font("Lucida Sans", 0, 13)); // NOI18N
        btnIniciarSesion.setText("INGRESAR");
        btnIniciarSesion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnIniciarSesion.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnIniciarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarSesionActionPerformed(evt);
            }
        });
        jPanelTransparente1.add(btnIniciarSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 260, 160, 33));
        jPanelTransparente1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 170, 210, 10));
        jPanelTransparente1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 120, 210, 10));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_lock_30px_1.png"))); // NOI18N
        jPanelTransparente1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 140, 40, 40));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_user_30px.png"))); // NOI18N
        jPanelTransparente1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, 40, 40));

        jLabel3.setFont(new java.awt.Font("Lucida Bright", 0, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("INICIAR SESION");
        jPanelTransparente1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 20, -1, -1));

        btnConfigBD.setForeground(new java.awt.Color(255, 255, 255));
        btnConfigBD.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_database_administrator_40px.png"))); // NOI18N
        btnConfigBD.setBorder(null);
        btnConfigBD.setContentAreaFilled(false);
        btnConfigBD.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnConfigBD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfigBDActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jEImagePanel1Layout = new javax.swing.GroupLayout(jEImagePanel1);
        jEImagePanel1.setLayout(jEImagePanel1Layout);
        jEImagePanel1Layout.setHorizontalGroup(
            jEImagePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jEImagePanel1Layout.createSequentialGroup()
                .addContainerGap(200, Short.MAX_VALUE)
                .addGroup(jEImagePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jEImagePanel1Layout.createSequentialGroup()
                        .addComponent(btnConfigBD, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jEImagePanel1Layout.createSequentialGroup()
                        .addComponent(jPanelTransparente1, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(200, 200, 200))))
        );
        jEImagePanel1Layout.setVerticalGroup(
            jEImagePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jEImagePanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnConfigBD, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanelTransparente1, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(72, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jEImagePanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jEImagePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIniciarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarSesionActionPerformed
        if (txtUsuario.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Falta ingresar el usuario y la contraseña, intentalo de nuevo.", "Mensaje del sistema", JOptionPane.INFORMATION_MESSAGE);
        } else {
            RepositoryEmployee<Trabajador> repositoryEmployee = new TrabajadorRepositoryImp();
            int contador = 0;
            String pass = "";

            if (!txtPassMuestra.getText().isEmpty()) {
                txtPassOculto.setText("");
                pass = txtPassMuestra.getText().trim();
                contador++;
            } else if (!txtPassOculto.getText().isEmpty()) {
                txtPassMuestra.setText("");
                pass = txtPassOculto.getText().trim();
                contador++;
            }

            if (checkMostrar.isSelected()) {
                if (txtPassMuestra.getText().isEmpty()) {
                    contador = 0;
                }
            }

            if (txtUsuario.getText().isEmpty() || contador == 0) {
                JOptionPane.showMessageDialog(this, "Falta ingresar el usuario o la contraseña.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            } else {
                trabajador = repositoryEmployee.verificationLogin(txtUsuario.getText().trim(), EncriptacionPasswordAES.encriptar(pass), this);

                String password = EncriptacionPasswordAES.desencriptar(trabajador.getPassword());

                try {
                    if (!trabajador.getUsername().equals(txtUsuario.getText().trim()) || !password.equals(pass)) {
                        JOptionPane.showMessageDialog(this, "Usuario o contraseña incorrecta.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                    } else {
                        cargo = trabajador.getCargo();
                        mainFrame = new MainPanelServices();

                        mainFrame.setPassword(password);
                        mainFrame.setCodigoUsuario(trabajador.getCodTrabajador());
                        mainFrame.mostrarNombreUsuario();

                        if (trabajador.getEstado().equals("1") && trabajador.getCargo().toUpperCase().equals("ADMINISTRADOR")) {
                            this.setVisible(false);
                            inicioMain(mainFrame);
                        } else if (trabajador.getEstado().equals("1") && trabajador.getCargo().toUpperCase().equals("EJECUTIVO DE VENTAS")) {
                            this.setVisible(false);
                            mainFrame.tabbedPanel.remove(mainFrame.capaCompras);
                            mainFrame.tabbedPanel.remove(mainFrame.capaTrabajadores);
                            mainFrame.checkMostrarContra.setVisible(false);
                            mainFrame.chkClienteDescartados.setVisible(false);
                            mainFrame.chkUsuariosDescartados.setVisible(false);
                            mainFrame.btnCompras.setVisible(false);
                            mainFrame.btnTrabajadores.setVisible(false);
                            mainFrame.menuArchivos.setVisible(false);
                            inicioMain(mainFrame);
                        } else {
                            JOptionPane.showMessageDialog(this, "El usuario " + trabajador.getNombre().toUpperCase() + " " + trabajador.getApellidoPaterno().toUpperCase() + " no se encuentra activo.");
                            txtUsuario.setText("");
                            txtPassOculto.setText("");
                            txtPassMuestra.setText("");
                        }
                    }
                } catch (HeadlessException e) {
                }
            }
        }
    }//GEN-LAST:event_btnIniciarSesionActionPerformed

    private void checkMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkMostrarActionPerformed
        if (checkMostrar.isSelected()) {
            txtPassMuestra.setVisible(true);
            txtPassOculto.setVisible(false);
            txtPassMuestra.setText(txtPassOculto.getText());
        } else {
            txtPassMuestra.setVisible(false);
            txtPassOculto.setVisible(true);
            txtPassOculto.setText(txtPassMuestra.getText());
        }
    }//GEN-LAST:event_checkMostrarActionPerformed

    private void btnConfigBDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfigBDActionPerformed
        confiModal = new ConfigConexionBD(this, true);
        contolConexion = new ControllerConexionBD(confiModal);
        confiModal.setIconImage(new ImageIcon(ControllerConfigCompany.getRutaIconSistema()).getImage());
        confiModal.setVisible(true);
    }//GEN-LAST:event_btnConfigBDActionPerformed

    private void inicioMain(MainPanelServices mainFrame) {
        mainFrame.setIconImage(new ImageIcon(ControllerConfigCompany.getRutaIconSistema()).getImage());
        mainFrame.setVisible(true);
        ControllerEmployee controllerEmploye = new ControllerEmployee(null, mainFrame);
        this.dispose();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConfigBD;
    private javax.swing.JButton btnIniciarSesion;
    private javax.swing.JCheckBox checkMostrar;
    private LIB.JEImagePanel jEImagePanel1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private LIB.JPanelTransparente jPanelTransparente1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField txtPassMuestra;
    private javax.swing.JPasswordField txtPassOculto;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables

//    class ImageFondos extends JPanel {
//
//        @Override
//        public void paint(Graphics g) {
//            File ruta = new File("design" + File.separator + "imagen login");
//            String[] img = ruta.list();
//            String logo = ruta.getAbsolutePath() + File.separator + img[0];
//
//            Image fondo = new ImageIcon(logo).getImage();
//            g.drawImage(fondo, 0, 0, getWidth(), getHeight(), this);
//            setOpaque(false);
//            super.paint(g);
//        }
//    }
    private void loadData() {
        this.setTitle(Util.nombreEmpresa());
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setIconImage(new ImageIcon(ControllerConfigCompany.getRutaIconSistema()).getImage());

        txtUsuario.requestFocus();
        txtPassOculto.setVisible(true);
        txtPassMuestra.setVisible(false);

        TextPrompt usuario = new TextPrompt("usuario", txtUsuario);
        TextPrompt pass = new TextPrompt("contraseña", txtPassMuestra);
        TextPrompt pass1 = new TextPrompt("contraseña", txtPassOculto);
    }

    public static String getCargo() {
        return Login.cargo;
    }
}
