package com.edmi.model.invoice;

import java.util.List;

public class ResumenDiario {
    private String fecEmision;
    private String codResumen;
    private String fecResumen;
    private String tipDocResumen;
    private String idDocResumen;
    private String tipDocUsuario;
    private String numDocUsuario;
    private String tipMoneda;
    private String totValGrabado;
    private String totValExoneado;
    private String totValInafecto;
    private String totValExportado;
    private String totValGratuito;
    private String totOtroCargo;
    private String totImpCpe;
    private String tipDocModifico;
    private String serDocModifico;
    private String numDocModifico;
    private String tipRegPercepcion;
    private String porPercepcion;
    private String monBasePercepcion;
    private String monPercepcion;
    private String monTotIncPercepcion;
    private String tipEstado;
    private String situacionSunat;
    private List<ResumenDiario.TributosResumen> tributosDocResumen;

    public ResumenDiario() {
    }

    public String getFecEmision() {
        return fecEmision;
    }

    public void setFecEmision(String fecEmision) {
        this.fecEmision = fecEmision;
    }
    
    public String getCodResumen() {
        return codResumen;
    }

    public void setCodResumen(String codResumen) {
        this.codResumen = codResumen;
    }

    public String getFecResumen() {
        return fecResumen;
    }

    public void setFecResumen(String fecResumen) {
        this.fecResumen = fecResumen;
    }

    public String getTipDocResumen() {
        return tipDocResumen;
    }

    public void setTipDocResumen(String tipDocResumen) {
        this.tipDocResumen = tipDocResumen;
    }

    public String getIdDocResumen() {
        return idDocResumen;
    }

    public void setIdDocResumen(String idDocResumen) {
        this.idDocResumen = idDocResumen;
    }

    public String getTipDocUsuario() {
        return tipDocUsuario;
    }

    public void setTipDocUsuario(String tipDocUsuario) {
        this.tipDocUsuario = tipDocUsuario;
    }

    public String getNumDocUsuario() {
        return numDocUsuario;
    }

    public void setNumDocUsuario(String numDocUsuario) {
        this.numDocUsuario = numDocUsuario;
    }

    public String getTipMoneda() {
        return tipMoneda;
    }

    public void setTipMoneda(String tipMoneda) {
        this.tipMoneda = tipMoneda;
    }

    public String getTotValGrabado() {
        return totValGrabado;
    }

    public void setTotValGrabado(String totValGrabado) {
        this.totValGrabado = totValGrabado;
    }

    public String getTotValExoneado() {
        return totValExoneado;
    }

    public void setTotValExoneado(String totValExoneado) {
        this.totValExoneado = totValExoneado;
    }

    public String getTotValInafecto() {
        return totValInafecto;
    }

    public void setTotValInafecto(String totValInafecto) {
        this.totValInafecto = totValInafecto;
    }

    public String getTotValExportado() {
        return totValExportado;
    }

    public void setTotValExportado(String totValExportado) {
        this.totValExportado = totValExportado;
    }

    public String getTotValGratuito() {
        return totValGratuito;
    }

    public void setTotValGratuito(String totValGratuito) {
        this.totValGratuito = totValGratuito;
    }

    public String getTotOtroCargo() {
        return totOtroCargo;
    }

    public void setTotOtroCargo(String totOtroCargo) {
        this.totOtroCargo = totOtroCargo;
    }

    public String getTotImpCpe() {
        return totImpCpe;
    }

    public void setTotImpCpe(String totImpCpe) {
        this.totImpCpe = totImpCpe;
    }

    public String getTipDocModifico() {
        return tipDocModifico;
    }

    public void setTipDocModifico(String tipDocModifico) {
        this.tipDocModifico = tipDocModifico;
    }

    public String getSerDocModifico() {
        return serDocModifico;
    }

    public void setSerDocModifico(String serDocModifico) {
        this.serDocModifico = serDocModifico;
    }

    public String getNumDocModifico() {
        return numDocModifico;
    }

    public void setNumDocModifico(String numDocModifico) {
        this.numDocModifico = numDocModifico;
    }

    public String getTipRegPercepcion() {
        return tipRegPercepcion;
    }

    public void setTipRegPercepcion(String tipRegPercepcion) {
        this.tipRegPercepcion = tipRegPercepcion;
    }

    public String getPorPercepcion() {
        return porPercepcion;
    }

    public void setPorPercepcion(String porPercepcion) {
        this.porPercepcion = porPercepcion;
    }

    public String getMonBasePercepcion() {
        return monBasePercepcion;
    }

    public void setMonBasePercepcion(String monBasePercepcion) {
        this.monBasePercepcion = monBasePercepcion;
    }

    public String getMonPercepcion() {
        return monPercepcion;
    }

    public void setMonPercepcion(String monPercepcion) {
        this.monPercepcion = monPercepcion;
    }

    public String getMonTotIncPercepcion() {
        return monTotIncPercepcion;
    }

    public void setMonTotIncPercepcion(String monTotIncPercepcion) {
        this.monTotIncPercepcion = monTotIncPercepcion;
    }

    public String getTipEstado() {
        return tipEstado;
    }

    public void setTipEstado(String tipEstado) {
        this.tipEstado = tipEstado;
    }

    public List<TributosResumen> getTributosDocResumen() {
        return tributosDocResumen;
    }

    public void setTributosDocResumen(List<TributosResumen> tributosDocResumen) {
        this.tributosDocResumen = tributosDocResumen;
    }

    public String getSituacionSunat() {
        return situacionSunat;
    }

    public void setSituacionSunat(String situacionSunat) {
        this.situacionSunat = situacionSunat;
    }

    public class TributosResumen {
        private String idLineaRd;
        private String ideTributoRd;
        private String nomTributoRd;
        private String codTipTributoRd;
        private String mtoBaseImponibleRd;
        private String mtoTributoRd;

        public String getIdLineaRd() {
            return idLineaRd;
        }

        public void setIdLineaRd(String idLineaRd) {
            this.idLineaRd = idLineaRd;
        }

        public String getIdeTributoRd() {
            return ideTributoRd;
        }

        public void setIdeTributoRd(String ideTributoRd) {
            this.ideTributoRd = ideTributoRd;
        }

        public String getNomTributoRd() {
            return nomTributoRd;
        }

        public void setNomTributoRd(String nomTributoRd) {
            this.nomTributoRd = nomTributoRd;
        }

        public String getCodTipTributoRd() {
            return codTipTributoRd;
        }

        public void setCodTipTributoRd(String codTipTributoRd) {
            this.codTipTributoRd = codTipTributoRd;
        }

        public String getMtoBaseImponibleRd() {
            return mtoBaseImponibleRd;
        }

        public void setMtoBaseImponibleRd(String mtoBaseImponibleRd) {
            this.mtoBaseImponibleRd = mtoBaseImponibleRd;
        }

        public String getMtoTributoRd() {
            return mtoTributoRd;
        }

        public void setMtoTributoRd(String mtoTributoRd) {
            this.mtoTributoRd = mtoTributoRd;
        }
    }
}
