package com.edmi.repository;

import java.util.List;

public interface RepositoryVoucher<T> {
    List<T> findAll();
    List<T> findAllWhere(String condition, String value);
    T findById(Object id);
    int create(T voucher);
    int update(T voucher);
    int delete(Object id);
}
