package com.edmi.view.panel;

public class PanelReservation extends javax.swing.JPanel {
    private static PanelReservation instance;
    
    public static PanelReservation getInstance(){
        if(instance == null){
            instance = new PanelReservation();
        }
        return instance;
    }
    
    private PanelReservation() {
        initComponents();
        hideColumnsTable();
    }
    
    private void hideColumnsTable(){
        this.tablaReserva.getColumnModel().getColumn(0).setMaxWidth(0);
        this.tablaReserva.getColumnModel().getColumn(0).setMinWidth(0);
        this.tablaReserva.getColumnModel().getColumn(0).setPreferredWidth(0);

        this.tablaReserva.getColumnModel().getColumn(1).setMaxWidth(0);
        this.tablaReserva.getColumnModel().getColumn(1).setMinWidth(0);
        this.tablaReserva.getColumnModel().getColumn(1).setPreferredWidth(0);

        this.tablaReserva.getColumnModel().getColumn(2).setMaxWidth(0);
        this.tablaReserva.getColumnModel().getColumn(2).setMinWidth(0);
        this.tablaReserva.getColumnModel().getColumn(2).setPreferredWidth(0);
        
        this.tablaReserva.getColumnModel().getColumn(12).setMaxWidth(0);
        this.tablaReserva.getColumnModel().getColumn(12).setMinWidth(0);
        this.tablaReserva.getColumnModel().getColumn(12).setPreferredWidth(0);
    }
    
    public static void setNullInstance(){
        instance = null;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        tablaReserva = new javax.swing.JTable();
        reservaBtnNuevaReserva = new javax.swing.JButton();
        cbxCriterio = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        txtCriteriosPanelReservation = new javax.swing.JTextField();

        tablaReserva.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tablaReserva.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "COD. CLIENTE", "COD. USUARIO", "# HABITACION", "NOMBRES CLIENTES", "NOMBRES USUARIOS", "FECHA DE INGRESO", "FECHA DE SALIDA", "IMP. RESERVA (S/)", "IMP. PRODUCTOS (S/)", "IMP. TOTAL (S/)", "TIEMPO RESERVA", "ESTADO", "OBSERVACION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaReserva.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaReserva.setRowHeight(25);
        tablaReserva.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tablaReserva);
        if (tablaReserva.getColumnModel().getColumnCount() > 0) {
            tablaReserva.getColumnModel().getColumn(0).setPreferredWidth(80);
            tablaReserva.getColumnModel().getColumn(1).setPreferredWidth(80);
            tablaReserva.getColumnModel().getColumn(2).setPreferredWidth(80);
            tablaReserva.getColumnModel().getColumn(3).setPreferredWidth(100);
            tablaReserva.getColumnModel().getColumn(4).setPreferredWidth(250);
            tablaReserva.getColumnModel().getColumn(5).setPreferredWidth(200);
            tablaReserva.getColumnModel().getColumn(6).setPreferredWidth(160);
            tablaReserva.getColumnModel().getColumn(7).setPreferredWidth(160);
            tablaReserva.getColumnModel().getColumn(8).setPreferredWidth(135);
            tablaReserva.getColumnModel().getColumn(9).setPreferredWidth(135);
            tablaReserva.getColumnModel().getColumn(10).setPreferredWidth(135);
            tablaReserva.getColumnModel().getColumn(11).setPreferredWidth(100);
            tablaReserva.getColumnModel().getColumn(12).setPreferredWidth(250);
            tablaReserva.getColumnModel().getColumn(13).setPreferredWidth(300);
        }

        reservaBtnNuevaReserva.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        reservaBtnNuevaReserva.setText("NUEVA RESERVA");
        reservaBtnNuevaReserva.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        cbxCriterio.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxCriterio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --", "NRO. HABITACION", "CLIENTE", "USUARIO", "FECHA INGRESO", "FECHA SALIDA" }));

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Criterios:");

        txtCriteriosPanelReservation.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 979, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(reservaBtnNuevaReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addGap(8, 8, 8)
                        .addComponent(cbxCriterio, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtCriteriosPanelReservation, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(reservaBtnNuevaReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbxCriterio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(txtCriteriosPanelReservation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 491, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JComboBox<String> cbxCriterio;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JScrollPane jScrollPane2;
    public javax.swing.JButton reservaBtnNuevaReserva;
    public javax.swing.JTable tablaReserva;
    public javax.swing.JTextField txtCriteriosPanelReservation;
    // End of variables declaration//GEN-END:variables
}
