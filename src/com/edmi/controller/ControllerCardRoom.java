package com.edmi.controller;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.model.Cliente;
import com.edmi.model.Habitacion;
import com.edmi.model.Reservacion;
import com.edmi.model.Trabajador;
import com.edmi.repository.RepositoryClient;
import com.edmi.repository.RepositoryEmployee;
import com.edmi.repository.RepositoryReservation;
import com.edmi.repository.RepositoryRoom;
import com.edmi.repository.imp.ClienteRepositoryImp;
import com.edmi.repository.imp.HabitacionRepositoryImp;
import com.edmi.repository.imp.ReservaRepositoryImp;
import com.edmi.repository.imp.TrabajadorRepositoryImp;
import com.edmi.util.MessageView;
import com.edmi.util.Util;
import com.edmi.view.MainPanelServices;
import com.edmi.view.ModalNewReservation;
import com.edmi.view.ModalProductService;
import com.edmi.view.widget.CardRoom;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;

public class ControllerCardRoom implements MouseListener {
    private final CardRoom card;
    private final MainPanelServices frame;
    private RepositoryRoom<Habitacion> repositoryRoom;
    private RepositoryReservation<Reservacion> repositoryReserva;
    private RepositoryClient<Cliente> repositoryClient;
    private RepositoryEmployee<Trabajador> repositoryEmployee;
    private ModalNewReservation reservaView = null;
    private ModalProductService service;
    private Reservacion reserva;
    private Trabajador trabajador;
    private Cliente cliente;
    private String codRoom;
    private String stateRoom;
    private ControllerNewReservation controllerNewReservation = null;

    public ControllerCardRoom(CardRoom panel, MainPanelServices frame) {
        this.card = panel;
        this.frame = frame;
    }

    private void getDataBase() {
        repositoryReserva = new ReservaRepositoryImp();
        repositoryClient = new ClienteRepositoryImp();
        repositoryEmployee = new TrabajadorRepositoryImp();
        reserva = repositoryReserva.findForAddProduct(codRoom);
        trabajador = repositoryEmployee.findById(reserva.getCodTrabajador());
        cliente = repositoryClient.findById(reserva.getCodCliente());
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mousePressed(MouseEvent e) {
        getDataFromCard();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        verifyStateRoom();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
        card.setOpaque(false);
        card.repaint();
    }

    private void verifyStateRoom() {
        switch (stateRoom.toLowerCase()) {
            case "ocupado":
                methodRoomOccupied();
                break;
            case "disponible":
                methodRoomAvailable();
                break;
            case "mantenimiento":
                metodoEstadoMantenimiento();
                break;
        }
    }

    private void getDataFromCard() {
        String cod = card.txtCodigoHabitacion.getText();
        String estado = card.txtEstadoHabitacion.getText();
        codRoom = cod.trim();
        stateRoom = estado.trim();

        //Si la habitacion esta ocupada, entonces realizar peticion a la base de datos
        if (estado.equalsIgnoreCase("ocupado")) {
            getDataBase();
        }
    }

    private void methodRoomOccupied() {
        int option = JOptionPane.showConfirmDialog(frame, MessageView.CARD_ROOM_ADD_PRODUCTS, "Mensaje del Sistema", JOptionPane.YES_NO_OPTION);
        if (option == JOptionPane.YES_OPTION) {
            service = new ModalProductService(frame, true);
            service.setLocationRelativeTo(null);
            setDataReserva();

            //Mostrar los consumos despues de presionar sobre una habitacion ocupada
            service.mostrarConsumos(reserva.getCodReserva());
            service.setVisible(true);
        }
    }

    private void methodRoomAvailable() {
        //Habitacion disponible - preparar datos para la reserva
        reservaView = new ModalNewReservation(frame,true);
        reservaView.btnModificar.setVisible(false);
        controllerNewReservation = new ControllerNewReservation(reservaView, frame, null);
        controllerNewReservation.loadDataFromDataBase();
        showModalPreparedReservation();
        controllerNewReservation.initViewNewReservation();
    }
    
    private void metodoEstadoMantenimiento() {
        JOptionPane.showMessageDialog(frame, "La habitación se encuentra en mantenimiento:"
                + "\nCambie el estado de esta habitacion a DISPONIBLE en\n el menu HABITACIONES.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
    }

    private void showModalPreparedReservation() {
        //Mostrar datos de una habitacion diponible para una reserva
        repositoryRoom = new HabitacionRepositoryImp();
        Habitacion room = repositoryRoom.findById(codRoom);
        reservaView.txtNroHabitacion.setText(String.valueOf(room.getNroHabitacion()));
        reservaView.txtCodigoHabitacion.setText(String.valueOf(room.getCodhaHabitacion()));
        reservaView.txtCostoAlquiler.setText(String.valueOf(room.getCostoAlquiler()));
        reservaView.btnBuscarHabitacion.setEnabled(false);
        reservaView.txtTipoHabitacion.setText(room.getTipoHabitacion());
        reservaView.txtCaracteristicas.setText(room.getCaracteristicas());
    }

    private void setDataReserva() {
        //Mostrar datos en el formulario cuando una habitacion esta ocupada.
        ModalProductService.btnServiceBuscarReserva.setEnabled(false);
        ModalProductService.btnServiceNuevaReserva.setEnabled(false);
        ModalProductService.txtServiceNombreCliente.setText(cliente.getNombre().concat(" ").concat(cliente.getApellidoPaterno()).concat(" ").concat(cliente.getApellidoMaterno()));
        ModalProductService.txtServiceCostoReserva.setText(String.valueOf(reserva.getImporteReserva()));
        ModalProductService.txtImporteProductos.setText(reserva.getImporteProductos() <= 0.0D ? "0.00" : String.valueOf(reserva.getImporteProductos()));
        ModalProductService.txtImporteTotal.setText(String.valueOf(reserva.getImporteTotal()));
        ModalProductService.txtServiceNroDocCliente.setText(cliente.getNroDocumento());
        ModalProductService.txtServiceTipoDocCiente.setText(cliente.getTipoDocumento());
        ModalProductService.txtServiceCodReserva.setText(String.valueOf(reserva.getCodReserva()));
        ModalProductService.txtServiceCodTrabajador.setText(reserva.getCodTrabajador());
        ModalProductService.txtImporteTotal.setText(card.txtCostoHabitacion.getText().trim());
        ModalProductService.txtNroHabitacion.setText(reserva.getAuxNroRoom());
        ModalProductService.txtServiceNombreTrabajador.setText(Util.getUserNamesConcatened(trabajador.getNombre(), trabajador.getApellidoPaterno(), trabajador.getApellidoMaterno()));
    }
}