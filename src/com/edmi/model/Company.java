package com.edmi.model;

public class Company {
    private String idCompanny;
    private String nombre;
    private String ruc;
    private String direccion;
    private String ciudad;
    private String pais;

    public Company(){}
    
    public Company(String idCompanny, String nombre, String ruc, String direccion, String ciudad, String pais) {
        this.idCompanny = idCompanny;
        this.nombre = nombre;
        this.ruc = ruc;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.pais = pais;
    }

    public String getIdCompanny() {
        return idCompanny;
    }

    public void setIdCompanny(String idCompanny) {
        this.idCompanny = idCompanny;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Company{idCompanny=").append(idCompanny);
        sb.append(", nombre=").append(nombre);
        sb.append(", ruc=").append(ruc);
        sb.append(", direccion=").append(direccion);
        sb.append(", ciudad=").append(ciudad);
        sb.append(", pais=").append(pais);
        sb.append('}');
        return sb.toString();
    }
}
