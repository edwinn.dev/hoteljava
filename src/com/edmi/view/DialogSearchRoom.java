package com.edmi.view;

import com.edmi.repository.imp.HabitacionRepositoryImp;
import com.edmi.model.Habitacion;
import com.edmi.repository.RepositoryRoom;
import com.edmi.util.UtilTable;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.table.DefaultTableModel;

public class DialogSearchRoom extends javax.swing.JDialog {
    private RepositoryRoom<Habitacion> repositoryRoom;
    private List<Habitacion> rooms;
    private DefaultTableModel model;
    private ModalNewReservation modal;
    private boolean stateRow = false;
    
    public DialogSearchRoom(ModalNewReservation modal) {
        super(modal, true);
        this.modal = modal;
        this.setTitle("Habitaciones Disponibles");
        initComponents();
        this.setModal(true);
        this.setLocationRelativeTo(null);
        model = (DefaultTableModel) tableRooms.getModel();
        repositoryRoom = new HabitacionRepositoryImp(); 
        loadTableAllRooms();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cbxCriterioBusqueda = new javax.swing.JComboBox<>();
        txtBusqueda = new javax.swing.JFormattedTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableRooms = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Criterios:");

        cbxCriterioBusqueda.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxCriterioBusqueda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --", "NUMERO", "TIPO DE HABITACION" }));

        txtBusqueda.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBusquedaKeyReleased(evt);
            }
        });

        tableRooms = new javax.swing.JTable(){
            public boolean isCellEditable(int row, int col){
                for(int i = 0; i < tableRooms.getRowCount(); i++){
                    if(row == i){
                        return false;
                    }
                }
                return true;
            }
        };
        tableRooms.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "# HABITACION", "TIPO DE HABITACION", "ESTADO", "NRO. PLANTA", "COSTO ALQUILER (S/)", "CARACTERISTACAS"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableRooms.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tableRooms.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tableRooms.getTableHeader().setReorderingAllowed(false);
        tableRooms.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableRoomsMouseClicked(evt);
            }
        });
        tableRooms.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableRoomsKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tableRooms);
        if (tableRooms.getColumnModel().getColumnCount() > 0) {
            tableRooms.getColumnModel().getColumn(0).setPreferredWidth(100);
            tableRooms.getColumnModel().getColumn(1).setPreferredWidth(100);
            tableRooms.getColumnModel().getColumn(2).setPreferredWidth(200);
            tableRooms.getColumnModel().getColumn(3).setPreferredWidth(90);
            tableRooms.getColumnModel().getColumn(4).setPreferredWidth(100);
            tableRooms.getColumnModel().getColumn(5).setPreferredWidth(100);
            tableRooms.getColumnModel().getColumn(6).setPreferredWidth(300);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbxCriterioBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cbxCriterioBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 443, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void tableRoomsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableRoomsMouseClicked
        if(evt.getClickCount() == 2){
            setRoomValues();
            Habitacion habitacion = this.setRoomValues();
            modal.txtCodigoHabitacion.setValue(habitacion.getCodhaHabitacion());
            modal.txtCostoAlquiler.setValue(String.valueOf(habitacion.getCostoAlquiler()));
            modal.txtNroHabitacion.setValue(String.valueOf(habitacion.getNroHabitacion()));
            modal.txtTipoHabitacion.setValue(String.valueOf(habitacion.getTipoHabitacion()));
            modal.txtCaracteristicas.setValue(String.valueOf(habitacion.getCaracteristicas()));
            this.dispose();
        }
    }//GEN-LAST:event_tableRoomsMouseClicked

    private void tableRoomsKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableRoomsKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            setRoomValues();
        }
    }//GEN-LAST:event_tableRoomsKeyPressed

    private void txtBusquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBusquedaKeyReleased
        filterRooms();
    }//GEN-LAST:event_txtBusquedaKeyReleased
    
    private void filterRooms(){
        rooms = null;
        int index = cbxCriterioBusqueda.getSelectedIndex();

        if(index == 0){
            loadTableAllRooms();
        }else{
            String valueSend = txtBusqueda.getText().trim();
            String criterio = null;
            switch(index){
                case 1 : criterio = "nro_habitacion"; break;
                case 2 : criterio = "tipo_habitacion"; break;
            }
            rooms = repositoryRoom.findAllWhereFiltrosReserva(criterio, valueSend, "1");
            loadPreferencesTableRooms();
        }
    }
    
    public Habitacion setRoomValues(){
        Habitacion habitacion = new Habitacion();
        int row = tableRooms.getSelectedRow();
        String codigo = tableRooms.getValueAt(row, 0).toString();
        Habitacion h = repositoryRoom.findById(codigo);
        habitacion.setCodhaHabitacion(codigo);
        habitacion.setNroHabitacion(h.getNroHabitacion());
        habitacion.setTipoHabitacion(h.getTipoHabitacion());
        habitacion.setEstado(h.getEstado());
        habitacion.setNroPlanta(h.getNroPlanta());
        habitacion.setCostoAlquiler(h.getCostoAlquiler());
        habitacion.setCaracteristicas(h.getCaracteristicas());
        stateRow = true; //Significa que se ha selecciondo una fila
        
        //Despues de haber presionado Enter o doble click sobre el registro. El formulario se cierra
        this.setVisible(false);
        this.dispose();
        
        return habitacion;
    }
    
    public boolean isSelectedRow(){
        return this.stateRow;
    }

    private void loadPreferencesTableRooms(){
        UtilTable.clearTable(tableRooms);
        rooms.forEach(r ->{
            model.addRow(new Object[]{
                r.getCodhaHabitacion(), r.getNroHabitacion(), r.getTipoHabitacion(), r.getEstado(), r.getNroPlanta(),
                r.getCostoAlquiler(), r.getCaracteristicas()
            });
        });
        tableRooms.setModel(model);
    }
    
    private void loadTableAllRooms(){
        rooms = null;
        rooms = repositoryRoom.findAllAvailable();
        UtilTable.clearTable(tableRooms);
        rooms.forEach(r ->{
            model.addRow(new Object[]{
                r.getCodhaHabitacion(), r.getNroHabitacion(), r.getTipoHabitacion(), r.getEstado(), r.getNroPlanta(),
                r.getCostoAlquiler(), r.getCaracteristicas()
            });
        });
        tableRooms.setModel(model);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cbxCriterioBusqueda;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableRooms;
    private javax.swing.JFormattedTextField txtBusqueda;
    // End of variables declaration//GEN-END:variables
}
