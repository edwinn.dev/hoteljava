package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.model.TipoHabitacion;
import com.edmi.database.query.Query;
import com.edmi.repository.RepositoryProduct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TipoHabitacionRepositoryImp implements RepositoryProduct<TipoHabitacion> {

    private Connection getConnection() throws SQLException{
        return getInstance();
    }
    
    @Override
    public List<TipoHabitacion> findAll() {
        List<TipoHabitacion> list = new ArrayList<>();
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TYPE_ROOM_FIND_ALL);
            ResultSet rs = ps.executeQuery()) {
            while(rs.next()){
                TipoHabitacion type = new TipoHabitacion();
                type.setCodTipo(rs.getString("cod_tipohabitacion"));
                type.setCapacidad(rs.getInt("capacidad"));
                type.setTipoHabitacion(rs.getString("tipo_habitacion"));
                type.setDescripcion(rs.getString("descripcion"));
                list.add(type);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TipoHabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public TipoHabitacion findById(Object id) {
        return null;
    }

    @Override
    public int create(TipoHabitacion tipo) {
        int response = 0;
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TYPE_ROOM_CREATE)){
            ps.setString(1, tipo.getCodTipo());
            ps.setInt(2, tipo.getCapacidad());
            ps.setString(3, tipo.getTipoHabitacion());
            ps.setString(4, tipo.getDescripcion());
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TipoHabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    @Override
    public int update(TipoHabitacion tipo) {
        
        return 0;
    }

    @Override
    public int delete(Object id, Object value) {
        return 0;
    }

    @Override
    public int updateStock(Object id, Double value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TipoHabitacion> findAllWhere(String condition, String value) {
        return null;
    }

    @Override
    public List<TipoHabitacion> findAllCodeProduct() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
