package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.database.query.Query;
import com.edmi.model.CabeceraModel;
import com.edmi.model.DetalleFacbo;
import com.edmi.model.NotaCD;
import com.edmi.model.invoice.ComunicacionBaja;
import com.edmi.model.invoice.ResumenDiario;
import java.util.*;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CabeceraRepositoryImp {

    public List<CabeceraModel> findAllCabeceraFacbo() {
        List<CabeceraModel> lista = new ArrayList();
        try ( PreparedStatement ps = getInstance().prepareStatement(Query.SQL_CAB_FACBO_FIND_ALL)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    CabeceraModel cfb = new CabeceraModel();
                    cfb.setIdCabecera(rs.getLong("id_cabecera"));
                    cfb.setCodCliente(rs.getString("cod_cliente"));
                    cfb.setFechaEmision(rs.getString("fecha_emision"));
                    cfb.setHoraEmision(rs.getString("hora_emision"));
                    cfb.setFechaVencimiento(rs.getString("fecha_vencimiento"));
                    cfb.setTipoMoneda(rs.getString("tipo_moneda"));
                    cfb.setTotalTributos(rs.getString("total_tributos"));
                    cfb.setTotalDescuento(rs.getString("total_descuento"));
                    cfb.setImporteVenta(rs.getString("importe_venta"));
                    cfb.setTotalVenta(rs.getString("total_venta"));
                    cfb.setPrecioVenta(rs.getString("precio_venta"));
                    cfb.setOtrosCargos(rs.getString("otros_cargos"));
                    cfb.setTipoComprobante(rs.getString("tipo_comprobante"));
                    cfb.setSerie(rs.getString("serie"));
                    cfb.setNumero(rs.getString("numero"));
                    cfb.setAuxNombreCliente(rs.getString("cliente"));
                    cfb.setAuxNumDoc(rs.getString("nro_documento"));
                    cfb.setAuxEstado(rs.getString("estado"));
                    cfb.setSituacionSunat(rs.getString("situacion_sunat"));
                    lista.add(cfb);
                }
            }
        } catch (SQLException e) {
            lista = null;
            Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }

    public List<CabeceraModel> findAllCabeceraFacboSerieNumero(String serie, String numero) {
        List<CabeceraModel> lista = new ArrayList();
        try ( PreparedStatement ps = getInstance().prepareStatement(Query.SQL_CAB_FACBO_FIND_ALL_SN)) {
            ps.setString(1, serie);
            ps.setString(2, numero);
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    CabeceraModel cfb = new CabeceraModel();
                    cfb.setIdCabecera(rs.getLong("id_cabecera"));
                    cfb.setCodCliente(rs.getString("cod_cliente"));
                    cfb.setFechaEmision(rs.getString("fecha_emision"));
                    cfb.setHoraEmision(rs.getString("hora_emision"));
                    cfb.setFechaVencimiento(rs.getString("fecha_vencimiento"));
                    cfb.setTipoMoneda(rs.getString("tipo_moneda"));
                    cfb.setTotalTributos(rs.getString("total_tributos"));
                    cfb.setTotalDescuento(rs.getString("total_descuento"));
                    cfb.setImporteVenta(rs.getString("importe_venta"));
                    cfb.setTotalVenta(rs.getString("total_venta"));
                    cfb.setPrecioVenta(rs.getString("precio_venta"));
                    cfb.setOtrosCargos(rs.getString("otros_cargos"));
                    cfb.setTipoComprobante(rs.getString("tipo_comprobante"));
                    cfb.setSerie(rs.getString("serie"));
                    cfb.setNumero(rs.getString("numero"));
                    cfb.setAuxNombreCliente(rs.getString("cliente"));
                    cfb.setAuxNumDoc(rs.getString("nro_documento"));
                    cfb.setAuxEstado(rs.getString("estado"));
                    cfb.setSituacionSunat(rs.getString("situacion_sunat"));
                    lista.add(cfb);
                }
            }
        } catch (SQLException e) {
            lista = null;
            Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }

    public CabeceraModel findAllCabeceraFacboNumero(String numero) {
        CabeceraModel cfb = null;
        try ( PreparedStatement ps = getInstance().prepareStatement(Query.SQL_CAB_FACBO_FIND_ALL)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    cfb = new CabeceraModel();
                    cfb.setIdCabecera(rs.getLong("id_cabecera"));
                    cfb.setCodCliente(rs.getString("cod_cliente"));
                    cfb.setFechaEmision(rs.getString("fecha_emision"));
                    cfb.setHoraEmision(rs.getString("hora_emision"));
                    cfb.setFechaVencimiento(rs.getString("fecha_vencimiento"));
                    cfb.setTipoMoneda(rs.getString("tipo_moneda"));
                    cfb.setTotalTributos(rs.getString("total_tributos"));
                    cfb.setTotalDescuento(rs.getString("total_descuento"));
                    cfb.setImporteVenta(rs.getString("importe_venta"));
                    cfb.setTotalVenta(rs.getString("total_venta"));
                    cfb.setPrecioVenta(rs.getString("precio_venta"));
                    cfb.setOtrosCargos(rs.getString("otros_cargos"));
                    cfb.setTipoComprobante(rs.getString("tipo_comprobante"));
                    cfb.setSerie(rs.getString("serie"));
                    cfb.setNumero(rs.getString("numero"));
                    cfb.setAuxNombreCliente(rs.getString("cliente"));
                    cfb.setAuxNumDoc(rs.getString("nro_documento"));
                    cfb.setAuxEstado(rs.getString("estado"));
                    cfb.setSituacionSunat(rs.getString("situacion_sunat"));
                }
            }
        } catch (SQLException e) {
            cfb = null;
            Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return cfb;
    }

    public List<CabeceraModel> findAllWhere(String condition, String value) {
        List<CabeceraModel> lista = new ArrayList<>();
        String consulta = "SELECT h.*, concat(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) as cliente, c.nro_documento FROM cabec_facbo h inner join cliente c on h.cod_cliente = c.cod_cliente"
                .concat(" WHERE " + condition + " LIKE '%" + value + "%'  ORDER BY h.id_cabecera DESC");
        try ( PreparedStatement ps = getInstance().prepareStatement(consulta)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    CabeceraModel cfb = new CabeceraModel();
                    cfb.setIdCabecera(rs.getLong("id_cabecera"));
                    cfb.setCodCliente(rs.getString("cod_cliente"));
                    cfb.setFechaEmision(rs.getString("fecha_emision"));
                    cfb.setHoraEmision(rs.getString("hora_emision"));
                    cfb.setFechaVencimiento(rs.getString("fecha_vencimiento"));
                    cfb.setTipoMoneda(rs.getString("tipo_moneda"));
                    cfb.setTotalTributos(rs.getString("total_tributos"));
                    cfb.setTotalDescuento(rs.getString("total_descuento"));
                    cfb.setImporteVenta(rs.getString("importe_venta"));
                    cfb.setTotalVenta(rs.getString("total_venta"));
                    cfb.setPrecioVenta(rs.getString("precio_venta"));
                    cfb.setOtrosCargos(rs.getString("otros_cargos"));
                    cfb.setTipoComprobante(rs.getString("tipo_comprobante"));
                    cfb.setSerie(rs.getString("serie"));
                    cfb.setNumero(rs.getString("numero"));
                    cfb.setAuxNombreCliente(rs.getString("cliente"));
                    cfb.setAuxNumDoc(rs.getString("nro_documento"));
                    cfb.setAuxEstado(rs.getString("estado"));
                    cfb.setSituacionSunat(rs.getString("situacion_sunat"));
                    lista.add(cfb);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProveedorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    public int createCabeceraFacbo(CabeceraModel cb) {
        int response = 0;
        try ( PreparedStatement ps = getInstance().prepareStatement(Query.SQL_CAB_FACBO_CREATE)) {
            ps.setString(1, cb.getCodCliente());
            ps.setString(2, cb.getFechaEmision());
            ps.setString(3, cb.getHoraEmision());
            ps.setString(4, cb.getFechaVencimiento());
            ps.setString(5, cb.getTipoMoneda());
            ps.setString(6, cb.getTotalTributos());
            ps.setString(7, cb.getTotalDescuento());
            ps.setString(8, cb.getImporteVenta());
            ps.setString(9, cb.getTotalVenta());
            ps.setString(10, cb.getPrecioVenta());
            ps.setString(11, cb.getOtrosCargos());
            ps.setString(12, cb.getTipoComprobante());
            ps.setString(13, cb.getSerie());
            ps.setString(14, cb.getNumero());
            ps.setString(15, "-");
            ps.setString(16, cb.getSituacionSunat());
            response = ps.executeUpdate();
        } catch (SQLException e) {
            Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }

    public List<DetalleFacbo> findAllDetalleFacbo() {
        List<DetalleFacbo> lista = new ArrayList();
        try ( PreparedStatement ps = getInstance().prepareStatement(Query.SQL_DETALLE_FACBO_FIND_ALL)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    DetalleFacbo dfb = new DetalleFacbo();
                    dfb.setCod_producto(rs.getString("cod_producto"));
                    dfb.setCantidad(rs.getString("cantidad"));
                    dfb.setValor_unitario(rs.getString("valor_unitario"));
                    dfb.setTotal_tributo_item(rs.getString("total_tributo_item"));
                    dfb.setCod_afectacion(rs.getString("cod_afectacion"));
                    dfb.setTipo_afectacion(rs.getString("tipo_afectacion"));
                    dfb.setPorcentaje_afectacion(rs.getString("porcentaje_afectacion"));
                    dfb.setDescuento(rs.getString("descuento"));
                    dfb.setOtros_cargos(rs.getString("otros_cargos"));
                    dfb.setPrecio_venta_item(rs.getString("precio_venta_item"));
                    dfb.setTotal_importe_item(rs.getString("total_importe_item"));
                    //dfb.setAuxEstado(rs.getString("estado"));
                    lista.add(dfb);
                }
            }
        } catch (SQLException e) {
            lista = null;
            Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }

    public int createDetalleFacbo(DetalleFacbo dfb) {
        int response = 0;
        try ( PreparedStatement ps = getInstance().prepareStatement(Query.SQL_DETALLE_FACBO_CREATE)) {
            ps.setLong(1, dfb.getIdCabecera());
            ps.setString(2, dfb.getCod_producto());
            ps.setString(3, dfb.getCantidad());
            ps.setString(4, dfb.getValor_unitario());
            ps.setString(5, dfb.getTotal_tributo_item());
            ps.setString(6, dfb.getCod_afectacion());
            ps.setString(7, dfb.getTipo_afectacion());
            ps.setString(8, dfb.getPorcentaje_afectacion());
            ps.setString(9, dfb.getDescuento());
            ps.setString(10, dfb.getOtros_cargos());
            ps.setString(11, dfb.getPrecio_venta_item());
            ps.setString(12, dfb.getTotal_importe_item());
            response = ps.executeUpdate();
        } catch (SQLException e) {
            Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }

    public List<NotaCD> findAllNotaCD() {
        List<NotaCD> lista = new ArrayList();
        try ( PreparedStatement ps = getInstance().prepareStatement(Query.SQL_NOTACD_FIND_ALL)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    NotaCD nota = new NotaCD();
                    nota.setCod_cabecera(rs.getString("cod_cabecera"));
                    nota.setFecha_emision(rs.getString("fecha_emision"));
                    nota.setHora_emision(rs.getString("hora_emision"));
                    nota.setTipo_nota(rs.getString("tipo_nota"));
                    nota.setNroDocuCliente(rs.getString("nro_doc_cliente"));
                    nota.setDescrip_motivo(rs.getString("descrip_motivo"));
                    nota.setDocAfectado(rs.getString("doc_afectado"));
                    nota.setSituacionSunat(rs.getString("situacion_sunat"));
                    lista.add(nota);
                }
            }
        } catch (SQLException e) {
            Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }

    public Long getLastHeader() {
        Long lastHeader;
        try ( CallableStatement cs = getInstance().prepareCall("CALL getLastHeader()");  ResultSet rs = cs.executeQuery()) {
            if (rs.next()) {
                lastHeader = rs.getLong("ultimaCab");
            } else {
                lastHeader = 0L;
            }
        } catch (SQLException ex) {
            lastHeader = -1L;
            Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lastHeader;
    }

    public List<NotaCD> findAllWhereCD(String condition, String value) {
        List<NotaCD> lista = new ArrayList<>();
        String consulta = "SELECT * FROM nota_cd"
                .concat(" WHERE " + condition + " LIKE '%" + value + "%'  SELECT * FROM nota_cd ORDER BY id_nota DESC;");
        try ( PreparedStatement ps = getInstance().prepareStatement(consulta)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    NotaCD nota = new NotaCD();
                    nota.setCod_cabecera(rs.getString("cod_cabecera"));
                    nota.setFecha_emision(rs.getString("fecha_emision"));
                    nota.setHora_emision(rs.getString("hora_emision"));
                    nota.setTipo_nota(rs.getString("tipo_nota"));
                    nota.setNroDocuCliente(rs.getString("nro_doc_cliente"));
                    nota.setDescrip_motivo(rs.getString("descrip_motivo"));
                    nota.setDocAfectado(rs.getString("doc_afectado"));
                    nota.setSituacionSunat(rs.getString("situacion_sunat"));
                    lista.add(nota);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProveedorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    public List<NotaCD> findAllWhereCD(String codCabecera) {
        List<NotaCD> lista = new ArrayList<>();
        String consulta = "SELECT * FROM nota_cd WHERE cod_cabecera = '" + codCabecera+"' ORDER BY id_nota DESC";
        try ( PreparedStatement ps = getInstance().prepareStatement(consulta)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    NotaCD nota = new NotaCD();
                    nota.setCod_cabecera(rs.getString("cod_cabecera"));
                    nota.setFecha_emision(rs.getString("fecha_emision"));
                    nota.setHora_emision(rs.getString("hora_emision"));
                    nota.setTipo_nota(rs.getString("tipo_nota"));
                    nota.setNroDocuCliente(rs.getString("nro_doc_cliente"));
                    nota.setDescrip_motivo(rs.getString("descrip_motivo"));
                    nota.setDocAfectado(rs.getString("doc_afectado"));
                    nota.setSituacionSunat(rs.getString("situacion_sunat"));
                    lista.add(nota);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProveedorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    public static int createNotaCD(NotaCD nota) {
        int response = 0;
        try ( PreparedStatement ps = getInstance().prepareStatement(Query.SQL_NOTACD_CREATE)) {
            ps.setString(1, nota.getCod_cabecera());
            ps.setString(2, nota.getDocAfectado());
            ps.setString(3, nota.getFecha_emision());
            ps.setString(4, nota.getHora_emision());
            ps.setString(5, nota.getTipo_nota());
            ps.setString(6, nota.getNroDocuCliente());
            ps.setString(7, nota.getDescrip_motivo());
            ps.setString(8, nota.getSituacionSunat());
            response = ps.executeUpdate();
        } catch (SQLException e) {
            Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }

    public static int actualizarEstadoComprobante(String valor, String estado) {
        int response = 0;
        String sqlScript = "UPDATE CABEC_FACBO SET estado= '" + estado + "' WHERE id_cabecera = ?";
        try ( PreparedStatement ps = getInstance().prepareStatement(sqlScript)) {
            ps.setString(1, valor);
            response = ps.executeUpdate();
        } catch (SQLException e) {
            Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }

    public static int acutalizarSituacionSunat(int op, String situacion, String serie, String numero) {
        int response = 0;
        String sqlScript;
        switch (op) {
            case 1:
                sqlScript = "UPDATE cabec_facbo SET situacion_sunat = '" + situacion + "' WHERE serie = ? AND numero = ?";
                try ( PreparedStatement ps = getInstance().prepareStatement(sqlScript)) {
                    ps.setString(1, serie);
                    ps.setString(2, numero);
                    response = ps.executeUpdate();
                } catch (SQLException e) {
                    Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
                }
                break;
            case 2:
                sqlScript = "UPDATE nota_cd SET situacion_sunat = '" + situacion + "' WHERE cod_cabecera = ?";
                try ( PreparedStatement ps = getInstance().prepareStatement(sqlScript)) {
                    ps.setString(1, serie);
                    response = ps.executeUpdate();
                } catch (SQLException e) {
                    Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
                }
                break;
            case 3:
                sqlScript = "UPDATE resumen_diario SET situacion_sunat = '" + situacion + "' WHERE cod_resumen = ?";
                try ( PreparedStatement ps = getInstance().prepareStatement(sqlScript)) {
                    ps.setString(1, serie);
                    response = ps.executeUpdate();
                } catch (SQLException e) {
                    Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
                }
                break;
            case 4:
                sqlScript = "UPDATE comunicacion_baja SET situacion_sunat = '" + situacion + "' WHERE cod_baja = ?";
                 try ( PreparedStatement ps = getInstance().prepareStatement(sqlScript)) {
                    ps.setString(1, serie);
                    response = ps.executeUpdate();
                } catch (SQLException e) {
                    Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
                }
                break;
        }
        return response;
    }

    public List<ResumenDiario> findAllResumenDiarioCod(String codResumen) {
        List<ResumenDiario> lista = new ArrayList();
        try ( PreparedStatement st = getInstance().prepareStatement(Query.SQL_RESU_FIND_BY_CODRESUMEN)) {
            st.setString(1, codResumen);
            try ( ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    ResumenDiario resumen = new ResumenDiario();
                    resumen.setCodResumen(rs.getString("cod_resumen"));
                    resumen.setFecEmision(rs.getString("fecEmision"));
                    resumen.setFecResumen(rs.getString("fecResumen"));
                    resumen.setTipDocResumen(rs.getString("tipDocResumen"));
                    resumen.setIdDocResumen(rs.getString("idDocResumen"));
                    resumen.setSituacionSunat(rs.getString("situacion_sunat"));
                    lista.add(resumen);
                }
            }
        } catch (SQLException ex) {
            lista = null;
            Logger.getLogger(RepositoryResumen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    public List<ComunicacionBaja> findAllCodBaja(String codBaja) {
        List<ComunicacionBaja> lista = new ArrayList();
        try ( PreparedStatement st = getInstance().prepareStatement(Query.SQL_COM_FIND_BY_CODBAJA)) {
            st.setString(1, codBaja);
            try ( ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    ComunicacionBaja com = new ComunicacionBaja();
                    com.setCodBaja(rs.getString("cod_baja"));
                    com.setFecGeneracion(rs.getString("fecGeneracion"));
                    com.setFecComunicacion(rs.getString("fecComunicacion"));
                    com.setTipDocBaja(rs.getString("tipDocBaja"));
                    com.setNumDocBaja(rs.getString("numDocBaja"));
                    com.setDesMotivoBaja(rs.getString("desMotivoBaja"));
                    com.setSituacionSunat(rs.getString("situacion_sunat"));
                    lista.add(com);
                }
            }
        } catch (SQLException ex) {
            lista = null;
            Logger.getLogger(RepositoryComunicacionBaja.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
}
