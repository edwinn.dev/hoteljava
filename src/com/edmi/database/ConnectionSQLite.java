package com.edmi.database;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.ControllerUbicacionSFS;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ConnectionSQLite {
    private static Connection connection;

    public static synchronized Connection getInstanceSQLite() throws SQLException {
        try {
            Class.forName("org.sqlite.JDBC");
            if (connection == null) {
                String pathSqlite = ControllerUbicacionSFS.obtenerRutaSFS()[1];
                if(pathSqlite.trim().isEmpty() || !pathSqlite.trim().endsWith(".db")){
                    JOptionPane.showMessageDialog(null, "La ruta de la DB del Facturador es incorrecta.\n"
                            + "O configure la BD del Facturador", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                    connection = DriverManager.getConnection("jdbc:sqlite:C:\\SFS_v1.4\\bd\\BDFacturador.db");
                }else{
                    connection = DriverManager.getConnection("jdbc:sqlite:".concat(pathSqlite));
                }
            }
        } catch (ClassNotFoundException ex) {
            System.err.println("La ruta de la base de datos del Facturador es incorrecta." + ex.getClass());
        }
        return connection;
    }
}
