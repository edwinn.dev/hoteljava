package com.edmi.controller;

import static com.edmi.database.ConnectionDb.getInstance;
import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.database.ConnectionDb;
import com.edmi.util.CapturaException;
import com.edmi.util.EncriptacionPasswordAES;
import com.edmi.view.ConfigConexionBD;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import org.apache.log4j.Logger;

public class ControllerConexionBD implements ActionListener {
    private ConfigConexionBD modalConexion = null;
    private static final Logger LOGGER = Logger.getLogger(ControllerConexionBD.class);

    public ControllerConexionBD(ConfigConexionBD modalConexion) {
        this.modalConexion = modalConexion;
        setListeners();
        recuperarDatos();
        mostrarDatos();
    }

    private void escribirDatosBD() {
        ConnectionDb.setConnection(null);
        
        String[] datos = new String[5];
        datos[0] = modalConexion.txtHost.getText().trim();
        datos[1] = modalConexion.txtPuerto.getText().trim();
        datos[2] = modalConexion.txtBaseDatos.getText().trim();
        datos[3] = modalConexion.txtUsuario.getText().trim();
        datos[4] = EncriptacionPasswordAES.encriptar(modalConexion.txtPassword.getText().trim());

        try {
            File ruta = new File("design" + File.separator + "conexion" + File.separator + "datosBD.txt");
            ruta.createNewFile();

            try (FileWriter escribir = new FileWriter(ruta.getAbsolutePath())) {
                for (String ds : datos) {
                    for (int j = 0; j < ds.length(); j++) {
                        escribir.write(ds.charAt(j));
                    }
                    escribir.write("\n");
                }
            }
            JOptionPane.showMessageDialog(modalConexion, "Datos guardados.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException e) {
            LOGGER.error("Error: \n"+CapturaException.exceptionStacktraceToString(e));
        }
    }

    private String[] recuperarDatos() {
        String[] datos = new String[5];
        int cont = 0;
        String bfRead;

        try {
            File ruta = new File("design" + File.separator + "conexion" + File.separator + "datosBD.txt");
            try (BufferedReader br = new BufferedReader(new FileReader(ruta.getAbsolutePath()))) {
                while ((bfRead = br.readLine()) != null) {
                    if(cont == 4){
                        datos[cont] = EncriptacionPasswordAES.desencriptar(bfRead);
                    }else{
                        datos[cont] = bfRead;
                    }
                    cont++;
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error: \n"+CapturaException.exceptionStacktraceToString(e));
        }

        return datos;
    }

    private void mostrarDatos() {
        if (modalConexion != null) {
            modalConexion.txtHost.setText(recuperarDatos()[0]);
            modalConexion.txtPuerto.setText(recuperarDatos()[1]);
            modalConexion.txtBaseDatos.setText(recuperarDatos()[2]);
            modalConexion.txtUsuario.setText(recuperarDatos()[3]);
            modalConexion.txtPassword.setText(recuperarDatos()[4]);
        }
    }

    public void conectarDatosBD() {
        recuperarDatos();
        ConnectionDb.setDatosConnection(recuperarDatos()[0], recuperarDatos()[1], recuperarDatos()[2], recuperarDatos()[3], recuperarDatos()[4]);
    }

    private Connection getConnection() throws SQLException {
        return getInstance();
    }

    private void comprobarConexion() {
        conectarDatosBD();
        try {
            if (getConnection() != null) {
                JOptionPane.showMessageDialog(modalConexion, "Conexion exitosa.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(modalConexion, "Conexion fallida.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        } catch (HeadlessException | SQLException e) {
            LOGGER.error("Error: \n"+CapturaException.exceptionStacktraceToString(e));
            JOptionPane.showMessageDialog(modalConexion, "Conexion fallida.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }

    private void setListeners() {
        if (modalConexion != null) {
            modalConexion.btnGuardar.addActionListener(this);
            modalConexion.btnProbar.addActionListener(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == modalConexion.btnProbar) {
            comprobarConexion();
        }

        if (e.getSource() == modalConexion.btnGuardar) {
            escribirDatosBD();
        }
    }
}