package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.model.Proveedor;
import com.edmi.repository.RepositorySupplier;
import com.edmi.database.query.Query;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProveedorRepositoryImp implements RepositorySupplier<Proveedor> {
    
    private Connection getConnection() throws SQLException {
        return getInstance();
    }
    
    @Override
    public List<Proveedor> findAllEstado(String estado) {
        List<Proveedor> list = new ArrayList<>();
        try ( PreparedStatement st = getConnection().prepareStatement(Query.SQL_PROV_FIND_ALL_STATE)) {
            st.setString(1, estado);
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    Proveedor prov = generateSupplier(rs);
                    list.add(prov);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProveedorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    @Override
    public List<Proveedor> findAll() {
        List<Proveedor> list = new ArrayList<>();
        try ( PreparedStatement st = getConnection().prepareStatement(Query.SQL_PROV_FIND_ALL);  ResultSet rs = st.executeQuery()) {
            while (rs.next()) {
                Proveedor prov = generateSupplier(rs);
                list.add(prov);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProveedorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    @Override
    public List<Proveedor> findAllWhere(String condition, String value, String estado) {
        List<Proveedor> list = new ArrayList<>();
        try ( PreparedStatement ps = getConnection().prepareStatement("SELECT * FROM proveedor WHERE " + condition + " LIKE " + "'" + value + "%' AND estado = '" + estado + "'")) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Proveedor prov = generateSupplier(rs);
                    list.add(prov);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProveedorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    @Override
    public Proveedor findById(Object id) {
        Proveedor prov = null;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_PROV_FIND_BY_ID)) {
            ps.setString(1, id.toString());
            try ( ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    prov = generateSupplier(rs);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return prov;
    }
    
    @Override
    public int create(Proveedor prov) {
        int response = 0;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_PROV_CREATE)) {
            ps.setString(1, prov.getCodProveedor());
            ps.setString(2, prov.getCodTipoDoc());
            ps.setString(3, prov.getTipoDocumento());
            ps.setString(4, prov.getNroDocumento());
            ps.setString(5, prov.getRazonSocial());
            ps.setString(6, prov.getProvincia());
            ps.setString(7, prov.getDireccion());
            ps.setString(8, prov.getTelefonoUno());
            ps.setString(9, prov.getTelefonoDos());
            ps.setString(10, prov.getEmail());
            ps.setString(11, prov.getObservacion());
            ps.setString(12, prov.getEstado());
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProveedorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }
    
    @Override
    public int update(Proveedor prov) {
        int op = 0;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_PROV_UPDATE)) {
            ps.setString(1, prov.getCodTipoDoc());
            ps.setString(2, prov.getTipoDocumento());
            ps.setString(3, prov.getNroDocumento());
            ps.setString(4, prov.getRazonSocial());
            ps.setString(5, prov.getProvincia());
            ps.setString(6, prov.getDireccion());
            ps.setString(7, prov.getTelefonoUno());
            ps.setString(8, prov.getTelefonoDos());
            ps.setString(9, prov.getEmail());
            ps.setString(10, prov.getObservacion());
            ps.setString(11, prov.getCodProveedor());
            op = ps.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return op;
    }
    
    @Override
    public int delete(Object id) {
        return 0;
    }
    
    @Override
    public String findAllTipeDocu(Object tipoDocu) {
        String cod = "";
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_PROV_TIPODOCU)) {
            ps.setString(1, tipoDocu.toString());
            try ( ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    cod = rs.getString("cod_tipodocumento");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return cod;
    }
    
    @Override
    public List<String> findAllDocuDescripciones() {
        List<String> lista = new ArrayList();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TBJ_FIND_AL_DOCU)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    lista.add(rs.getString("descripcion"));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }
    
    @Override
    public int state(Object id, Object actividad) {
        int response = 0;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_PROV_STATE)) {
            ps.setString(1, actividad.toString());
            ps.setString(2, id.toString());
            response = ps.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }
    
    public List<String> obtenerNroDocu() {
        List<String> lista = new ArrayList();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_PROV_FIND_AL_NRODOCU)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    String nroDocu = rs.getString("nro_documento");
                    lista.add(nroDocu);
                }
            }
        } catch (Exception e) {
        }
        return lista;
    }
    
    private Proveedor generateSupplier(ResultSet rs) throws SQLException {
        Proveedor p = new Proveedor();
        p.setCodProveedor(rs.getString("cod_proveedor"));
        p.setCodTipoDoc(rs.getString("cod_tipodocumento"));
        p.setTipoDocumento(rs.getString("tipo_documento"));
        p.setNroDocumento(rs.getString("nro_documento"));
        p.setRazonSocial(rs.getString("razon_social"));
        p.setProvincia(rs.getString("provincia"));
        p.setDireccion(rs.getString("direccion"));
        p.setTelefonoUno(rs.getString("telefono_uno"));
        p.setTelefonoDos(rs.getString("telefono_dos"));
        p.setEmail(rs.getString("email"));
        p.setObservacion(rs.getString("observacion"));
        p.setTipoDocumento(rs.getString("tipo_documento"));
        p.setEstado(rs.getString("estado"));
        return p;
    }
}
