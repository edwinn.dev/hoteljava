package com.edmi.reports;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.controller.invoice.JsonFiles;
import com.edmi.controller.panel.ControllerFacura;
import com.edmi.model.Company;
import com.edmi.model.DetalleFacbo;
import com.edmi.model.NotaCD;
import com.edmi.model.invoice.CabeceraNota;
import com.edmi.model.invoice.DatoPago;
import com.edmi.model.invoice.Detalle;
import com.edmi.model.invoice.Leyenda;
import com.edmi.model.invoice.NotaModel;
import com.edmi.model.invoice.Tributo;
import com.edmi.repository.imp.CabeceraRepositoryImp;
import com.edmi.repository.imp.CompanyRepositoryImp;
import com.edmi.repository.imp.NotaCreditoRepository;
import com.edmi.util.AmountLetters;
import com.edmi.util.NombresArchivosJSON;
import com.edmi.util.QR;
import com.edmi.util.Util;
import com.edmi.view.DialogNotaDebito;
import com.edmi.view.DialogReportFiles;
import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class ReportNotaDebito {
    private List<DetalleFacbo> lista;
    private DetalleFacbo dfb;
    private String codProductoTabla = "";
    private String operacion = "";
    private String motivo = "";
    private String codigoOperacion = "";
    private long idCabe = 0L;
    private double totalIgv = 0.00D;
    private double costo = 0.00D;
    private double totalTributos = 0.00D;
    private double importeTotal = 0.00D;
    private double mtoBaseImponibleIgv = 0.00D;
    private double mtoBaseImponibleIna = 0.00D;
    private double valorVenta = 0.00D;
    private double subTotalVentas = 0.00D;
    private CabeceraNota cabecera;

    private List<Detalle> details = null;
    private List<Tributo> tributes = null;
    private List<Leyenda> leyendas;
    private DatoPago pago = null;
    private Company c;
    private DialogNotaDebito dialogNote = null;

    public void enviarDatosNota(long idCabecera, DialogNotaDebito dialogo) {
        String serieNumero = dialogo.txtCodigoNota.getText();
        this.dialogNote = dialogo;
        Map<String, Object> parametro = new HashMap();

        operacion = dialogo.cboTipoOperacion.getSelectedItem().toString();

        List<Company> datosCompany = new CompanyRepositoryImp().findAll();
        c = datosCompany.get(0);
        parametro.put("nombre_empresa", c.getNombre().toUpperCase());
        parametro.put("direccion_empresa", c.getDireccion());
        parametro.put("ciudad_empresa", c.getCiudad());
        parametro.put("pais_empresa", c.getPais());
        parametro.put("ruc_empresa", c.getRuc());

        parametro.put("codigo_credito", serieNumero);
        parametro.put("fecha_emision", Util.getDateFormat(dialogo.dcFechaEmision.getDate()));
        parametro.put("comprobante_emitido", dialogo.txtSerieComprobante.getText() + "-" + dialogo.txtNumeroComprobante.getText());
        parametro.put("cliente", dialogo.txtRazonSocial.getText());
        parametro.put("nro_documento", dialogo.txtNumeroDocumento.getText());
        parametro.put("tipo_moneda", "PEN");

        switch (dialogo.cboTipoOperacion.getSelectedIndex()) {
            case 0:
                break;
            //INTERESES POR MORA
            case 1:
                lista = NotaCreditoRepository.findAllProductNotaCredito(idCabecera, "");
                costo = Double.parseDouble(dialogo.txtCosto.getText().trim());
                codigoOperacion = "01";
                idCabe = idCabecera;
                motivo = "INTERESES POR MORA";
                break;
            //AUMENTO EN EL VALOR
            case 2:
                lista = NotaCreditoRepository.findAllProductNotaCredito(idCabecera, "");
                costo = Double.parseDouble(dialogo.txtCosto.getText().trim());
                codigoOperacion = "02";
                idCabe = idCabecera;
                motivo = "AUMENTO EN EL VALOR";
                break;
            //PENALIDAD
            case 3:
                lista = NotaCreditoRepository.findAllProductNotaCredito(idCabecera, "");
                costo = Double.parseDouble(dialogo.txtCosto.getText().trim());
                codigoOperacion = "03";
                idCabe = idCabecera;
                motivo = "PENALIDAD";
                break;
        }

        dfb = lista.get(0);
        
        datosQR(dialogo);
        
        File imagen = new File("design" + File.separator + "logo inicio");
        String[] imagenes = imagen.list();
        String rutaLogo = "";
        for (String i : imagenes) {
            File archivo = new File(imagen.getAbsoluteFile(), i);
            rutaLogo = archivo.getAbsolutePath();
        }

        parametro.put("pid_cabecera", idCabe);
        parametro.put("codigo_tabla", codProductoTabla);

        parametro.put("motivo", motivo);
        parametro.put("descripcion", dialogo.txtDescripcion.getText().trim());
        parametro.put("sub_total", Util.decimalRound(costo));
        parametro.put("descuentos", "0.00");
        parametro.put("operacion", operacion);
        parametro.put("valor_venta", Util.decimalRound(costo));
        parametro.put("isc", "0.00");
        parametro.put("igv", Util.decimalRound(totalIgv));
        parametro.put("interesesMora", "0.00");
        parametro.put("importe_total", Util.decimalRound(costo));
        parametro.put("valorUnitario", Util.decimalRound(costo));
        parametro.put("imagen_qr", "src/com/edmi/images/qr/qrhotel.png");
        parametro.put("rutaLogo", rutaLogo);
        
        String montoLetras;
        if (costo > 0.0D) {
            montoLetras = createLegend(String.valueOf(costo));
        } else {
            montoLetras = createLegend(String.valueOf(importeTotal));
        }
        parametro.put("monto_letras", montoLetras);

        try {
            JasperReport report = (JasperReport) JRLoader.loadObjectFromFile("src\\com\\edmi\\reports\\NotaDebito.jasper");
            JasperPrint print = JasperFillManager.fillReport(report, parametro, getInstance());
            JasperViewer view = new JasperViewer(print, false);
            view.setTitle("Nota de Débito");
            view.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            view.setVisible(true);

            String pathReportsPdf = DialogReportFiles.obtenerRutaArchiReporPdfXml()[1];
            if(pathReportsPdf == null){
                JOptionPane.showMessageDialog(this.dialogNote, "El documento no se guardó. \nDebe configurar la ruta para almacenar los PDF", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }else{
                JasperExportManager.exportReportToPdfFile(print, (pathReportsPdf+File.separator + serieNumero+".pdf"));
            }
            
            NombresArchivosJSON.generarNombreNotaDebito();
            generarNotaDebito(dialogo);

        } catch (SQLException | JRException e) {
            Logger.getLogger(ReportNotaCredito.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private CabeceraNota createCabeNota(DialogNotaDebito dialogo) {
        CabeceraNota cn = new CabeceraNota();
        cn.setTipOperacion(operacion);
        cn.setFecEmision(Util.getDateFormat(dialogo.dcFechaEmision.getDate()));
        cn.setHorEmision(Util.getTimeFormat(new Date()));
        cn.setCodLocalEmisor("0000");
        cn.setTipDocUsuario(dfb.getCod_tipodocumento());
        cn.setNumDocUsuario(dialogo.txtNumeroDocumento.getText());
        cn.setRznSocialUsuario(dialogo.txtRazonSocial.getText());
        cn.setTipMoneda("PEN");
        cn.setCodMotivo(codigoOperacion);
        cn.setDesMotivo(motivo);
        cn.setTipDocAfectado(dialogo.txtSerieComprobante.getText().substring(0, 1).equals("F") ? "01" : "03");
        cn.setNumDocAfectado(dialogo.txtSerieComprobante.getText() + "-" + dialogo.txtNumeroComprobante.getText());
        cn.setSumTotTributos(Util.decimalRound(totalTributos));
        cn.setSumTotValVenta(Util.decimalRound(valorVenta));
        cn.setSumPrecioVenta(Util.decimalRound(importeTotal));
        cn.setSumDescTotal(Util.decimalRound(costo));
        cn.setSumOtrosCargos("0.00");
        cn.setSumTotalAnticipos("0.00");
        cn.setSumImpVenta(Util.decimalRound(importeTotal));
        cn.setUblVersionId("2.1");
        cn.setCustomizationId("2.0");
        return cn;
    }

    private void generarNotaDebito(DialogNotaDebito dialogo) {
        NotaModel nota = new NotaModel();

        if (costo > 0.0D) {
            generarDctoGlobal(dialogo);
            nota.setCabecera(cabecera);
            nota.setDetalle(details);
            createLegend(String.valueOf(costo));
            crearTributosDescuento();
        } else {
            cabecera = createCabeNota(dialogo);
            nota.setCabecera(cabecera);

            createDetail();
            nota.setDetalle(details);

            createLegend(String.valueOf(importeTotal));
            createTribute();
        }
        nota.setLeyendas(leyendas);

        //createTribute();
        nota.setTributos(tributes);

        JsonFiles jsonFiles = new JsonFiles();
        CompanyRepositoryImp repositoryCompany = new CompanyRepositoryImp();
        Company c = repositoryCompany.getCompany();

        boolean response = jsonFiles.createFileJson(nota, c.getRuc() + "-" + "08" + "-" + dialogo.txtCodigoNota.getText());
        if (response) {
            int responseUpdate = CabeceraRepositoryImp.actualizarEstadoComprobante(this.dialogNote.txtIdCabecera.getText().trim(), "NOTA DE DEBITO GENERADO");
            if(responseUpdate <= 0){
                JOptionPane.showMessageDialog(dialogo, "No se pudo actualizar estado del comprobante.", "Mensaje del Sistema", JOptionPane.INFORMATION_MESSAGE);
            }
            
            NotaCD notaCD = new NotaCD();
            notaCD.setCod_cabecera(dialogo.txtCodigoNota.getText());
            notaCD.setDescrip_motivo(motivo);
            notaCD.setFecha_emision(cabecera.getFecEmision());
            notaCD.setHora_emision(cabecera.getHorEmision());
            notaCD.setTipo_nota("08");
            notaCD.setDocAfectado(dialogo.txtSerieComprobante.getText().trim()+"-"+dialogo.txtNumeroComprobante.getText().trim());
            notaCD.setNroDocuCliente(dialogo.txtNumeroDocumento.getText());
            notaCD.setSituacionSunat("-");
            
            int responseDB = CabeceraRepositoryImp.createNotaCD(notaCD);
            if(responseDB <= 0){
                //JOptionPane.showMessageDialog(dialogo, "Error al registar la Nota en la BD.", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
            
            JOptionPane.showMessageDialog(dialogo, "Nota de débito generada.", "Mensaje del Sistema", JOptionPane.INFORMATION_MESSAGE);
            ControllerFacura.loadTableVouchers();
            ControllerFacura.loadTableNotasCD();
        } else {
            JOptionPane.showMessageDialog(dialogo, "Error al generar nota de débito\n"
                    + "contacte con el administrador", "Mensaje del Sistema", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void createDetail() {
        details = new ArrayList();
        Detalle d;
        for (DetalleFacbo dfb : lista) {
            d = new Detalle();
            d.setCodUnidadMedida(dfb.getUnidadMedida());
            d.setCtdUnidadItem(dfb.getCantidad());
            d.setCodProducto(dfb.getCod_producto());
            d.setCodProductoSUNAT("-");
            d.setDesItem(dfb.getNombreProducto());
            d.setMtoValorUnitario(dfb.getValor_unitario());
            d.setSumTotTributosItem("0.00");

            d.setCodTriIGV(dfb.getCod_afectacion());
            d.setMtoIgvItem(Double.parseDouble(dfb.getPorcentaje_afectacion()) > 0.00D ? dfb.getPorcentaje_afectacion() : "0.00");
            d.setMtoBaseIgvItem(dfb.getTotal_importe_item());
            d.setNomTributoIgvItem(Double.parseDouble(dfb.getPorcentaje_afectacion()) > 0.00D ? "IGV" : "INA");
            d.setCodTipTributoIgvItem(Double.parseDouble(dfb.getPorcentaje_afectacion()) > 0.00D ? "VAT" : "FRE");
            d.setTipAfeIGV(dfb.getTipo_afectacion());
            d.setPorIgvItem(Double.parseDouble(dfb.getPorcentaje_afectacion()) > 0.00D ? "18.00" : "0.00");

            d.setCodTriISC("-");
            d.setMtoIscItem("0.00");
            d.setMtoBaseIscItem("0.00");
            d.setNomTributoIscItem("-");
            d.setCodTipTributoIscItem("-");
            d.setTipSisISC("-");
            d.setPorIscItem("0.00");

            d.setCodTriOtro("9999");
            d.setMtoTriOtroItem("0.00");
            d.setMtoBaseTriOtroItem("0.00");
            d.setNomTributoOtroItem("OTROS");
            d.setCodTipTributoOtroItem("OTH");
            d.setPorTriOtroItem("0.00");

            d.setCodTriIcbper("7152");
            d.setMtoTriIcbperItem("0.00");
            d.setCtdBolsasTriIcbperItem("0");
            d.setNomTributoIcbperItem("ICBPER");
            d.setCodTipTributoIcbperItem("OTH");
            d.setMtoTriIcbperUnidad("0.00");

            if (Double.parseDouble(dfb.getPorcentaje_afectacion()) > 0.00D) {
                mtoBaseImponibleIgv += Double.parseDouble(dfb.getTotal_importe_item());
            } else {
                mtoBaseImponibleIna += Double.parseDouble(dfb.getTotal_importe_item());
            }

            d.setMtoPrecioVentaUnitario(dfb.getValor_unitario());
            d.setMtoValorVentaItem(dfb.getTotal_importe_item());
            d.setMtoValorReferencialUnitario("0.00");
            details.add(d);
        }
    }

    private void createTribute() {
        tributes = new ArrayList<>();
        Tributo tIgv = new Tributo();
        if (totalIgv > 0.00D) {
            System.out.println("Entro");
            tIgv.setIdeTributo("1000");
            tIgv.setNomTributo("IGV");
            tIgv.setCodTipTributo("VAT");
            tIgv.setMtoBaseImponible(Util.decimalRound(mtoBaseImponibleIgv)); //Suma de precios de productos que se ha aplicado IGV.-
            tIgv.setMtoTributo(Util.decimalRound(totalIgv)); //Suma de los IGVs
        } else {
            tIgv.setIdeTributo("1000");
            tIgv.setNomTributo("IGV");
            tIgv.setCodTipTributo("VAT");
            tIgv.setMtoBaseImponible("0.00");
            tIgv.setMtoTributo("0.00");
        }

        Tributo tIgvIna = new Tributo();

        tIgvIna.setIdeTributo("9998");
        tIgvIna.setNomTributo("INA");
        tIgvIna.setCodTipTributo("FRE");
        tIgvIna.setMtoBaseImponible(Util.decimalRound(mtoBaseImponibleIna));
        tIgvIna.setMtoTributo("0.00");

        Tributo tIsc = new Tributo();
        tIsc.setIdeTributo("2000");
        tIsc.setNomTributo("ISC");
        tIsc.setCodTipTributo("EXC");
        tIsc.setMtoBaseImponible("0.00");
        tIsc.setMtoTributo("0.00");

        Tributo tIcbper = new Tributo();
        tIcbper.setIdeTributo("7152");
        tIcbper.setNomTributo("ICBPER");
        tIcbper.setCodTipTributo("OTH");
        tIcbper.setMtoBaseImponible("0.00");
        tIcbper.setMtoTributo("0.00");

        tributes.add(tIgv);
        tributes.add(tIgvIna);
        tributes.add(tIsc);
        tributes.add(tIcbper);
    }

    private void crearTributosDescuento() {
        tributes = new ArrayList<>();
        Tributo tIgv = new Tributo();
        tIgv.setIdeTributo("1000");
        tIgv.setNomTributo("IGV");
        tIgv.setCodTipTributo("VAT");
        tIgv.setMtoBaseImponible("0.00"); //Suma de precios de productos que se ha aplicado IGV.-
        tIgv.setMtoTributo("0.00"); //Suma de los IGVs

        Tributo tIgvIna = new Tributo();
        tIgvIna.setIdeTributo("9998");
        tIgvIna.setNomTributo("INA");
        tIgvIna.setCodTipTributo("FRE");
        tIgvIna.setMtoBaseImponible(Util.decimalRound(costo));
        tIgvIna.setMtoTributo("0.00");

        Tributo tIsc = new Tributo();
        tIsc.setIdeTributo("2000");
        tIsc.setNomTributo("ISC");
        tIsc.setCodTipTributo("EXC");
        tIsc.setMtoBaseImponible("0.00");
        tIsc.setMtoTributo("0.00");

        Tributo tIcbper = new Tributo();
        tIcbper.setIdeTributo("7152");
        tIcbper.setNomTributo("ICBPER");
        tIcbper.setCodTipTributo("OTH");
        tIcbper.setMtoBaseImponible("0.00");
        tIcbper.setMtoTributo("0.00");

        tributes.add(tIgv);
        tributes.add(tIgvIna);
        tributes.add(tIsc);
        tributes.add(tIcbper);
    }

    private String createLegend(String amountString) {
        String amountLetters = AmountLetters.convertToAmountInLetters(amountString, true);
        leyendas = new ArrayList<>();
        Leyenda legend = new Leyenda();
        legend.setCodLeyenda("1000");
        legend.setDesLeyenda(amountLetters);
        leyendas.add(legend);
        return amountLetters;
    }

    private void createInfoPayment() {
        pago = new DatoPago();
        pago.setFormaPago("Contado");
        pago.setMtoNetoPendientePago("0.00");
        pago.setTipMonedaMtoNetoPendientePago("PEN");
    }

    private void generarDctoGlobal(DialogNotaDebito dialogo) {
        cabecera = new CabeceraNota();
        cabecera.setTipOperacion(operacion);
        cabecera.setFecEmision(Util.getDateFormat(dialogo.dcFechaEmision.getDate()));
        cabecera.setHorEmision(Util.getTimeFormat(new Date()));
        cabecera.setCodLocalEmisor("0000");
        cabecera.setTipDocUsuario(dfb.getCod_tipodocumento());
        cabecera.setNumDocUsuario(dialogo.txtNumeroDocumento.getText());
        cabecera.setRznSocialUsuario(dialogo.txtRazonSocial.getText());
        cabecera.setTipMoneda("PEN");
        cabecera.setCodMotivo(codigoOperacion);
        cabecera.setDesMotivo(motivo);
        cabecera.setTipDocAfectado(dialogo.txtSerieComprobante.getText().substring(0, 1).equals("F") ? "01" : "03");
        cabecera.setNumDocAfectado(dialogo.txtSerieComprobante.getText() + "-" + dialogo.txtNumeroComprobante.getText());
        cabecera.setSumTotTributos(Util.decimalRound(0.0));
        cabecera.setSumTotValVenta(Util.decimalRound(costo));
        cabecera.setSumPrecioVenta(Util.decimalRound(costo));
        cabecera.setSumDescTotal(Util.decimalRound(0.0));
        cabecera.setSumOtrosCargos("0.00");
        cabecera.setSumTotalAnticipos("0.00");
        cabecera.setSumImpVenta(Util.decimalRound(costo));
        cabecera.setUblVersionId("2.1");
        cabecera.setCustomizationId("2.0");

        details = new ArrayList<>();
        Detalle d = new Detalle();
        d.setCodUnidadMedida("NIU");
        d.setCtdUnidadItem("1.00");
        
        String dato = ""; 
        if (codigoOperacion.equals("01")) {
            dato = "INTERESES POR MORA";
        } else if (codigoOperacion.equals("02")) {
            dato = "AUMENTO EN EL VALOR";
        } else if (codigoOperacion.equals("03")) {
            dato = "PENALIDAD";
        }
        
        d.setCodProducto(dato);
        d.setCodProductoSUNAT("-");
        d.setDesItem(motivo);
        d.setMtoValorUnitario(Util.decimalRound(costo));
        d.setSumTotTributosItem("0.00");

        d.setCodTriIGV("9998");
        d.setMtoIgvItem("0.00");
        d.setMtoBaseIgvItem(Util.decimalRound(costo));
        d.setNomTributoIgvItem("INA");
        d.setCodTipTributoIgvItem("FRE");
        d.setTipAfeIGV("30");
        d.setPorIgvItem("0.00");

        d.setCodTriISC("-");
        d.setMtoIscItem("0.00");
        d.setMtoBaseIscItem("0.00");
        d.setNomTributoIscItem("-");
        d.setCodTipTributoIscItem("-");
        d.setTipSisISC("-");
        d.setPorIscItem("0.00");

        d.setCodTriOtro("9999");
        d.setMtoTriOtroItem("0.00");
        d.setMtoBaseTriOtroItem("0.00");
        d.setNomTributoOtroItem("OTROS");
        d.setCodTipTributoOtroItem("OTH");
        d.setPorTriOtroItem("0.00");

        d.setCodTriIcbper("7152");
        d.setMtoTriIcbperItem("0.00");
        d.setCtdBolsasTriIcbperItem("0");
        d.setNomTributoIcbperItem("ICBPER");
        d.setCodTipTributoIcbperItem("OTH");
        d.setMtoTriIcbperUnidad("0.00");

        d.setMtoPrecioVentaUnitario(Util.decimalRound(costo));
        d.setMtoValorVentaItem(Util.decimalRound(costo));
        d.setMtoValorReferencialUnitario("0.00");
        details.add(d);
    }

    private void datosQR(DialogNotaDebito dialogo) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String[] datos = new String[9];
        datos[0] = c.getRuc();
        datos[1] = "08";
        datos[2] = dialogo.txtCodigoNota.getText().substring(0, 4);
        datos[3] = dialogo.txtCodigoNota.getText().substring(5);
        datos[4] = Util.decimalRound(totalIgv);
        datos[5] = Util.decimalRound(costo);
        datos[6] = sdf.format(dialogo.dcFechaEmision.getDate());
        datos[7] = dfb.getCod_tipodocumento();
        datos[8] = dfb.getNumeroDocumento();
        QR.generarQR(datos);
    }
}
