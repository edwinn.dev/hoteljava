package com.edmi.repository;

import java.util.List;

public interface RepositoryProduct<T> {
    List<T> findAll();
    List<T> findAllCodeProduct();
    List<T> findAllWhere(String condition, String value);
    T findById(Object id);
    int create(T object);
    int update(T object);
    int updateStock(Object id, Double value);
    int delete(Object id, Object value);
}
