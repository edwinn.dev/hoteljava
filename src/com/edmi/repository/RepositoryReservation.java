package com.edmi.repository;

import java.util.List;

public interface RepositoryReservation<T> {
    List<T> findAll();
    List<T> findAllWhere(String condition, String value);
    T findById(Object id);
    T findForAddProduct(Object codRoom);
    int create(T reserva);
    int update(T reserva);
    int updateReservaConsumo(T reserva);
    int cancel(T reserva, String value);
    int delete(Object id);
    int state(Object id, Object estado);
}
