package com.edmi.model.invoice;

public class VariableGlobal {
    private String tipVariableGlobal;
    private String codTipoVariableGlobal;
    private String porVariableGlobal;
    private String monMontoVariableGlobal;
    private String mtoVariableGlobal;
    private String monBaseImponibleVariableGlobal;
    private String mtoBaseImpVariableGlobal;

    public String getTipVariableGlobal() {
        return tipVariableGlobal;
    }

    public void setTipVariableGlobal(String tipVariableGlobal) {
        this.tipVariableGlobal = tipVariableGlobal;
    }

    public String getCodTipoVariableGlobal() {
        return codTipoVariableGlobal;
    }

    public void setCodTipoVariableGlobal(String codTipoVariableGlobal) {
        this.codTipoVariableGlobal = codTipoVariableGlobal;
    }

    public String getPorVariableGlobal() {
        return porVariableGlobal;
    }

    public void setPorVariableGlobal(String porVariableGlobal) {
        this.porVariableGlobal = porVariableGlobal;
    }

    public String getMonMontoVariableGlobal() {
        return monMontoVariableGlobal;
    }

    public void setMonMontoVariableGlobal(String monMontoVariableGlobal) {
        this.monMontoVariableGlobal = monMontoVariableGlobal;
    }

    public String getMtoVariableGlobal() {
        return mtoVariableGlobal;
    }

    public void setMtoVariableGlobal(String mtoVariableGlobal) {
        this.mtoVariableGlobal = mtoVariableGlobal;
    }

    public String getMonBaseImponibleVariableGlobal() {
        return monBaseImponibleVariableGlobal;
    }

    public void setMonBaseImponibleVariableGlobal(String monBaseImponibleVariableGlobal) {
        this.monBaseImponibleVariableGlobal = monBaseImponibleVariableGlobal;
    }

    public String getMtoBaseImpVariableGlobal() {
        return mtoBaseImpVariableGlobal;
    }

    public void setMtoBaseImpVariableGlobal(String mtoBaseImpVariableGlobal) {
        this.mtoBaseImpVariableGlobal = mtoBaseImpVariableGlobal;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("VariableGlobal{tipVariableGlobal=").append(tipVariableGlobal);
        sb.append(", codTipoVariableGlobal=").append(codTipoVariableGlobal);
        sb.append(", porVariableGlobal=").append(porVariableGlobal);
        sb.append(", monMontoVariableGlobal=").append(monMontoVariableGlobal);
        sb.append(", mtoVariableGlobal=").append(mtoVariableGlobal);
        sb.append(", monBaseImponibleVariableGlobal=").append(monBaseImponibleVariableGlobal);
        sb.append(", mtoBaseImpVariableGlobal=").append(mtoBaseImpVariableGlobal);
        sb.append('}');
        return sb.toString();
    }
}
