package com.edmi.model;

public class Email {
    private String correoEmpresa;
    private String password;
    private String rutaArchivoPDF;
    private String rutaArchivoXML;
    private String destino;
    private String asunto;
    private String mensaje;
    private String host;
    private String puerto;
    private String cc;
    private String cco;

    public String getCorreoEmpresa() {
        return correoEmpresa;
    }

    public void setCorreoEmpresa(String correoEmpresa) {
        this.correoEmpresa = correoEmpresa;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRutaArchivoPDF() {
        return rutaArchivoPDF;
    }

    public void setRutaArchivoPDF(String rutaArchivoPDF) {
        this.rutaArchivoPDF = rutaArchivoPDF;
    }

    public String getRutaArchivoXML() {
        return rutaArchivoXML;
    }

    public void setRutaArchivoXML(String rutaArchivoXML) {
        this.rutaArchivoXML = rutaArchivoXML;
    }

    
    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPuerto() {
        return puerto;
    }

    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getCco() {
        return cco;
    }

    public void setCco(String cco) {
        this.cco = cco;
    }
}
