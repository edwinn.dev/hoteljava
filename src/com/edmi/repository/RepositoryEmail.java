package com.edmi.repository;

public interface RepositoryEmail<E> {
    E getConfigEmail();
    int updateConfgEmail(E email);
    int insertConfigEmail(E email);
}
