package com.edmi.controller;

import com.edmi.view.MainPanelServices;
import com.edmi.view.panel.*;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;

public class ControllerReport implements ActionListener {
    private MainPanelServices mainFrame = null;
    private PanelReportClient reportClient = null;
    private PanelReportEmployee reportEmployee = null;
    private PanelReportProduct reportProduct = null;
    private PanelReportReserva reportReserva = null;
    private PanelReportRoom reportRoom = null;
    private PanelReportVentas reportVentas = null;

    public ControllerReport(MainPanelServices mainFrame) {
        this.mainFrame = mainFrame;
        this.mainFrame.btnReturnReport.setVisible(false);
        this.reportClient = PanelReportClient.getInstance();
        this.reportEmployee = PanelReportEmployee.getInstance();
        this.reportProduct = PanelReportProduct.getInstance();
        this.reportReserva = PanelReportReserva.getInstance();
        this.reportRoom = PanelReportRoom.getInstance();
        this.reportVentas = PanelReportVentas.getInstance();

        this.mainFrame.panelReports.setLayout(new GridBagLayout());

        addComponents(mainFrame.panelReports, this.reportClient);
        addComponents(mainFrame.panelReports, this.reportEmployee);
        addComponents(mainFrame.panelReports, this.reportProduct);
        addComponents(mainFrame.panelReports, this.reportReserva);
        addComponents(mainFrame.panelReports, this.reportRoom);
        addComponents(mainFrame.panelReports, this.reportVentas);
        
        this.reportProduct.setVisible(true);
        this.reportClient.setVisible(false);
        this.reportEmployee.setVisible(false);
        this.reportReserva.setVisible(false);
        this.reportRoom.setVisible(false);
        this.reportVentas.setVisible(false);
        setListener();
    }

    private void setListener() {
        this.mainFrame.btnReportProducts.addActionListener(this);
        this.mainFrame.btnReportClients.addActionListener(this);
        this.mainFrame.btnReportEmployee.addActionListener(this);
        this.mainFrame.btnReportReserva.addActionListener(this);
        this.mainFrame.btnReportRoom.addActionListener(this);
        this.mainFrame.btnReportVentas.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == mainFrame.btnReportProducts) {

            this.reportClient.setVisible(false);
            this.reportEmployee.setVisible(false);
            this.reportReserva.setVisible(false);
            this.reportRoom.setVisible(false);
            this.reportVentas.setVisible(false);

            this.reportProduct.setVisible(true);
        } else if (e.getSource() == mainFrame.btnReportClients) {

            this.reportEmployee.setVisible(false);
            this.reportProduct.setVisible(false);
            this.reportReserva.setVisible(false);
            this.reportRoom.setVisible(false);
            this.reportVentas.setVisible(false);

            this.reportClient.setVisible(true);
        } else if (e.getSource() == mainFrame.btnReturnReport) {

            this.reportClient.setVisible(false);
            this.reportEmployee.setVisible(false);
            this.reportProduct.setVisible(false);
            this.reportReserva.setVisible(false);
            this.reportRoom.setVisible(false);
            this.reportVentas.setVisible(false);

        } else if (e.getSource() == mainFrame.btnReportEmployee) {

            this.reportClient.setVisible(false);
            this.reportProduct.setVisible(false);
            this.reportReserva.setVisible(false);
            this.reportRoom.setVisible(false);
            this.reportVentas.setVisible(false);

            this.reportEmployee.setVisible(true);
        } else if (e.getSource() == mainFrame.btnReportReserva) {

            this.reportClient.setVisible(false);
            this.reportEmployee.setVisible(false);
            this.reportProduct.setVisible(false);
            this.reportRoom.setVisible(false);
            this.reportVentas.setVisible(false);

            this.reportReserva.setVisible(true);
        } else if (e.getSource() == mainFrame.btnReportRoom) {

            this.reportClient.setVisible(false);
            this.reportEmployee.setVisible(false);
            this.reportProduct.setVisible(false);
            this.reportReserva.setVisible(false);
            this.reportVentas.setVisible(false);

            this.reportRoom.setVisible(true);
        } else if (e.getSource() == mainFrame.btnReportVentas) {
            
            this.reportClient.setVisible(false);
            this.reportEmployee.setVisible(false);
            this.reportProduct.setVisible(false);
            this.reportReserva.setVisible(false);
            this.reportRoom.setVisible(false);
            
            this.reportVentas.setVisible(true);
        }
    }

    private void addComponents(JComponent parent, JComponent child) {
        GridBagConstraints conf = new GridBagConstraints();
        conf.gridx = 0;
        conf.gridy = 0;
        conf.gridwidth = 1;
        conf.gridheight = 1;
        conf.weightx = 1.0;
        conf.weighty = 1.0;
        conf.anchor = GridBagConstraints.CENTER;
        conf.fill = GridBagConstraints.BOTH;

        parent.add(child, conf);
        child.setVisible(true);
    }
}
