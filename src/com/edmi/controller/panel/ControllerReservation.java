package com.edmi.controller.panel;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.ControllerNewReservation;
import com.edmi.controller.MenuTablaReservas;
import com.edmi.model.Habitacion;
import com.edmi.model.Reservacion;
import com.edmi.repository.RepositoryReservation;
import com.edmi.repository.RepositoryRoom;
import com.edmi.repository.imp.HabitacionRepositoryImp;
import com.edmi.repository.imp.ReservaRepositoryImp;
import com.edmi.util.Util;
import com.edmi.view.MainPanelServices;
import com.edmi.view.ModalNewReservation;
import com.edmi.view.panel.PanelDetailReservation;
import com.edmi.view.panel.PanelReservation;
import com.edmi.view.widget.CardRoom;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ControllerReservation implements ActionListener, MouseListener, KeyListener {
    private final ImageIcon ocupado = new ImageIcon(getClass().getResource("/com/edmi/images/sleeping_bed.png"));
    private final ImageIcon disponible = new ImageIcon(getClass().getResource("/com/edmi/images/icon_double_bed.png"));
    private MainPanelServices mainFrame = null;
    private PanelReservation panelReserva = null;
    private PanelDetailReservation panelDetailReserva = null;
    private MenuTablaReservas menuTableReserva = null;
    private RepositoryRoom<Habitacion> repositoryRoom = null;
    private RepositoryReservation<Reservacion> repositoryReserva = null;
    private ControllerDetailReserva controllerDetailReserva = null;
    private List<Reservacion> listaReserv = null;
    private boolean filtro = false;

    //Utils
    private String codigoReserva;
    private int row = -5;

    public ControllerReservation(MainPanelServices mainFrame) {
        this.mainFrame = mainFrame;
        this.panelDetailReserva = PanelDetailReservation.getInstance();
        this.panelReserva = PanelReservation.getInstance();
        this.menuTableReserva = MenuTablaReservas.getInstance();
        this.menuTableReserva.initMenuReserva(this.panelReserva.tablaReserva);
        this.repositoryRoom = new HabitacionRepositoryImp();
        this.repositoryReserva = new ReservaRepositoryImp();

        this.mainFrame.panelReservacion.setLayout(new GridBagLayout());

        addComponents(this.mainFrame.panelReservacion, this.panelReserva);
        addComponents(this.mainFrame.panelReservacion, this.panelDetailReserva);

        this.panelDetailReserva.setVisible(false);
        this.panelReserva.setVisible(true);

        setListener();
    }

    private void setListener() {
        this.panelReserva.tablaReserva.addMouseListener(this);
        this.panelReserva.reservaBtnNuevaReserva.addActionListener(this);

        this.menuTableReserva.itemDetalle.addActionListener(this);
        this.menuTableReserva.itemDescartado.addActionListener(this);
        this.menuTableReserva.itemModificar.addActionListener(this);

        this.panelDetailReserva.btnRegresar.addActionListener(this);
        this.panelReserva.txtCriteriosPanelReservation.addKeyListener(this);
    }

    public void showRooms(int valor) {
        List<Habitacion> rooms = null;
        switch(valor) {
            case 0:
                rooms = repositoryRoom.findAllEstado("1");
                break;
            case 1:
                rooms = repositoryRoom.findAllWhere("estado","DISPONIBLE", "1");
                break;
            case 2:
                rooms = repositoryRoom.findAllWhere("estado","MANTENIMIENTO", "1");
                break;
            case 3:
                rooms = repositoryRoom.findAllWhere("estado","OCUPADO", "1");
                break;
        }
        mainFrame.panelRooms.removeAll();

        if(rooms != null){
            GridLayout layout = new GridLayout((5 - 1), 5, 15, 15);
            if (rooms.size() <= 4 || rooms.size() <= 8) {
                layout = new GridLayout(3, 8, 15, 15);
            }
            mainFrame.panelRooms.setLayout(layout);

            rooms.stream().map(room -> {
                CardRoom card = new CardRoom(mainFrame);
                card.txtImagenHabitacion.setIcon(room.getEstado().equalsIgnoreCase("ocupado") ? ocupado : disponible);
                card.txtCodigoHabitacion.setText(room.getCodhaHabitacion());
                card.txtNumeroHabitacion.setText(String.valueOf(room.getNroHabitacion()));
                card.txtEstadoHabitacion.setText(room.getEstado());
                card.txtTipoHabitacion.setText(room.getTipoHabitacion());
                card.txtNroPlanta.setText(String.valueOf(room.getNroPlanta()));
                card.txtCostoHabitacion.setText(String.valueOf(room.getCostoAlquiler()));
                return card;
            }).forEachOrdered(panel -> {
                mainFrame.panelRooms.add(panel);
            });
            mainFrame.repaint();
        }
    }

    public void showReservas() {
        clearTable(this.panelReserva.tablaReserva);
        DefaultTableModel modelReserva = (DefaultTableModel) this.panelReserva.tablaReserva.getModel();
        List<Reservacion> reservaciones = repositoryReserva.findAll();
        if (filtro) {
            listaReserv.forEach((r) -> {
                modelReserva.addRow(new Object[]{
                    r.getCodReserva(), r.getCodCliente(), r.getCodTrabajador(), r.getAuxNroRoom(), r.getNombreCliente(), r.getNombreTrabajador(),
                    r.getFechaIngreso(), r.getFechaSalida(),
                    r.getImporteReserva(), r.getImporteProductos() > 0.0D ? r.getImporteProductos() : "0.00",
                    r.getImporteTotal(), String.valueOf(r.getTiempoReserva()).concat(" ").concat(r.getMagnitudReserva()),
                    r.getEstado(), r.getObservacion(),});
            });
        } else {
            reservaciones.forEach((r) -> {
                modelReserva.addRow(new Object[]{
                    r.getCodReserva(), r.getCodCliente(), r.getCodTrabajador(), r.getAuxNroRoom(), r.getNombreCliente(), r.getNombreTrabajador(),
                    r.getFechaIngreso(), r.getFechaSalida(),
                    r.getImporteReserva(), r.getImporteProductos() > 0.0D ? r.getImporteProductos() : "0.00",
                    r.getImporteTotal(), String.valueOf(r.getTiempoReserva()).concat(" ").concat(r.getMagnitudReserva()),
                    r.getEstado(), r.getObservacion(),});
            });
        }
        this.panelReserva.tablaReserva.setModel(modelReserva);
    }

    private void filterReservation() {
        filtro = true;
        listaReserv = new ArrayList();
        int index = panelReserva.cbxCriterio.getSelectedIndex();

        if (index != 0) {
            String valueSend = panelReserva.txtCriteriosPanelReservation.getText().trim();
            String condition = null;
            switch (index) {
                case 1:
                    condition = "nro_habitacion";
                    break;
                case 2:
                    condition = "CONCAT(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno)";
                    break;
                case 3:
                    condition = "CONCAT(t.nombre, ' ', t.apellido_paterno, ' ', t.apellido_materno)";
                    break;
                case 4:
                    condition = "fecha_ingreso";
                    break;
                case 5:
                    condition = "fecha_salida";
                    break;
            }

            listaReserv = repositoryReserva.findAllWhere(condition, valueSend);
            showReservas();
            filtro = false;
        }
    }
    
    private void cambiarEstadoHabitacion(String estado, String codigoHab) {
        this.repositoryRoom.updateState(codigoHab, estado);
    }
    
    private void cambiarEstadoReserva(String estado) {
        int fila = this.panelReserva.tablaReserva.getSelectedRow();
        int op = JOptionPane.showConfirmDialog(this.panelReserva, "¿Está seguro de eliminar la reserva?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if (op == JOptionPane.YES_OPTION) {
            if (repositoryReserva.state(this.panelReserva.tablaReserva.getValueAt(fila, 0), estado) != 0) {
                JOptionPane.showMessageDialog(this.panelReserva, "Reserva eliminada.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                cambiarEstadoHabitacion("DISPONIBLE", this.panelReserva.tablaReserva.getValueAt(fila, 3).toString());
                showReservas();
                showRooms(0);
                ControllerPanelRom.loadTableRooms("1");
            } else {
                JOptionPane.showMessageDialog(this.panelReserva, "Hubo un problema al cambiar el estado del cliente, contacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == panelReserva.reservaBtnNuevaReserva) {
            ModalNewReservation form = new ModalNewReservation(mainFrame,true);
            form.btnModificar.setEnabled(false);
            ControllerNewReservation controllerReservation = new ControllerNewReservation(form, this.mainFrame, this);
            controllerReservation.loadDataFromDataBase();
            controllerReservation.initViewNewReservation();
            return;
        }

        if (e.getSource() == menuTableReserva.itemDetalle) {
            if (row < 0) {
                JOptionPane.showMessageDialog(mainFrame, "Debe seleccionar un registro para continuar.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }
            this.panelReserva.setVisible(false);
            this.panelDetailReserva.setVisible(true);
            controllerDetailReserva = new ControllerDetailReserva(this.panelDetailReserva, mainFrame, panelReserva);
            controllerDetailReserva.setDataDetailReserva(codigoReserva);
            controllerDetailReserva.mostrarConsumos(codigoReserva);
            return;
        }
        
        if (e.getSource() == this.menuTableReserva.itemDescartado) {
            cambiarEstadoReserva("0");
        }
        
        if(e.getSource() == this.menuTableReserva.itemModificar){
            row = this.panelReserva.tablaReserva.getSelectedRow();
            if (row < 0) {
                JOptionPane.showMessageDialog(mainFrame, "Debe seleccionar una reserva.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }
            String idReservaEditar = panelReserva.tablaReserva.getValueAt(row, 0).toString();
            Reservacion reservaResponse = repositoryReserva.findById(idReservaEditar);
            if(reservaResponse == null){
                JOptionPane.showMessageDialog(mainFrame, "La reserva no se encontro,\ncontacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }
            mostarReservaParaEditar(reservaResponse, idReservaEditar);
        }

        if (e.getSource() == panelDetailReserva.btnRegresar) {
            panelDetailReserva.txtIdReserva.setText("");
            controllerDetailReserva = null;
            System.gc();
        }
    }
    
    private void mostarReservaParaEditar(Reservacion rva, String idReservaEditar) {
        ModalNewReservation form = new ModalNewReservation(mainFrame,true);
        form.btnRegistrarReserva.setEnabled(false);
        form.btnModificar.setEnabled(true);
        form.btnBuscarCliente.setVisible(false);
        form.btnNuevoCliente.setVisible(false);
        form.txtCaracteristicas.setVisible(false);
        form.txtTipoHabitacion.setVisible(false);
        form.txtTelefono.setVisible(false);
        form.txtEmail.setVisible(false);
        form.lblEmail.setVisible(false);
        form.lblTelefono.setVisible(false);
        form.lblTipoHab.setVisible(false);
        form.lblCarcteristicas.setVisible(false);
        ControllerNewReservation controllerReservation = new ControllerNewReservation(form, this.mainFrame, this);
        controllerReservation.loadDataFromDataBase();
        cargarDatosParaModificarReserva(rva, form, idReservaEditar);
        form.setTitle("Modificar Reservación");
        controllerReservation.initViewNewReservation();
    }
    
    private void cargarDatosParaModificarReserva(Reservacion rva, ModalNewReservation form, String idReservaEditar){
        form.txtIdReserva.setText(idReservaEditar);
        form.txtNroDoc.setText(rva.getNroDocumento());
        form.txtTipoDoc.setText(rva.getTipoDocumento());
        form.txtCodigoCliente.setText(rva.getCodCliente());
        form.txtNombres.setText(rva.getNombreCliente());
        form.txtNroHabitacion.setText(rva.getAuxNroRoom());
        form.txtNroHabitacionMod.setText(rva.getAuxNroRoom());
        form.txtCodigoHabitacion.setText(rva.getCodHabitacion());
        form.txtFechaHoraIngreso.setText(rva.getFechaIngreso());
        form.txtFechaHoraSalida.setText(rva.getFechaSalida());
        form.txtImporteProductos.setText(rva.getImporteProductos() <= 0.0 ? "0.00" :Util.decimalRound(rva.getImporteProductos()));
        form.lblCantidadHoras.setText(rva.getTiempoReserva().substring(0,2));
        form.lblCantidadMinutos.setText(rva.getTiempoReserva().substring(3,5));
        form.txtCostoAlquiler.setText(Util.decimalRound(rva.getImporteReserva()));
        form.txtObservacion.setText(rva.getObservacion());
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        row = this.panelReserva.tablaReserva.getSelectedRow();
        codigoReserva = null;
        codigoReserva = this.panelReserva.tablaReserva.getValueAt(row, 0).toString();
    }

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}

    private void clearTable(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    private void addComponents(JComponent parent, JComponent child) {
        GridBagConstraints conf = new GridBagConstraints();
        conf.gridx = 0;
        conf.gridy = 0;
        conf.gridwidth = 1;
        conf.gridheight = 1;
        conf.weightx = 1.0;
        conf.weighty = 1.0;
        conf.anchor = GridBagConstraints.CENTER;
        conf.fill = GridBagConstraints.BOTH;

        parent.add(child, conf);
        child.setVisible(true);
    }

    @Override
    public void keyTyped(KeyEvent arg0) {}

    @Override
    public void keyPressed(KeyEvent arg0) {}

    @Override
    public void keyReleased(KeyEvent arg0) {
        if (arg0.getSource() == panelReserva.txtCriteriosPanelReservation) {
            filterReservation();
        }
    }
}