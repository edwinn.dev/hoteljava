package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.database.query.Query;
import com.edmi.model.Company;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.edmi.repository.RepositoryCompany;

public class CompanyRepositoryImp implements RepositoryCompany<Company> {

    private Connection getConnection() throws SQLException {
        return getInstance();
    }

    @Override
    public List<Company> findAll() {
        List<Company> lista = new ArrayList();
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_COMPANY_FIND_ALL)) {
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Company compa = new Company();
                    compa.setIdCompanny(rs.getString("company_id"));
                    compa.setNombre(rs.getString("company_name"));
                    compa.setRuc(rs.getString("company_ruc"));
                    compa.setDireccion(rs.getString("company_address"));
                    compa.setCiudad(rs.getString("company_city"));
                    compa.setPais(rs.getString("company_country"));
                    lista.add(compa);
                }
            }
        } catch (SQLException e) {
            Logger.getLogger(CompanyRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }
    
    @Override
    public Company getCompany(){
        Company company = new Company();
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_COMPANY_FIND)) {
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    company.setIdCompanny(rs.getString("company_id"));
                    company.setNombre(rs.getString("company_name"));
                    company.setRuc(rs.getString("company_ruc"));
                    company.setDireccion(rs.getString("company_address"));
                    company.setCiudad(rs.getString("company_city"));
                    company.setPais(rs.getString("company_country"));
                }
            }
        } catch (SQLException e) {
            Logger.getLogger(CompanyRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return company;
    }
    
    @Override
    public String findName() {
        String nombreEmpresa = "";
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_COMPANY_FIND_NAME)) {
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    nombreEmpresa = rs.getString("company_name");
                }
            }
        } catch (SQLException e) {
            Logger.getLogger(CompanyRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return nombreEmpresa;
    }

    @Override
    public int create(Company company) {
        int rs = 0;
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_COMPANY_CREATE)) {
            ps.setString(1, company.getIdCompanny());
            ps.setString(2, company.getNombre());
            ps.setString(3, company.getRuc());
            ps.setString(4, company.getDireccion());
            ps.setString(5, company.getCiudad());
            ps.setString(6, company.getPais());
            rs = ps.executeUpdate();
        } catch (SQLException e) {
            Logger.getLogger(CompanyRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return rs;
    }

    @Override
    public int update(Company company) {
        int op = 0;
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_COMPANY_UPDATE)) {
            ps.setString(1, company.getIdCompanny());
            ps.setString(2, company.getNombre());
            ps.setString(3, company.getRuc());
            ps.setString(4, company.getDireccion());
            ps.setString(5, company.getCiudad());
            ps.setString(6, company.getPais());
            op = ps.executeUpdate();
        } catch (SQLException e) {
            Logger.getLogger(CompanyRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return op;
    }

    @Override
    public int delete() {
        return 0;
    }
}
