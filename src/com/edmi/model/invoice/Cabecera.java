package com.edmi.model.invoice;

public class Cabecera {
    private String tipOperacion;
    private String fecEmision;
    private String horEmision;
    private String fecVencimiento;
    private String codLocalEmisor;
    private String tipDocUsuario;
    private String numDocUsuario;
    private String rznSocialUsuario;
    private String tipMoneda;
    private String sumTotTributos;
    private String sumTotValVenta;
    private String sumPrecioVenta;
    private String sumDescTotal;
    private String sumOtrosCargos;
    private String sumTotalAnticipos;
    private String sumImpVenta;
    private String ublVersionId;
    private String customizationId;

    public String getTipOperacion() {
        return tipOperacion;
    }

    public void setTipOperacion(String tipOperacion) {
        this.tipOperacion = tipOperacion;
    }

    public String getFecEmision() {
        return fecEmision;
    }

    public void setFecEmision(String fecEmision) {
        this.fecEmision = fecEmision;
    }

    public String getHorEmision() {
        return horEmision;
    }

    public void setHorEmision(String horEmision) {
        this.horEmision = horEmision;
    }

    public String getFecVencimiento() {
        return fecVencimiento;
    }

    public void setFecVencimiento(String fecVencimiento) {
        this.fecVencimiento = fecVencimiento;
    }

    public String getCodLocalEmisor() {
        return codLocalEmisor;
    }

    public void setCodLocalEmisor(String codLocalEmisor) {
        this.codLocalEmisor = codLocalEmisor;
    }

    public String getTipDocUsuario() {
        return tipDocUsuario;
    }

    public void setTipDocUsuario(String tipDocUsuario) {
        this.tipDocUsuario = tipDocUsuario;
    }

    public String getNumDocUsuario() {
        return numDocUsuario;
    }

    public void setNumDocUsuario(String numDocUsuario) {
        this.numDocUsuario = numDocUsuario;
    }

    public String getRznSocialUsuario() {
        return rznSocialUsuario;
    }

    public void setRznSocialUsuario(String rznSocialUsuario) {
        this.rznSocialUsuario = rznSocialUsuario;
    }

    public String getTipMoneda() {
        return tipMoneda;
    }

    public void setTipMoneda(String tipMoneda) {
        this.tipMoneda = tipMoneda;
    }

    public String getSumTotTributos() {
        return sumTotTributos;
    }

    public void setSumTotTributos(String sumTotTributos) {
        this.sumTotTributos = sumTotTributos;
    }

    public String getSumTotValVenta() {
        return sumTotValVenta;
    }

    public void setSumTotValVenta(String sumTotValVenta) {
        this.sumTotValVenta = sumTotValVenta;
    }

    public String getSumPrecioVenta() {
        return sumPrecioVenta;
    }

    public void setSumPrecioVenta(String sumPrecioVenta) {
        this.sumPrecioVenta = sumPrecioVenta;
    }

    public String getSumDescTotal() {
        return sumDescTotal;
    }

    public void setSumDescTotal(String sumDescTotal) {
        this.sumDescTotal = sumDescTotal;
    }

    public String getSumOtrosCargos() {
        return sumOtrosCargos;
    }

    public void setSumOtrosCargos(String sumOtrosCargos) {
        this.sumOtrosCargos = sumOtrosCargos;
    }

    public String getSumTotalAnticipos() {
        return sumTotalAnticipos;
    }

    public void setSumTotalAnticipos(String sumTotalAnticipos) {
        this.sumTotalAnticipos = sumTotalAnticipos;
    }

    public String getSumImpVenta() {
        return sumImpVenta;
    }

    public void setSumImpVenta(String sumImpVenta) {
        this.sumImpVenta = sumImpVenta;
    }

    public String getUblVersionId() {
        return ublVersionId;
    }

    public void setUblVersionId(String ublVersionId) {
        this.ublVersionId = ublVersionId;
    }

    public String getCustomizationId() {
        return customizationId;
    }

    public void setCustomizationId(String customizationId) {
        this.customizationId = customizationId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Cabecera{tipOperacion=").append(tipOperacion);
        sb.append(", fecEmision=").append(fecEmision);
        sb.append(", horEmision=").append(horEmision);
        sb.append(", fecVencimiento=").append(fecVencimiento);
        sb.append(", codLocalEmisor=").append(codLocalEmisor);
        sb.append(", tipDocUsuario=").append(tipDocUsuario);
        sb.append(", numDocUsuario=").append(numDocUsuario);
        sb.append(", rznSocialUsuario=").append(rznSocialUsuario);
        sb.append(", tipMoneda=").append(tipMoneda);
        sb.append(", sumTotTributos=").append(sumTotTributos);
        sb.append(", sumTotValVenta=").append(sumTotValVenta);
        sb.append(", sumPrecioVenta=").append(sumPrecioVenta);
        sb.append(", sumDescTotal=").append(sumDescTotal);
        sb.append(", sumOtrosCargos=").append(sumOtrosCargos);
        sb.append(", sumTotalAnticipos=").append(sumTotalAnticipos);
        sb.append(", sumImpVenta=").append(sumImpVenta);
        sb.append(", ublVersionId=").append(ublVersionId);
        sb.append(", customizationId=").append(customizationId);
        sb.append('}');
        return sb.toString();
    }
}
