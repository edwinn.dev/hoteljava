package com.edmi.controller;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.repository.RepositoryCategoryProduct;
import com.edmi.repository.RepositoryProduct;
import com.edmi.repository.imp.CategoriaProductoRepositoryImp;
import com.edmi.repository.imp.ProductRepositoryImp;
import com.edmi.model.CategoriaProducto;
import com.edmi.model.Product;
import com.edmi.util.MessageView;
import com.edmi.util.MiRenderer;
import com.edmi.util.Util;
import com.edmi.view.ModalNewProduct;
import com.edmi.view.panel.PanelReportProduct;
import com.edmi.view.panel.PanelShopping;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControllerNewProduct implements ActionListener{
    private RepositoryCategoryProduct<CategoriaProducto> reporitoryCategoryProduct = null;
    private RepositoryProduct<Product> repositoryProduct = null;
    private List<CategoriaProducto> categories = null;
    private ModalNewProduct modalProduct = null;
        
    public ControllerNewProduct(ModalNewProduct modalNewProduct, PanelShopping panelShopping){
        this.modalProduct = modalNewProduct;
        setListener();
        this.categories = new ArrayList<>();
        reporitoryCategoryProduct = new CategoriaProductoRepositoryImp();
        repositoryProduct = new ProductRepositoryImp();
    }
    
    private void setListener(){
        KeyListenerEvent keyListener = new KeyListenerEvent();
        modalProduct.txtCantidadIngreso.addKeyListener(keyListener);
        modalProduct.txtPrecioCompra.addKeyListener(keyListener);
        modalProduct.txtPrecioVenta.addKeyListener(keyListener);
        
        modalProduct.btnRegistrarProducto.addActionListener(this);
        modalProduct.btnActualizarProducto.addActionListener(this);
    }
    
    public void loadDataFromNewProduct(){
        loadCategory();
        setCodeProduct();
    }
    
    private void loadCategory(){
        categories = reporitoryCategoryProduct.findAll();
        modalProduct.cbxCategoria.removeAllItems();
        modalProduct.cbxCategoria.addItem("--SELECCIONAR--");
        categories.forEach( c ->{
            modalProduct.cbxCategoria.addItem(c.getCodCategoria().concat("    ").concat(c.getNombreCategoria()));
        });
    }
    
    public void loadDataFromUpdateProduct(String idProduct){
        loadCategory();
        Product product = repositoryProduct.findById(idProduct);
        this.modalProduct.txtCodigoProducto.setText(product.getCodProducto());
        this.modalProduct.txtNombreProducto.setText(product.getNombreProducto());
        this.modalProduct.txtCantidadIngreso.setText(String.valueOf(product.getCantidadStock()));
        this.modalProduct.txtPrecioCompra.setText(String.valueOf(product.getPrecioCompra()));
        this.modalProduct.txtPrecioVenta.setText(String.valueOf(product.getPrecioVenta()));
        this.modalProduct.cbxUnidaMedida.setSelectedItem(product.getCodUnidadMedida().concat("   ").concat(product.getNombreUnidadMedida()));
        this.modalProduct.cbxCategoria.setSelectedItem(product.getCodCategoria().concat("   ").concat(product.getNombreCategoria()));
        this.modalProduct.chooserFechaVenc.setDate(new Date());
        this.modalProduct.txtDescripcion.setText(product.getDescripcion());
        this.modalProduct.txtMarca.setText(product.getMarca());
        this.modalProduct.cbxCategoria.setSelectedItem(product.getCodCategoria().concat("    ").concat(product.getNombreCategoria()));
        this.modalProduct.cbxUnidaMedida.setSelectedIndex(1);
    }
    
    private void setCodeProduct(){
        String codProduct = createCodeProduct();
        modalProduct.txtCodigoProducto.setText(codProduct);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == modalProduct.btnRegistrarProducto){
            registrarProducto(); return;
        }
        
        if(e.getSource() == modalProduct.btnActualizarProducto){
            updateProduct();
        }
    }
    
    private void updateProduct(){
        String codProducto = modalProduct.txtCodigoProducto.getText().trim();
        if(isFieldsInvalid()){
            JOptionPane.showMessageDialog(modalProduct, MessageView.MODAL_PRODUCT_VALIDATE); return;
        }
        
        if(modalProduct.cbxCategoria.getSelectedIndex() == 0){
            JOptionPane.showMessageDialog(modalProduct, "Seleccione una categoria para el producto.", SYS_MSG, JOptionPane.ERROR_MESSAGE); return;
        }
        
        if(modalProduct.cbxUnidaMedida.getSelectedIndex() == 0){
            JOptionPane.showMessageDialog(modalProduct, "Seleccione una Unidad de Medida para el producto.", SYS_MSG, JOptionPane.ERROR_MESSAGE); return;
        }
        
        int option = JOptionPane.showConfirmDialog(modalProduct, "¿Está seguro de actualizar el producto?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if(option == JOptionPane.YES_OPTION){
            Product p = new Product();
            String codCategoria = modalProduct.cbxCategoria.getSelectedItem().toString().substring(0,9).trim().toUpperCase();
            String codUnidad = modalProduct.cbxUnidaMedida.getSelectedItem().toString().substring(0,3).trim().toUpperCase();
            String codIGV = modalProduct.txtCodAfectacion.getText().trim();
            String marca = modalProduct.txtMarca.getText().trim();

            if(codCategoria == null){
                p.setCodCategoria("CAT-0000");
            }else{
                p.setCodCategoria(codCategoria);
            }

            p.setCodProducto(codProducto);
            p.setCodUnidadMedida((codUnidad.length() > 3) ? "NIU" : codUnidad);
            p.setNombreProducto(modalProduct.txtNombreProducto.getText());
            p.setCantidadStock(Double.parseDouble(modalProduct.txtCantidadIngreso.getText()));
            p.setPrecioCompra(Double.parseDouble(modalProduct.txtPrecioCompra.getText()));
            p.setPrecioVenta(Double.parseDouble(modalProduct.txtPrecioVenta.getText()));
            p.setTipoAfectacionIgv(modalProduct.txtCodAfectacion.getText().trim());
            p.setFechaVenc(modalProduct.chooserFechaVenc.getDate() == null ? Util.getDateTimeFormat(new Date()) : Util.getDateTimeFormat(modalProduct.chooserFechaVenc.getDate()));
            p.setFechaReg(Util.getDateTimeFormat(new Date()));
            //p.setCantidadSalida(0.0D);
            p.setEstado("1");
            p.setMontoIgv(codIGV.equals("1000") ? 18.00D : 0.0D);
            p.setTributoIcbper(modalProduct.rbtnOk.isSelected() ? "ICBPER" : "-");
            p.setMontoIcbper(modalProduct.rbtnOk.isSelected() ? Double.parseDouble(modalProduct.txtMontoIcbper.getText().trim()) : 0.0D);
            p.setMarca(marca == null ? "-" : marca.toUpperCase());
            p.setDescripcion(modalProduct.txtDescripcion.getText().isEmpty() ? "-" : modalProduct.txtDescripcion.getText().toUpperCase());

            int response = repositoryProduct.update(p);

            if(response <= 0){
                JOptionPane.showMessageDialog(modalProduct, MessageView.MODAL_PRODUCT_UPDATE_FAILED,SYS_MSG, JOptionPane.INFORMATION_MESSAGE); 
                return;
            }
            JOptionPane.showMessageDialog(modalProduct, MessageView.MODAL_PRODUCT_UPDATE_OK,SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            clearTextField();
            showRefreshTableProducts();
            PanelReportProduct.getInstance().showRefreshTableProducts();
            modalProduct.dispose();
        }
    }
    
    private void registrarProducto(){
        String codigoProducto = createCodeProduct();
        
        if(isFieldsInvalid()){
            JOptionPane.showMessageDialog(modalProduct, MessageView.MODAL_PRODUCT_VALIDATE); return;
        }
        
        if(modalProduct.cbxCategoria.getSelectedIndex() == 0){
            JOptionPane.showMessageDialog(modalProduct, "Seleccione una categoria \npara el producto.", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE); return;
        }
        
        if(modalProduct.cbxUnidaMedida.getSelectedIndex() == 0){
            JOptionPane.showMessageDialog(modalProduct, "Seleccione una Unidad de \nmedida para el producto.", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE); return;
        }
        
        validateNumberFormat();
        
        int option = JOptionPane.showConfirmDialog(modalProduct, "¿Está seguro de actualizar el producto?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if(option == JOptionPane.YES_OPTION){
            Product p = new Product();
            String codCategoria = modalProduct.cbxCategoria.getSelectedItem().toString().substring(0,9).trim().toUpperCase();
            String codUnidad = modalProduct.cbxUnidaMedida.getSelectedItem().toString().substring(0,3).trim().toUpperCase();
            String codIGV = modalProduct.txtCodAfectacion.getText().trim();
            String marca = modalProduct.txtMarca.getText().trim();

            if(codCategoria == null){
                p.setCodCategoria("CAT-0000");
            }else{
                p.setCodCategoria(codCategoria);
            }

            p.setCodProducto(codigoProducto);
            p.setCodUnidadMedida((codUnidad.length() > 3) ? "NIU" : codUnidad);
            p.setNombreProducto(modalProduct.txtNombreProducto.getText());
            p.setCantidadStock(Double.parseDouble(modalProduct.txtCantidadIngreso.getText()));
            p.setPrecioCompra(Double.parseDouble(modalProduct.txtPrecioCompra.getText()));
            p.setPrecioVenta(Double.parseDouble(modalProduct.txtPrecioVenta.getText()));
            p.setTipoAfectacionIgv(modalProduct.txtCodAfectacion.getText().trim());
            p.setFechaVenc(modalProduct.chooserFechaVenc.getDate() == null ? "-" : Util.getDateTimeFormat(modalProduct.chooserFechaVenc.getDate()));
            p.setFechaReg(Util.getDateTimeFormat(new Date()));
            p.setCantidadSalida(0.0D);
            p.setEstado("1");
            p.setMontoIgv(codIGV.equals("1000") ? 18.00D : 0.0D);
            p.setTributoIcbper(modalProduct.rbtnOk.isSelected() ? "ICBPER" : "-");
            p.setMontoIcbper(modalProduct.rbtnOk.isSelected() ? Double.parseDouble(modalProduct.txtMontoIcbper.getText().trim()) : 0.0D);
            p.setMarca(marca == null ? "-" : marca.toUpperCase());
            p.setDescripcion(modalProduct.txtDescripcion.getText().isEmpty() ? "-" : modalProduct.txtDescripcion.getText().toUpperCase());

            int response = repositoryProduct.create(p);

            if(response <= 0){
                JOptionPane.showMessageDialog(modalProduct, MessageView.MODAL_PRODUCT_INSERT_FAILED,SYS_MSG, JOptionPane.ERROR_MESSAGE); return;
            }
            JOptionPane.showMessageDialog(modalProduct, MessageView.MODAL_PRODUCT_INSERT_OK,SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            clearTextField();
            setCodeProduct();
            modalProduct.dispose();
            showRefreshTableProducts();
            PanelReportProduct.getInstance().showRefreshTableProducts();
        }
    }
    
    private void showRefreshTableProducts(){
        clearTableProducts();
        DefaultTableModel modelProduct = (DefaultTableModel) PanelShopping.tableProducts.getModel();
        List<Product> products = repositoryProduct.findAll();
        products.forEach((p) -> {
            modelProduct.addRow(new Object[]{
                p.getCodProducto(), p.getNombreProducto(), p.getMarca(), p.getNombreCategoria(), p.getCantidadStock(),
                p.getFechaVenc(), p.getFechaReg(), "-", p.getPrecioCompra(), 
                p.getPrecioVenta(),  p.getMontoIgv(), p.getMontoIcbper(), p.getDescripcion()
            });
        });
        PanelShopping.tableProducts.setDefaultRenderer(Object.class, new MiRenderer());
        PanelShopping.tableProducts.setModel(modelProduct);
    }
    
    private void clearTableProducts(){
        DefaultTableModel model = (DefaultTableModel) PanelShopping.tableProducts.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }
    
    private void validateNumberFormat(){
        try{
            Double.parseDouble(modalProduct.txtCantidadIngreso.getText());
            Double.parseDouble(modalProduct.txtPrecioCompra.getText());
            Double.parseDouble(modalProduct.txtPrecioVenta.getText());
        }catch(NumberFormatException e){
            JOptionPane.showMessageDialog(modalProduct, "Los precios son incorrectos", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private String createCodeProduct(){
        List<Product> products = repositoryProduct.findAllCodeProduct();
        
        if(products == null || products.isEmpty()){
            return "PROD-0000000001";
        }
        
        Product product = products.get(products.size() - 1);
        String codeString = product.getCodProducto();
        Integer value = Integer.parseInt(codeString.substring(5));
        
        String codigo = String.format("%10s", (value+1)).replace(" ", "0");
        
        return "PROD-" + codigo;
    }
    
    private boolean isFieldsInvalid(){
        boolean invalid = modalProduct.txtNombreProducto.getText().isEmpty() || modalProduct.txtPrecioCompra.getText().isEmpty() || modalProduct.txtCantidadIngreso.getText().isEmpty()
                           || modalProduct.txtPrecioCompra.getText().isEmpty() || modalProduct.txtPrecioVenta.getText().isEmpty();
        return invalid;
    }
    
    private void clearTextField(){
        Component[] components = modalProduct.panelProducts.getComponents();
        for(Component c : components){
            if(c instanceof JFormattedTextField){
                ((JFormattedTextField) c).setValue("");
            }
        }
        modalProduct.txtDescripcion.setText("");
        modalProduct.chooserFechaVenc.setDate(null);
    }
    
    public class KeyListenerEvent implements KeyListener{
        @Override
        public void keyTyped(KeyEvent e) {
            char caracter = e.getKeyChar();
            if(Character.isLetter(caracter) || caracter == KeyEvent.VK_COMMA){
                e.consume();
                modalProduct.getToolkit().beep();
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
            
        }

        @Override
        public void keyReleased(KeyEvent e) {
            
        }
    }
}
