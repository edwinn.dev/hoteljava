package com.edmi.controller;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.model.Cliente;
import com.edmi.model.Company;
import com.edmi.model.Email;
import com.edmi.repository.imp.ClienteRepositoryImp;
import com.edmi.repository.imp.CompanyRepositoryImp;
import com.edmi.repository.imp.EmailRepository;
import com.edmi.util.EncriptacionPasswordAES;
import com.edmi.view.DialogReportFiles;
import com.edmi.view.FrmConfigEmail;
import com.edmi.view.FrmEnviarEmail;
import com.edmi.view.MainPanelServices;
import com.edmi.view.panel.PanelMainVoucher;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class ControllerEmail implements ActionListener {
    private EmailRepository repository;
    private ClienteRepositoryImp repositoryCliente;
    private FrmConfigEmail frmConfigEmail;
    private FrmEnviarEmail frmEnviarEmail;
    private PanelMainVoucher panelMainVoucher;
    private JFileChooser jfc;

    public ControllerEmail(FrmConfigEmail frmConfigEmail, FrmEnviarEmail frmEnviarEmail, PanelMainVoucher panelMainVoucher) {
        this.frmConfigEmail = frmConfigEmail;
        this.frmEnviarEmail = frmEnviarEmail;
        this.panelMainVoucher = panelMainVoucher;
        this.repository = new EmailRepository();
        this.repositoryCliente = new ClienteRepositoryImp();
        setListeners();
    }

    public void envioAutomaticoCorreo(String rucEmpresa, String tipoComp, String serie, String numero, String nroDocuCliente, MainPanelServices mainFrame) {
        Email e = repository.getConfigEmail();
        Cliente c = repositoryCliente.findByNroDocu(nroDocuCliente);
        Company company = new CompanyRepositoryImp().getCompany();

        String asunto = "";
        switch (tipoComp) {
            case "BOLETA":
                asunto = "Comprobante electrónico de " + company.getNombre() + " Boleta de Venta " + serie + "-" + numero;
                tipoComp = "03";
                break;
            case "FACTURA":
                asunto = "Comprobante electrónico de " + company.getNombre() + " Factura de Venta " + serie + "-" + numero;
                tipoComp = "01";
                break;
            case "CREDITO":
                asunto = "Comprobante electrónico de " + company.getNombre() + " Nota de Crédito " + serie + "-" + numero;
                tipoComp = "07";
                break;
            case "DEBITO":
                asunto = "Comprobante electrónico de " + company.getNombre() + " Nota de Débito " + serie + "-" + numero;
                tipoComp = "08";
        }

        String[] rutas = DialogReportFiles.obtenerRutaArchiReporPdfXml();
        File fPDF = new File(rutas[1] + File.separator + serie + "-" + numero + ".pdf");
        if (!fPDF.exists()) {
            JOptionPane.showMessageDialog(mainFrame, "Comprobante no encontrado. \nCompruebe si el archivo pdf ha sigo guardado en la ruta específica:\n " + rutas[1], SYS_MSG, JOptionPane.INFORMATION_MESSAGE);;
            return;
        }
        
        if (rutas[2] == null) {
            JOptionPane.showMessageDialog(mainFrame, "Debe guardar la ruta para los archivos XML.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        File f = new File(rutas[2] + File.separator + rucEmpresa + "-" + tipoComp + "-" + serie + "-" + numero + ".xml");
        if (!f.exists()) {
            JOptionPane.showMessageDialog(mainFrame, "El archivo XML no existe, debe de generarlo desde el facturador.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        
        e.setDestino(c.getEmail());
        e.setAsunto(asunto);
        e.setRutaArchivoPDF(rutas[1] + File.separator + serie + "-" + numero + ".pdf");
        e.setRutaArchivoXML(rutas[2] + File.separator + rucEmpresa + "-" + tipoComp + "-" + serie + "-" + numero + ".xml");

        if (confingEmail(e, serie + "-" + numero + ".pdf", serie + "-" + numero + ".xml")) {
            JOptionPane.showMessageDialog(mainFrame, "Envio de correo exitoso.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } else {
//            JOptionPane.showMessageDialog(mainFrame, "Error al enviar el correo, contacte con el adminstrador.", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void enviarCorreo() {
        Email e = null;
        if (this.frmConfigEmail != null) {
            if (isFieldsInvalidTest()) {
                JOptionPane.showMessageDialog(this.frmConfigEmail, "Debe de llenar los datos correspondientes.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            } else {
                e = new Email();
                e.setCorreoEmpresa(this.frmConfigEmail.txtCorreoEmpresa.getText().trim());
                e.setPassword(this.frmConfigEmail.txtConfirmarPassword.getText().trim());
                e.setHost(this.frmConfigEmail.txtHost.getText().trim());
                e.setPuerto(this.frmConfigEmail.txtPuerto.getText().trim());
                e.setDestino(this.frmConfigEmail.txtDestinatario.getText().trim());
                e.setAsunto(this.frmConfigEmail.txtAsunto.getText().trim());
                e.setCc(this.frmConfigEmail.txtCC.getText().trim());
                e.setCco(this.frmConfigEmail.txtCCO.getText().trim());
                e.setMensaje(this.frmConfigEmail.txtCuerpo.getText().trim());

                if (confingEmail(e, "Sin contenido archivo pdf", "Sin contenido archivo xml")) {
                    JOptionPane.showMessageDialog(this.frmConfigEmail, "Envio de correo exitoso.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                } else {
//                    JOptionPane.showMessageDialog(this.frmConfigEmail, "Error al enviar el correo, contacte con el adminstrador.", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else if (this.frmEnviarEmail != null) {
            e = repository.getConfigEmail();
            e.setDestino(this.frmEnviarEmail.txtDestinatario.getText().trim());
            e.setAsunto(this.frmEnviarEmail.txtAsunto.getText().trim());
            e.setCc(this.frmEnviarEmail.txtCC.getText().trim());
            e.setCco(this.frmEnviarEmail.txtCCO.getText().trim());
            e.setMensaje(this.frmEnviarEmail.txtCuerpo.getText().trim());
            e.setRutaArchivoPDF(this.frmEnviarEmail.txtRutaArchivo.getText().trim());

            if (confingEmail(e, "Sin contenido archivo pdf", "Sin contenido archivo xml")) {
                JOptionPane.showMessageDialog(this.frmEnviarEmail, "Envio de correo exitoso.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            } else {
//                JOptionPane.showMessageDialog(this.frmEnviarEmail, "Error al enviar el correo, contacte con el adminstrador.", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void obtenerRutaArchivo() {
        jfc = new JFileChooser();
        jfc.setDialogTitle("Seleccione un archivo");
        jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        if (jfc.showOpenDialog(this.frmEnviarEmail) == JFileChooser.APPROVE_OPTION) {
            this.frmEnviarEmail.txtRutaArchivo.setText(jfc.getSelectedFile().toString());
        }
    }

    private boolean confingEmail(Email e, String nombreArchivoPFF, String nombreArchivoXML) {
        int flagIndicador;
        String msg, correo = e.getCorreoEmpresa();
        String contrase = EncriptacionPasswordAES.desencriptar(e.getPassword());
        String smtp = e.getHost();
        String puerto = e.getPuerto();
        final String Username = correo;
        final String PassWord = contrase;
        String Asunto = e.getAsunto();
        Properties props = new Properties();
        props.put("mail.smtp.host", smtp);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", puerto);
        props.put("mail.smtp.user", Username);
        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(Username, PassWord);
            }
        });
        try {
            MimeBodyPart parteTexto = new MimeBodyPart();
            parteTexto.setContent(e.getMensaje(), "text/html; charset=utf-8");
            if (this.frmConfigEmail != null) {
                MimeMultipart multiParte = new MimeMultipart(); //Generacion del cuerpo.
                multiParte.addBodyPart((BodyPart) parteTexto);
                MimeMessage message = new MimeMessage(session);
                message.setFrom((Address) new InternetAddress(Username));
                message.addRecipient(Message.RecipientType.TO, (Address) new InternetAddress(e.getDestino()));//correo 1
                message.addRecipient(Message.RecipientType.TO, (Address) new InternetAddress(e.getDestino()));//correo 2
                if (!e.getCc().isEmpty()) {
                    message.addRecipient(Message.RecipientType.CC, (Address) new InternetAddress(e.getCc()));//con copia a
                }
                if (!e.getCco().isEmpty()) {
                    message.addRecipient(Message.RecipientType.BCC, (Address) new InternetAddress(e.getCco()));//va en copia pero no se visualiza
                }
                message.setSubject(Asunto);
                message.setContent((Multipart) multiParte);
                Transport.send((Message) message);
                flagIndicador = 0;
                msg = "ok";
            } else {
                MimeBodyPart parteArchivoPDF = new MimeBodyPart();
                parteArchivoPDF.setDataHandler(new DataHandler(new FileDataSource(e.getRutaArchivoPDF())));
                parteArchivoPDF.setFileName(nombreArchivoPFF);
                MimeBodyPart parteArchivoXML = new MimeBodyPart();
                parteArchivoXML.setDataHandler(new DataHandler(new FileDataSource(e.getRutaArchivoXML())));
                parteArchivoXML.setFileName(nombreArchivoXML);
                MimeMultipart multiParte = new MimeMultipart(); //Generacion del cuerpo.
                multiParte.addBodyPart((BodyPart) parteTexto);
                multiParte.addBodyPart((BodyPart) parteArchivoPDF);
                multiParte.addBodyPart((BodyPart) parteArchivoXML);
                MimeMessage message = new MimeMessage(session);
                message.setFrom((Address) new InternetAddress(Username));
                message.addRecipient(Message.RecipientType.TO, (Address) new InternetAddress(e.getDestino()));//correo 1
                message.addRecipient(Message.RecipientType.TO, (Address) new InternetAddress(e.getDestino()));//correo 2
                if (e.getCc() != null) {
                    message.addRecipient(Message.RecipientType.CC, (Address) new InternetAddress(e.getCc()));//con copia a
                }
                if (e.getCco() != null) {
                    message.addRecipient(Message.RecipientType.BCC, (Address) new InternetAddress(e.getCco()));//va en copia pero no se visualiza
                }
                message.setSubject(Asunto);
                message.setContent((Multipart) multiParte);
                Transport.send((Message) message);
                flagIndicador = 0;
                msg = "ok";
            }
        } catch (MessagingException ex) {
            if (ex.getMessage().equalsIgnoreCase("failed to connect")) {
                JOptionPane.showMessageDialog(this.panelMainVoucher, "Conexión de correo fallida, compruebe los siguientes puntos:\n - Correo remitente no guardado en la base de datos."
                        + "\n - El correo o contraseña son incorrectos."
                        + "\n - El host o el puerto es el incorrecto.", SYS_MSG, JOptionPane.WARNING_MESSAGE);
            }
            Logger.getLogger(ControllerEmail.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    private boolean isFieldsInvalidConfigEmail() {
        boolean invalid = this.frmConfigEmail.txtCorreoEmpresa.getText().isEmpty() || this.frmConfigEmail.txtPassword.getText().isEmpty() || this.frmConfigEmail.txtConfirmarPassword.getText().isEmpty()
                || this.frmConfigEmail.txtHost.getText().isEmpty() || this.frmConfigEmail.txtPuerto.getText().isEmpty();
        return invalid;
    }

    private boolean isFieldsInvalidTest() {
        boolean invalid = this.frmConfigEmail.txtCorreoEmpresa.getText().isEmpty() || this.frmConfigEmail.txtPassword.getText().isEmpty() || this.frmConfigEmail.txtConfirmarPassword.getText().isEmpty()
                || this.frmConfigEmail.txtHost.getText().isEmpty() || this.frmConfigEmail.txtPuerto.getText().isEmpty() || this.frmConfigEmail.txtDestinatario.getText().isEmpty()
                || this.frmConfigEmail.txtCuerpo.getText().isEmpty();
        return invalid;
    }

    private void saveConfigEmail() {
        if (isFieldsInvalidConfigEmail()) {
            JOptionPane.showMessageDialog(this.frmConfigEmail, "Debe llenar los datos correspondientes.", SYS_MSG, JOptionPane.WARNING_MESSAGE);
            return;
        }

        if (!this.frmConfigEmail.txtPassword.getText().trim().equals(this.frmConfigEmail.txtConfirmarPassword.getText().trim())) {
            JOptionPane.showMessageDialog(this.frmConfigEmail, "Las contraseñas no coinciden, vuelve a intentarlo.", SYS_MSG, JOptionPane.WARNING_MESSAGE);
            return;
        }

        Email e = new Email();
        e.setCorreoEmpresa(this.frmConfigEmail.txtCorreoEmpresa.getText().trim());
        e.setPassword(EncriptacionPasswordAES.encriptar(this.frmConfigEmail.txtConfirmarPassword.getText().trim()));
        e.setHost(this.frmConfigEmail.txtHost.getText().trim());
        e.setPuerto(this.frmConfigEmail.txtPuerto.getText().trim());
        e.setAsunto(this.frmConfigEmail.txtAsunto.getText().trim());
        e.setMensaje(this.frmConfigEmail.txtCuerpo.getText().trim());
        int op = repository.updateConfgEmail(e);

        if (op <= 0) {
            int op2 = repository.insertConfigEmail(e);
            if (op2 <= 0) {
                JOptionPane.showMessageDialog(this.frmConfigEmail, "Error al guardar los datos, contacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }
        }

        JOptionPane.showMessageDialog(this.frmConfigEmail, "Datos guardados.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
    }

    public void mostrarDatosConfigEmail(String nroDocu) {
        Email e = repository.getConfigEmail();
        if (this.frmConfigEmail != null) {
            this.frmConfigEmail.txtCorreoEmpresa.setText(e.getCorreoEmpresa());
            this.frmConfigEmail.txtPassword.setText(EncriptacionPasswordAES.desencriptar(e.getPassword()));
            this.frmConfigEmail.txtConfirmarPassword.setText(EncriptacionPasswordAES.desencriptar(e.getPassword()));
            this.frmConfigEmail.txtAsunto.setText(e.getAsunto());
            this.frmConfigEmail.txtCuerpo.setText(e.getMensaje());

            if (e.getHost() != null && e.getPuerto() != null) {
                this.frmConfigEmail.txtHost.setText(e.getHost());
                this.frmConfigEmail.txtPuerto.setText(e.getPuerto());
                int op = e.getHost().equals("smtp.gmail.com") ? 0 : 1;
                this.frmConfigEmail.cboHost.setSelectedIndex(op);
            }

        } else if (this.frmEnviarEmail != null) {
            Cliente c = repositoryCliente.findByNroDocu(nroDocu);
            this.frmEnviarEmail.txtDestinatario.setText(c.getEmail());
            this.frmEnviarEmail.txtAsunto.setText(e.getAsunto());
            this.frmEnviarEmail.txtCuerpo.setText(e.getMensaje());
        }
    }

    private void setListeners() {
        if (this.frmConfigEmail != null) {
            this.frmConfigEmail.btnGuardarConfig.addActionListener(this);
            this.frmConfigEmail.btnTest.addActionListener(this);
        } else if (this.frmEnviarEmail != null) {
            this.frmEnviarEmail.btnEnviarCorreo.addActionListener(this);
            this.frmEnviarEmail.btnRutaArchivo.addActionListener(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        if (this.frmConfigEmail != null) {
            if (arg0.getSource() == this.frmConfigEmail.btnGuardarConfig) {
                saveConfigEmail();
            } else if (arg0.getSource() == this.frmConfigEmail.btnTest) {
                enviarCorreo();
            }
        } else if (arg0.getSource() == this.frmEnviarEmail.btnEnviarCorreo) {
            enviarCorreo();
        } else if (arg0.getSource() == this.frmEnviarEmail.btnRutaArchivo) {
            obtenerRutaArchivo();
        }
    }
}