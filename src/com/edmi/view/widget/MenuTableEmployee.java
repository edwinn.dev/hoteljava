package com.edmi.view.widget;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

public class MenuTableEmployee {
    private static MenuTableEmployee instance;
    private JPopupMenu menu;
    private JMenu cambiarEstado;
    public JMenuItem itemDescartado;
    
    public static MenuTableEmployee getInstance() {
        return instance == null ? new MenuTableEmployee(): instance; 
    }
    
    public void initMenuTableClient(JTable tabla) {
        menu = new JPopupMenu();
        cambiarEstado = new JMenu("Acción");
        itemDescartado = new JMenuItem("Eliminar");
        
        menu.add(cambiarEstado);
        cambiarEstado.add(itemDescartado);
        
        tabla.setComponentPopupMenu(menu);
    }
}
