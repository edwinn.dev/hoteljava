package com.edmi.model;

public class Consumo {
    private Integer codConsumo;
    private Integer codReserva;
    private String codProducto;
    private Double cantidad;
    private Double precioUnitario;
    private Double importeTotal;
    private String gratuito;
    private String estado;
    private String nombreProducto;
    private String unidadMedida;
    private Double tributoIcbper;
    private Double tributoIsc;
    private Double tributoIgv;
    private Double descuento;
    private String fechaVenta;

    public Consumo() {
        
    }

    public Integer getCodConsumo() {
        return codConsumo;
    }

    public void setCodConsumo(Integer codConsumo) {
        this.codConsumo = codConsumo;
    }

    public Integer getCodReserva() {
        return codReserva;
    }

    public void setCodReserva(Integer codReserva) {
        this.codReserva = codReserva;
    }

    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(Double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Double getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(Double importeTotal) {
        this.importeTotal = importeTotal;
    }

    public String getGratuito() {
        return gratuito;
    }

    public void setGratuito(String gratuito) {
        this.gratuito = gratuito;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado.toUpperCase();
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto.toUpperCase();
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida.toUpperCase();
    }

    public Double getTributoIcbper() {
        return tributoIcbper;
    }

    public void setTributoIcbper(Double tributoIcbper) {
        this.tributoIcbper = tributoIcbper;
    }

    public Double getTributoIsc() {
        return tributoIsc;
    }

    public void setTributoIsc(Double tributoIsc) {
        this.tributoIsc = tributoIsc;
    }

    public Double getTributoIgv() {
        return tributoIgv;
    }

    public void setTributoIgv(Double tributoIgv) {
        this.tributoIgv = tributoIgv;
    }

    public Double getDescuento() {
        return descuento;
    }

    public void setDescuento(Double descuento) {
        this.descuento = descuento;
    }

    public String getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(String fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Consumo{codConsumo=").append(codConsumo);
        sb.append(", codReserva=").append(codReserva);
        sb.append(", codProducto=").append(codProducto);
        sb.append(", cantidad=").append(cantidad);
        sb.append(", precioUnitario=").append(precioUnitario);
        sb.append(", importeTotal=").append(importeTotal);
        sb.append(", gratuito=").append(gratuito);
        sb.append(", estado=").append(estado);
        sb.append(", nombreProducto=").append(nombreProducto);
        sb.append(", unidadMedida=").append(unidadMedida);
        sb.append(", tributoIcbper=").append(tributoIcbper);
        sb.append(", tributoIsc=").append(tributoIsc);
        sb.append(", tributoIgv=").append(tributoIgv);
        sb.append(", descuento=").append(descuento);
        sb.append('}');
        return sb.toString();
    }
}
