package com.edmi.view.widget;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

public class MenuTableSupplier {
    private static MenuTableSupplier instance;
    
    private JPopupMenu menu;
    private JMenu cambiarEstado;
    public JMenuItem itemDescartado;
    
    public static MenuTableSupplier getInstance() {
        return instance == null ? new MenuTableSupplier() : instance; 
    }
    
    public void initMenuTableClient(JTable tabla) {
        menu = new JPopupMenu();
        cambiarEstado = new JMenu("Acción");
        itemDescartado = new JMenuItem("Eliminar");
        
        menu.add(cambiarEstado);
        cambiarEstado.add(itemDescartado);
        
        tabla.setComponentPopupMenu(menu);
    }
}