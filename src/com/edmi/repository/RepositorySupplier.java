package com.edmi.repository;

import java.util.List;

public interface RepositorySupplier<T> {
    List<T> findAllEstado(String estado);
    List<T> findAll();
    List<T> findAllWhere(String condition, String value, String estado);
    T findById(Object id);
    String findAllTipeDocu(Object tipoDocu);
    List<String> findAllDocuDescripciones();
    int state(Object id, Object actividad);
    int create(T supplier);
    int update(T supplier);
    int delete(Object id);    
}
