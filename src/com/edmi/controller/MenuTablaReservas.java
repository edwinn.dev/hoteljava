package com.edmi.controller;

import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

public class MenuTablaReservas {
    private static MenuTablaReservas instance;
    
    private MenuTablaReservas(){}
    
    public static MenuTablaReservas getInstance(){
        if(instance == null){
            instance = new MenuTablaReservas();
        }
        return instance;
    }
    
    public void initMenuReserva(JTable table){
        menu = new JPopupMenu();
        this.cambiarEstado = new JMenu("Acción");
        this.itemDetalle = new JMenuItem("Ver detalle", new ImageIcon(getClass().getResource("/com/edmi/images/icon_detail.png")));
        this.itemDescartado = new JMenuItem("Eliminar");
        this.itemModificar = new JMenuItem("Modificar");
        
        this.menu.add(this.itemDetalle);
        this.cambiarEstado.add(this.itemDescartado);
        this.menu.add(this.cambiarEstado);
        this.menu.add(this.itemModificar);
        table.setComponentPopupMenu(menu);
    }
    
    private JPopupMenu menu;
    private JMenu cambiarEstado;
    public JMenuItem itemDetalle;
    public JMenuItem itemDescartado;
    public JMenuItem itemModificar;
}