package com.edmi.controller.panel;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.ControllerEmail;
import com.edmi.controller.menu.ControllerTableFacturacion;
import com.edmi.database.DocumentSQLite;
import com.edmi.model.CabeceraModel;
import com.edmi.model.Company;
import com.edmi.model.NotaCD;
import com.edmi.model.invoice.ComunicacionBaja;
import com.edmi.model.invoice.ResumenDiario;
import com.edmi.repository.imp.CabeceraRepositoryImp;
import com.edmi.repository.imp.CompanyRepositoryImp;
import com.edmi.repository.imp.DocumetSQLiteI;
import com.edmi.repository.imp.RepositoryComunicacionBaja;
import com.edmi.repository.imp.RepositoryResumen;
import com.edmi.util.Constant;
import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.util.UtilTable;
import com.edmi.view.MainPanelServices;
import com.edmi.view.panel.PanelMainVoucher;
import com.edmi.view.panel.PanelVoucherTicket;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public final class ControllerFacura implements ActionListener, KeyListener {
    private MainPanelServices mainFrame = null;
    private static PanelMainVoucher mainPanelVoucher = null;
    private static PanelVoucherTicket panelVoucherTicket = null;
    private static List<CabeceraModel> listaCabecera = null;
    private static List<NotaCD> listaCD = null;
    private static DefaultTableModel dtmVoucher;
    private static CabeceraRepositoryImp cabeceraRepositoryImp;
    private static RepositoryResumen resositoryResumen;
    private static RepositoryComunicacionBaja resositoryComunicaBaja;
    private static boolean value = false;
    private static boolean valueCD = false;

    public ControllerFacura(MainPanelServices mainFrame, int option, PanelVoucherTicket panelVoucherTicket, PanelMainVoucher mainPanelVoucher) {
        this.mainFrame = mainFrame;
        ControllerFacura.mainPanelVoucher = mainPanelVoucher;
        ControllerFacura.panelVoucherTicket = panelVoucherTicket;
        cabeceraRepositoryImp = new CabeceraRepositoryImp();
        resositoryResumen = new RepositoryResumen();
        resositoryComunicaBaja = new RepositoryComunicacionBaja();
        dtmVoucher = (DefaultTableModel) PanelMainVoucher.tableVouchers.getModel();

        new ControllerTableFacturacion(this.mainFrame).setListener();

        this.mainFrame.panelParentVoucher.setLayout(new GridBagLayout());

        addComponents(this.mainFrame.panelParentVoucher, mainPanelVoucher);
        addComponents(this.mainFrame.panelParentVoucher, panelVoucherTicket);

        ControllerFacura.loadTableVouchers();
        ControllerFacura.loadTableNotasCD();
        ControllerFacura.loadTableResumenDiario();
        ControllerFacura.loadTableComunicacionBaja();
        if (option < 0) {
            ControllerFacura.mainPanelVoucher.setVisible(true);
            ControllerFacura.panelVoucherTicket.setVisible(false);
        } else {
            ControllerFacura.mainPanelVoucher.setVisible(false);
            ControllerFacura.panelVoucherTicket.setVisible(true);
        }
        setListener();
    }

    public static void setVisiblePanels() {
        mainPanelVoucher.setVisible(false);
        panelVoucherTicket.setVisible(true);
    }

    private void setListener() {
        panelVoucherTicket.btnBoletaRegresar.addActionListener(this);
        mainPanelVoucher.txtFiltroBoletasFacturas.addKeyListener(this);
        mainPanelVoucher.txtFiltroNotas.addKeyListener(this);
        mainPanelVoucher.btnEnviarCorreoBotelasFacturas.addActionListener(this);
        mainPanelVoucher.btnEnviarCorreoNotas.addActionListener(this);
    }

    public static void loadTableVouchers() {
        UtilTable.clearTable(PanelMainVoucher.tableVouchers);
        if (value) {
            for (CabeceraModel h : listaCabecera) {
                getStatusVoucher(h.getSerie().concat("-").concat(h.getNumero()));
                dtmVoucher.addRow(new Object[]{
                    h.getIdCabecera(), h.getAuxNombreCliente().replace("-", ""), h.getAuxNumDoc(), h.getTipoMoneda(), h.getImporteVenta(),
                    h.getImporteVenta(), h.getFechaEmision().concat(" ").concat(h.getHoraEmision()), h.getTipoComprobante().equals("01") ? "FACTURA" : "BOLETA",
                    h.getSerie(), h.getNumero(), h.getAuxEstado(), h.getSituacionSunat()
                });
            }
        } else {
            List<CabeceraModel> headers = cabeceraRepositoryImp.findAllCabeceraFacbo();
            UtilTable.clearTable(PanelMainVoucher.tableVouchers);
            for (CabeceraModel h : headers) {
                getStatusVoucher(h.getSerie().concat("-").concat(h.getNumero()));
                dtmVoucher.addRow(new Object[]{
                    h.getIdCabecera(), h.getAuxNombreCliente().replace("-", ""), h.getAuxNumDoc(), h.getTipoMoneda(), h.getImporteVenta(),
                    h.getImporteVenta(), h.getFechaEmision().concat(" ").concat(h.getHoraEmision()), h.getTipoComprobante().equals("01") ? "FACTURA" : "BOLETA",
                    h.getSerie(), h.getNumero(), h.getAuxEstado(), h.getSituacionSunat()
                });
            }
        }
        PanelMainVoucher.tableVouchers.setModel(dtmVoucher);
    }

    public static void loadTableNotasCD() {
        UtilTable.clearTable(PanelMainVoucher.tblNotasCD);
        List<NotaCD> headers = cabeceraRepositoryImp.findAllNotaCD();
        DefaultTableModel model = (DefaultTableModel) PanelMainVoucher.tblNotasCD.getModel();
        if (valueCD) {
            for (NotaCD n : listaCD) {
                getStatusCD(n.getCod_cabecera());
                model.addRow(new Object[]{
                    n.getCod_cabecera(), n.getTipo_nota().equals("07") ? "CREDITO" : "DEBITO", n.getDocAfectado(), n.getNroDocuCliente(), n.getFecha_emision(), n.getHora_emision(),
                    n.getDescrip_motivo(), n.getSituacionSunat()
                });
            }
        } else {
            for (NotaCD n : headers) {
                getStatusCD(n.getCod_cabecera());
                model.addRow(new Object[]{
                    n.getCod_cabecera(), n.getTipo_nota().equals("07") ? "CREDITO" : "DEBITO", n.getDocAfectado(), n.getNroDocuCliente(), n.getFecha_emision(), n.getHora_emision(),
                    n.getDescrip_motivo(), n.getSituacionSunat()
                });
            }
        }
        PanelMainVoucher.tblNotasCD.setModel(model);
    }

    public static void loadTableResumenDiario() {
        UtilTable.clearTable(PanelMainVoucher.tblResumen);
        List<ResumenDiario> headers = resositoryResumen.findAll();
        DefaultTableModel model = (DefaultTableModel) PanelMainVoucher.tblResumen.getModel();
        for (ResumenDiario r : headers) {
            getStatusResumenDiario(r.getCodResumen());
            model.addRow(new Object[]{
                r.getCodResumen(), r.getTipDocResumen().equals("03") ? "BOLETA" : "FACTURA", r.getIdDocResumen(), r.getFecEmision(), r.getFecResumen(),
                r.getSituacionSunat()
            });
        }
        PanelMainVoucher.tblResumen.setModel(model);
    }

    public static void loadTableComunicacionBaja() {
        UtilTable.clearTable(PanelMainVoucher.tblBajas);
        List<ComunicacionBaja> headers = resositoryComunicaBaja.findAll();
        DefaultTableModel model = (DefaultTableModel) PanelMainVoucher.tblBajas.getModel();
        for (ComunicacionBaja c : headers) {
            getStatusComunicacionBaja(c.getCodBaja());
            model.addRow(new Object[]{
                c.getCodBaja(), c.getTipDocBaja().equals("01") ? "FACTURA" : "BOLETA", c.getNumDocBaja(), c.getFecGeneracion(), c.getFecComunicacion(), c.getDesMotivoBaja(),
                c.getSituacionSunat()
            });
        }
        PanelMainVoucher.tblBajas.setModel(model);
    }

    public static void filterVouchers() {
        int index = mainPanelVoucher.cboCriteriosBoletasFacturas.getSelectedIndex();
        value = true;
        if (index != 0) {
            String valueSend = mainPanelVoucher.txtFiltroBoletasFacturas.getText().trim();
            String condition = null;
            switch (index) {
                case 1:
                    condition = "nro_documento";
                    break;
                case 2:
                    condition = "concat(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno)";
                    break;
                case 3:
                    condition = "nro_documento";
                    break;
                case 4:
                    condition = "fecha_emision";
                    break;
                case 5:
                    condition = "tipo_comprobante";
                    valueSend = "01";
                    break;
                case 6:
                    condition = "tipo_comprobante";
                    valueSend = "03";
            }

            listaCabecera = cabeceraRepositoryImp.findAllWhere(condition, valueSend);
            loadTableVouchers();
            value = false;
        }
    }

    public static void filterNotaCD() {
        int index = mainPanelVoucher.cboCriteriosNotas.getSelectedIndex();
        valueCD = true;
        if (index != 0) {
            String valueSend = mainPanelVoucher.txtFiltroNotas.getText().trim();
            String condition = null;
            switch (index) {
                case 1:
                    condition = "cod_cabecera";
                    break;
                case 2:
                    condition = "cod_cabecera";
                    break;
                case 3:
                    condition = "fecha_emision";
                    break;
                case 4:
                    condition = "tipo_nota";
                    valueSend = "07";
                    break;
                case 5:
                    condition = "tipo_nota";
                    valueSend = "08";
            }

            listaCD = cabeceraRepositoryImp.findAllWhereCD(condition, valueSend);
            loadTableNotasCD();
            valueCD = false;
        }
    }

    private static void getStatusVoucher(String nroDoc) {
        DocumentSQLite docSunat = DocumetSQLiteI.obtenerComprobantesPorNroDoc(nroDoc);
        if (docSunat == null) {
            return;
        }
        String situacion;
        List<CabeceraModel> headers = cabeceraRepositoryImp.findAllCabeceraFacboSerieNumero(nroDoc.substring(0, 4), nroDoc.substring(5));
        for (CabeceraModel h : headers) {
            if (h.getSituacionSunat().equals("Enviado y Aceptado SUNAT")) {
                return;
            } else {
                switch (docSunat.getSituacion()) {
                    case "01":
                        situacion = "Por Generar XML";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(1, situacion, nroDoc.substring(0, 4), nroDoc.substring(5));
                        break;
                    case "02":
                        situacion = "XML Generado";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(1, situacion, nroDoc.substring(0, 4), nroDoc.substring(5));
                        break;
                    case "03":
                        situacion = "Enviado y Aceptado SUNAT";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(1, situacion, nroDoc.substring(0, 4), nroDoc.substring(5));
                        break;
                    case "05":
                        situacion = "Rechazado por SUNAT";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(1, situacion, nroDoc.substring(0, 4), nroDoc.substring(5));
                        break;
                }
            }
        }
    }

    private static void getStatusCD(String nroDoc) {
        DocumentSQLite docSunat = DocumetSQLiteI.obtenerComprobantesPorNroDoc(nroDoc);
        if (docSunat == null) {
            return;
        }
        String situacion;
        List<NotaCD> headers = cabeceraRepositoryImp.findAllWhereCD(nroDoc);
        for (NotaCD h : headers) {
            if (h.getSituacionSunat().equals("Enviado y Aceptado SUNAT")) {
                return;
            } else {
                switch (docSunat.getSituacion()) {
                    case "01":
                        situacion = "Por Generar XML";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(2, situacion, nroDoc, "");
                        break;
                    case "02":
                        situacion = "XML Generado";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(2, situacion, nroDoc, "");
                        break;
                    case "03":
                        situacion = "Enviado y Aceptado SUNAT";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(2, situacion, nroDoc, "");
                        break;
                    case "05":
                        situacion = "Rechazado por SUNAT";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(2, situacion, nroDoc, "");
                        break;
                }
            }
        }
    }
    
    private static void getStatusResumenDiario(String nroDoc) {
        DocumentSQLite docSunat = DocumetSQLiteI.obtenerComprobantesPorNroDoc(nroDoc);
        if (docSunat == null) {
            return;
        }
        String situacion;
        List<ResumenDiario> headers = cabeceraRepositoryImp.findAllResumenDiarioCod(nroDoc);
        for (ResumenDiario h : headers) {
            if (h.getSituacionSunat().equals("Enviado y Aceptado SUNAT")) {
                return;
            } else {
                switch (docSunat.getSituacion()) {
                    case "01":
                        situacion = "Por Generar XML";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(3, situacion, nroDoc, "");
                        break;
                    case "02":
                        situacion = "XML Generado";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(3, situacion, nroDoc, "");
                        break;
                    case "03":
                        situacion = "Enviado y Aceptado SUNAT";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(3, situacion, nroDoc, "");
                        break;
                    case "05":
                        situacion = "Rechazado por SUNAT";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(3, situacion, nroDoc, "");
                        break;
                }
            }
        }
    }
    
    private static void getStatusComunicacionBaja(String nroDoc) {
        DocumentSQLite docSunat = DocumetSQLiteI.obtenerComprobantesPorNroDoc(nroDoc);
        if (docSunat == null) {
            return;
        }
        String situacion;
        List<ComunicacionBaja> headers = cabeceraRepositoryImp.findAllCodBaja(nroDoc);
        for (ComunicacionBaja h : headers) {
            if (h.getSituacionSunat().equals("Enviado y Aceptado SUNAT")) {
                return;
            } else {
                switch (docSunat.getSituacion()) {
                    case "01":
                        situacion = "Por Generar XML";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(4, situacion, nroDoc, "");
                        break;
                    case "02":
                        situacion = "XML Generado";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(4, situacion, nroDoc, "");
                        break;
                    case "03":
                        situacion = "Enviado y Aceptado SUNAT";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(4, situacion, nroDoc, "");
                        break;
                    case "05":
                        situacion = "Rechazado por SUNAT";
                        CabeceraRepositoryImp.acutalizarSituacionSunat(4, situacion, nroDoc, "");
                        break;
                }
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == panelVoucherTicket.btnBoletaRegresar) {
            panelVoucherTicket.txtIdTipoComprobante.setText("");
            panelVoucherTicket.lblTitleVoucher.setText("");
            panelVoucherTicket.txtTipoComprobante.setText("");
            panelVoucherTicket.txtIdTipoDocCliente.setText("");
            panelVoucherTicket.txtNroDocumento.setText("");
            panelVoucherTicket.txtNombreCliente.setText("");
            panelVoucherTicket.txtTipoDoc.setText("");
            panelVoucherTicket.txtImporteTotal.setText("");
            panelVoucherTicket.dateEmit.setDate(new Date());
            ControllerFacura.panelVoucherTicket.setVisible(false);
            mainPanelVoucher.setVisible(true);
            this.mainFrame.tabbedPanel.setSelectedComponent(this.mainFrame.capaReservas);
        }

        if (e.getSource() == mainPanelVoucher.btnEnviarCorreoBotelasFacturas) {
            int row = PanelMainVoucher.tableVouchers.getSelectedRow();
            if (row < 0) {
                JOptionPane.showMessageDialog(mainFrame, "Debe seleccionar un registro.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            String exists = PanelMainVoucher.tableVouchers.getValueAt(row, 11).toString().trim(); 
            if(! exists.equalsIgnoreCase(Constant.ENVIADO_ACEPTADO)) {
                JOptionPane.showMessageDialog(mainFrame, Constant.MSG_NO_ENVIADO, SYS_MSG, JOptionPane.WARNING_MESSAGE);
                return;
            }
            int op = JOptionPane.showConfirmDialog(mainFrame, "¿Desea enviar el comprobante por correo?", SYS_MSG, JOptionPane.YES_NO_OPTION);
            if (op == JOptionPane.YES_OPTION) {
                ControllerEmail control = new ControllerEmail(null, null, mainPanelVoucher);
                String docuCli = PanelMainVoucher.tableVouchers.getValueAt(row, 2).toString();
                String serie = PanelMainVoucher.tableVouchers.getValueAt(row, 8).toString();
                String numero = PanelMainVoucher.tableVouchers.getValueAt(row, 9).toString();
                String tipoComp = PanelMainVoucher.tableVouchers.getValueAt(row, 7).toString();
                Company company = new CompanyRepositoryImp().getCompany();
                control.envioAutomaticoCorreo(company.getRuc(), tipoComp, serie, numero, docuCli, mainFrame);
            }
        }

        if (e.getSource() == mainPanelVoucher.btnEnviarCorreoNotas) {
            int row = PanelMainVoucher.tblNotasCD.getSelectedRow();
            if (row < 0) {
                JOptionPane.showMessageDialog(mainFrame, "Debe seleccionar un registro.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            String exists = PanelMainVoucher.tableVouchers.getValueAt(row, 11).toString().trim(); 
            if(! exists.equalsIgnoreCase(Constant.ENVIADO_ACEPTADO)) {
                JOptionPane.showMessageDialog(mainFrame, Constant.MSG_NO_ENVIADO, SYS_MSG, JOptionPane.WARNING_MESSAGE);
                return;
            }
            int op = JOptionPane.showConfirmDialog(mainFrame, "¿Desea enviar el comprobante por correo?", SYS_MSG, JOptionPane.YES_NO_OPTION);
            if (op == JOptionPane.YES_OPTION) {
                ControllerEmail control = new ControllerEmail(null, null, mainPanelVoucher);
                String docuCli = PanelMainVoucher.tblNotasCD.getValueAt(row, 3).toString();
                String serie = PanelMainVoucher.tblNotasCD.getValueAt(row, 0).toString().substring(0, 4);
                String numero = PanelMainVoucher.tblNotasCD.getValueAt(row, 0).toString().substring(5);
                String tipoComp = PanelMainVoucher.tblNotasCD.getValueAt(row, 1).toString();
                Company company = new CompanyRepositoryImp().getCompany();
                control.envioAutomaticoCorreo(company.getRuc(), tipoComp, serie, numero, docuCli, mainFrame);
            }
        }
    }

    private static void addComponents(JComponent parent, JComponent child) {
        GridBagConstraints conf = new GridBagConstraints();
        conf.gridx = 0;
        conf.gridy = 0;
        conf.gridwidth = 1;
        conf.gridheight = 1;
        conf.weightx = 1.0;
        conf.weighty = 1.0;
        conf.anchor = GridBagConstraints.CENTER;
        conf.fill = GridBagConstraints.BOTH;

        parent.add(child, conf);
        child.setVisible(true);
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {}

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == mainPanelVoucher.txtFiltroBoletasFacturas) {
            filterVouchers();
        }
        if (e.getSource() == mainPanelVoucher.txtFiltroNotas) {
            filterNotaCD();
        }
    }
}