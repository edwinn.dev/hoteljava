package com.edmi.view.widget;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

public class MenuTableFacturacion {
    private static MenuTableFacturacion instance;
    private JPopupMenu menu;
    public JMenuItem itemNotaCredito;
    public JMenuItem itemNotaDebito;
    public JMenuItem itemBaja;
    public JMenuItem itemResumen;
    
    public static MenuTableFacturacion getInstance() {
        return instance == null ? new MenuTableFacturacion(): instance;
    }
    
    public void initMenuTableFacturacion(JTable tabla) {
        menu = new JPopupMenu();
        itemNotaCredito = new JMenuItem("Nota de crédito");
        itemNotaDebito = new JMenuItem("Nota de debito");
        itemBaja = new JMenuItem("Dar de baja");
        itemResumen = new JMenuItem("Emitir resumen diario");
        
        menu.add(itemNotaCredito);
        menu.add(itemNotaDebito);
        menu.add(itemBaja);
        menu.add(itemResumen);
        
        tabla.setComponentPopupMenu(menu);
    }
}
