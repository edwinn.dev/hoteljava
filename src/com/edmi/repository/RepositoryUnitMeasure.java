package com.edmi.repository;

import java.util.List;

public interface RepositoryUnitMeasure<T> {
    List<T> findAll();
    List<T> findAllWhere(String condition, String value);
    T findById(Object id);
    int create(T unitMeasure);
    int update(T unitMeasure);
    int delete(Object id);
}
