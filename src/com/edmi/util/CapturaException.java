package com.edmi.util;

import java.util.Arrays;

public class CapturaException {

    public static String exceptionStacktraceToString(Exception e) {
        return e.getClass() + "::::" + Arrays.toString(e.getStackTrace()).replace(",", "\n");
    }
}
