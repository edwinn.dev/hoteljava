package com.edmi.controller.panel;

import static com.edmi.database.ConnectionDb.getInstance;
import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.invoice.JsonFiles;
import com.edmi.model.CabeceraModel;
import com.edmi.model.Company;
import com.edmi.model.Consumo;
import com.edmi.model.DetalleFacbo;
import com.edmi.model.Reservacion;
import com.edmi.model.invoice.*;
import com.edmi.repository.RepositoryConsumo;
import com.edmi.repository.RepositoryReservation;
import com.edmi.repository.imp.CompanyRepositoryImp;
import com.edmi.repository.imp.ConsumoRepositoryImp;
import com.edmi.repository.imp.CabeceraRepositoryImp;
import com.edmi.repository.imp.ReservaRepositoryImp;
import com.edmi.util.AmountLetters;
import com.edmi.util.CapturaException;
import com.edmi.util.NombresArchivosJSON;
import com.edmi.util.QR;
import com.edmi.util.Util;
import com.edmi.util.UtilTable;
import com.edmi.view.DialogReportFiles;
import com.edmi.view.panel.PanelVoucherTicket;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.apache.log4j.Logger;

public class ControllerVoucherTicket implements ActionListener {
    private PanelVoucherTicket panelVoucherTicket = null;
    private RepositoryConsumo<Consumo> repositoryConsumo = null;
    private RepositoryReservation<Reservacion> repositoryReservation = null;
    private List<Detalle> details = null;
    private List<Consumo> cons = null;
    private List<Tributo> tributes = null;
    private List<Leyenda> leyendas;

    private int item = 1;
    private Integer idReserva;
    private Reservacion reservacion = null;

    private Double mtoBaseImponibleIgv = 0.00D;
    private Double mtoBaseImponibleIna = 0.00D;
    private Double mtoTributoIgv = 0.00D;
    private Double mtoTributoIsc = 0.00D;
    private Double mtoTributoIcbper = 0.00D;

    private DatoPago pago = null;
    private String tipoComprobante = null;
    private String fechaEmision;
    private String cod;

    private static ControllerVoucherTicket instance = null;
    
    private static final Logger LOGGER = Logger.getLogger(ControllerVoucherTicket.class);
    
    public static ControllerVoucherTicket getInstanceVoucher(PanelVoucherTicket panelVoucherTicket) {
        if (instance == null) {
            instance = new ControllerVoucherTicket(panelVoucherTicket);
        }
        return instance;
    }

    private ControllerVoucherTicket(PanelVoucherTicket panelVoucherTicket) {
        this.panelVoucherTicket = panelVoucherTicket;
        repositoryConsumo = new ConsumoRepositoryImp();
        repositoryReservation = new ReservaRepositoryImp();
        setListener();
    }

    private void setListener() {
        this.panelVoucherTicket.btnEmitirBoleta.addActionListener(this);
        this.panelVoucherTicket.btnAgregarProducto.addActionListener(this);
    }

    public void loadDataConsumption(String codRes) {
        DefaultTableModel dtmCons = (DefaultTableModel) panelVoucherTicket.tablaConsumos.getModel();
    
        idReserva = Integer.parseInt(codRes);
        reservacion = repositoryReservation.findById(idReserva);
        cons = repositoryConsumo.findByReservation(idReserva);

        UtilTable.clearTable(panelVoucherTicket.tablaConsumos);

        cons.forEach(c -> {
            dtmCons.addRow(new Object[]{
                item++, c.getCodProducto(), c.getNombreProducto(), c.getUnidadMedida(), c.getCantidad(), c.getPrecioUnitario(),
                "-", c.getTributoIgv(), c.getDescuento(), c.getTributoIsc(), c.getTributoIcbper(), c.getImporteTotal()
            });
        });
        item = cons.size() + 1;
        dtmCons.addRow(new Object[]{
            item++, "RSE", "Reservacion de la habitacion N°".concat(reservacion.getAuxNroRoom()),
            "NIU", 1.00, reservacion.getImporteReserva(), "-", 0.0, 0.0, 0.0, 0.0, reservacion.getImporteReserva()
        });
        panelVoucherTicket.tablaConsumos.setModel(dtmCons);
    }

    public void loadDataPanel(Reservacion rva, String tipoDoc) {
        if (tipoDoc.equalsIgnoreCase("B")) {
            panelVoucherTicket.txtIdTipoComprobante.setText("1");
            panelVoucherTicket.lblTitleVoucher.setText("EMISION DE BOLETA DE VENTA ELECTRONICA");
            panelVoucherTicket.txtTipoComprobante.setText("BOLETA DE VENTA");
        } else {
            panelVoucherTicket.txtIdTipoComprobante.setText("6");
            panelVoucherTicket.lblTitleVoucher.setText("EMISION DE FACTURA ELECTRONICA");
            panelVoucherTicket.txtTipoComprobante.setText("FACTURA ELECTRONICA");
        }
        if (rva != null) {
            panelVoucherTicket.txtIdTipoDocCliente.setText(rva.getCodTipoDoc());
            panelVoucherTicket.txtNroDocumento.setText(rva.getNroDocumento());
            panelVoucherTicket.txtNombreCliente.setText(rva.getNombreCliente());
            panelVoucherTicket.txtTipoDoc.setText(rva.getTipoDocumento());
            panelVoucherTicket.txtImporteTotal.setText(String.valueOf(rva.getImporteReserva() + rva.getImporteProductos()));
            panelVoucherTicket.dateEmit.setDate(new Date());
        }
    }

    private void setDefaultValues() {
        mtoBaseImponibleIgv = 0.00D;
        mtoBaseImponibleIna = 0.00D;
        mtoTributoIgv = 0.00D;
        mtoTributoIsc = 0.00D;
        mtoTributoIcbper = 0.00D;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        LOGGER.info("test logging");
        if (e.getSource() == panelVoucherTicket.btnEmitirBoleta) {
            int op = JOptionPane.showConfirmDialog(panelVoucherTicket, "¿Desea generar el comprobante?", SYS_MSG, JOptionPane.YES_NO_OPTION);
            if (op == JOptionPane.YES_OPTION) {
                String fechaVencimiento;
                String titleVoucher = null;
                String voucherType = panelVoucherTicket.txtIdTipoComprobante.getText().trim().toUpperCase();
                String pathReport = null;

                switch (voucherType) {
                    case "1":
                        pathReport = "src\\com\\edmi\\reports\\ReportVoucher.jasper";
                        titleVoucher = "BOLETA DE VENTA ELECTRONICA";
                        tipoComprobante = "03";
                        break;
                    case "6":
                        pathReport = "src\\com\\edmi\\reports\\ReportVoucher.jasper";
                        titleVoucher = "FACTURA ELECTRONICA";
                        tipoComprobante = "01";
                        break;
                }

                try {
                    fechaEmision = Util.getDateFormat(panelVoucherTicket.dateEmit.getDate());

                    if (panelVoucherTicket.dateExpiration.getDate() != null) {
                        fechaVencimiento = Util.getDateFormat(panelVoucherTicket.dateExpiration.getDate());
                    } else {
                        fechaVencimiento = "";
                    }
                } catch (Exception ex) {
                    LOGGER.error("Error: \n"+CapturaException.exceptionStacktraceToString(ex));
                    return;
                }

                File imagen = new File("design" + File.separator + "logo inicio");
                String[] imagenes = imagen.list();
                String rutaLogo = "";
                for (String i : imagenes) {
                    File archivo = new File(imagen.getAbsoluteFile(), i);
                    rutaLogo = archivo.getAbsolutePath();
                }

                Map<String, Object> parameters = new HashMap();
                parameters.put("id_reserva", this.idReserva);
                parameters.put("tipo_moneda", this.panelVoucherTicket.cbxTipoMoneda.getSelectedItem().toString().substring(5).trim());
                parameters.put("observacion", this.panelVoucherTicket.txtObservacion.getText());
                parameters.put("cliente", this.panelVoucherTicket.txtNombreCliente.getText());
                parameters.put("nro_documento", this.panelVoucherTicket.txtNroDocumento.getText());
                parameters.put("direccion_cliente", "-");
                parameters.put("fecha_emision", fechaEmision);
                parameters.put("fecha_vencimiento", fechaVencimiento);
                parameters.put("p_imagenQR", "src/com/edmi/images/qr/qrhotel.png");
                parameters.put("logoEmpresa", rutaLogo);

                generateVoucher(fechaEmision, fechaVencimiento, tipoComprobante);
                datosQR();

                parameters.put("codigoVoucher", cod);
                parameters.put("subTotalVenta", reservacion.getImporteTotal().toString());
                parameters.put("anticipos", "0.00");
                parameters.put("descuentos", "0.00");
                parameters.put("valorVenta", reservacion.getImporteTotal().toString());
                parameters.put("total_isc", Util.decimalRound(mtoTributoIsc));
                parameters.put("total_igv", Util.decimalRound(mtoTributoIgv));
                parameters.put("total_ICBPER", Util.decimalRound(mtoTributoIcbper));
                parameters.put("otroCargos", "0.00");
                parameters.put("otros_tributos", "0.00");
                parameters.put("importe_total", reservacion.getImporteTotal() + mtoTributoIgv + mtoTributoIsc + mtoTributoIcbper + "");
                String montoLetras = createLegend(panelVoucherTicket.txtImporteTotal.getText().trim());
                parameters.put("monto_letras", montoLetras);

                try {
                    JasperReport report = (JasperReport) JRLoader.loadObjectFromFile(pathReport);
                    JasperPrint jprint = JasperFillManager.fillReport(report, parameters, getInstance());
                    JasperViewer view = new JasperViewer(jprint, false);
                    view.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                    view.setTitle(titleVoucher);
                    view.setVisible(true);
                    String pathReportsPdf = DialogReportFiles.obtenerRutaArchiReporPdfXml()[1];
                    if (pathReportsPdf == null) {
                        JOptionPane.showMessageDialog(panelVoucherTicket, "El documento no se guardó. \nDebe configurar la ruta para almacenar los PDF.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                    } else {
                        JasperExportManager.exportReportToPdfFile(jprint, (pathReportsPdf + File.separator + cod + ".pdf"));
                    }
                } catch (SQLException | JRException ex) {
                    LOGGER.error("Hay error papu \n"+CapturaException.exceptionStacktraceToString(ex));
                }
            }
        }
    }
    
    private void generateVoucher(String fechaEmision, String fechaVencimiento, String tipoComprobante) {
        VoucherModel voucher = new VoucherModel();

        createDetail();
        voucher.setDetalle(details);

        Cabecera cab = createHeader(fechaEmision, fechaVencimiento);
        voucher.setCabecera(cab);

        createLegend(panelVoucherTicket.txtImporteTotal.getText().trim());
        voucher.setLeyendas(leyendas);

        createTribute();
        voucher.setTributos(tributes);

        if (this.tipoComprobante.equals("01")) {
            createInfoPayment();
            voucher.setDatoPago(pago);
        }

        JsonFiles jsonFiles = new JsonFiles();
        CompanyRepositoryImp repositoryCompany = new CompanyRepositoryImp();
        Company c = repositoryCompany.getCompany();
        if (c == null) {
            JOptionPane.showMessageDialog(panelVoucherTicket, "El RUC es requerido.\n\n"
                    + "¡Configure los datos de la empresa!", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (tipoComprobante.equals("01")) {
            cod = NombresArchivosJSON.getNombreFacturaJSON();
            boolean response = jsonFiles.createFileJson(voucher, c.getRuc() + "-" + tipoComprobante + "-" + cod);
            if (response) {
                JOptionPane.showMessageDialog(panelVoucherTicket, "Factura generada.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                setDefaultValues();
            } else {
                JOptionPane.showMessageDialog(panelVoucherTicket, "Error al generar Factura\n"
                        + "contacte con el administrador", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            }
            guardarDatosCabeFacbo(cab, cod);
            ControllerFacura.loadTableVouchers();
        } else {
            cod = NombresArchivosJSON.getNombreBoletaJSON();
            boolean response = jsonFiles.createFileJson(voucher, c.getRuc() + "-" + tipoComprobante + "-" + cod);
            if (response) {
                JOptionPane.showMessageDialog(panelVoucherTicket, "Boleta generada.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                guardarDatosCabeFacbo(cab, cod);
                setDefaultValues();
            } else {
                JOptionPane.showMessageDialog(panelVoucherTicket, "Error al generar Boleta\n"
                        + "contacte con el administrador", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            }
        }
        guardarDatosDetallesCabeFacbo();
        ControllerFacura.loadTableVouchers();
    }

    private Cabecera createHeader(String fechaEmision, String fechaVencimiento) {
        Cabecera cab = new Cabecera();
        cab.setTipOperacion(panelVoucherTicket.cbxTipoOperacion.getSelectedItem().toString().substring(0, 4).trim());
        cab.setFecEmision(fechaEmision);
        cab.setHorEmision(Util.getTimeFormat(new Date()));
        cab.setFecVencimiento(fechaVencimiento);
        cab.setCodLocalEmisor("0000");
        cab.setTipDocUsuario(panelVoucherTicket.txtIdTipoDocCliente.getText());
        cab.setNumDocUsuario(panelVoucherTicket.txtNroDocumento.getText().trim());
        cab.setRznSocialUsuario(panelVoucherTicket.txtNombreCliente.getText());
        cab.setTipMoneda(panelVoucherTicket.cbxTipoMoneda.getSelectedItem().toString().substring(0, 3).trim());
        cab.setSumTotTributos(String.format("%.2f", mtoTributoIgv + mtoTributoIsc + mtoTributoIcbper));
        cab.setSumTotValVenta(String.format("%.2f", reservacion.getImporteTotal()));
        cab.setSumPrecioVenta(String.format("%.2f", reservacion.getImporteTotal() + mtoTributoIgv + mtoTributoIsc + mtoTributoIcbper));
        cab.setSumDescTotal("0.00");
        cab.setSumOtrosCargos("0.00");
        cab.setSumTotalAnticipos("0.00");
        cab.setSumImpVenta(reservacion.getImporteTotal() + mtoTributoIgv + mtoTributoIsc + mtoTributoIcbper + "");
        cab.setUblVersionId("2.1");
        cab.setCustomizationId("2.0");
        return cab;
    }

    private void createDetail() {
        setDefaultValues();
        Detalle dt;
        details = new ArrayList<>();

        dt = new Detalle();
        dt.setCodUnidadMedida("NIU");
        dt.setCtdUnidadItem("1.0");
        dt.setCodProducto("RESERVA-H".concat(reservacion.getAuxNroRoom()));
        dt.setCodProductoSUNAT("-");
        dt.setDesItem("Reservacion de la habitacion N°".concat(reservacion.getAuxNroRoom()));
        dt.setMtoValorUnitario(String.format("%.2f", reservacion.getImporteReserva()));
        dt.setSumTotTributosItem("0.00");

        dt.setCodTriIGV("9998");
        dt.setMtoIgvItem("0.00");
        dt.setMtoBaseIgvItem(String.format("%.2f", reservacion.getImporteReserva()));
        dt.setNomTributoIgvItem("INA");
        dt.setCodTipTributoIgvItem("FRE");
        dt.setTipAfeIGV("30");
        dt.setPorIgvItem("0.00");

        dt.setCodTriISC("-");
        dt.setMtoIscItem("0.00");
        dt.setMtoBaseIscItem("0.00");
        dt.setNomTributoIscItem("-");
        dt.setCodTipTributoIscItem("-");
        dt.setTipSisISC("-");
        dt.setPorIscItem("0.00");

        dt.setCodTriOtro("9999");
        dt.setMtoTriOtroItem("0.00");
        dt.setMtoBaseTriOtroItem("0.00");
        dt.setNomTributoOtroItem("OTROS");
        dt.setCodTipTributoOtroItem("OTH");
        dt.setPorTriOtroItem("0.00");

        dt.setCodTriIcbper("7152");
        dt.setMtoTriIcbperItem("0.00");
        dt.setCtdBolsasTriIcbperItem("0");
        dt.setNomTributoIcbperItem("ICBPER");
        dt.setCodTipTributoIcbperItem("OTH");
        dt.setMtoTriIcbperUnidad("0.00");

        mtoBaseImponibleIna += reservacion.getImporteReserva();

        dt.setMtoPrecioVentaUnitario(String.format("%.2f", reservacion.getImporteReserva()));
        dt.setMtoValorVentaItem(String.format("%.2f", reservacion.getImporteReserva()));
        dt.setMtoValorReferencialUnitario("0.00");
        details.add(dt);

        for (Consumo c : cons) {
            dt = new Detalle();
            dt.setCodUnidadMedida(c.getUnidadMedida());
            dt.setCtdUnidadItem(String.format("%.2f", c.getCantidad()));
            dt.setCodProducto(c.getCodProducto());
            dt.setCodProductoSUNAT("-");
            dt.setDesItem(c.getNombreProducto());
            dt.setMtoValorUnitario(String.format("%.2f", c.getPrecioUnitario()));
            dt.setSumTotTributosItem("0.00");

            dt.setCodTriIGV(c.getTributoIgv() > 0.00D ? "1000" : "9998");
            dt.setMtoIgvItem(c.getTributoIgv() > 0.00D ? String.valueOf(String.format("%.2f", c.getTributoIgv())) : "0.00");
            dt.setMtoBaseIgvItem(String.format("%.2f", c.getImporteTotal()));
            dt.setNomTributoIgvItem(c.getTributoIgv() > 0.00D ? "IGV" : "INA");
            dt.setCodTipTributoIgvItem(c.getTributoIgv() > 0.00D ? "VAT" : "FRE");
            dt.setTipAfeIGV(c.getTributoIgv() > 0.00D ? "10" : "30");
            dt.setPorIgvItem(c.getTributoIgv() > 0.00D ? "18.00" : "0.00");

            dt.setCodTriISC("-");
            dt.setMtoIscItem("0.00");
            dt.setMtoBaseIscItem("0.00");
            dt.setNomTributoIscItem("-");
            dt.setCodTipTributoIscItem("-");
            dt.setTipSisISC("-");
            dt.setPorIscItem("0.00");

            dt.setCodTriOtro("9999");
            dt.setMtoTriOtroItem("0.00");
            dt.setMtoBaseTriOtroItem("0.00");
            dt.setNomTributoOtroItem("OTROS");
            dt.setCodTipTributoOtroItem("OTH");
            dt.setPorTriOtroItem("0.00");

            dt.setCodTriIcbper("7152");
            dt.setMtoTriIcbperItem("0.00");
            dt.setCtdBolsasTriIcbperItem("0");
            dt.setNomTributoIcbperItem("ICBPER");
            dt.setCodTipTributoIcbperItem("OTH");
            dt.setMtoTriIcbperUnidad("0.00");

            if (c.getTributoIgv() > 0.00D) {
                mtoBaseImponibleIgv += c.getImporteTotal();
            } else {
                mtoBaseImponibleIna += c.getImporteTotal();
            }

            mtoTributoIgv += c.getTributoIgv();
            mtoTributoIsc += c.getTributoIsc();
            mtoTributoIcbper += c.getTributoIcbper();

            dt.setMtoPrecioVentaUnitario(String.format("%.2f", c.getPrecioUnitario()));
            dt.setMtoValorVentaItem(String.format("%.2f", c.getImporteTotal()));
            dt.setMtoValorReferencialUnitario("0.00");
            details.add(dt);
        }
    }

    private void createTribute() {
        tributes = new ArrayList<>();
        Tributo tIgv = new Tributo();
        if (mtoTributoIgv > 0.00D) {
            tIgv.setIdeTributo("1000");
            tIgv.setNomTributo("IGV");
            tIgv.setCodTipTributo("VAT");
            tIgv.setMtoBaseImponible(String.format("%.2f", mtoBaseImponibleIgv));
            tIgv.setMtoTributo(String.format("%.2f", mtoTributoIgv));
        } else {
            tIgv.setIdeTributo("1000");
            tIgv.setNomTributo("IGV");
            tIgv.setCodTipTributo("VAT");
            tIgv.setMtoBaseImponible("0.00");
            tIgv.setMtoTributo("0.00");
        }

        Tributo tIgvIna = new Tributo();

        tIgvIna.setIdeTributo("9998");
        tIgvIna.setNomTributo("INA");
        tIgvIna.setCodTipTributo("FRE");
        tIgvIna.setMtoBaseImponible(String.format("%.2f", mtoBaseImponibleIna));

        tIgvIna.setMtoTributo("0.00");

        Tributo tIsc = new Tributo();
        tIsc.setIdeTributo("2000");
        tIsc.setNomTributo("ISC");
        tIsc.setCodTipTributo("EXC");
        tIsc.setMtoBaseImponible("0.00");
        tIsc.setMtoTributo("0.00");

        Tributo tIcbper = new Tributo();
        tIcbper.setIdeTributo("7152");
        tIcbper.setNomTributo("ICBPER");
        tIcbper.setCodTipTributo("OTH");
        tIcbper.setMtoBaseImponible("0.00");
        tIcbper.setMtoTributo("0.00");

        tributes.add(tIgv);
        tributes.add(tIgvIna);
        tributes.add(tIsc);
        tributes.add(tIcbper);
    }

    private String createLegend(String amountString) {
        String amountLetters = AmountLetters.convertToAmountInLetters(amountString, true);
        leyendas = new ArrayList<>();
        Leyenda legend = new Leyenda();
        legend.setCodLeyenda("1000");
        legend.setDesLeyenda(amountLetters);
        leyendas.add(legend);
        return amountLetters;
    }

    private void createInfoPayment() {
        pago = new DatoPago();
        pago.setFormaPago("Contado");
        pago.setMtoNetoPendientePago("0.00");
        pago.setTipMonedaMtoNetoPendientePago("PEN");
    }

    private CabeceraModel guardarDatosCabeFacbo(Cabecera c, String cod) {
        CabeceraRepositoryImp facbono = new CabeceraRepositoryImp();
        CabeceraModel cfb = new CabeceraModel();

        cfb.setCodCliente(reservacion.getCodCliente());
        cfb.setFechaEmision(c.getFecEmision());
        cfb.setHoraEmision(c.getHorEmision());
        cfb.setFechaVencimiento(c.getFecVencimiento());
        cfb.setTipoMoneda(c.getTipMoneda());
        cfb.setTotalTributos(c.getSumTotTributos());
        cfb.setTotalDescuento(c.getSumDescTotal());
        cfb.setTotalVenta(c.getSumTotValVenta());
        cfb.setPrecioVenta(c.getSumPrecioVenta());
        cfb.setImporteVenta(c.getSumImpVenta());
        cfb.setOtrosCargos(c.getSumOtrosCargos());
        cfb.setTipoComprobante(this.tipoComprobante);
        cfb.setSerie(cod.substring(0, 4));
        cfb.setNumero(cod.substring(5));
        cfb.setSituacionSunat("-");
        int res = facbono.createCabeceraFacbo(cfb);
        if (res <= 0) {
            JOptionPane.showMessageDialog(panelVoucherTicket, "Error al guardar cabecera.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
        return cfb;
    }

    private void guardarDatosDetallesCabeFacbo() {
        CabeceraRepositoryImp repository = new CabeceraRepositoryImp();
        Long idCabecera = repository.getLastHeader();
        DetalleFacbo dfb = new DetalleFacbo();
        for (Detalle d : details) {
            dfb.setIdCabecera(idCabecera);
            dfb.setCod_producto(d.getCodProducto());
            dfb.setCantidad(d.getCtdUnidadItem());
            dfb.setValor_unitario(d.getMtoValorUnitario());
            dfb.setTotal_tributo_item(d.getSumTotTributosItem());
            dfb.setCod_afectacion(d.getCodTriIGV());
            dfb.setTipo_afectacion(d.getTipAfeIGV());
            dfb.setPorcentaje_afectacion(d.getPorIgvItem());
            dfb.setDescuento("0.00");
            dfb.setOtros_cargos(d.getMtoTriOtroItem());
            dfb.setPrecio_venta_item(d.getMtoPrecioVentaUnitario());
            dfb.setTotal_importe_item(d.getMtoValorVentaItem());
            repository.createDetalleFacbo(dfb);
        }
    }

    private void datosQR() {
        CompanyRepositoryImp repository = new CompanyRepositoryImp();
        Company c = repository.getCompany();

        String[] datos = new String[9];
        datos[0] = c.getRuc();
        datos[1] = tipoComprobante;
        datos[2] = cod.substring(0, 4);
        datos[3] = cod.substring(5);
        datos[4] = String.valueOf(mtoTributoIgv);
        datos[5] = String.format("%.2f", reservacion.getImporteTotal() + mtoTributoIgv + mtoTributoIsc + mtoTributoIcbper);
        datos[6] = fechaEmision;
        datos[7] = this.panelVoucherTicket.txtIdTipoDocCliente.getText();
        datos[8] = this.panelVoucherTicket.txtNroDocumento.getText();
        QR.generarQR(datos);
    }
}
