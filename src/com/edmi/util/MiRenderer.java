package com.edmi.util;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class MiRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        double stock = Double.parseDouble(table.getValueAt(row, 4).toString());
        if (stock < 5) {
            this.setBackground(new Color(255,90,90));
        } else {
            this.setBackground(Color.WHITE);
        }
        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
}
