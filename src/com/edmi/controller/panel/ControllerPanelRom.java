package com.edmi.controller.panel;

import com.edmi.controller.menu.ControllerTablaRom;
import com.edmi.model.Habitacion;
import com.edmi.repository.RepositoryRoom;
import com.edmi.repository.imp.HabitacionRepositoryImp;
import com.edmi.view.MainPanelServices;
import com.edmi.view.ModalNewRoom;
import com.edmi.view.panel.PanelRom;
import com.edmi.view.widget.CardRoom;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public final class ControllerPanelRom implements ActionListener, MouseListener, KeyListener {
    private final ImageIcon ocupado = new ImageIcon(getClass().getResource("/com/edmi/images/sleeping_bed.png"));
    private final ImageIcon disponible = new ImageIcon(getClass().getResource("/com/edmi/images/icon_double_bed.png"));
    private RepositoryRoom<Habitacion> repositoryRom = new HabitacionRepositoryImp();
    private static List<Habitacion> roomBySearch;
    private static boolean filtroRoom = false;

    private MainPanelServices mainFrame;
    private static PanelRom panelRom;
    private ModalNewRoom modalRom;
    private ControllerTablaRom control;

    public ControllerPanelRom(MainPanelServices mainFrame) {
        this.mainFrame = mainFrame;
        panelRom = new PanelRom();
        modalRom = new ModalNewRoom(mainFrame, true, this);
        control = new ControllerTablaRom(panelRom, mainFrame, this);

        this.mainFrame.panelHabitaciones.setLayout(new GridBagLayout());

        addComponents(mainFrame.panelHabitaciones, panelRom);

        panelRom.setVisible(true);
        setListener();
        loadTableRooms("1");
    }

    private void setListener() {
        panelRom.btnNuevaHabitacion.addActionListener(this);
        PanelRom.roomTableRooms.addMouseListener(this);
        panelRom.txtBuscarHabitacion.addKeyListener(this);

        modalRom.btnNuevaHabitacion.addActionListener(this);

        panelRom.chkHabitacionesDesc.addActionListener(this);
    }

    public static void loadTableRooms(String estado) {
        clearTable(PanelRom.roomTableRooms);
        RepositoryRoom<Habitacion> repositoryRoom = new HabitacionRepositoryImp();
        DefaultTableModel modelRoom = (DefaultTableModel) PanelRom.roomTableRooms.getModel();
        List<Habitacion> rooms = repositoryRoom.findAllEstado(estado);
        if (filtroRoom) {
            roomBySearch.forEach((h) -> {
                if (h.getState().equals("1")) {
                    modelRoom.addRow(new Object[]{
                        h.getCodhaHabitacion(), h.getNroHabitacion(), h.getTipoHabitacion(), h.getEstado(),
                        h.getCostoAlquiler(), h.getNroPlanta(), h.getCaracteristicas(), "REGISTRADO"
                    });
                } else {
                    modelRoom.addRow(new Object[]{
                        h.getCodhaHabitacion(), h.getNroHabitacion(), h.getTipoHabitacion(), h.getEstado(),
                        h.getCostoAlquiler(), h.getNroPlanta(), h.getCaracteristicas(), "DESCARTADO"
                    });
                }
            });
            PanelRom.roomTableRooms.setModel(modelRoom);
        } else {
            rooms.forEach((h) -> {
                if (h.getState().equals("1")) {
                    modelRoom.addRow(new Object[]{
                        h.getCodhaHabitacion(), h.getNroHabitacion(), h.getTipoHabitacion(), h.getEstado(),
                        h.getCostoAlquiler(), h.getNroPlanta(), h.getCaracteristicas(), "REGISTRADO"
                    });
                } else {
                    modelRoom.addRow(new Object[]{
                        h.getCodhaHabitacion(), h.getNroHabitacion(), h.getTipoHabitacion(), h.getEstado(),
                        h.getCostoAlquiler(), h.getNroPlanta(), h.getCaracteristicas(), "DESCARTADO"
                    });
                }
            });
            PanelRom.roomTableRooms.setModel(modelRoom);
        }
    }
    
    public void showRooms(){
        RepositoryRoom<Habitacion> repositoryRoom = new HabitacionRepositoryImp();
        List<Habitacion> rooms = repositoryRoom.findAllEstado("1");
        mainFrame.panelRooms.removeAll();
        int columns = 5;
        
        GridLayout layout = new GridLayout((columns - 1), columns, 15, 15);
        if(rooms.size() <= 4 || rooms.size() <= 8){
            layout = new GridLayout(3, 8, 15,15);
        }
        mainFrame.panelRooms.setLayout(layout);

        rooms.stream().map(room -> {
            CardRoom card = new CardRoom(mainFrame);
            card.txtImagenHabitacion.setIcon(room.getEstado().equalsIgnoreCase("ocupado") ? ocupado : disponible);
            card.txtCodigoHabitacion.setText(room.getCodhaHabitacion());
            card.txtNumeroHabitacion.setText(String.valueOf(room.getNroHabitacion()));
            card.txtEstadoHabitacion.setText(room.getEstado());
            card.txtTipoHabitacion.setText(room.getTipoHabitacion());
            card.txtNroPlanta.setText(String.valueOf(room.getNroPlanta()));
            //card.txtWifi.setText("SI");
            card.txtCostoHabitacion.setText(String.valueOf(room.getCostoAlquiler()));
            return card;
        }).forEachOrdered(panel -> {
            mainFrame.panelRooms.add(panel);
        });
        mainFrame.repaint();
    }

    private static DefaultTableModel clearTable(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        return model;
    }

    private void mostrarDatosHabitacion(int row) {
        if (row >= 0) {
            Habitacion h = repositoryRom.findById(PanelRom.roomTableRooms.getValueAt(row, 0));
            modalRom.txtCodHabitacion.setText(h.getCodhaHabitacion());
            modalRom.cbxTipoHabitacion.setSelectedItem(h.getTipoHabitacion());
            modalRom.txtNroHabitacion.setText("" + h.getNroHabitacion());
            modalRom.cbxEstadoHabitacion.setSelectedItem(h.getEstado());
//        modalRom.spinnerNroPlanta.setValue(h.getNroPlanta());
            modalRom.txtCostoAlquiler.setText("" + h.getCostoAlquiler());
            modalRom.txtCaracteristicas.setText(h.getCaracteristicas());
        }
    }

    private void filterRoom() {
        filtroRoom = true;
        roomBySearch = new ArrayList<>();
        int index = panelRom.cbxCriteriosRoom.getSelectedIndex();

        if (index != 0) {
            String valueSend = panelRom.txtBuscarHabitacion.getText().trim();
            String condition = null;
            switch (index) {
                case 1:
                    condition = "cod_habitacion";
                    break;
                case 2:
                    condition = "nro_habitacion";
                    break;
                case 3:
                    condition = "tipo_habitacion";
                    break;
                case 4:
                    condition = "estado";
                    break;
            }

            String estado;

            if (panelRom.chkHabitacionesDesc.isSelected()) {
                estado = "0";
            } else {
                estado = "1";
            }
            
            roomBySearch = repositoryRom.findAllWhere(condition, valueSend, estado);
            loadTableRooms(estado);
            filtroRoom = false;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == panelRom.btnNuevaHabitacion) {
            ModalNewRoom modal = new ModalNewRoom(mainFrame, true, this);
            modal.btnModificarHabitacion.setEnabled(false);
            modal.setVisible(true);
            loadTableRooms("1");
        }
        if (e.getSource() == panelRom.chkHabitacionesDesc) {
            if (panelRom.chkHabitacionesDesc.isSelected()) {
                loadTableRooms("0");
            } else {
                loadTableRooms("1");
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            int row = PanelRom.roomTableRooms.getSelectedRow();
            modalRom = new ModalNewRoom(mainFrame, true, this);
            modalRom.loadRoomType(modalRom);
            mostrarDatosHabitacion(row);
            modalRom.btnNuevaHabitacion.setEnabled(false);
            modalRom.setTitle("Modificar datos de la habitación.");
            modalRom.setVisible(true);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == panelRom.txtBuscarHabitacion) {
            filterRoom();
        }
    }
    
    private void addComponents(JComponent parent, JComponent child) {
        GridBagConstraints conf = new GridBagConstraints();
        conf.gridx = 0;
        conf.gridy = 0;
        conf.gridwidth = 1;
        conf.gridheight = 1;
        conf.weightx = 1.0;
        conf.weighty = 1.0;
        conf.anchor = GridBagConstraints.CENTER;
        conf.fill = GridBagConstraints.BOTH;

        parent.add(child, conf);
        child.setVisible(true);
    }
}
