package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.model.Trabajador;
import com.edmi.database.query.Query;
import com.edmi.repository.RepositoryEmployee;
import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.view.Login;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class TrabajadorRepositoryImp implements RepositoryEmployee<Trabajador> {

    private Connection getConnection() throws SQLException {
        return getInstance();
    }

    @Override
    public List<Trabajador> findAllEstado(String estado) {
        List<Trabajador> list = new ArrayList<>();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TBJ_FIND_ALL_STATE)) {
            ps.setString(1, estado);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Trabajador t = createEmployee(rs);
                list.add(t);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public List<Trabajador> findAll() {
        List<Trabajador> list = new ArrayList<>();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TBJ_FIND_ALL);  ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                Trabajador t = createEmployee(rs);
                list.add(t);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    @Override
    public Trabajador findUserWhere(Object value) {
        Trabajador trabajador = null;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TBJ_FIND_USER_SYSTEM)) {
            ps.setString(1, value.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                trabajador = createEmployee(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return trabajador;
    }

    @Override
    public List<Trabajador> findAllWhere(String condition, String value, String estado) {
        String SQL = "SELECT * FROM trabajador WHERE " + condition + " LIKE " + "'" + value + "%' AND estado = '" + estado + "'";
        List<Trabajador> lista = new ArrayList<>();
        try ( PreparedStatement ps = getConnection().prepareStatement(SQL);  ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                Trabajador t = createEmployee(rs);
                lista.add(t);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @Override
    public List<String> findAllDocuDescripciones() {
        List<String> lista = new ArrayList();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TBJ_FIND_AL_DOCU)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    lista.add(rs.getString("descripcion"));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }

    @Override
    public String findAllTipeDocu(Object tipoDocu) {
        String cod = "";
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TBJ_TIPODOCU)) {
            ps.setString(1, tipoDocu.toString());
            try ( ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    cod = rs.getString("cod_tipodocumento");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return cod;
    }

    @Override
    public Trabajador findById(Object id) {
        Trabajador trabajador = null;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TBJ_FIND_BY_ID)) {
            ps.setString(1, id.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                trabajador = createEmployee(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return trabajador;
    }

    @Override
    public int create(Trabajador employee) {
        int response = 0;
        try {
            PreparedStatement st = getConnection().prepareStatement(Query.SQL_TBJ_CREATE);
            st.setString(1, employee.getCodTrabajador());
            st.setString(2, employee.getCodTipoDocumento());
            st.setString(3, employee.getNombre());
            st.setString(4, employee.getApellidoPaterno());
            st.setString(5, employee.getApellidoMaterno());
            st.setString(6, employee.getTipoDococumento());
            st.setString(7, employee.getNroDocumento());
            st.setString(8, employee.getNroTelefono());
            st.setString(9, employee.getEmail());
            st.setString(10, employee.getObervacion());
            st.setString(11, employee.getCargo());
            st.setString(12, employee.getUsername());
            st.setString(13, employee.getPassword());
            st.setString(14, employee.getEstado());
            response = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    @Override
    public Trabajador verificationLogin(String username, String password, Login login) {
        Trabajador trabajador = new Trabajador();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TBJ_FIND_LOGIN)) {
            ps.setString(1, username);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                trabajador = createEmployee(rs);
            }
        } catch (Exception e) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
            if (e.getMessage().equals("Access denied for user 'root'@'localhost' (using password: YES)")) {
                JOptionPane.showMessageDialog(login, "Verifique la configuración de conexión de la base de datos.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        }
        return trabajador;
    }

    @Override
    public int update(Trabajador employee) {
        int response = 0;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TBJ_UPDATE)) {
            ps.setString(1, employee.getNombre());
            ps.setString(2, employee.getApellidoPaterno());
            ps.setString(3, employee.getApellidoMaterno());
            ps.setString(4, employee.getTipoDococumento());
            ps.setString(5, employee.getNroDocumento());
            ps.setString(6, employee.getNroTelefono());
            ps.setString(7, employee.getEmail());
            ps.setString(8, employee.getObervacion());
            ps.setString(9, employee.getCargo());
            ps.setString(10, employee.getUsername());
            ps.setString(11, employee.getPassword());
            //ps.setString(12, employee.getEstado());
            ps.setString(12, employee.getCodTrabajador());
            response = ps.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }

    @Override
    public int delete(Object id) {
        int response = 0;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TBJ_DELETE)) {
            ps.setString(1, id.toString());
            response = ps.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }

    @Override
    public int state(Object id, Object actividad) {
        int response = 0;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TBJ_STATE)) {
            ps.setString(1, actividad.toString());
            ps.setString(2, id.toString());
            response = ps.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }
    
    public List<String> obtenerNroDocu() {
        List<String> lista = new ArrayList();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_TBJ_FIND_AL_NRODOCU)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    String nroDocu = rs.getString("nro_documento");
                    lista.add(nroDocu);
                }
            }
        } catch (Exception e) {
        }
        return lista;
    }

    private Trabajador createEmployee(ResultSet rs) throws SQLException {
        Trabajador emp = new Trabajador();
        emp.setCodTrabajador(rs.getString("cod_trabajador"));
        emp.setCodTipoDocumento(rs.getString("cod_tipodocumento"));
        emp.setNombre(rs.getString("nombre"));
        emp.setApellidoPaterno(rs.getString("apellido_paterno"));
        emp.setApellidoMaterno(rs.getString("apellido_materno"));
        emp.setTipoDococumento(rs.getString("tipo_documento"));
        emp.setNroDocumento(rs.getString("nro_documento"));
        emp.setNroTelefono(rs.getString("nro_telefono"));
        emp.setEmail(rs.getString("email"));
        emp.setObervacion(rs.getString("observacion"));
        emp.setCargo(rs.getString("cargo"));
        emp.setUsername(rs.getString("username"));
        emp.setPassword(rs.getString("password"));
        emp.setEstado(rs.getString("estado"));
        return emp;
    }
}
