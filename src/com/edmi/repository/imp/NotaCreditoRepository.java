package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.model.DetalleFacbo;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NotaCreditoRepository {

    public static List<DetalleFacbo> findAllProductNotaCredito(long idCabecera, String cod_producto) {
        List<DetalleFacbo> lista = new ArrayList();
        try (PreparedStatement ps = getInstance().prepareStatement("CALL getDatosNotaCredito(?,?)")) {
            ps.setLong(1, idCabecera);
            ps.setString(2, cod_producto);
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    DetalleFacbo dfb = new DetalleFacbo();
                    dfb.setCod_producto(rs.getString("dc.cod_producto"));
                    dfb.setNombreProducto(rs.getString("nombre_producto"));
                    dfb.setCantidad(rs.getString("dc.cantidad"));
                    dfb.setValor_unitario(rs.getString("dc.valor_unitario"));
                    dfb.setUnidadMedida(rs.getString("cod_unidadmedida"));
                    dfb.setCod_cliente(rs.getString("cli.cod_cliente"));
                    dfb.setCod_tipodocumento(rs.getString("cli.cod_tipodocumento"));
                    dfb.setNumeroDocumento(rs.getString("cli.nro_documento"));
                    dfb.setRazonSocial(rs.getString("razon_social"));
                    dfb.setFecha_emision(rs.getString("fecha_emision"));
                    dfb.setSerie(rs.getString("c.serie"));
                    dfb.setNumero(rs.getString("c.numero"));
                    dfb.setTotal_importe_item(rs.getString("dc.total_importe_item"));
                    dfb.setPorcentaje_afectacion(rs.getString("dc.porcentaje_afectacion"));
                    dfb.setCod_afectacion(rs.getString("dc.cod_afectacion"));
                    dfb.setTipo_afectacion(rs.getString("dc.tipo_afectacion"));
                    lista.add(dfb);
                }
            }
        } catch (SQLException e) {
            Logger.getLogger(CabeceraRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }
}