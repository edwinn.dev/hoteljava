package com.edmi.util;

import java.util.regex.Pattern;

public class AmountLetters {
    private static final String[] UNIDADES = {"", "uno ", "dos ", "tres ", "cuatro ", "cinco ", "seis ", "siete ", "ocho ", "nueve "};
    private static final String[] DECENAS = {"diez ", "once ", "doce ", "trece ", "catorce ", "quince ", "dieciseis ", "diecisiete ", "dieciocho ", "diecinueve ", "veinte ", "treinta ", "cuarenta ", "cincuenta ", "sesenta ", "setenta ", "ochenta ", "noventa "};
    private static final String[] CENTENAS = {"", "ciento ", "doscientos ", "trescientos ", "cuatrocientos ", "quinientos ", "seiscientos ", "setecientos ", "ochocientos ", "novecientos "};

    public AmountLetters() {
    }

    public static String convertToAmountInLetters(String number, boolean isUpperCase) {
        String literal;
        String decimalPart;
        number = number.replace(".", ",");
        if (!number.contains(",")) {
            number = number + ",00";
        }
        if (Pattern.matches("\\d{1,9},\\d{1,2}", number)) {
            String Num[] = number.split(",");
            decimalPart = "con " + Num[1] + "/100 Soles";
            if (Integer.parseInt(Num[0]) == 0) {
                literal = "cero ";
            } else if (Integer.parseInt(Num[0]) > 999999) {
                literal = getMillions(Num[0]);
            } else if (Integer.parseInt(Num[0]) > 999) {
                literal = getThousands(Num[0]);
            } else if (Integer.parseInt(Num[0]) > 99) {
                literal = getHundreds(Num[0]);
            } else if (Integer.parseInt(Num[0]) > 9) {
                literal = getTens(Num[0]);
            } else {
                literal = getUnits(Num[0]);
            }
            if (isUpperCase) {
                return (literal + decimalPart).toUpperCase();
            } else {
                return (literal + decimalPart);
            }
        } else {
            literal = null;
        }
        return literal;
    }

    private static String getUnits(String number) {
        String num = number.substring(number.length() - 1);
        return UNIDADES[Integer.parseInt(num)];
    }

    private static String getTens(String num) {
        int n = Integer.parseInt(num);
        if (n < 10) {
            return getUnits(num);
        } else if (n > 19) {
            String u = getUnits(num);
            if (u.equals("")) {
                return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8];
            } else {
                switch (n) {
                    case 21:
                        return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8].substring(0, 5) + "i" + u;
                    case 22:
                        return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8].substring(0, 5) + "i" + u;
                    case 23:
                        return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8].substring(0, 5) + "i" + u;
                    case 24:
                        return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8].substring(0, 5) + "i" + u;
                    case 25:
                        return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8].substring(0, 5) + "i" + u;
                    case 26:
                        return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8].substring(0, 5) + "i" + u;
                    case 27:
                        return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8].substring(0, 5) + "i" + u;
                    case 28:
                        return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8].substring(0, 5) + "i" + u;
                    case 29:
                        return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8].substring(0, 5) + "i" + u;
                    default:
                        break;
                }
                return DECENAS[Integer.parseInt(num.substring(0, 1)) + 8] + "y " + u;
            }
        } else {
            return DECENAS[n - 10]; 
        }
    }

    private static String getHundreds(String num) {
        if (Integer.parseInt(num) > 99) {
            if (Integer.parseInt(num) == 100) {
                return " cien ";
            } else {
                return CENTENAS[Integer.parseInt(num.substring(0, 1))] + getTens(num.substring(1));
            }
        } else {
            return getTens(Integer.parseInt(num) + "");
        }
    }

    private static String getThousands(String numero) {
        String c = numero.substring(numero.length() - 3);
        String m = numero.substring(0, numero.length() - 3);
        String n;
        if (Integer.parseInt(m) > 0) {
            n = getHundreds(m);
            return n + "mil " + getHundreds(c);
        } else {
            return "" + getHundreds(c);
        }

    }

    private static String getMillions(String numero) {      
        String miles = numero.substring(numero.length() - 6);
        String millon = numero.substring(0, numero.length() - 6);
        String n;
        if (millon.length() > 1) {
            n = getHundreds(millon) + "millones ";
        } else {
            n = getUnits(millon) + "millon ";
        }
        return n + getThousands(miles);
    }
}
