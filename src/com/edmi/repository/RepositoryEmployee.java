package com.edmi.repository;

import com.edmi.view.Login;
import java.util.List;

public interface RepositoryEmployee<T> {
    List<T> findAllEstado(String estado);
    List<T> findAll();
    T verificationLogin(String username, String password, Login login);
    T findById(Object id);
    List<T> findAllWhere(String condition, String value, String estado);
    List<String> findAllDocuDescripciones();
    String findAllTipeDocu(Object tipoDocu); 
    T findUserWhere(Object value);
    int state(Object id, Object actividad);
    int create(T employee);
    int update(T employee);
    int delete(Object id);
}
