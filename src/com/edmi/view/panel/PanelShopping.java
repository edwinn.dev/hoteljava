package com.edmi.view.panel;

public class PanelShopping extends javax.swing.JPanel {

    public PanelShopping() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane3 = new javax.swing.JScrollPane();
        tableProducts = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnAgregarProducto = new javax.swing.JButton();
        btnProveedores = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtBuscarProducto = new javax.swing.JFormattedTextField();
        btnImportarExcel = new javax.swing.JButton();
        btnExportarExcel = new javax.swing.JButton();

        tableProducts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "NOMBRE DE PRODUCTO", "MARCA", "CATEGORIA", "STOCK", "FECHA VENCIMIENTO", "FECHA REG.", "PROVEEDOR", "P. COMPRA (S/)", "P. VENTA (S/)", "MONTO IGV", "MONTO ICBPER", "DESCRIPCION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableProducts.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tableProducts.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tableProducts);
        if (tableProducts.getColumnModel().getColumnCount() > 0) {
            tableProducts.getColumnModel().getColumn(0).setPreferredWidth(150);
            tableProducts.getColumnModel().getColumn(1).setPreferredWidth(350);
            tableProducts.getColumnModel().getColumn(2).setPreferredWidth(200);
            tableProducts.getColumnModel().getColumn(3).setPreferredWidth(200);
            tableProducts.getColumnModel().getColumn(4).setPreferredWidth(80);
            tableProducts.getColumnModel().getColumn(5).setPreferredWidth(200);
            tableProducts.getColumnModel().getColumn(6).setPreferredWidth(200);
            tableProducts.getColumnModel().getColumn(7).setPreferredWidth(100);
            tableProducts.getColumnModel().getColumn(8).setPreferredWidth(120);
            tableProducts.getColumnModel().getColumn(9).setPreferredWidth(120);
            tableProducts.getColumnModel().getColumn(10).setPreferredWidth(100);
            tableProducts.getColumnModel().getColumn(11).setPreferredWidth(100);
            tableProducts.getColumnModel().getColumn(12).setPreferredWidth(300);
        }

        btnAgregarProducto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnAgregarProducto.setText("NUEVO PRODUCTO");
        btnAgregarProducto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnProveedores.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnProveedores.setText("PROVEEDORES");
        btnProveedores.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Buscar producto:");

        txtBuscarProducto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        btnImportarExcel.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnImportarExcel.setText("IMPORTAR PRODUCTOS");
        btnImportarExcel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnExportarExcel.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnExportarExcel.setText("OBTENER FORMATO");
        btnExportarExcel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnAgregarProducto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnImportarExcel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnExportarExcel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnProveedores, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtBuscarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAgregarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(txtBuscarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnProveedores, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnImportarExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnExportarExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnAgregarProducto;
    public javax.swing.JButton btnExportarExcel;
    public javax.swing.JButton btnImportarExcel;
    public javax.swing.JButton btnProveedores;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane3;
    public static javax.swing.JTable tableProducts;
    public javax.swing.JFormattedTextField txtBuscarProducto;
    // End of variables declaration//GEN-END:variables
}
