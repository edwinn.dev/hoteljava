package com.edmi.view.panel;

public class PanelVoucherTicket extends javax.swing.JPanel {
    private static PanelVoucherTicket instance = null;
    
    public static PanelVoucherTicket getInstance(){
        if(instance == null){
            instance = new PanelVoucherTicket();
        }
        return instance;
    }
    
    private PanelVoucherTicket() {
        initComponents();
        setPreferences();
        btnDocRelacionados.setVisible(false);
        btnAdicionalCliente.setVisible(false);
        btnInfoTransporte.setVisible(false);
        this.btnAgregarProducto.setVisible(false);
    }
    
    private void setPreferences(){
        this.txtIdTipoComprobante.setVisible(false);
        this.txtIdTipoDocCliente.setVisible(false);
    }
    
    public static void setNullInstance(){
        instance = null;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitleVoucher = new javax.swing.JLabel();
        btnBoletaRegresar = new javax.swing.JButton();
        btnEmitirBoleta = new javax.swing.JButton();
        btnAgregarProducto = new javax.swing.JButton();
        btnDocRelacionados = new javax.swing.JButton();
        btnAdicionalCliente = new javax.swing.JButton();
        btnInfoTransporte = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaConsumos = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cbxTipoOperacion = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        cbxTipoMoneda = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        txtTipoComprobante = new javax.swing.JFormattedTextField();
        txtIdTipoComprobante = new javax.swing.JFormattedTextField();
        jLabel9 = new javax.swing.JLabel();
        dateEmit = new com.toedter.calendar.JDateChooser();
        jLabel12 = new javax.swing.JLabel();
        dateExpiration = new com.toedter.calendar.JDateChooser();
        txtIdTipoDocCliente = new javax.swing.JFormattedTextField();
        txtTipoDoc = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        txtNroDocumento = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        txtNombreCliente = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        txtObservacion = new javax.swing.JFormattedTextField();
        jLabel10 = new javax.swing.JLabel();
        txtImporteTotal = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();

        lblTitleVoucher.setFont(new java.awt.Font("Calibri", 1, 18)); // NOI18N
        lblTitleVoucher.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        btnBoletaRegresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icon_left.png"))); // NOI18N
        btnBoletaRegresar.setFocusPainted(false);

        btnEmitirBoleta.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnEmitirBoleta.setText("EMITIR");
        btnEmitirBoleta.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnAgregarProducto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnAgregarProducto.setText("AGREGAR PRODUCTO");
        btnAgregarProducto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnDocRelacionados.setText("Agregar documentos relacionados");

        btnAdicionalCliente.setText("Informacion adicional del cliente");

        btnInfoTransporte.setText("Agregar informacion de transporte");

        tablaConsumos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tablaConsumos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ITEM", "CODIGO", "DESCRIPCION", "U. MEDIDA", "CANTIDAD", "P. UNITARIO", "COD. SUNAT", "IGV(18%) (S/)", "DCTOS (S/)", "ISC (S/)", "ICBPER (S/)", "TOTAL (S/)"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaConsumos.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tablaConsumos);
        if (tablaConsumos.getColumnModel().getColumnCount() > 0) {
            tablaConsumos.getColumnModel().getColumn(0).setPreferredWidth(50);
            tablaConsumos.getColumnModel().getColumn(1).setPreferredWidth(100);
            tablaConsumos.getColumnModel().getColumn(2).setPreferredWidth(300);
            tablaConsumos.getColumnModel().getColumn(3).setPreferredWidth(100);
            tablaConsumos.getColumnModel().getColumn(4).setPreferredWidth(100);
            tablaConsumos.getColumnModel().getColumn(5).setPreferredWidth(100);
            tablaConsumos.getColumnModel().getColumn(6).setPreferredWidth(150);
            tablaConsumos.getColumnModel().getColumn(7).setPreferredWidth(100);
            tablaConsumos.getColumnModel().getColumn(8).setPreferredWidth(100);
            tablaConsumos.getColumnModel().getColumn(9).setPreferredWidth(100);
            tablaConsumos.getColumnModel().getColumn(10).setPreferredWidth(100);
            tablaConsumos.getColumnModel().getColumn(11).setPreferredWidth(100);
        }

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("TIPO DE OPERACION :");

        cbxTipoOperacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxTipoOperacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0101 - Venta interna", "0113 - Venta Interna-NRUS", "0200 - Exportación de Bienes", "0201 - Exportación de Servicios – Prestación servicios realizados íntegramente en el país", "0202 - Exportación de Servicios – Prestación de servicios de hospedaje No Domiciliado", "0203 - Exportación de Servicios – Transporte de navieras", "0204 - Exportación de Servicios – Servicios  a naves y aeronaves de bandera extranjera", "0205 - Exportación de Servicios  - Servicios que conformen un Paquete Turístico", "0206 - Exportación de Servicios – Servicios complementarios al transporte de carga", "0207 - Exportación de Servicios – Suministro de energía eléctrica a favor de sujetos domiciliados en ZED", "0208 - Exportación de Servicios – Prestación servicios realizados parcialmente en el extranjero", "0301 - Operaciones con Carta de porte aéreo (emitidas en el ámbito nacional)", "0302 - Operaciones de Transporte ferroviario de pasajeros", "0303 - Operaciones de Pago de regalía petrolera", "0401 - Ventas no domiciliados que no califican como exportación", "1001 - Operación Sujeta a Detracción", "1002 - Operación Sujeta a Detracción- Recursos Hidrobiológicos", "1003 - Operación Sujeta a Detracción- Servicios de Transporte Pasajeros", "1004 - Operación Sujeta a Detracción- Servicios de Transporte Carga", "2001 - Operación Sujeta a Percepción" }));

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("TIPO DE MONEDA :");

        cbxTipoMoneda.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxTipoMoneda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "PEN - SOLES", "USD - DOLARES", "EUR - EUROS" }));

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("TIPO DE COMPROBANTE :");

        txtTipoComprobante.setEditable(false);
        txtTipoComprobante.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtIdTipoComprobante.setEditable(false);
        txtIdTipoComprobante.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("FECHA DE EMISION :");

        dateEmit.setDateFormatString("dd-MM-yyyy");
        dateEmit.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel12.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel12.setText("FECHA DE VENCIMIENTO :");

        dateExpiration.setDateFormatString("dd-MM-yyyy");

        txtIdTipoDocCliente.setEditable(false);
        txtIdTipoDocCliente.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtTipoDoc.setEditable(false);
        txtTipoDoc.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("TIPO DE DOCUMENTO :");

        txtNroDocumento.setEditable(false);
        txtNroDocumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("NRO DE DOCUMENTO :");

        txtNombreCliente.setEditable(false);
        txtNombreCliente.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("CLIENTE - RAZON SOCIAL :");

        txtObservacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel10.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("OBSERVACION :");

        txtImporteTotal.setEditable(false);

        jLabel11.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("IMPORTE TOTAL : ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(dateEmit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(212, 212, 212))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(cbxTipoMoneda, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cbxTipoOperacion, javax.swing.GroupLayout.Alignment.LEADING, 0, 1, Short.MAX_VALUE)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txtTipoComprobante, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtIdTipoComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(25, 25, 25))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dateExpiration, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(212, 212, 212)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtTipoDoc)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtIdTipoDocCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtObservacion)
                            .addComponent(txtNroDocumento)
                            .addComponent(txtNombreCliente)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 354, Short.MAX_VALUE)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtImporteTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cbxTipoOperacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIdTipoDocCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTipoDoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cbxTipoMoneda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNroDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtTipoComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIdTipoComprobante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txtObservacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel10)
                                .addComponent(jLabel9))
                            .addGap(40, 40, 40))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtImporteTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(dateEmit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(dateExpiration, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnBoletaRegresar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblTitleVoucher, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnInfoTransporte, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnDocRelacionados, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnAdicionalCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnAgregarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnEmitirBoleta, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBoletaRegresar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTitleVoucher, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAgregarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEmitirBoleta, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 317, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnInfoTransporte, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDocRelacionados, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAdicionalCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnAdicionalCliente;
    public javax.swing.JButton btnAgregarProducto;
    public javax.swing.JButton btnBoletaRegresar;
    public javax.swing.JButton btnDocRelacionados;
    public javax.swing.JButton btnEmitirBoleta;
    public javax.swing.JButton btnInfoTransporte;
    public javax.swing.JComboBox<String> cbxTipoMoneda;
    public javax.swing.JComboBox<String> cbxTipoOperacion;
    public com.toedter.calendar.JDateChooser dateEmit;
    public com.toedter.calendar.JDateChooser dateExpiration;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    public javax.swing.JLabel lblTitleVoucher;
    public javax.swing.JTable tablaConsumos;
    public javax.swing.JFormattedTextField txtIdTipoComprobante;
    public javax.swing.JFormattedTextField txtIdTipoDocCliente;
    public javax.swing.JTextField txtImporteTotal;
    public javax.swing.JFormattedTextField txtNombreCliente;
    public javax.swing.JFormattedTextField txtNroDocumento;
    public javax.swing.JFormattedTextField txtObservacion;
    public javax.swing.JFormattedTextField txtTipoComprobante;
    public javax.swing.JFormattedTextField txtTipoDoc;
    // End of variables declaration//GEN-END:variables
}
