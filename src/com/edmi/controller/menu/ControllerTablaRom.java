package com.edmi.controller.menu;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.panel.ControllerPanelRom;
import com.edmi.repository.imp.HabitacionRepositoryImp;
import com.edmi.view.MainPanelServices;
import com.edmi.view.panel.PanelRom;
import com.edmi.view.widget.MenuTableRom;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class ControllerTablaRom implements ActionListener {
    private MenuTableRom menuRom = null;
    private MainPanelServices mainFrame = null;
    private HabitacionRepositoryImp repositoryRom = null;
    private PanelRom panelRom = null;
    private ControllerPanelRom control = null;

    public ControllerTablaRom(PanelRom panelRom, MainPanelServices main, ControllerPanelRom control) {
        this.mainFrame = main;
        this.panelRom = panelRom;
        this.control = control;
        menuRom = MenuTableRom.getInstance();
        menuRom.initMenuTableRom(PanelRom.roomTableRooms);
        setListener();
    }

    private void cambiarEstado(String estado) {
        repositoryRom = new HabitacionRepositoryImp();
        int row = PanelRom.roomTableRooms.getSelectedRow();
        int op = JOptionPane.showConfirmDialog(mainFrame, "¿Está seguro de eliminar la habitación?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if (op == JOptionPane.YES_OPTION) {
            if (repositoryRom.state(PanelRom.roomTableRooms.getValueAt(row, 0), estado) != 0) {
                JOptionPane.showMessageDialog(mainFrame, "Habitación eliminada.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                ControllerPanelRom.loadTableRooms("1");
                control.showRooms();
            } else {
                JOptionPane.showMessageDialog(mainFrame, "Hubo un problema al cambiar el estado del cliente,\ncontacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void setListener() {
//        menuRom.itemRegistrado.addActionListener(this);
        menuRom.itemDescartado.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == menuRom.itemRegistrado) {
            cambiarEstado("1");
        } else if (e.getSource() == menuRom.itemDescartado) {
            cambiarEstado("0");
        }
    }
}
