package com.edmi.model.invoice;

public class DetallePago {
    private String mtoCuotaPago;
    private String fecCuotaPago;
    private String tipMonedaCuotaPago;

    public String getMtoCuotaPago() {
        return mtoCuotaPago;
    }

    public void setMtoCuotaPago(String mtoCuotaPago) {
        this.mtoCuotaPago = mtoCuotaPago;
    }

    public String getFecCuotaPago() {
        return fecCuotaPago;
    }

    public void setFecCuotaPago(String fecCuotaPago) {
        this.fecCuotaPago = fecCuotaPago;
    }

    public String getTipMonedaCuotaPago() {
        return tipMonedaCuotaPago;
    }

    public void setTipMonedaCuotaPago(String tipMonedaCuotaPago) {
        this.tipMonedaCuotaPago = tipMonedaCuotaPago;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DetallePago{mtoCuotaPago=").append(mtoCuotaPago);
        sb.append(", fecCuotaPago=").append(fecCuotaPago);
        sb.append(", tipMonedaCuotaPago=").append(tipMonedaCuotaPago);
        sb.append('}');
        return sb.toString();
    }
}
