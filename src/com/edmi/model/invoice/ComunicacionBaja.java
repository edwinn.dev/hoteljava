package com.edmi.model.invoice;

import java.util.Objects;

public class ComunicacionBaja {
    private String codBaja;
    private String fecGeneracion;
    private String fecComunicacion;
    private String tipDocBaja;
    private String numDocBaja;
    private String desMotivoBaja;
    private String situacionSunat;
    
    public ComunicacionBaja() {}

    public String getCodBaja() {
        return codBaja;
    }

    public void setCodBaja(String codBaja) {
        this.codBaja = codBaja;
    }

    public String getFecGeneracion() {
        return fecGeneracion;
    }

    public void setFecGeneracion(String fecGeneracion) {
        this.fecGeneracion = fecGeneracion;
    }

    public String getFecComunicacion() {
        return fecComunicacion;
    }

    public void setFecComunicacion(String fecComunicacion) {
        this.fecComunicacion = fecComunicacion;
    }

    public String getTipDocBaja() {
        return tipDocBaja;
    }

    public void setTipDocBaja(String tipDocBaja) {
        this.tipDocBaja = tipDocBaja;
    }

    public String getNumDocBaja() {
        return numDocBaja;
    }

    public void setNumDocBaja(String numDocBaja) {
        this.numDocBaja = numDocBaja;
    }

    public String getDesMotivoBaja() {
        return desMotivoBaja;
    }

    public void setDesMotivoBaja(String desMotivoBaja) {
        this.desMotivoBaja = desMotivoBaja;
    }

    public String getSituacionSunat() {
        return situacionSunat;
    }

    public void setSituacionSunat(String situacionSunat) {
        this.situacionSunat = situacionSunat;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ComunicacionBaja{codBaja=").append(codBaja);
        sb.append(", fecGeneracion=").append(fecGeneracion);
        sb.append(", fecComunicacion=").append(fecComunicacion);
        sb.append(", tipDocBaja=").append(tipDocBaja);
        sb.append(", numDocBaja=").append(numDocBaja);
        sb.append(", desMotivoBaja=").append(desMotivoBaja);
        sb.append(", situacionSunat=").append(situacionSunat);
        sb.append('}');
        return sb.toString();
    }
}