package com.edmi.controller.menu;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.ControllerCliente;
import com.edmi.model.Cliente;
import com.edmi.repository.RepositoryClient;
import com.edmi.repository.imp.ClienteRepositoryImp;
import com.edmi.view.MainPanelServices;
import com.edmi.view.widget.MenuTableClient;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class ControllerTableClient implements ActionListener {
    private static RepositoryClient<Cliente> repositoryClient;
    private MenuTableClient menuCliente;
    private MainPanelServices mainFrame;
    private ControllerCliente controllerCliente;

    public ControllerTableClient(MainPanelServices mainFrame) {
        this.mainFrame = mainFrame;
        controllerCliente = new ControllerCliente(mainFrame, null);
        repositoryClient = new ClienteRepositoryImp();
        menuCliente = MenuTableClient.getInstance();
        menuCliente.initMenuTableClient(mainFrame.tableClients);
        setListener();
    }

    private void cambiarEstado(String estado) {
        int row = mainFrame.tableClients.getSelectedRow();

        int op = JOptionPane.showConfirmDialog(mainFrame, "¿Está seguro de eliminar al cliente?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if (op == JOptionPane.YES_OPTION) {
            if (repositoryClient.state(mainFrame.tableClients.getValueAt(row, 0), estado) != 0) {
                JOptionPane.showMessageDialog(mainFrame, "Cliente eliminado.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                controllerCliente.loadTableClients("1");
                mainFrame.chkClienteDescartados.setSelected(false);
            } else {
                JOptionPane.showMessageDialog(mainFrame, "Hubo un problema al cambiar el estado del cliente, contacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void setListener() {
        menuCliente.itemDescartado.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == menuCliente.itemDescartado) {
            cambiarEstado("0");
        }
    }
}
