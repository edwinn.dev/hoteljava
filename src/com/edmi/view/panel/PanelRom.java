package com.edmi.view.panel;

import com.edmi.view.Login;

public class PanelRom extends javax.swing.JPanel {

    public PanelRom() {
        initComponents();
        setPreferences();
    }
    
    private void setPreferences() {
        roomTableRooms.getColumnModel().getColumn(7).setMaxWidth(0);
        roomTableRooms.getColumnModel().getColumn(7).setMinWidth(0);
        roomTableRooms.getColumnModel().getColumn(7).setPreferredWidth(0);
        
        boolean value;
        
        value = !Login.getCargo().toUpperCase().equals("EJECUTIVO DE VENTAS");
        
        this.chkHabitacionesDesc.setVisible(value);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane4 = new javax.swing.JScrollPane();
        roomTableRooms = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnNuevaHabitacion = new javax.swing.JButton();
        txtBuscarHabitacion = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        cbxCriteriosRoom = new javax.swing.JComboBox<>();
        chkHabitacionesDesc = new javax.swing.JCheckBox();

        jScrollPane4.setPreferredSize(new java.awt.Dimension(500, 500));

        roomTableRooms.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        roomTableRooms.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "NRO. HABITACION", "TIPO DE HABITACION", "ESTADO", "COSTO ALQUILER (S/)", "NRO. PLANTA", "CARACTERISTICAS", "ESTADO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        roomTableRooms.getTableHeader().setReorderingAllowed(false);
        jScrollPane4.setViewportView(roomTableRooms);
        if (roomTableRooms.getColumnModel().getColumnCount() > 0) {
            roomTableRooms.getColumnModel().getColumn(0).setPreferredWidth(80);
            roomTableRooms.getColumnModel().getColumn(1).setPreferredWidth(150);
            roomTableRooms.getColumnModel().getColumn(2).setPreferredWidth(250);
            roomTableRooms.getColumnModel().getColumn(3).setPreferredWidth(100);
            roomTableRooms.getColumnModel().getColumn(4).setPreferredWidth(150);
            roomTableRooms.getColumnModel().getColumn(5).setPreferredWidth(100);
            roomTableRooms.getColumnModel().getColumn(6).setPreferredWidth(300);
            roomTableRooms.getColumnModel().getColumn(7).setPreferredWidth(80);
        }

        btnNuevaHabitacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnNuevaHabitacion.setText("NUEVA HABITACION");
        btnNuevaHabitacion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        txtBuscarHabitacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Criterio:");

        cbxCriteriosRoom.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxCriteriosRoom.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --", "CODIGO", "NUMERO", "TIPO", "ESTADO" }));

        chkHabitacionesDesc.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        chkHabitacionesDesc.setText("Habitaciones descartadas");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnNuevaHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(chkHabitacionesDesc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbxCriteriosRoom, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtBuscarHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNuevaHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscarHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(cbxCriteriosRoom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkHabitacionesDesc))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnNuevaHabitacion;
    public javax.swing.JComboBox<String> cbxCriteriosRoom;
    public javax.swing.JCheckBox chkHabitacionesDesc;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    public javax.swing.JScrollPane jScrollPane4;
    public static javax.swing.JTable roomTableRooms;
    public javax.swing.JTextField txtBuscarHabitacion;
    // End of variables declaration//GEN-END:variables
}
