package com.edmi.controller;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.model.Cliente;
import com.edmi.repository.imp.ClienteRepositoryImp;
import com.edmi.util.Http;
import com.edmi.util.MessageView;
import com.edmi.util.Util;
import com.edmi.view.MainPanelServices;
import com.edmi.view.ModalNewClient;
import com.edmi.view.ModalNewReservation;
import com.edmi.view.panel.PanelReportClient;
import com.google.gson.Gson;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public final class ControllerCliente implements ActionListener, MouseListener, KeyListener {
    private boolean filtroCliente = false;
    private static ClienteRepositoryImp repositoryClient;
    private final MainPanelServices mainFrame;
    private ModalNewClient modalC;
    private List<Cliente> clientsBySearch;
    private Cliente cliente = null;

    public ControllerCliente(MainPanelServices mainFrame, ModalNewClient modal) {
        this.mainFrame = mainFrame;
        repositoryClient = new ClienteRepositoryImp();
        modalC = modal != null ? modal : new ModalNewClient(this.mainFrame, true);
        loadTableClients("1");
        setListeners();
        modalC.txtCodCliente.setText(generarCodCliente());
        mostrarDescripionDocu();
    }

    public static Cliente getClientById(String codClient) {
        return repositoryClient.findById(codClient);
    }

    private void setListeners() {
        modalC.btnRegistrarCliente.addActionListener(this);
        modalC.btnModificarCliente.addActionListener(this);
        modalC.btnBuscarRUCCliente.addActionListener(this);
        mainFrame.chkClienteDescartados.addActionListener(this);
    }

    public void mostrarDatosClienteEnReservacion(ModalNewReservation modal) {
        modal.txtTipoDoc.setText(cliente.getTipoDocumento());
        modal.txtNroDoc.setText(cliente.getNroDocumento());
        modal.txtNombres.setText(cliente.getNombre() + " " + cliente.getApellidoPaterno() + " " + cliente.getApellidoMaterno());
        modal.txtTelefono.setText(cliente.getNroTelefono());
        modal.txtCodigoCliente.setText(cliente.getCodCliente());
        modal.txtEmail.setText(cliente.getEmail());
    }

    public void loadTableClients(String estado) {
        clearTable(mainFrame.tableClients);
        DefaultTableModel modelClient = (DefaultTableModel) mainFrame.tableClients.getModel();
        List<Cliente> clients = repositoryClient.findAllEstado(estado);
        if (filtroCliente) {
            clientsBySearch.forEach(c -> {
                if (c.getEstado().equals("1")) {
                    modelClient.addRow(new Object[]{
                        c.getCodCliente(), c.getNombre().concat(" ").concat(c.getApellidoPaterno()).concat(" ").concat(c.getApellidoMaterno()),
                        c.getTipoDocumento(), c.getNroDocumento(), c.getNroTelefono(), c.getEmail(), c.getObervacion(), "REGISTRADO"
                    });
                } else {
                    modelClient.addRow(new Object[]{
                        c.getCodCliente(), c.getNombre().concat(" ").concat(c.getApellidoPaterno()).concat(" ").concat(c.getApellidoMaterno()),
                        c.getTipoDocumento(), c.getNroDocumento(), c.getNroTelefono(), c.getEmail(), c.getObervacion(), "DESCARTADO"
                    });
                }
            });
            mainFrame.tableClients.setModel(modelClient);
        } else {
            clients.forEach(c -> {
                if (c.getEstado().equals("1")) {
                    modelClient.addRow(new Object[]{
                        c.getCodCliente(), c.getNombre().concat(" ").concat(c.getApellidoPaterno()).concat(" ").concat(c.getApellidoMaterno()),
                        c.getTipoDocumento(), c.getNroDocumento(), c.getNroTelefono(), c.getEmail(), c.getObervacion(), "REGISTRADO"
                    });
                } else {
                    modelClient.addRow(new Object[]{
                        c.getCodCliente(), c.getNombre().concat(" ").concat(c.getApellidoPaterno()).concat(" ").concat(c.getApellidoMaterno()),
                        c.getTipoDocumento(), c.getNroDocumento(), c.getNroTelefono(), c.getEmail(), c.getObervacion(), "DESCARTADO"
                    });
                }
            });
            mainFrame.tableClients.setModel(modelClient);
        }
    }

    public void loadTableClientsId() {
        clearTable(mainFrame.tableClients);
        DefaultTableModel modelClient = (DefaultTableModel) mainFrame.tableClients.getModel();
        List<Cliente> listaClientes = new ArrayList();
        Cliente clienteResponse = repositoryClient.findById(mainFrame.txtBuscarCliente.getText());
        listaClientes.add(clienteResponse);
        listaClientes.forEach(c -> {
            modelClient.addRow(new Object[]{
                c.getCodCliente(), c.getNombre().concat(" ").concat(c.getApellidoPaterno()).concat(" ").concat(c.getApellidoMaterno()),
                c.getTipoDocumento(), c.getNroDocumento(), c.getNroTelefono(), c.getEmail(), c.getObervacion(), c.getEstado()
            });
        });
        mainFrame.tableClients.setModel(modelClient);
    }

    private void clearTable(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    private void mostrarDatosCliente(int row) {
        Cliente c = repositoryClient.findById(mainFrame.tableClients.getValueAt(row, 0));
        modalC.txtCodCliente.setText(c.getCodCliente());
        modalC.cbxTipoDoc.setSelectedItem(c.getTipoDocumento());
        modalC.txtNroDoc.setText(c.getNroDocumento());
        modalC.txtNombre.setText(c.getNombre());
        modalC.txtApellidoPaterno.setText(c.getApellidoPaterno());
        modalC.txtApellidoMaterno.setText(c.getApellidoMaterno());
        modalC.txtTelefono.setText(c.getNroTelefono());
        modalC.txtEmail.setText(c.getEmail());
        modalC.txtObservacion.setText(c.getObervacion());
        modalC.setTitle("Modificar datos del Cliente: " + c.getNombre());
    }

    private void registrarCliente() {
        int response;

        if (isFieldsInvalid()) {
            JOptionPane.showMessageDialog(modalC, MessageView.MODAL_CLIENT_VALIDATE, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalC.cbxTipoDoc.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(modalC, MessageView.MODAL_CLIENT_SELECTED_INDEX_ERROR, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalC.txtNroDoc.getText().length() < 8) {
            JOptionPane.showMessageDialog(modalC, MessageView.DOCUMENT_INVALID, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        List<String> nroDocumentos = repositoryClient.obtenerNroDocu();
        for (String nd : nroDocumentos) {
            if (modalC.txtNroDoc.getText().equals(nd)) {
                JOptionPane.showMessageDialog(modalC, "El cliente ya se encuentra registrado.", SYS_MSG, JOptionPane.WARNING_MESSAGE);
                return;
            }
        }

        int option = JOptionPane.showConfirmDialog(modalC, "¿Está seguro de registrar al cliente?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if(option == JOptionPane.YES_OPTION){
            Cliente c = new Cliente();
            c.setCodCliente(modalC.txtCodCliente.getText());
            c.setCodTipoDocumento(repositoryClient.findAllTipeDocu(modalC.cbxTipoDoc.getSelectedItem()));
            c.setNombre(modalC.txtNombre.getText());
            c.setApellidoPaterno(modalC.txtApellidoPaterno.getText());
            c.setApellidoMaterno(modalC.txtApellidoMaterno.getText());
            c.setTipoDocumento(modalC.cbxTipoDoc.getSelectedItem().toString());
            c.setNroDocumento(modalC.txtNroDoc.getText());
            c.setNroTelefono(modalC.txtTelefono.getText().isEmpty() ? "-" : modalC.txtTelefono.getText());
            c.setEmail(modalC.txtEmail.getText().isEmpty() ? "-" : modalC.txtEmail.getText().toLowerCase());
            c.setObervacion(modalC.txtObservacion.getText().isEmpty() ? "-" : modalC.txtObservacion.getText());
            c.setEstado("1");
            response = repositoryClient.create(c);
            this.cliente = c; //Obtengo los datos de cliente para reutilizarlo.
            if (response <= 0) {
                JOptionPane.showMessageDialog(modalC, "Error al registrar cliente", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }
            JOptionPane.showMessageDialog(modalC, "Cliente registrado", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            clearTextField();
            loadTableClients("1");
            PanelReportClient.getInstance().loadTableClients();
            modalC.txtCodCliente.setText(generarCodCliente());
            modalC.cbxTipoDoc.setSelectedIndex(0);
            modalC.dispose();
        }
    }

    private void modificarCliente() {
        int response;

        if (isFieldsInvalid()) {
            JOptionPane.showMessageDialog(modalC, MessageView.MODAL_CLIENT_VALIDATE, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalC.cbxTipoDoc.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(modalC, MessageView.MODAL_CLIENT_SELECTED_INDEX_ERROR, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalC.txtNroDoc.getText().length() < 8) {
            JOptionPane.showMessageDialog(modalC, MessageView.DOCUMENT_INVALID, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        int option = JOptionPane.showConfirmDialog(modalC, "¿Está seguro de modificar los datos del cliente?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if(option == JOptionPane.YES_OPTION){
            Cliente c = new Cliente();
            c.setCodCliente(modalC.txtCodCliente.getText());
            c.setCodTipoDocumento(repositoryClient.findAllTipeDocu(modalC.cbxTipoDoc.getSelectedItem()));
            c.setNombre(modalC.txtNombre.getText());
            c.setApellidoPaterno(modalC.txtApellidoPaterno.getText());
            c.setApellidoMaterno(modalC.txtApellidoMaterno.getText());
            c.setTipoDocumento(modalC.cbxTipoDoc.getSelectedItem().toString());
            c.setNroDocumento(modalC.txtNroDoc.getText());
            c.setNroTelefono(modalC.txtTelefono.getText().isEmpty() ? "-" : modalC.txtTelefono.getText());
            c.setEmail(modalC.txtEmail.getText().isEmpty() ? "-" : modalC.txtEmail.getText().toLowerCase());
            c.setObervacion(modalC.txtObservacion.getText().isEmpty() ? "-" : modalC.txtObservacion.getText());
            response = repositoryClient.update(c);
            if (response <= 0) {
                JOptionPane.showMessageDialog(modalC, "Error al modifcar datos del cliente, consulte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }
            JOptionPane.showMessageDialog(modalC, "Registro modificado", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            loadTableClients("1");
            PanelReportClient.getInstance().loadTableClients();
            modalC.dispose();
        }
    }

    private String generarCodCliente() {
        List<Cliente> lista = repositoryClient.findAll();
        String codigo;
        if (lista.size() > 0) {
            Cliente ultimoCliente = lista.get(lista.size() - 1);
            String cod = ultimoCliente.getCodCliente();
            Integer codigoCliente = Integer.parseInt(cod.substring(4));
            codigo = String.format("%10s", (codigoCliente + 1)).replace(" ", "0");
        } else {
            codigo = "0000000001";
        }
        return "CLI-" + codigo;
    }

    private boolean isFieldsInvalid() {
        boolean invalid = modalC.txtNombre.getText().isEmpty() || modalC.txtApellidoPaterno.getText().isEmpty() || modalC.txtApellidoMaterno.getText().isEmpty()
                || modalC.txtNroDoc.getText().isEmpty();
        return invalid;
    }

    private void clearTextField() {
        Component[] components = modalC.getContentPane().getComponents();
        for (Component c : components) {
            if (c instanceof JFormattedTextField) {
                ((JFormattedTextField) c).setValue("");
            }
        }
        modalC.txtObservacion.setText("");
    }

    private void mostrarDescripionDocu() {
        List<String> lista = repositoryClient.findAllDocuDescripciones();
        for (String datos : lista) {
            modalC.cbxTipoDoc.addItem(datos);
        }
    }

    private void filterClient() {
        filtroCliente = true;
        repositoryClient = new ClienteRepositoryImp();
        clientsBySearch = new ArrayList<>();
        int index = mainFrame.cbxCriterioBusqueda.getSelectedIndex();

        if (index != 0) {
            String valueSend = mainFrame.txtBuscarCliente.getText().trim();
            String condition = null;
            switch (index) {
                case 1:
                    condition = "cod_cliente";
                    break;
                case 2:
                    condition = "nombre";
                    break;
                case 3:
                    condition = "apellido_paterno";
                    break;
                case 4:
                    condition = "apellido_materno";
                    break;
                case 5:
                    condition = "nro_documento";
                    break;
            }

            String estado;

            //Permite buscar al cliente dependiendo su estado.
            if (mainFrame.chkClienteDescartados.isSelected()) {
                estado = "0";
            } else {
                estado = "1";
            }

            clientsBySearch = repositoryClient.findAllWhere(condition, valueSend, estado);
            loadTableClients(estado);
            filtroCliente = false;
        }
    }

    public void buscarValidarNumDocu() {
        String cad = modalC.cbxTipoDoc.getSelectedItem().toString().toLowerCase();
        String numDocu = modalC.txtNroDoc.getText().trim();
        Gson g = new Gson();

        if (modalC.cbxTipoDoc.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(modalC, "Debe de seleccionar el tipo de documento.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalC.txtNroDoc.getText().isEmpty()) {
            JOptionPane.showMessageDialog(modalC, "Ingrese el DNI o RUC del cliente para la búsqueda y validación.", SYS_MSG, JOptionPane.WARNING_MESSAGE);
            return;
        }

        if (cad.equals("documento nacional de identidad") && numDocu.length() == 8) {
            Cliente c = g.fromJson(Http.httpGetMethod("dni", numDocu), Cliente.class);

            if (c == null) {
                JOptionPane.showMessageDialog(modalC, "El número de documento ingresado es inválido", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }

            String[] datos = Util.separarCadena(c.getNombre(), " ");

            switch (datos.length) {
                case 3:
                    modalC.txtNombre.setText(datos[2]);
                    break;
                case 4:
                    modalC.txtNombre.setText(datos[2] + " " + datos[3]);
                    break;
                case 5:
                    modalC.txtNombre.setText(datos[2] + " " + datos[3] + " " + datos[4]);
                    break;
                case 6:
                    modalC.txtNombre.setText(datos[2] + " " + datos[3] + " " + datos[4] + " " + datos[5]);
                    break;
            }
            modalC.txtApellidoPaterno.setText(c.getApellidoPaterno());
            modalC.txtApellidoMaterno.setText(c.getApellidoMaterno());
            Util.rellenarVacios(modalC);
            return;
        }

        if (cad.equals("registro unico de contribuyentes") || cad.equals("registro único de contribuyentes") && numDocu.length() == 11) {
            Cliente c = g.fromJson(Http.httpGetMethod("ruc", numDocu), Cliente.class);

            if (c == null) {
                JOptionPane.showMessageDialog(modalC, "El número de documento ingresado es inválido", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }

            modalC.txtNombre.setText(c.getNombre());
            Util.rellenarVacios(modalC);
            return;
        }

        JOptionPane.showMessageDialog(modalC, "Número de documento incorrecto.", SYS_MSG, JOptionPane.WARNING_MESSAGE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == modalC.btnRegistrarCliente) {
            registrarCliente();
        }

        if (e.getSource() == modalC.btnModificarCliente) {
            modificarCliente();
        }

        if (e.getSource() == modalC.btnBuscarRUCCliente) {
            buscarValidarNumDocu();
        }

        if (e.getSource() == mainFrame.chkClienteDescartados) {
            if (mainFrame.chkClienteDescartados.isSelected()) {
                loadTableClients("0");
            } else {
                loadTableClients("1");
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            modalC.btnRegistrarCliente.setEnabled(false);
            mostrarDatosCliente(mainFrame.tableClients.getSelectedRow());
            modalC.setVisible(true);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == mainFrame.txtBuscarCliente) {
            filterClient();
        }
    }
}