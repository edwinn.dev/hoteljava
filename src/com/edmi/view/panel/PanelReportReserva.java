package com.edmi.view.panel;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.model.Reservacion;
import com.edmi.model.Trabajador;
import com.edmi.repository.imp.ReservaRepositoryImp;
import com.edmi.repository.imp.TrabajadorRepositoryImp;
import com.edmi.util.Util;
import com.edmi.view.DialogReportFiles;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public final class PanelReportReserva extends javax.swing.JPanel {
    private static PanelReportReserva instance = null;
    private ReservaRepositoryImp repositoryReserva = null;
    private TrabajadorRepositoryImp repositoryTrabajador = null;
    private String opcion = "1";
    private String hojaExcel = "TODAS LAS RESERVACIONES";
    private String nombreArchivo = "";

    private PanelReportReserva() {
        initComponents();
        repositoryReserva = new ReservaRepositoryImp();
        repositoryTrabajador = new TrabajadorRepositoryImp();
        this.rbnTodo.setSelected(true);
        this.txtFecha.setEnabled(false);
        this.cboUsuarios.setEnabled(false);
        showReservas();
        mostrarUsuarios();
        nombreArchivo = "TR_".concat(Util.getDateFormat(new Date()));
        
        this.tablaReservaReportes.getColumnModel().getColumn(0).setMaxWidth(0);
        this.tablaReservaReportes.getColumnModel().getColumn(0).setMinWidth(0);
        this.tablaReservaReportes.getColumnModel().getColumn(0).setPreferredWidth(0);
        
        this.tablaReservaReportes.getColumnModel().getColumn(12).setMaxWidth(0);
        this.tablaReservaReportes.getColumnModel().getColumn(12).setMinWidth(0);
        this.tablaReservaReportes.getColumnModel().getColumn(12).setPreferredWidth(0);
    }

    public static PanelReportReserva getInstance() {
        if (instance == null) {
            instance = new PanelReportReserva();
        }
        return instance;
    }

    public void mostrarUsuarios() {
        List<Trabajador> lista = repositoryTrabajador.findAll();
        cboUsuarios.removeAllItems();
        cboUsuarios.addItem("-- SELECCIONAR --");
        for (Trabajador t : lista) {
            cboUsuarios.addItem(t.getCodTrabajador() + " - " + t.getNombre() + " " + t.getApellidoPaterno() + " " + t.getApellidoMaterno());
        }
    }

    private void almacenarReporte() {
        String path = DialogReportFiles.obtenerRutaArchiReporPdfXml()[0];
        boolean res = generareReporte(path + File.separator + nombreArchivo + ".xlsx");
        if (res) {
            JOptionPane.showMessageDialog(this, "Exportacion generada con exito\n\n"
                    + "RUTA: " + path + File.separator + nombreArchivo, SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(this, "No se pudo generar el reporte,\n\ncontacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }

    private boolean generareReporte(String sheetName) {
        List<Reservacion> reserva = null;
        switch (opcion) {
            case "1":
                reserva = repositoryReserva.findAll();
                break;

            case "2":
                reserva = repositoryReserva.findAllWhere("fecha_ingreso", txtFecha.getText().trim());
                break;

            case "3":
                if (cboUsuarios.getSelectedIndex() > 0) {
                    String[] usuario = Util.separarCadena(cboUsuarios.getSelectedItem().toString(), " ");
                    reserva = repositoryReserva.obtenerTodaReserva(usuario[0]);
                } else {
                    reserva = repositoryReserva.findAll();
                }
                break;
        }

        boolean response = false;
        Workbook book = new XSSFWorkbook();
        Sheet sheet = book.createSheet(this.hojaExcel);

        try {
            File ruta = new File("design" + File.separator + "logo inicio");
            String[] img = ruta.list();
            String logo = ruta.getAbsolutePath() + File.separator + img[0];

            InputStream is = new FileInputStream(logo);
            byte[] bytes = IOUtils.toByteArray(is);
            int imgIndex = book.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG); //Agrega la imagen y su tipo de imagen.
            is.close();

            //Agregamos la imagen al archivo.
            CreationHelper help = book.getCreationHelper();
            Drawing draw = sheet.createDrawingPatriarch(); //Para crear la imagen.

            ClientAnchor anchor = help.createClientAnchor(); //Sacamos el ancho de la imagen para colocarlo de forma correcta.
            anchor.setCol1(0); //Posicion columna.
            anchor.setRow1(1); //Posicion fila.
            Picture pict = draw.createPicture(anchor, imgIndex); //Aparece la imagen.
            pict.resize(1, 5); //Indicamos el tamaño, de donde comenzara y cuanto espacio utilizara.

        } catch (IOException e) {
            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, e);
        }

        CellStyle estiloTitulo = book.createCellStyle(); //Para crear el diseño del titulo.
        estiloTitulo.setAlignment(HorizontalAlignment.CENTER); //Para centrar el titulo.
        estiloTitulo.setVerticalAlignment(VerticalAlignment.CENTER);

        org.apache.poi.ss.usermodel.Font fontTitulo = book.createFont(); //Problemas de compatibilidad con el itextPdf no se podia importar.

        fontTitulo.setFontName("Arial");
        fontTitulo.setBold(true);
        fontTitulo.setFontHeightInPoints((short) 22); //tamaño de letra
        estiloTitulo.setFont(fontTitulo); //agregamos los cambios.
        Row filaTitulo = sheet.createRow(2); //Creamos la fila para el titulo
        Cell celdaTitulo = filaTitulo.createCell(1); //Posicion columna
        celdaTitulo.setCellStyle(estiloTitulo); //Agregamos el estilo
        celdaTitulo.setCellValue("REPORTE DE RESERVAS");

        sheet.addMergedRegion(new CellRangeAddress(2, 2, 1, 3));

        sheet.setColumnWidth(0, 6000);
        sheet.setColumnWidth(1, 7000);
        sheet.setColumnWidth(2, 7000);
        sheet.setColumnWidth(3, 6000);
        sheet.setColumnWidth(4, 6000);
        sheet.setColumnWidth(5, 6000);
        sheet.setColumnWidth(6, 7000);
        sheet.setColumnWidth(7, 6000);
        sheet.setColumnWidth(8, 6000);
        sheet.setColumnWidth(9, 7000);
        sheet.setColumnWidth(10, 8000);

        CellStyle estiloTablaTitulos = book.createCellStyle(); //Para crear el diseño del titulo.
        estiloTablaTitulos.setAlignment(HorizontalAlignment.CENTER); //Para centrar el titulo.
        estiloTablaTitulos.setVerticalAlignment(VerticalAlignment.CENTER);

        String cabecera[] = new String[]{"CODIGO CLIENTE", "CODIGO TRABAJADOR", "NRO HABITACION", "FECHA INGRESO", "FECHA SALIDA", "IMPORTE RESERVA",
            "IMPORTE PRODUCTOS", "IMPORTE TOTAL", "TIEMPO RESERVA", "MAGNITUD RESERVA", "OBSERVACION"};

        CellStyle estiloTablaCabezera = book.createCellStyle();
        estiloTablaCabezera.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        estiloTablaCabezera.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        estiloTablaCabezera.setBorderBottom(BorderStyle.THIN);
        estiloTablaCabezera.setBorderLeft(BorderStyle.THIN);
        estiloTablaCabezera.setBorderRight(BorderStyle.THIN);
        estiloTablaCabezera.setBorderTop(BorderStyle.THIN);

        org.apache.poi.ss.usermodel.Font font = book.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setFontHeightInPoints((short) 12);
        font.setColor(IndexedColors.WHITE.getIndex());
        estiloTablaCabezera.setFont(font);

        Row row = sheet.createRow(7);

        for (int i = 0; i < cabecera.length; i++) {
            Cell celdaCabecera = row.createCell(i);
            celdaCabecera.setCellStyle(estiloTablaCabezera);
            celdaCabecera.setCellValue(cabecera[i]);
        }
        
        CellStyle estiloTablaDatos = book.createCellStyle();
        estiloTablaDatos.setBorderBottom(BorderStyle.THIN);
        estiloTablaDatos.setBorderLeft(BorderStyle.THIN);
        estiloTablaDatos.setBorderRight(BorderStyle.THIN);
        estiloTablaDatos.setBorderTop(BorderStyle.THIN);
        
        int nroRow = 8;
        for (Reservacion r : reserva) {
            Row fila = sheet.createRow(nroRow);
            
            Cell c0 = fila.createCell(0);
            c0.setCellValue(r.getCodCliente());
            c0.setCellStyle(estiloTablaDatos);
            
            Cell c1 = fila.createCell(1);
            c1.setCellValue(r.getCodTrabajador());
            c1.setCellStyle(estiloTablaDatos);
            
            Cell c2 = fila.createCell(2);
            c2.setCellValue(r.getAuxNroRoom());
            c2.setCellStyle(estiloTablaDatos);
            
            Cell c3 = fila.createCell(3);
            c3.setCellValue(r.getFechaIngreso());
            c3.setCellStyle(estiloTablaDatos);
            
            Cell c4 = fila.createCell(4);
            c4.setCellValue(r.getFechaSalida());
            c4.setCellStyle(estiloTablaDatos);
            
            Cell c5 = fila.createCell(5);
            c5.setCellValue(r.getImporteReserva());
            c5.setCellStyle(estiloTablaDatos);
            
            Cell c6 = fila.createCell(6);
            c6.setCellValue(r.getImporteReserva());
            c6.setCellStyle(estiloTablaDatos);
            
            Cell c7 = fila.createCell(7);
            c7.setCellValue(r.getImporteTotal());
            c7.setCellStyle(estiloTablaDatos);
            
            Cell c8 = fila.createCell(8);
            c8.setCellValue(r.getTiempoReserva());
            c8.setCellStyle(estiloTablaDatos);
            
            Cell c9 = fila.createCell(9);
            c9.setCellValue(r.getMagnitudReserva());
            c9.setCellStyle(estiloTablaDatos);
            
            Cell c10 = fila.createCell(10);
            c10.setCellValue(r.getObservacion());
            c10.setCellStyle(estiloTablaDatos);
            
            nroRow++;
        }

        try {
            try (FileOutputStream out = new FileOutputStream(sheetName)) {
                book.write(out);
                response = true;
            }
        } catch (IOException ex) {
            Logger.getLogger(PanelReportProduct.class.getName()).log(Level.SEVERE, null, ex);
            response = false;
        }

        try {
            book.close();
        } catch (IOException ex) {
            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    private void generarPDF() {
        Document documento = new Document();
        documento.setMargins(24f, 24f, 24f, 24f);

        String path = DialogReportFiles.obtenerRutaArchiReporPdfXml()[0];

        try {
            PdfWriter.getInstance(documento, new FileOutputStream(path + File.separator + nombreArchivo + ".pdf"));
            documento.open();
        } catch (DocumentException | HeadlessException | FileNotFoundException e) {
            JOptionPane.showMessageDialog(this, "No se pudo generar el reporte,\n\ncontacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        List<Reservacion> reserva = null;

        switch (opcion) {
            case "1":
                reserva = repositoryReserva.findAll();
                break;

            case "2":
                reserva = repositoryReserva.findAllWhere("fecha_ingreso", txtFecha.getText().trim());
                break;

            case "3":
                if (cboUsuarios.getSelectedIndex() > 0) {
                    String[] usuario = Util.separarCadena(cboUsuarios.getSelectedItem().toString(), " ");
                    reserva = repositoryReserva.obtenerTodaReserva(usuario[0]);
                } else {
                    reserva = repositoryReserva.findAll();
                }
                break;
        }

        try {
            documento.setPageSize(PageSize.A2.rotate());
            documento.open();

            BaseFont fuenteUser = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, true);
            Font tituloUser = new Font(fuenteUser, 20f, Font.BOLD, BaseColor.BLACK);

            BaseFont fuenteUser1 = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, true);
            Font celdaTitulo = new Font(fuenteUser1, 15f, Font.BOLD, BaseColor.RED);

            var titulo = new Paragraph(hojaExcel + "\n\n", tituloUser);
            titulo.setAlignment(Paragraph.ALIGN_CENTER);
            documento.add(titulo);

            PdfPTable tablapdf = new PdfPTable(new float[]{75f, 75f, 75f, 75f, 75f, 75f, 75f, 75f, 75f, 75f, 75f});
            tablapdf.setWidthPercentage(100f);
            tablapdf.addCell(new Phrase("CODIGO CLIENTE", celdaTitulo));
            tablapdf.addCell(new Phrase("CODIGO TRABAJADOR", celdaTitulo));
            tablapdf.addCell(new Phrase("CODIGO HABITACION", celdaTitulo));
            tablapdf.addCell(new Phrase("FECHA INGRESO", celdaTitulo));
            tablapdf.addCell(new Phrase("FECHA SALIDA", celdaTitulo));
            tablapdf.addCell(new Phrase("IMPORTE RESERVA", celdaTitulo));
            tablapdf.addCell(new Phrase("IMPORTE PRODUCTOS", celdaTitulo));
            tablapdf.addCell(new Phrase("IMPORTE TOTAL", celdaTitulo));
            tablapdf.addCell(new Phrase("TIEMPO RESERVA", celdaTitulo));
            tablapdf.addCell(new Phrase("MAGNITUD RESERVA", celdaTitulo));
            tablapdf.addCell(new Phrase("OBSERVACION", celdaTitulo));

            for (Reservacion r : reserva) {
                tablapdf.addCell(r.getCodCliente());
                tablapdf.addCell(r.getCodTrabajador());
                tablapdf.addCell(r.getAuxNroRoom());
                tablapdf.addCell(r.getFechaIngreso());
                tablapdf.addCell(r.getFechaSalida());
                tablapdf.addCell("" + r.getImporteReserva());
                tablapdf.addCell("" + r.getImporteProductos());
                tablapdf.addCell("" + r.getImporteTotal());
                tablapdf.addCell("" + r.getTiempoReserva());
                tablapdf.addCell(r.getMagnitudReserva());
                tablapdf.addCell(r.getObservacion());
            }
            documento.add(tablapdf);
            documento.close();

            JOptionPane.showMessageDialog(this, "Exportación generada con exito\n\n"
                    + "RUTA: " + path + File.separator + nombreArchivo, SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } catch (DocumentException | IOException ex) {
            JOptionPane.showMessageDialog(this, "Error al exportar archivo, intente nuevamente.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }

    public void showReservas() {
        clearTable(tablaReservaReportes);
        List<Reservacion> reservaciones = null;
        switch (opcion) {
            case "1":
                reservaciones = repositoryReserva.findAll();
                break;

            case "2":
                reservaciones = repositoryReserva.findAllWhere("fecha_ingreso", txtFecha.getText().trim());
                break;

            case "3":
                if (cboUsuarios.getSelectedIndex() > 0) {
                    String[] usuario = Util.separarCadena(cboUsuarios.getSelectedItem().toString(), " ");
                    reservaciones = repositoryReserva.obtenerTodaReserva(usuario[0]);
                } else {
                    reservaciones = repositoryReserva.findAll();
                }
                break;
        }

        if(reservaciones != null){
            DefaultTableModel modelReserva = (DefaultTableModel) tablaReservaReportes.getModel();
            reservaciones.forEach((r) -> {
                modelReserva.addRow(new Object[]{
                    r.getCodReserva(), r.getCodCliente(), r.getCodTrabajador(), r.getAuxNroRoom(), r.getNombreCliente(), r.getNombreTrabajador(),
                    r.getFechaIngreso(), r.getFechaSalida(),
                    r.getImporteReserva(), r.getImporteProductos() > 0.0D ? r.getImporteProductos() : "0.00",
                    r.getImporteTotal(), String.valueOf(r.getTiempoReserva()).concat(" ").concat(r.getMagnitudReserva()),
                    r.getEstado(), r.getObservacion(),});
            });
            tablaReservaReportes.setModel(modelReserva);
        }
    }

    private void clearTable(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rbnTodo = new javax.swing.JRadioButton();
        rdbUsuarios = new javax.swing.JRadioButton();
        rdbFecha = new javax.swing.JRadioButton();
        cboUsuarios = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        btnExportarExcel = new javax.swing.JButton();
        btnExportarPDF = new javax.swing.JButton();
        scroll1 = new javax.swing.JScrollPane();
        tablaReservaReportes = new javax.swing.JTable();
        txtFecha = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        rbnTodo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbnTodo.setText("TODO");
        rbnTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbnTodoActionPerformed(evt);
            }
        });

        rdbUsuarios.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rdbUsuarios.setText("USUARIO");
        rdbUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbUsuariosActionPerformed(evt);
            }
        });

        rdbFecha.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rdbFecha.setText("FECHA");
        rdbFecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbFechaActionPerformed(evt);
            }
        });

        cboUsuarios.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cboUsuarios.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --" }));
        cboUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboUsuariosActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        btnExportarExcel.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnExportarExcel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_microsoft_excel_26px.png"))); // NOI18N
        btnExportarExcel.setText("EXPORTAR EXCEL");
        btnExportarExcel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExportarExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarExcelActionPerformed(evt);
            }
        });

        btnExportarPDF.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnExportarPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_pdf_30px.png"))); // NOI18N
        btnExportarPDF.setText("EXPORTAR PDF");
        btnExportarPDF.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExportarPDF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarPDFActionPerformed(evt);
            }
        });

        tablaReservaReportes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "COD. CLIENTE", "COD. USUARIO", "COD. HABITACION", "NOMBRES CLIENTES", "NOMBRES USUARIOS", "FECHA DE INGRESO", "FECHA DE SALIDA", "IMP. RESERVA (S/)", "IMP. PRODUCTOS (S/)", "IMP. TOTAL (S/)", "TIEMPO RESERVA", "ESTADO", "OBSERVACION"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaReservaReportes.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaReservaReportes.setRowHeight(25);
        tablaReservaReportes.getTableHeader().setReorderingAllowed(false);
        scroll1.setViewportView(tablaReservaReportes);
        if (tablaReservaReportes.getColumnModel().getColumnCount() > 0) {
            tablaReservaReportes.getColumnModel().getColumn(0).setPreferredWidth(80);
            tablaReservaReportes.getColumnModel().getColumn(1).setPreferredWidth(120);
            tablaReservaReportes.getColumnModel().getColumn(2).setPreferredWidth(120);
            tablaReservaReportes.getColumnModel().getColumn(3).setPreferredWidth(120);
            tablaReservaReportes.getColumnModel().getColumn(4).setPreferredWidth(300);
            tablaReservaReportes.getColumnModel().getColumn(5).setPreferredWidth(300);
            tablaReservaReportes.getColumnModel().getColumn(6).setPreferredWidth(120);
            tablaReservaReportes.getColumnModel().getColumn(7).setPreferredWidth(120);
            tablaReservaReportes.getColumnModel().getColumn(8).setPreferredWidth(120);
            tablaReservaReportes.getColumnModel().getColumn(9).setPreferredWidth(120);
            tablaReservaReportes.getColumnModel().getColumn(10).setPreferredWidth(120);
            tablaReservaReportes.getColumnModel().getColumn(11).setPreferredWidth(120);
            tablaReservaReportes.getColumnModel().getColumn(12).setPreferredWidth(100);
            tablaReservaReportes.getColumnModel().getColumn(13).setPreferredWidth(200);
        }

        txtFecha.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtFecha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFechaKeyReleased(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel1.setText("REPORTE DE RESERVAS");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbnTodo)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(rdbFecha)
                                .addGap(24, 24, 24)
                                .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(rdbUsuarios)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cboUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 449, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(22, 22, 22)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnExportarExcel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnExportarPDF, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(scroll1, javax.swing.GroupLayout.DEFAULT_SIZE, 892, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addComponent(rbnTodo)
                                .addGap(20, 20, 20)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(rdbFecha)
                                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(20, 20, 20)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(rdbUsuarios)
                                    .addComponent(cboUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(10, 10, 10))
                            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(btnExportarExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnExportarPDF, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(scroll1, javax.swing.GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void rbnTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbnTodoActionPerformed
        opcion = "1";
        hojaExcel = "TODAS LAS RESERVACIONES";
        nombreArchivo = "TR_".concat(Util.getDateFormat(new Date()));

        this.rbnTodo.setSelected(true);
        this.cboUsuarios.setEnabled(false);
        this.txtFecha.setEnabled(false);
        this.rdbFecha.setSelected(false);
        this.rdbUsuarios.setSelected(false);
        this.cboUsuarios.setSelectedIndex(0);
        this.txtFecha.setText("");

        showReservas();
    }//GEN-LAST:event_rbnTodoActionPerformed

    private void rdbFechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbFechaActionPerformed
        opcion = "2";
        hojaExcel = "RESERVACIONES POR FECHA";
        nombreArchivo = "RF_".concat(Util.getDateFormat(new Date()));;

        this.rdbFecha.setSelected(true);
        this.txtFecha.setEnabled(true);
        this.cboUsuarios.setEnabled(false);
        this.rbnTodo.setSelected(false);
        this.rdbUsuarios.setSelected(false);
        this.cboUsuarios.setSelectedIndex(0);
        this.txtFecha.setText("");
    }//GEN-LAST:event_rdbFechaActionPerformed

    private void rdbUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbUsuariosActionPerformed
        opcion = "3";
        hojaExcel = "RESERVACIONES POR USUARIO";
        nombreArchivo = "RU_".concat(Util.getDateFormat(new Date()));

        this.rdbUsuarios.setSelected(true);
        this.cboUsuarios.setEnabled(true);
        this.txtFecha.setEnabled(false);
        this.rbnTodo.setSelected(false);
        this.rdbFecha.setSelected(false);
        this.cboUsuarios.setSelectedIndex(0);
        this.txtFecha.setText("");
    }//GEN-LAST:event_rdbUsuariosActionPerformed

    private void btnExportarExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarExcelActionPerformed
        almacenarReporte();
    }//GEN-LAST:event_btnExportarExcelActionPerformed

    private void btnExportarPDFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarPDFActionPerformed
        generarPDF();
    }//GEN-LAST:event_btnExportarPDFActionPerformed

    private void txtFechaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFechaKeyReleased
        if (evt.getSource() == txtFecha) {
            showReservas();
        }
    }//GEN-LAST:event_txtFechaKeyReleased

    private void cboUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboUsuariosActionPerformed
        showReservas();
    }//GEN-LAST:event_cboUsuariosActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExportarExcel;
    private javax.swing.JButton btnExportarPDF;
    private javax.swing.JComboBox<String> cboUsuarios;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JRadioButton rbnTodo;
    private javax.swing.JRadioButton rdbFecha;
    private javax.swing.JRadioButton rdbUsuarios;
    public javax.swing.JScrollPane scroll1;
    public javax.swing.JTable tablaReservaReportes;
    private javax.swing.JTextField txtFecha;
    // End of variables declaration//GEN-END:variables
}
