package com.edmi.util;

public final class Constant {
    public static final String ROOM_STATE_OCCUPIED = "Ocupado";
    public static final String ROOM_STATE_AVAILABLE = "Disponible";
    public static final String ENVIADO_ACEPTADO = "Enviado y Aceptado SUNAT";
    public static final String GENERAR_XML = "Por Generar XML";
    public static final String XML_GENERADO = "XML Generado";
    public static final String MSG_ENVIADO_ACEPTADO = "Este comprobante ya ha sido generado y enviado a SUNAT.";
    public static final String MSG_NO_ENVIADO = "El comprobante debe ser enviado y aceptado por SUNAT.";
    public static final String MSG_GENERAR_XML = "Primero debe generar el XML en el facturador.";
    public static final String MSG_XML_GENERADO = "Primero debe enviar el XML a la SUNAT.";
}
