package com.edmi.controller.menu;

import com.edmi.controller.ControllerConfigCompany;
import com.edmi.controller.ControllerEmail;
import com.edmi.controller.ControllerUbicacionSFS;
import com.edmi.view.ConfigCompany;
import com.edmi.view.DialogReportFiles;
import com.edmi.view.FrmConfigEmail;
import com.edmi.view.MainPanelServices;
import com.edmi.view.RutaSFS1;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;

public class ControllerArchivos implements ActionListener {
    private final MainPanelServices mainFrame;
    
    public ControllerArchivos(MainPanelServices mainFrame) {
        this.mainFrame = mainFrame;
        setListener();
    }
    
    private void setListener() {
        mainFrame.itemConfiguracion.addActionListener(this);
        mainFrame.itemRutaSFS.addActionListener(this);
        mainFrame.itemRutasArchivos.addActionListener(this);
        mainFrame.itemEmail.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == mainFrame.itemConfiguracion) {
            ConfigCompany configCompany = new ConfigCompany(mainFrame,true);
            ControllerConfigCompany control = new ControllerConfigCompany(configCompany);
            configCompany.setIconImage(new ImageIcon(ControllerConfigCompany.getRutaIconSistema()).getImage());
            control.iniciarVista();
        }
        
        if (e.getSource() == mainFrame.itemRutaSFS) {
            RutaSFS1 rutasfs1 = new RutaSFS1(mainFrame,true);
            ControllerUbicacionSFS control = new ControllerUbicacionSFS(rutasfs1);
            control.iniciarVista();
        }
        
        if (e.getSource() == mainFrame.itemRutasArchivos) {
            DialogReportFiles reportFiles = new DialogReportFiles(mainFrame, true);
            reportFiles.setVisible(true);
        }
        
        if (e.getSource() == mainFrame.itemEmail) {
            FrmConfigEmail frmConfigEmail = new FrmConfigEmail(mainFrame, true);
            ControllerEmail controller = new ControllerEmail(frmConfigEmail, null, null);
            controller.mostrarDatosConfigEmail("");
            frmConfigEmail.setVisible(true);
        }
    }
}