package com.edmi.view.panel;

import com.edmi.controller.panel.ControllerFacura;
import javax.swing.JPanel;

public class PanelMainVoucher extends JPanel {
    private static PanelMainVoucher instance = null;
    
    public static PanelMainVoucher getInstance(){
        if(instance == null){
            instance = new PanelMainVoucher();
        }
        return instance;
    }
    
    private PanelMainVoucher() {
        initComponents();
        setPreferencias();
    }
    
    private void setPreferencias() {
        PanelMainVoucher.tableVouchers.getColumnModel().getColumn(0).setMaxWidth(0);
        PanelMainVoucher.tableVouchers.getColumnModel().getColumn(0).setMinWidth(0);
        PanelMainVoucher.tableVouchers.getColumnModel().getColumn(0).setPreferredWidth(0);
    }
    
    public static void setNullInstance(){
        instance = null;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        btnEnviarCorreoBotelasFacturas = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        tableVouchers = new javax.swing.JTable();
        txtFiltroBoletasFacturas = new javax.swing.JTextField();
        cboCriteriosBoletasFacturas = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        btnActulaizarBoletasFacturas = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        btnEnviarCorreoNotas = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        cboCriteriosNotas = new javax.swing.JComboBox<>();
        txtFiltroNotas = new javax.swing.JTextField();
        btnActualizarNotas = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblNotasCD = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblResumen = new javax.swing.JTable();
        btnActualizarResumenDiario = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblBajas = new javax.swing.JTable();
        btnActualizarComunicBajas = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("LISTA DE COMPROBANTES GENERADOS");

        jTabbedPane1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        btnEnviarCorreoBotelasFacturas.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnEnviarCorreoBotelasFacturas.setText("ENVIAR CORREO");
        btnEnviarCorreoBotelasFacturas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        tableVouchers.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tableVouchers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "NOMBRE O RAZON SOCIAL", "NRO. DOCUMENTO", "MONEDA", "IMPORTE (S/)", "TOTAL (S/)", "FECHA Y HORA", "COMPROBANTE", "SERIE", "NUMERO", "DOC. AFECTADO", "SITUACION SUNAT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableVouchers.setAutoscrolls(false);
        tableVouchers.getTableHeader().setReorderingAllowed(false);
        jScrollPane6.setViewportView(tableVouchers);
        if (tableVouchers.getColumnModel().getColumnCount() > 0) {
            tableVouchers.getColumnModel().getColumn(1).setPreferredWidth(300);
            tableVouchers.getColumnModel().getColumn(2).setPreferredWidth(120);
            tableVouchers.getColumnModel().getColumn(3).setPreferredWidth(70);
            tableVouchers.getColumnModel().getColumn(4).setPreferredWidth(80);
            tableVouchers.getColumnModel().getColumn(5).setPreferredWidth(80);
            tableVouchers.getColumnModel().getColumn(6).setPreferredWidth(130);
            tableVouchers.getColumnModel().getColumn(7).setPreferredWidth(100);
            tableVouchers.getColumnModel().getColumn(8).setPreferredWidth(60);
            tableVouchers.getColumnModel().getColumn(9).setPreferredWidth(70);
            tableVouchers.getColumnModel().getColumn(10).setPreferredWidth(200);
            tableVouchers.getColumnModel().getColumn(11).setPreferredWidth(180);
        }

        txtFiltroBoletasFacturas.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        cboCriteriosBoletasFacturas.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cboCriteriosBoletasFacturas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --", "TODO", "RAZON SOCIAL", "NRO. DOCUMENTO", "FECHA EMISION", "FACTURA", "BOLETA" }));
        cboCriteriosBoletasFacturas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCriteriosBoletasFacturasActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setText("Criterios:");

        btnActulaizarBoletasFacturas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_update_left_rotation_30px.png"))); // NOI18N
        btnActulaizarBoletasFacturas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnActulaizarBoletasFacturas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActulaizarBoletasFacturasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnEnviarCorreoBotelasFacturas)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 315, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboCriteriosBoletasFacturas, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtFiltroBoletasFacturas, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActulaizarBoletasFacturas, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane6))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnEnviarCorreoBotelasFacturas, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(7, 7, 7)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtFiltroBoletasFacturas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cboCriteriosBoletasFacturas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addComponent(btnActulaizarBoletasFacturas, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Boletas y Facturas", jPanel1);

        btnEnviarCorreoNotas.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnEnviarCorreoNotas.setText("ENVIAR CORREO");
        btnEnviarCorreoNotas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText("Criterios:");

        cboCriteriosNotas.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cboCriteriosNotas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --", "TODO", "NOMBRE DOCUMENTO", "FECHA EMISION", "CREDITO", "DEBITO" }));
        cboCriteriosNotas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCriteriosNotasActionPerformed(evt);
            }
        });

        txtFiltroNotas.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        btnActualizarNotas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_update_left_rotation_30px.png"))); // NOI18N
        btnActualizarNotas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnActualizarNotas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarNotasActionPerformed(evt);
            }
        });

        tblNotasCD.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tblNotasCD.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "SERIE - NUMERO", "TIPO NOTA", "DOC. AFECTADO", "NRO. DOCUMENTO", "FECHA EMISION", "HORA EMISION", "DESCRIPCION", "SITUACION SUNAT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblNotasCD.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblNotasCD);
        if (tblNotasCD.getColumnModel().getColumnCount() > 0) {
            tblNotasCD.getColumnModel().getColumn(0).setPreferredWidth(150);
            tblNotasCD.getColumnModel().getColumn(1).setPreferredWidth(120);
            tblNotasCD.getColumnModel().getColumn(2).setPreferredWidth(150);
            tblNotasCD.getColumnModel().getColumn(3).setPreferredWidth(100);
            tblNotasCD.getColumnModel().getColumn(4).setPreferredWidth(100);
            tblNotasCD.getColumnModel().getColumn(5).setPreferredWidth(100);
            tblNotasCD.getColumnModel().getColumn(6).setPreferredWidth(250);
            tblNotasCD.getColumnModel().getColumn(7).setPreferredWidth(300);
        }

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnEnviarCorreoNotas)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 315, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboCriteriosNotas, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtFiltroNotas, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActualizarNotas, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnEnviarCorreoNotas, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGap(7, 7, 7)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtFiltroNotas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cboCriteriosNotas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addComponent(btnActualizarNotas, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Notas", jPanel2);

        tblResumen.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tblResumen.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NRO. DOCUMENTO", "TIPO DOCUMENTO", "DOC. AFECTADO", "FECHA EMISION", "FECHA RESUMEN", "SITUACION SUNAT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblResumen.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tblResumen);
        if (tblResumen.getColumnModel().getColumnCount() > 0) {
            tblResumen.getColumnModel().getColumn(0).setPreferredWidth(100);
            tblResumen.getColumnModel().getColumn(1).setPreferredWidth(100);
            tblResumen.getColumnModel().getColumn(2).setPreferredWidth(100);
            tblResumen.getColumnModel().getColumn(3).setPreferredWidth(100);
            tblResumen.getColumnModel().getColumn(4).setPreferredWidth(100);
            tblResumen.getColumnModel().getColumn(5).setPreferredWidth(300);
        }

        btnActualizarResumenDiario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_update_left_rotation_30px.png"))); // NOI18N
        btnActualizarResumenDiario.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnActualizarResumenDiario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarResumenDiarioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 959, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnActualizarResumenDiario, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(btnActualizarResumenDiario, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Resumen Diario", jPanel3);

        tblBajas.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tblBajas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NRO. DOCUMENTO", "TIPO DE DOCUMENTO", "DOC AFECTADO", "FECHA EMISION", "FECHA DE BAJA", "MOTIVO", "SITUACION SUNAT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblBajas.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tblBajas);
        if (tblBajas.getColumnModel().getColumnCount() > 0) {
            tblBajas.getColumnModel().getColumn(0).setPreferredWidth(100);
            tblBajas.getColumnModel().getColumn(1).setPreferredWidth(100);
            tblBajas.getColumnModel().getColumn(2).setPreferredWidth(100);
            tblBajas.getColumnModel().getColumn(3).setPreferredWidth(100);
            tblBajas.getColumnModel().getColumn(4).setPreferredWidth(100);
            tblBajas.getColumnModel().getColumn(5).setPreferredWidth(300);
            tblBajas.getColumnModel().getColumn(6).setPreferredWidth(300);
        }

        btnActualizarComunicBajas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_update_left_rotation_30px.png"))); // NOI18N
        btnActualizarComunicBajas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnActualizarComunicBajas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarComunicBajasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 959, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnActualizarComunicBajas, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(btnActualizarComunicBajas, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 438, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Comunicación de Baja", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTabbedPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnActulaizarBoletasFacturasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActulaizarBoletasFacturasActionPerformed
        ControllerFacura.loadTableVouchers();
    }//GEN-LAST:event_btnActulaizarBoletasFacturasActionPerformed

    private void btnActualizarNotasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarNotasActionPerformed
        ControllerFacura.loadTableNotasCD();
    }//GEN-LAST:event_btnActualizarNotasActionPerformed

    private void cboCriteriosBoletasFacturasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCriteriosBoletasFacturasActionPerformed
        ControllerFacura.filterVouchers();
    }//GEN-LAST:event_cboCriteriosBoletasFacturasActionPerformed

    private void cboCriteriosNotasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCriteriosNotasActionPerformed
        ControllerFacura.filterNotaCD();
    }//GEN-LAST:event_cboCriteriosNotasActionPerformed

    private void btnActualizarResumenDiarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarResumenDiarioActionPerformed
        ControllerFacura.loadTableResumenDiario();
    }//GEN-LAST:event_btnActualizarResumenDiarioActionPerformed

    private void btnActualizarComunicBajasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarComunicBajasActionPerformed
        ControllerFacura.loadTableComunicacionBaja();
    }//GEN-LAST:event_btnActualizarComunicBajasActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnActualizarComunicBajas;
    public javax.swing.JButton btnActualizarNotas;
    public javax.swing.JButton btnActualizarResumenDiario;
    public javax.swing.JButton btnActulaizarBoletasFacturas;
    public javax.swing.JButton btnEnviarCorreoBotelasFacturas;
    public javax.swing.JButton btnEnviarCorreoNotas;
    public javax.swing.JComboBox<String> cboCriteriosBoletasFacturas;
    public javax.swing.JComboBox<String> cboCriteriosNotas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jTabbedPane1;
    public static javax.swing.JTable tableVouchers;
    public static javax.swing.JTable tblBajas;
    public static javax.swing.JTable tblNotasCD;
    public static javax.swing.JTable tblResumen;
    public javax.swing.JTextField txtFiltroBoletasFacturas;
    public javax.swing.JTextField txtFiltroNotas;
    // End of variables declaration//GEN-END:variables
}
