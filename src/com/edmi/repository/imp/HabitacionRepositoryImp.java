package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.model.Habitacion;
import com.edmi.database.query.Query;
import com.edmi.repository.RepositoryRoom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HabitacionRepositoryImp implements RepositoryRoom<Habitacion> {

    private Connection getConnection() throws SQLException {
        return getInstance();
    }
    
    @Override
    public List<Habitacion> findAllEstado(String estado) {
        List<Habitacion> lista = new ArrayList<>();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_HAB_FIND_ALL_STATE)) {
            ps.setString(1, estado);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Habitacion client = createRoom(rs);
                    lista.add(client);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    @Override
    public List<Habitacion> findAllEstado(String estado, String state) {
        List<Habitacion> lista = new ArrayList<>();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_HAB_FIND_ALL_STATE_TWO)) {
            ps.setString(1, estado);
            ps.setString(2, state);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    Habitacion room = createRoom(rs);
                    lista.add(room);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    @Override
    public int state(Object id, Object estado) {
        int response = 0;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_HAB_STATE)) {
            ps.setString(1, estado.toString());
            ps.setString(2, id.toString());
            response = ps.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }

    @Override
    public List<Habitacion> findAll() {
        List<Habitacion> rooms = new ArrayList<>();
        try (PreparedStatement st = getConnection().prepareStatement(Query.SQL_HAB_FIND_ALL);
                ResultSet rs = st.executeQuery()) {
            while (rs.next()) {
                Habitacion room = createRoom(rs);
                rooms.add(room);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rooms;
    }
    
    @Override
    public List<Habitacion> findAllDesc() {
        List<Habitacion> rooms = new ArrayList<>();
        try (PreparedStatement st = getConnection().prepareStatement(Query.SQL_HAB_FIND_DESC);
                ResultSet rs = st.executeQuery()) {
            while (rs.next()) {
                Habitacion room = createRoom(rs);
                rooms.add(room);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rooms;
    }

    @Override
    public List<Habitacion> findAllAvailable() {
        List<Habitacion> rooms = new ArrayList<>();
        try (PreparedStatement st = getConnection().prepareStatement(Query.SQL_HAB_FIND_ALL_AVAILABLE);
                ResultSet rs = st.executeQuery()) {
            while (rs.next()) {
                Habitacion room = createRoom(rs);
                rooms.add(room);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rooms;
    }

    @Override
    public List<Habitacion> findAllWhere(String condition, String value, String estado) {
        String SQL = "SELECT * FROM habitacion WHERE " + condition + " LIKE " + "'%" + value + "%' AND state = '" + estado + "' ORDER BY nro_habitacion ASC";
//        String SQL = "SELECT * FROM habitacion WHERE " + condition + " LIKE " + "'" + value + "%' AND estado = 'DISPONIBLE' AND state = 1";
        List<Habitacion> rooms = new ArrayList<>();
        try (PreparedStatement st = getConnection().prepareStatement(SQL);
                ResultSet rs = st.executeQuery()) {
            while (rs.next()) {
                Habitacion room = createRoom(rs);
                rooms.add(room);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rooms;
    }
    
    @Override
    public List<Habitacion> findAllWhereFiltrosReserva(String condition, String value, String estado) {
//        String SQL = "SELECT * FROM habitacion WHERE " + condition + " LIKE " + "'" + value + "%' AND state = '" + estado + "' ORDER BY nro_habitacion ASC";
        String SQL = "SELECT * FROM habitacion WHERE " + condition + " LIKE " + "'" + value + "%' AND estado = 'DISPONIBLE' AND state = 1";
        List<Habitacion> rooms = new ArrayList<>();
        try (PreparedStatement st = getConnection().prepareStatement(SQL);
                ResultSet rs = st.executeQuery()) {
            while (rs.next()) {
                Habitacion room = createRoom(rs);
                rooms.add(room);
            }
        } catch (SQLException ex) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rooms;
    }

    @Override
    public Habitacion findById(Object id) {
        Habitacion room = null;
        try (PreparedStatement st = getConnection().prepareStatement(Query.SQL_HAB_FIND_BY_ID)) {
            st.setString(1, id.toString());
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    room = createRoom(rs);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return room;
    }
    
    @Override
    public List<Habitacion> findByIdBuscar(Object id) {
        List<Habitacion> lista = new ArrayList();
        try (PreparedStatement st = getConnection().prepareStatement(Query.SQL_HAB_FIND_BY_ID)) {
            st.setString(1, id.toString());
            try (ResultSet rs = st.executeQuery()) {
                if (rs.next()) {
                    lista.add(createRoom(rs));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @Override
    public int create(Habitacion habitacion) {
        int response = 0;
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_HAB_CREATE)) {
            ps.setString(1, habitacion.getCodhaHabitacion());
            ps.setString(2, habitacion.getCodTipoHabitacion());
            ps.setInt(3, habitacion.getNroHabitacion());
            ps.setString(4, habitacion.getTipoHabitacion());
            ps.setString(5, habitacion.getEstado());
            ps.setInt(6, habitacion.getNroPlanta());
            ps.setDouble(7, habitacion.getCostoAlquiler());
            ps.setString(8, habitacion.getCaracteristicas());
            ps.setString(9, habitacion.getState());
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    @Override
    public int update(Habitacion h) {
        int response = 0;
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_HAB_UPDATE)) {
            ps.setInt(1, h.getNroHabitacion());
            ps.setString(2, h.getTipoHabitacion());
            ps.setString(3, h.getEstado());
            ps.setInt(4, h.getNroPlanta());
            ps.setDouble(5, h.getCostoAlquiler());
            ps.setString(6, h.getCaracteristicas());
            ps.setString(7, h.getCodhaHabitacion());
            response = ps.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }

    @Override
    public int updateState(String codRoom, String state) {
        int response = 0;
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_HAB_UPDATE_STATE)) {
            ps.setString(1, state);
            ps.setString(2, codRoom);
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    @Override
    public int delete(Object id) {
        int response = 0;
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_HAB_DELETE)) {
            ps.setString(1, id.toString());
            response = ps.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }

    @Override
    public String findAllTipeRom(Object tipoHab) {
        String cod = "";
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_HAB_TIPEHAB)) {
            ps.setString(1, tipoHab.toString());
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {                    
                    cod = rs.getString("cod_tipohabitacion");
                }
            } catch (Exception e) {
            }
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return cod;
    }

    private Habitacion createRoom(ResultSet rs) throws SQLException {
        Habitacion h = new Habitacion();
        h.setCodhaHabitacion(rs.getString("cod_habitacion"));
        h.setCodTipoHabitacion(rs.getString("cod_tipohabitacion"));
        h.setNroHabitacion(rs.getInt("nro_habitacion"));
        h.setTipoHabitacion(rs.getString("tipo_habitacion"));
        h.setEstado(rs.getString("estado"));
        h.setNroPlanta(rs.getInt("nro_planta"));
        h.setCostoAlquiler(rs.getDouble("costo_alquiler"));
        h.setCaracteristicas(rs.getString("caracteristicas"));
        h.setState(rs.getString("state"));
        return h;
    }
}
