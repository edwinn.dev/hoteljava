package com.edmi.reports;

import com.edmi.controller.invoice.JsonFiles;
import com.edmi.controller.panel.ControllerFacura;
import com.edmi.model.Company;
import com.edmi.model.DetalleFacbo;
import com.edmi.model.invoice.ResumenDiario;
import com.edmi.repository.imp.CabeceraRepositoryImp;
import com.edmi.repository.imp.CompanyRepositoryImp;
import com.edmi.repository.imp.RepositoryResumen;
import com.edmi.util.NombresArchivosJSON;
import com.edmi.util.Util;
import com.edmi.view.DialogResumenDiario;
import com.edmi.view.MainPanelServices;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;

public class ReportResumenDiario implements ActionListener {
    private DialogResumenDiario dialogResumen;
    private List<DetalleFacbo> detallesBoleta;
    
    public ReportResumenDiario(DialogResumenDiario dialogResumen){
        this.dialogResumen = dialogResumen;
        this.setListener();
    }
    
    private void setListener(){
        this.dialogResumen.btnProcesar.addActionListener(this);
    }
    
    public void initView(MainPanelServices frame, List<DetalleFacbo> detalles){
        this.detallesBoleta = detalles;
        this.dialogResumen.setLocationRelativeTo(frame);
        this.dialogResumen.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == dialogResumen.btnProcesar){
            this.generarResumenDiario();
        }
    }

    private void generarResumenDiario() {
        String idTipoDocResumen = dialogResumen.txtSerieNroResumen.getText().trim();
        RepositoryResumen repoResumen = new RepositoryResumen();
        ResumenDiario resumenResponse = repoResumen.findById(idTipoDocResumen);
        if(resumenResponse != null){
            JOptionPane.showMessageDialog(dialogResumen, "Este documento ya ha sido emitido\nen anteriores ocasiones.","Mensaje", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        int option = JOptionPane.showConfirmDialog(dialogResumen, "¿Desea generar el resumen diario?", "Mensaje del Sistema", JOptionPane.YES_NO_OPTION);
        if(option == JOptionPane.YES_OPTION){
            Double opGravadas = Double.parseDouble(dialogResumen.txtOpGravadas.getText().trim());
            ModeloResumenDiario model = new ModeloResumenDiario();
            List<ResumenDiario> documentos = new ArrayList<>();
            ResumenDiario resumen = new ResumenDiario();
            resumen.setFecEmision(dialogResumen.txtFechaDocAfectado.getText().trim());
            resumen.setFecResumen(Util.getDateFormat(dialogResumen.fechaResumen.getDate()));
            resumen.setTipDocResumen(dialogResumen.txtTipoDocResumen.getText().trim());
            resumen.setIdDocResumen(idTipoDocResumen);
            resumen.setTipDocUsuario(dialogResumen.txtTipoDocUsuario.getText().trim());
            resumen.setNumDocUsuario(dialogResumen.txtNroDocUsuario.getText().trim());
            resumen.setTipMoneda(dialogResumen.txtTipoMoneda.getText().trim());
            resumen.setTotValGrabado(Util.decimalRound(opGravadas));
            resumen.setTotValExoneado(dialogResumen.txtOpExoneradas.getText().trim());
            resumen.setTotValInafecto(dialogResumen.txtOpInafectas.getText().trim());
            resumen.setTotValExportado(dialogResumen.txtOpExportacion.getText().trim());
            resumen.setTotValGratuito(dialogResumen.txtOpGratuitas.getText().trim());
            resumen.setTotOtroCargo(dialogResumen.txtOtrosCargos.getText().trim());
            resumen.setTotImpCpe(dialogResumen.txtImporteTotal.getText().trim());
            resumen.setTipEstado("3");

            List<ResumenDiario.TributosResumen> tributos = new ArrayList<>();

            ResumenDiario.TributosResumen tributoIgv = resumen.new TributosResumen();
            tributoIgv.setIdLineaRd("1");
            if(opGravadas > 0.00){
                tributoIgv.setIdeTributoRd("1000");
                tributoIgv.setNomTributoRd("IGV");
                tributoIgv.setCodTipTributoRd("VAT");
                tributoIgv.setMtoBaseImponibleRd(Util.decimalRound(opGravadas));
                tributoIgv.setMtoTributoRd((opGravadas * 0.18)+"");
            }else{
                tributoIgv.setIdeTributoRd("1000");
                tributoIgv.setNomTributoRd("IGV");
                tributoIgv.setCodTipTributoRd("VAT");
                tributoIgv.setMtoBaseImponibleRd("0.00");
                tributoIgv.setMtoTributoRd("0.00");
            }

            ResumenDiario.TributosResumen tributoExo = resumen.new TributosResumen();
            tributoExo.setIdLineaRd("1");
            tributoExo.setIdeTributoRd("9998");
            tributoExo.setNomTributoRd("INA");
            tributoExo.setCodTipTributoRd("FRE");
            tributoExo.setMtoBaseImponibleRd(Util.decimalRound(opGravadas));
            tributoExo.setMtoTributoRd("0.00");

            ResumenDiario.TributosResumen tributoIna = resumen.new TributosResumen();
            tributoIna.setIdLineaRd("1");
            tributoIna.setIdeTributoRd("9997");
            tributoIna.setNomTributoRd("EXO");
            tributoIna.setCodTipTributoRd("VAT");
            tributoIna.setMtoBaseImponibleRd("0.00");
            tributoIna.setMtoTributoRd("0.00");

            ResumenDiario.TributosResumen tributoOth = resumen.new TributosResumen();
            tributoOth.setIdLineaRd("1");
            tributoOth.setIdeTributoRd("9999");
            tributoOth.setNomTributoRd("OTROS");
            tributoOth.setCodTipTributoRd("OTH");
            tributoOth.setMtoBaseImponibleRd("0.00");
            tributoOth.setMtoTributoRd("0.00");

            tributos.add(tributoIgv);
            tributos.add(tributoExo);
            tributos.add(tributoIna);
            tributos.add(tributoOth);
            resumen.setTributosDocResumen(tributos);
            documentos.add(resumen);
            model.setResumenDiario(documentos);

            JsonFiles jsonFiles = new JsonFiles();
            CompanyRepositoryImp repositoryCompany = new CompanyRepositoryImp();
            Company c = repositoryCompany.getCompany();
            if(c == null){
                JOptionPane.showMessageDialog(dialogResumen, "El RUC es requerido.\n¡Configure los datos de la empresa!", "Mensaje del Sistema", JOptionPane.ERROR_MESSAGE);
                return;
            }
            //10767984176-RC-20220103-002
            String correlativo = NombresArchivosJSON.generarNombreResumenDiario();
            String nombreDocumento = c.getRuc() + "-RC-"+Util.getDateFormat(new Date()).replace("-", "")+"-" + correlativo;
            boolean response = jsonFiles.createFileJson(model, nombreDocumento);
            if (response) {
                JOptionPane.showMessageDialog(dialogResumen, "Resumen generado.", "Mensaje del Sistema", JOptionPane.INFORMATION_MESSAGE);
                
                int responseUpdate = CabeceraRepositoryImp.actualizarEstadoComprobante(this.dialogResumen.txtIdCabecera.getText().trim(), "RESUMEN DIARIO GENERADO");
                if(responseUpdate <= 0){
                    JOptionPane.showMessageDialog(dialogResumen, "No se pudo actualizar estado del comprobante", "Mensaje del Sistema", JOptionPane.INFORMATION_MESSAGE);
                }
                
                //Guardar en la base de datos
                resumen.setCodResumen("RC-"+Util.getDateFormat(new Date()).replace("-", "")+"-" + correlativo);
                int responseDatabase = repoResumen.guardarResumenDiario(resumen);
                if(responseDatabase <= 0){
                    JOptionPane.showMessageDialog(dialogResumen, "Error al registrar en la base de datos", "Mensaje", JOptionPane.ERROR_MESSAGE);
                    //return;
                }
                this.dialogResumen.dispose();
                ControllerFacura.loadTableVouchers();
                ControllerFacura.loadTableResumenDiario();
            } else {
                JOptionPane.showMessageDialog(dialogResumen, "Error al generar Resumen\n¡Contacte con el administrador!", "Mensaje del Sistema", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }
    
    private class ModeloResumenDiario{
        private List<ResumenDiario> resumenDiario;

        public List<ResumenDiario> getResumenDiario() {
            return resumenDiario;
        }

        public void setResumenDiario(List<ResumenDiario> resumenDiario) {
            this.resumenDiario = resumenDiario;
        }
    }
}