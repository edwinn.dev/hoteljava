package com.edmi.controller.invoice;

import com.edmi.controller.ControllerUbicacionSFS;
import com.edmi.controller.panel.ControllerVoucherTicket;
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class JsonFiles {
    
    public boolean createFileJson(Object obj, String voucherName){
        Gson gson = new Gson();
        String gsonResponse = gson.toJson(obj);
        
        String pathSfs = ControllerUbicacionSFS.obtenerRutaSFS()[0];
        
        boolean res = createFileJson(pathSfs.trim(), voucherName, gsonResponse);
        return res;
    }
    
    private static boolean createFileJson(String absolutePath, String fileName, String contentFile){
        boolean response;
        String path;
        if(absolutePath.endsWith(File.separator) || absolutePath.endsWith("\\")){
            path = absolutePath.concat(fileName.concat(".json"));
        }else{
            path = absolutePath.concat(File.separator).concat(fileName.concat(".json"));
        }
        
        File file = new File(path);
        try {
            try (PrintWriter writer = new PrintWriter(file)) {
                writer.println(contentFile);
            }
            response = true;
        } catch (IOException ex) {
            Logger.getLogger(ControllerVoucherTicket.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("Error al crear o leer el archivo json");
            response = false;
        }
        return response;
    }
}