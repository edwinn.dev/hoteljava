package com.edmi.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class NombresArchivosJSON {
    private static File ruta = null;

    public static String getNombreFacturaJSON() {
        ruta = new File("design" + File.separator + "name json" + File.separator + "codigos-F.txt");
        generarNameArchivoJSON(ruta);
        String[] codigos = recuperarDatos();
        String serie = String.format("F%3s", Integer.parseInt(codigos[1])).replace(" ", "0");
        String numero = String.format("%8s", Integer.parseInt(codigos[0])).replace(" ", "0");
        String nombreArchivo = serie + "-" + numero;
        return nombreArchivo;
    }
    
    public static String getNombreBoletaJSON() {
        ruta = new File("design" + File.separator + "name json" + File.separator + "codigos-B.txt");
        generarNameArchivoJSON(ruta);
        String[] codigos = recuperarDatos();
        String serie = String.format("B%3s", Integer.parseInt(codigos[1])).replace(" ", "0");
        String numero = String.format("%8s", Integer.parseInt(codigos[0])).replace(" ", "0");
        String nombreArchivo = serie + "-" + numero;
        return nombreArchivo;
    }
    
    public static String getNombreNotaCreditoFacturaJSON() {
        ruta = new File("design" + File.separator + "name json" + File.separator + "codigos-NCF.txt");
        generarNameArchivoJSON(ruta);
        String[] codigos = recuperarDatos();
        String serie = String.format("NCF%3s", Integer.parseInt(codigos[1])).replace(" ", "0");
        String numero = String.format("%8s", Integer.parseInt(codigos[0])).replace(" ", "0");
        String nombreArchivo = serie + "-" + numero;
        return nombreArchivo;
    }
    
    public static String getNombreNotaCreditoBoletaJSON() {
        ruta = new File("design" + File.separator + "name json" + File.separator + "codigos-NCB.txt");
        generarNameArchivoJSON(ruta);
        String[] codigos = recuperarDatos();
        String serie = String.format("B%3s", Integer.parseInt(codigos[1])).replace(" ", "0");
        String numero = String.format("%8s", Integer.parseInt(codigos[0])).replace(" ", "0");
        String nombreArchivo = serie + "-" + numero;
        return nombreArchivo;
    }
    
    public static String generarNombreNotaDebito() {
        ruta = new File("design" + File.separator + "name json" + File.separator + "codigos-ND.txt");
        generarNameArchivoJSON(ruta);
        String[] codigos = recuperarDatos();
        String serie = String.format("B%3s", Integer.parseInt(codigos[1])).replace(" ", "0");
        String numero = String.format("%8s", Integer.parseInt(codigos[0])).replace(" ", "0");
        String nombreArchivo = serie + "-" + numero;
        return nombreArchivo;
    }
    
    public static String generarNombreResumenDiario() {
        ruta = new File("design" + File.separator + "name json" + File.separator + "codigos-RD.txt");
        generarNameArchivoJSON(ruta);
        String[] codigos = recuperarDatos();
        String numero = String.format("%3s", Integer.parseInt(codigos[0])).replace(" ", "0");
        return numero;
    }
    
    public static String generarNombreComunicacionBaja() {
        ruta = new File("design" + File.separator + "name json" + File.separator + "codigos-CB.txt");
        generarNameArchivoJSON(ruta);
        String[] codigos = recuperarDatos();
        String numero = String.format("%3s", Integer.parseInt(codigos[0])).replace(" ", "0");
        return numero;
    }
    
    public static String getCodigoNotaCredito() {
        ruta = new File("design" + File.separator + "name json" + File.separator + "codigos-NCB.txt");
        String[] codigos = recuperarDatos();
        String serie = String.format("%2s", Integer.parseInt(codigos[1])).replace(" ", "0");
        String numero = String.format("%8s", Integer.parseInt(codigos[0])).replace(" ", "0");
        String nombreArchivo = serie + "-" + numero;
        return nombreArchivo;
    }
    
    public static String getCodigoNotaDebito() {
        ruta = new File("design" + File.separator + "name json" + File.separator + "codigos-ND.txt");
        String[] codigos = recuperarDatos();
        String serie = String.format("%2s", Integer.parseInt(codigos[1])).replace(" ", "0");
        String numero = String.format("%8s", Integer.parseInt(codigos[0])).replace(" ", "0");
        String nombreArchivo = serie + "-" + numero;
        return nombreArchivo;
    }

    private static void generarNameArchivoJSON(File ruta) {
        String[] recuperados = recuperarDatos();
        try {
            PrintWriter pw = new PrintWriter(ruta);
            if (recuperados[0].equals("99999999")) {
                pw.println("1");
                pw.println(Integer.parseInt(recuperados[1]) + 1 + "");
            } else {
                pw.println(Integer.parseInt(recuperados[0]) + 1 + "");
                pw.println(recuperados[1]);
            }
            pw.close();
        } catch (IOException e) {
        }
    }

    private static String[] recuperarDatos() {
        String[] datos = new String[2];
        int cont = 0;
        String bfreader;
        try {
            try (BufferedReader bf = new BufferedReader(new FileReader(ruta.getAbsolutePath()))) {
                while ((bfreader = bf.readLine()) != null) {
                    datos[cont] = bfreader;
                    cont++;
                }
            }
        } catch (IOException e) {
        }
        return datos;
    }
}
