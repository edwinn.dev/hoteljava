
package com.edmi.util;

import com.edmi.view.panel.PanelReportClient;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class EncriptacionPasswordAES {
    private final static String LLAVE = "Hola mundo muchas a todos";
    
    //Clave de encripatacion.
    public static SecretKeySpec crearClave(String llave){
        SecretKeySpec sks = null;
        try {
            byte cadena[] = llave.getBytes("UTF-8"); //Convertimos la llave a bytes, el UTF-8 sirve para acerpetar caracteres especiales.
            MessageDigest md = MessageDigest.getInstance("SHA-512"); //Como parametro se ingresara el metodo de incriptacion.
            cadena = md.digest(cadena);
            cadena = Arrays.copyOf(cadena, 32); //Agregamos el tamaño de bytes.
            sks = new SecretKeySpec(cadena, "AES"); //Se agrega la cadena y la encriptacion.
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, e);
        }
        return sks;
    }
    
    //Encriptar
    public static String encriptar(String encriptar) {
        String encriptacion = "";
        try {
            SecretKeySpec sks = crearClave(LLAVE); //Obtenemos la clave encriptada.
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.ENCRYPT_MODE, sks); //El primer parametro se pasara el modo, y el segundo la clave de incriptacion.
            
            byte cadena[] = encriptar.getBytes("UTF-8");
            byte encriptada[] = c.doFinal(cadena);
            
            encriptacion = Base64.getEncoder().encodeToString(encriptada);
        } catch (Exception e) {
            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, e);
        }
        return encriptacion;
    }
    
    //Desencriptar
    public static String desencriptar(String desencriptar) {
        String cadena_desencriptada = "";
        try {
            SecretKeySpec sks = crearClave(LLAVE); //Obtenemos la clave encriptada.
            Cipher c = Cipher.getInstance("AES");
            c.init(Cipher.DECRYPT_MODE, sks); //El primer parametro se pasara el modo, y el segundo la clave de incriptacion.
            
            byte cadena[] = Base64.getDecoder().decode(desencriptar);
            byte desencriptada[] = c.doFinal(cadena);
            
            cadena_desencriptada = new String(desencriptada);
        } catch (Exception e) {
//            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, e);
        }
        return cadena_desencriptada;
    }
}
