package com.edmi.view;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.repository.imp.CategoriaProductoRepositoryImp;
import com.edmi.model.CategoriaProducto;
import com.edmi.repository.RepositoryCategoryProduct;
import com.edmi.util.MessageView;
import com.edmi.view.panel.PanelReportProduct;
import java.awt.Component;
import java.util.List;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;

public class ModalNewCategory extends javax.swing.JDialog {
    private RepositoryCategoryProduct<CategoriaProducto> categoriaRepository = null;
    private ModalNewProduct modalProduct = null;
    
    public ModalNewCategory(javax.swing.JDialog parent, boolean modal, ModalNewProduct modalProduct) {
        super(parent, modal);
        this.modalProduct = modalProduct;
        initComponents();
        categoriaRepository = new CategoriaProductoRepositoryImp();
        this.txtCodigoCategoria.setText(getCodCategoria());    
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescripcion = new javax.swing.JTextArea();
        txtCategoria = new javax.swing.JFormattedTextField();
        btnRegistrar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtCodigoCategoria = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Nueva Categoria");

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("Nombre de categoría :");

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setText("Descripcion de la categoría:");

        txtDescripcion.setColumns(20);
        txtDescripcion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtDescripcion.setRows(5);
        jScrollPane1.setViewportView(txtDescripcion);

        txtCategoria.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        btnRegistrar.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnRegistrar.setText("REGISTRAR");
        btnRegistrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel3.setText("Codigo :");

        txtCodigoCategoria.setEditable(false);
        txtCodigoCategoria.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtCodigoCategoria.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 364, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCategoria)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtCodigoCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(136, 136, 136)
                        .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(txtCodigoCategoria, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
        categoriaRepository = new CategoriaProductoRepositoryImp();

        if(isFieldsInvalid()){
            JOptionPane.showMessageDialog(this, MessageView.MODAL_CATEGORY_VALIDATE); return;
        }
        
        String received = getCodCategoria();
        CategoriaProducto c = new CategoriaProducto();
        c.setCodCategoria(received);
        c.setNombreCategoria(txtCategoria.getText());
        c.setDescripcion(txtDescripcion.getText().isEmpty() ? "-" : txtDescripcion.getText());
        int response = categoriaRepository.create(c);
        if(response <= 0){
            JOptionPane.showMessageDialog(this, "Error al guardar Categoria", SYS_MSG, JOptionPane.ERROR_MESSAGE); return;
        }
        JOptionPane.showMessageDialog(this, "Categoria guardada", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        clearFields();
        loadCategory();
        PanelReportProduct.getInstance().loadCategory();
        this.dispose();
    }//GEN-LAST:event_btnRegistrarActionPerformed

    private boolean isFieldsInvalid(){
        boolean invalid = txtCategoria.getText().isEmpty() || txtCategoria.getText() == null;
        return invalid;
    }
    
    private void clearFields(){
        Component[] components = getContentPane().getComponents();
        for(Component c : components){
            if(c instanceof JFormattedTextField){
                ((JFormattedTextField) c).setValue("");
            }
        }
        txtDescripcion.setText("");
    }
    
    private String getCodCategoria(){
        List<CategoriaProducto> cts = categoriaRepository.findAll();
        String value;
        if(cts.size() > 0){
            CategoriaProducto ultimaCat = cts.get(cts.size() - 1);
            String uc = ultimaCat.getCodCategoria();//CAT-001//PROV-0001
            Integer n = Integer.parseInt(uc.substring(4).trim());
            value = String.format("%4s", (n+1)).replace(" ", "0");
        }else{
            value = "0001";
        }
        return "CAT-" + value;
    }
    
    private void loadCategory(){
        List<CategoriaProducto> categories = categoriaRepository.findAll();
        modalProduct.cbxCategoria.removeAllItems();
        modalProduct.cbxCategoria.addItem("-- SELECCIONAR --");
        categories.forEach( c ->{
            modalProduct.cbxCategoria.addItem(c.getCodCategoria().concat("    ").concat(c.getNombreCategoria()));
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnRegistrar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JFormattedTextField txtCategoria;
    private javax.swing.JTextField txtCodigoCategoria;
    private javax.swing.JTextArea txtDescripcion;
    // End of variables declaration//GEN-END:variables
}
