package com.edmi.model.invoice;

public class AdicionalCabecera {
    private String ctaBancoNacionDetraccion;
    private String codBienDetraccion;
    private String porDetraccion;
    private String mtoDetraccion;
    private String codMedioPago;
    private String codPaisCliente;
    private String codUbigeoCliente;
    private String desDireccionCliente;
    private String codPaisEntrega;
    private String codUbigeoEntrega;
    private String desDireccionEntrega;

    public String getCtaBancoNacionDetraccion() {
        return ctaBancoNacionDetraccion;
    }

    public void setCtaBancoNacionDetraccion(String ctaBancoNacionDetraccion) {
        this.ctaBancoNacionDetraccion = ctaBancoNacionDetraccion;
    }

    public String getCodBienDetraccion() {
        return codBienDetraccion;
    }

    public void setCodBienDetraccion(String codBienDetraccion) {
        this.codBienDetraccion = codBienDetraccion;
    }

    public String getPorDetraccion() {
        return porDetraccion;
    }

    public void setPorDetraccion(String porDetraccion) {
        this.porDetraccion = porDetraccion;
    }

    public String getMtoDetraccion() {
        return mtoDetraccion;
    }

    public void setMtoDetraccion(String mtoDetraccion) {
        this.mtoDetraccion = mtoDetraccion;
    }

    public String getCodMedioPago() {
        return codMedioPago;
    }

    public void setCodMedioPago(String codMedioPago) {
        this.codMedioPago = codMedioPago;
    }

    public String getCodPaisCliente() {
        return codPaisCliente;
    }

    public void setCodPaisCliente(String codPaisCliente) {
        this.codPaisCliente = codPaisCliente;
    }

    public String getCodUbigeoCliente() {
        return codUbigeoCliente;
    }

    public void setCodUbigeoCliente(String codUbigeoCliente) {
        this.codUbigeoCliente = codUbigeoCliente;
    }

    public String getDesDireccionCliente() {
        return desDireccionCliente;
    }

    public void setDesDireccionCliente(String desDireccionCliente) {
        this.desDireccionCliente = desDireccionCliente;
    }

    public String getCodPaisEntrega() {
        return codPaisEntrega;
    }

    public void setCodPaisEntrega(String codPaisEntrega) {
        this.codPaisEntrega = codPaisEntrega;
    }

    public String getCodUbigeoEntrega() {
        return codUbigeoEntrega;
    }

    public void setCodUbigeoEntrega(String codUbigeoEntrega) {
        this.codUbigeoEntrega = codUbigeoEntrega;
    }

    public String getDesDireccionEntrega() {
        return desDireccionEntrega;
    }

    public void setDesDireccionEntrega(String desDireccionEntrega) {
        this.desDireccionEntrega = desDireccionEntrega;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AdicionalCabecera{ctaBancoNacionDetraccion=").append(ctaBancoNacionDetraccion);
        sb.append(", codBienDetraccion=").append(codBienDetraccion);
        sb.append(", porDetraccion=").append(porDetraccion);
        sb.append(", mtoDetraccion=").append(mtoDetraccion);
        sb.append(", codMedioPago=").append(codMedioPago);
        sb.append(", codPaisCliente=").append(codPaisCliente);
        sb.append(", codUbigeoCliente=").append(codUbigeoCliente);
        sb.append(", desDireccionCliente=").append(desDireccionCliente);
        sb.append(", codPaisEntrega=").append(codPaisEntrega);
        sb.append(", codUbigeoEntrega=").append(codUbigeoEntrega);
        sb.append(", desDireccionEntrega=").append(desDireccionEntrega);
        sb.append('}');
        return sb.toString();
    }
}
