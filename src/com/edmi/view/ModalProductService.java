package com.edmi.view;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.ControllerCliente;
import com.edmi.model.Cliente;
import com.edmi.model.Consumo;
import com.edmi.model.Product;
import com.edmi.model.Reservacion;
import com.edmi.repository.RepositoryConsumo;
import com.edmi.repository.RepositoryProduct;
import com.edmi.repository.RepositoryReservation;
import com.edmi.repository.imp.ConsumoRepositoryImp;
import com.edmi.repository.imp.ProductRepositoryImp;
import com.edmi.repository.imp.ReservaRepositoryImp;
import com.edmi.util.MessageView;
import com.edmi.util.MiRenderer;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import com.edmi.util.Util;
import com.edmi.view.panel.PanelReportVentas;
import com.edmi.view.panel.PanelReservation;
import com.edmi.view.panel.PanelShopping;
import java.util.Date;
import javax.swing.JTable;

public class ModalProductService extends javax.swing.JDialog {
    private RepositoryProduct<Product> repositoryProduct = null;
    private RepositoryConsumo<Consumo> repositoryConsumo = null;
    private RepositoryReservation<Reservacion> repositoryReservation = null;
    private DialogAddProduct dialogAddProduct = null;
    private Double stock;
    private Double quantity;
    private String idProduct;
    private Double priceUnit = 0.00D;
    private Double igv = 0.00D;
    private Double isc = 0.00D;
    private Double icbper = 0.00D;
    private Double descuento = 0.00D;

    public ModalProductService(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setDefaultData();
    }

    private void setDefaultData() {
        txtServiceCodReserva.setVisible(false);
        txtServiceCodTrabajador.setVisible(false);
        txtImporteTotal.setText(txtServiceCostoReserva.getText());
        repositoryReservation = new ReservaRepositoryImp();
        repositoryProduct = new ProductRepositoryImp();
        repositoryConsumo = new ConsumoRepositoryImp();

        this.btnServicio.setVisible(false);
        btnServiceBuscarReserva.setVisible(false);
        btnServiceNuevaReserva.setVisible(false);

        ModalProductService.tableProducts.getColumnModel().getColumn(7).setMaxWidth(0);
        ModalProductService.tableProducts.getColumnModel().getColumn(7).setMinWidth(0);
        ModalProductService.tableProducts.getColumnModel().getColumn(7).setPreferredWidth(0);
    }
    
    private void showRefreshTableProducts(){
        clearTableProducts(PanelShopping.tableProducts);
        DefaultTableModel modelProduct = (DefaultTableModel) PanelShopping.tableProducts.getModel();
        List<Product> products = repositoryProduct.findAll();
        products.forEach((p) -> {
            modelProduct.addRow(new Object[]{
                p.getCodProducto(), p.getNombreProducto(), p.getMarca(), p.getNombreCategoria(), p.getCantidadStock(),
                p.getFechaVenc(), p.getFechaReg(), "-", p.getPrecioCompra(), 
                p.getPrecioVenta(),  p.getMontoIgv(), p.getMontoIcbper(), p.getDescripcion()
            });
        });
        PanelShopping.tableProducts.setDefaultRenderer(Object.class, new MiRenderer());
        PanelShopping.tableProducts.setModel(modelProduct);
    }
    
    private void showReservas(){
        ReservaRepositoryImp repositoryReserva = new ReservaRepositoryImp();
        PanelReservation panelReservation = PanelReservation.getInstance();
        clearTableProducts(panelReservation.tablaReserva);
        List<Reservacion> reservaciones = repositoryReserva.findAll();
        DefaultTableModel modelReserva = (DefaultTableModel) panelReservation.tablaReserva.getModel();
        reservaciones.forEach((r) -> {
            modelReserva.addRow(new Object[]{
                r.getCodReserva(), r.getCodCliente(), r.getCodTrabajador(), r.getAuxNroRoom(), r.getNombreCliente(), r.getNombreTrabajador(),
                r.getFechaIngreso(), r.getFechaSalida(),
                r.getImporteReserva(), r.getImporteProductos() > 0.0D ? r.getImporteProductos() : "0.00",
                r.getImporteTotal(), String.valueOf(r.getTiempoReserva()).concat(" ").concat( r.getMagnitudReserva()),
                r.getEstado(), r.getObservacion(), 
            });
        });
        panelReservation.tablaReserva.setModel(modelReserva);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelReserva = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblServiceCostoReserva = new javax.swing.JLabel();
        txtServiceNombreCliente = new javax.swing.JFormattedTextField();
        txtServiceTipoDocCiente = new javax.swing.JFormattedTextField();
        txtServiceNroDocCliente = new javax.swing.JFormattedTextField();
        txtServiceNombreTrabajador = new javax.swing.JFormattedTextField();
        txtServiceCostoReserva = new javax.swing.JFormattedTextField();
        btnServiceBuscarReserva = new javax.swing.JButton();
        btnServiceNuevaReserva = new javax.swing.JButton();
        txtServiceCodReserva = new javax.swing.JTextField();
        txtServiceCodTrabajador = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtNroHabitacion = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableProducts = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        txtImporteProductos = new javax.swing.JFormattedTextField();
        jLabel8 = new javax.swing.JLabel();
        btnSeriviceAgregarProductos = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtImporteTotal = new javax.swing.JFormattedTextField();
        btnServiceEliminarProducto = new javax.swing.JButton();
        btnServicio = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Registrar consumo de productos");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        panelReserva.setBorder(javax.swing.BorderFactory.createTitledBorder(null, " Reservación", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText("Nombre de cliente:");

        jLabel4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel4.setText("Tipo de documento:");

        jLabel5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel5.setText("Numero de documento:");

        jLabel6.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel6.setText("Nombre de trabajador:");

        lblServiceCostoReserva.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        lblServiceCostoReserva.setText("Costo de reservacion:");

        txtServiceNombreCliente.setEditable(false);
        txtServiceNombreCliente.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtServiceTipoDocCiente.setEditable(false);
        txtServiceTipoDocCiente.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtServiceNroDocCliente.setEditable(false);
        txtServiceNroDocCliente.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtServiceNombreTrabajador.setEditable(false);
        txtServiceNombreTrabajador.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        txtServiceCostoReserva.setEditable(false);
        txtServiceCostoReserva.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        btnServiceBuscarReserva.setText("Buscar Reserva");

        btnServiceNuevaReserva.setText("Nueva reserva");

        txtServiceCodReserva.setEditable(false);

        txtServiceCodTrabajador.setEditable(false);
        txtServiceCodTrabajador.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel9.setText("S/");

        jLabel10.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel10.setText("# Habitacion :");

        txtNroHabitacion.setEditable(false);
        txtNroHabitacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        javax.swing.GroupLayout panelReservaLayout = new javax.swing.GroupLayout(panelReserva);
        panelReserva.setLayout(panelReservaLayout);
        panelReservaLayout.setHorizontalGroup(
            panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelReservaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelReservaLayout.createSequentialGroup()
                        .addGap(408, 408, 408)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtServiceCostoReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelReservaLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtServiceNroDocCliente)
                            .addComponent(txtServiceTipoDocCiente)
                            .addComponent(txtServiceNombreCliente, javax.swing.GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE)
                            .addGroup(panelReservaLayout.createSequentialGroup()
                                .addComponent(txtServiceNombreTrabajador, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtServiceCodTrabajador)))
                        .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelReservaLayout.createSequentialGroup()
                                .addGap(109, 109, 109)
                                .addComponent(lblServiceCostoReserva, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(panelReservaLayout.createSequentialGroup()
                                .addGap(54, 54, 54)
                                .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtNroHabitacion)
                                    .addComponent(txtServiceCodReserva, javax.swing.GroupLayout.DEFAULT_SIZE, 88, Short.MAX_VALUE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelReservaLayout.createSequentialGroup()
                                        .addComponent(btnServiceBuscarReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnServiceNuevaReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addContainerGap())
        );
        panelReservaLayout.setVerticalGroup(
            panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelReservaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(txtServiceNombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtNroHabitacion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtServiceTipoDocCiente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnServiceNuevaReserva)
                    .addComponent(btnServiceBuscarReserva))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtServiceNroDocCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtServiceCodReserva, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblServiceCostoReserva)
                    .addGroup(panelReservaLayout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtServiceCostoReserva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9)))
                    .addGroup(panelReservaLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelReservaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtServiceNombreTrabajador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtServiceCodTrabajador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Consumo - Productos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N

        tableProducts.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tableProducts.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "COD. PRODUCTO", "NOMBRE", "CANTIDAD", "GRATUITO", "P. UNIDAD (S/)", "SUBTOTAL (S/)", "UNIDAD MEDIDA", "ID_RESERVA"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableProducts.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tableProducts.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tableProducts);
        if (tableProducts.getColumnModel().getColumnCount() > 0) {
            tableProducts.getColumnModel().getColumn(0).setPreferredWidth(150);
            tableProducts.getColumnModel().getColumn(1).setPreferredWidth(300);
            tableProducts.getColumnModel().getColumn(2).setPreferredWidth(100);
            tableProducts.getColumnModel().getColumn(3).setPreferredWidth(100);
            tableProducts.getColumnModel().getColumn(4).setPreferredWidth(100);
            tableProducts.getColumnModel().getColumn(5).setPreferredWidth(100);
            tableProducts.getColumnModel().getColumn(6).setPreferredWidth(110);
            tableProducts.getColumnModel().getColumn(7).setResizable(false);
            tableProducts.getColumnModel().getColumn(7).setPreferredWidth(50);
        }

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Costo en productos:");

        txtImporteProductos.setEditable(false);
        txtImporteProductos.setFocusable(false);
        txtImporteProductos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel8.setText("S/");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtImporteProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtImporteProductos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel8))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnSeriviceAgregarProductos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnSeriviceAgregarProductos.setText("AGREGAR PRODUCTOS");
        btnSeriviceAgregarProductos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSeriviceAgregarProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeriviceAgregarProductosActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Monto total del consumo:");

        txtImporteTotal.setEditable(false);
        txtImporteTotal.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        btnServiceEliminarProducto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnServiceEliminarProducto.setText("ELIMINAR");
        btnServiceEliminarProducto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnServiceEliminarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnServiceEliminarProductoActionPerformed(evt);
            }
        });

        btnServicio.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnServicio.setText("AGREGAR SERVICIO");
        btnServicio.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel7.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel7.setText("S/.");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(panelReserva, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtImporteTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnServicio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSeriviceAgregarProductos)
                        .addGap(18, 18, 18)
                        .addComponent(btnServiceEliminarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(panelReserva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSeriviceAgregarProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnServiceEliminarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnServicio, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtImporteTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel7))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSeriviceAgregarProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeriviceAgregarProductosActionPerformed
        DialogAddProduct dialog = new DialogAddProduct(this, true);
        dialog.loadTableProducts();
        this.dialogAddProduct = dialog;
        dialog.btnAgregar.addActionListener((ActionEvent e) -> {
            if (dialog.txtCodProducto.getText().isEmpty()) {
                JOptionPane.showMessageDialog(dialog, "Seleccionar un producto\npara continuar", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (dialog.txtCantidad.getText().isEmpty()) {
                JOptionPane.showMessageDialog(dialog, MessageView.DIALOG_ADD_PRODUCT_QUANTITY);
                return;
            }
            stock = Double.parseDouble(dialog.txtStock.getText().trim());
            quantity = Double.parseDouble(dialog.txtCantidad.getText());
            idProduct = dialog.txtCodProducto.getText().trim();
            priceUnit = Double.parseDouble(dialog.txtPrecioUnitario.getText());
            igv = Double.parseDouble(dialog.txtMontoIgv.getText());
            isc = 0.00D;
            icbper = 0.00d;

            Consumo consumo = new Consumo();
            consumo.setCodReserva(Integer.parseInt(txtServiceCodReserva.getText()));
            consumo.setCodProducto(idProduct);
            consumo.setCantidad(quantity);
            consumo.setPrecioUnitario(Double.parseDouble(dialog.txtPrecioUnitario.getText()));
            consumo.setGratuito(dialog.rbtnIsFree.isSelected() ? dialog.rbtnIsFree.getText() : dialog.rbtnNoFree.getText());
            consumo.setImporteTotal((quantity * priceUnit));
            consumo.setTributoIgv(igv);
            consumo.setTributoIsc(isc);
            consumo.setTributoIcbper(icbper);
            consumo.setDescuento(0.00D);
            consumo.setEstado("PRODUCTO");
            consumo.setUnidadMedida("NIU");
            consumo.setFechaVenta(Util.getDateTimeFormatMachine(new Date()));

            //Actualizando en la base de datos
            registrarConsumo(consumo, dialog);
        });
        dialog.setVisible(true);
        PanelReportVentas.getInstance().mostrarConsumos();
    }//GEN-LAST:event_btnSeriviceAgregarProductosActionPerformed
    
    private void registrarConsumo(Consumo consumo, DialogAddProduct dialog) {
        int rpt = JOptionPane.showConfirmDialog(dialogAddProduct, "¿Desea agregar el producto a la reserva?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if(rpt == JOptionPane.YES_OPTION){
            int response = repositoryConsumo.create(consumo);
            if (response == -1) {
                JOptionPane.showMessageDialog(dialogAddProduct, MessageView.DIALOG_CONSUMO_CREATE_ERROR,SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }
            mostrarConsumos(consumo.getCodReserva());
            calcularImporteReserva();
            actualizarReserva();
            actualizarStock();
            clearTextField();
            dialogAddProduct.dispose();
            
            dialog.txtCantidad.setText("");
            dialog.rbtnNoFree.setSelected(true);
            dialog.txtMontoIgv.setText("0.00");
            showRefreshTableProducts();
            showReservas();
        }
    }

    private void clearTextField() {
        dialogAddProduct.txtNombreProducto.setText("");
        dialogAddProduct.txtCodProducto.setText("");
        dialogAddProduct.txtPrecioUnitario.setText("");
        dialogAddProduct.txtCantidad.setText("");
        dialogAddProduct.txtUnidadmedida.setText("");
        dialogAddProduct.txtStock.setText("");
    }

    private void actualizarStock() {
        Double newStock = (stock - quantity);
        int response = repositoryProduct.updateStock(idProduct, newStock);
        if (response <= 0) {
            JOptionPane.showMessageDialog(this, "Error al actualizar stock de producto.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
        dialogAddProduct.loadTableProducts();
    }

    private void actualizarReserva() {
        Double importeProductos = Double.parseDouble(txtImporteProductos.getText());
        Double importeTotal = Double.parseDouble(txtImporteTotal.getText());
        Integer codReserva = Integer.parseInt(txtServiceCodReserva.getText().trim());

        Reservacion reserva = new Reservacion();
        reserva.setCodReserva(codReserva);
        reserva.setImporteProductos(importeProductos);
        reserva.setImporteTotal(importeTotal);
        int response = repositoryReservation.updateReservaConsumo(reserva);

        if (response == -1) {
            JOptionPane.showMessageDialog(this, "Error al modificar los importes de la reserva", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }

    public void mostrarConsumos(Integer codReserva) {
        List<Consumo> consumos = repositoryConsumo.findByReservation(codReserva);
        clearTableProducts(tableProducts);
        DefaultTableModel model = (DefaultTableModel) tableProducts.getModel();

        consumos.forEach(c -> {
            model.addRow(new Object[]{
                c.getCodProducto(), c.getNombreProducto(), c.getCantidad(), c.getGratuito(),
                c.getPrecioUnitario() <= 0.0D ? "0.00" : c.getPrecioUnitario(),
                c.getImporteTotal() <= 0.0D ? "0.00" : c.getImporteTotal(),
                c.getUnidadMedida(), c.getCodConsumo()
            });
        });
        tableProducts.setModel(model);
        calcularImporteReserva();
    }

    public void clearTableProducts(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        for (int i = tabla.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    private void calcularImporteReserva() {
        Double importeProducts = 0.0D;
        Double importeReserva = Double.parseDouble(ModalProductService.txtServiceCostoReserva.getText().trim());
        for (int i = ModalProductService.tableProducts.getRowCount() - 1; i >= 0; i--) {
            importeProducts += Double.parseDouble(ModalProductService.tableProducts.getModel().getValueAt(i, 5).toString());
        }

        if (importeProducts <= 0.0D) {
            ModalProductService.txtImporteProductos.setText("0.00");
        } else {
            ModalProductService.txtImporteProductos.setText(String.valueOf(importeProducts));
            ModalProductService.txtImporteTotal.setText(String.valueOf(importeProducts + importeReserva));
        }
    }

    public void mostrarDatosCliente(String codClient, String codRoom) {
        Cliente client = ControllerCliente.getClientById(codClient);
        Reservacion reserva = repositoryReservation.findForAddProduct(codRoom);
        ModalProductService.txtServiceNombreCliente.setValue(reserva.getNombreCliente());
        ModalProductService.txtServiceTipoDocCiente.setValue(client.getTipoDocumento());
        ModalProductService.txtServiceNroDocCliente.setValue(client.getNroDocumento());
        ModalProductService.txtServiceNombreTrabajador.setValue(reserva.getNombreTrabajador());
        ModalProductService.txtServiceCodReserva.setText(String.valueOf(reserva.getCodReserva()));

        ModalProductService.btnServiceBuscarReserva.setEnabled(false);
        ModalProductService.btnServiceNuevaReserva.setEnabled(false);
    }

    private void restarCosto() {
        Double totalImporteProduct = 0.00D;
        Double totalImporte = 0.00D;
        int tablaSelect = tableProducts.getSelectedRow();

        if (tablaSelect >= 0) {
            Double recuperado = Double.parseDouble(tableProducts.getValueAt(tablaSelect, 5).toString());
            Double importeProductos = Double.parseDouble(txtImporteProductos.getText().trim());
            Double resultadoImporte = Double.parseDouble(txtImporteTotal.getText().trim());

            totalImporteProduct = importeProductos - recuperado;
            totalImporte = resultadoImporte - recuperado;

            txtImporteProductos.setText(String.valueOf(totalImporteProduct));
            txtImporteTotal.setText(String.valueOf(totalImporte));
            mostrarConsumos(Integer.parseInt(txtServiceCodReserva.getText().trim()));
        }
    }

    private void btnServiceEliminarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnServiceEliminarProductoActionPerformed
        int row = ModalProductService.tableProducts.getSelectedRow();
        if (row < 0) {
            JOptionPane.showMessageDialog(this, "Seleccione un producto para continuar.",SYS_MSG, JOptionPane.ERROR_MESSAGE);
        } else {
            Integer idConsumo = Integer.parseInt(tableProducts.getValueAt(row, 7).toString().trim());
            int rs = JOptionPane.showConfirmDialog(this, "¿Desea eliminar el producto\nasociado a la reserva?", SYS_MSG, JOptionPane.YES_NO_OPTION);
            if (rs == JOptionPane.YES_OPTION) {
                int response = repositoryConsumo.delete(idConsumo);
                if (response <= 0) {
                    JOptionPane.showMessageDialog(this, "Error al eliminar el consumo.",SYS_MSG, JOptionPane.ERROR_MESSAGE);
                    return;
                }
                JOptionPane.showMessageDialog(this, "Producto eliminado del consumo.",SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                updateStockDeleteProduct(row);
                restarCosto();
                clearTableProducts(ModalProductService.tableProducts);
                mostrarConsumos(Integer.parseInt(txtServiceCodReserva.getText().trim()));
            }
        }
    }//GEN-LAST:event_btnServiceEliminarProductoActionPerformed

    private void updateStockDeleteProduct(int row){
        Double deleteStock = Double.parseDouble(tableProducts.getValueAt(row, 2).toString().trim());
        String codProducto = tableProducts.getValueAt(row, 0).toString().trim();
        
        Product producto = repositoryProduct.findById(codProducto);
        
        int response = repositoryProduct.updateStock(codProducto, (deleteStock + producto.getCantidadStock()));
        if(response <= 0){
            JOptionPane.showMessageDialog(this, "Error al actualizar el \nstock del producto.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Reservacion rs = new Reservacion();
        rs.setCodReserva(Integer.parseInt(ModalProductService.txtServiceCodReserva.getText().trim()));
        rs.setImporteProductos(Double.parseDouble(ModalProductService.txtImporteProductos.getText().trim()));
        rs.setImporteTotal(Double.parseDouble(ModalProductService.txtImporteTotal.getText().trim()));
        int response = repositoryReservation.updateReservaConsumo(rs);
        
        if(response < 0){
            JOptionPane.showMessageDialog(this, "Error al actualizar reserva.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
        this.showReservas();
        
        repositoryProduct = null;
        repositoryConsumo = null;
        repositoryReservation = null;
    }//GEN-LAST:event_formWindowClosing

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnSeriviceAgregarProductos;
    public static javax.swing.JButton btnServiceBuscarReserva;
    public javax.swing.JButton btnServiceEliminarProducto;
    public static javax.swing.JButton btnServiceNuevaReserva;
    private javax.swing.JButton btnServicio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    public static javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JLabel lblServiceCostoReserva;
    private javax.swing.JPanel panelReserva;
    public static javax.swing.JTable tableProducts;
    public static javax.swing.JFormattedTextField txtImporteProductos;
    public static javax.swing.JFormattedTextField txtImporteTotal;
    public static javax.swing.JTextField txtNroHabitacion;
    public static javax.swing.JTextField txtServiceCodReserva;
    public static javax.swing.JTextField txtServiceCodTrabajador;
    public static javax.swing.JFormattedTextField txtServiceCostoReserva;
    public static javax.swing.JFormattedTextField txtServiceNombreCliente;
    public static javax.swing.JFormattedTextField txtServiceNombreTrabajador;
    public static javax.swing.JFormattedTextField txtServiceNroDocCliente;
    public static javax.swing.JFormattedTextField txtServiceTipoDocCiente;
    // End of variables declaration//GEN-END:variables
}
