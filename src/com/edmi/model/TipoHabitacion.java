package com.edmi.model;

public class TipoHabitacion {
    private String codTipo;
    private String tipoHabitacion;
    private Integer capacidad;
    private String descripcion;
    
    public TipoHabitacion(){}

    public TipoHabitacion(String codTipo, String tipoHabitacion, Integer capacidad, String descripcion) {
        this.codTipo = codTipo;
        this.tipoHabitacion = tipoHabitacion;
        this.capacidad = capacidad;
        this.descripcion = descripcion;
    }

    public String getCodTipo() {
        return codTipo;
    }

    public void setCodTipo(String codTipo) {
        this.codTipo = codTipo;
    }

    public String getTipoHabitacion() {
        return tipoHabitacion;
    }

    public void setTipoHabitacion(String tipoHabitacion) {
        this.tipoHabitacion = tipoHabitacion.toUpperCase();
    }

    public Integer getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Integer capacidad) {
        this.capacidad = capacidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion.toUpperCase();
    }
}
