package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.database.query.Query;
import com.edmi.model.invoice.ComunicacionBaja;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RepositoryComunicacionBaja {
    
    public ComunicacionBaja findById(String numberDoc){
        ComunicacionBaja com = null;
        try (PreparedStatement st = getInstance().prepareStatement(Query.SQL_COM_FIND_BY_ID)){
            st.setString(1, numberDoc);
            try (ResultSet rs = st.executeQuery()) {
                if(rs.next()){
                    com = new ComunicacionBaja();
                    com.setCodBaja(rs.getString("cod_baja"));
                    com.setNumDocBaja(rs.getString("fecGeneracion"));
                    com.setNumDocBaja(rs.getString("fecComunicacion"));
                    com.setNumDocBaja(rs.getString("tipDocBaja"));
                    com.setNumDocBaja(rs.getString("numDocBaja"));
                    com.setNumDocBaja(rs.getString("desMotivoBaja"));
                    com.setSituacionSunat(rs.getString("situacion_sunat"));
                }
            }
        } catch (SQLException ex) {
            com = null;
            Logger.getLogger(RepositoryComunicacionBaja.class.getName()).log(Level.SEVERE, null, ex);
        }
        return com;
    }
    
    public List<ComunicacionBaja> findAll(){
        List<ComunicacionBaja> lista = new ArrayList();
        try (PreparedStatement st = getInstance().prepareStatement(Query.SQL_COM_FIND_ALL)){
            try (ResultSet rs = st.executeQuery()) {
                while(rs.next()){
                    ComunicacionBaja com = new ComunicacionBaja();
                    com.setCodBaja(rs.getString("cod_baja"));
                    com.setFecGeneracion(rs.getString("fecGeneracion"));
                    com.setFecComunicacion(rs.getString("fecComunicacion"));
                    com.setTipDocBaja(rs.getString("tipDocBaja"));
                    com.setNumDocBaja(rs.getString("numDocBaja"));
                    com.setDesMotivoBaja(rs.getString("desMotivoBaja"));
                    com.setSituacionSunat(rs.getString("situacion_sunat"));
                    lista.add(com);
                }
            }
        } catch (SQLException ex) {
            lista = null;
            Logger.getLogger(RepositoryComunicacionBaja.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    public int saveComunication(ComunicacionBaja com){
        int response;
        try (PreparedStatement ps = getInstance().prepareStatement(Query.SQL_COM_SAVE)){
            ps.setString(1, com.getCodBaja());
            ps.setString(2, com.getFecGeneracion());
            ps.setString(3, com.getFecComunicacion());
            ps.setString(4, com.getTipDocBaja());
            ps.setString(5, com.getNumDocBaja());
            ps.setString(6, com.getDesMotivoBaja());
            ps.setString(7, "-");
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            response = -1;
            Logger.getLogger(RepositoryComunicacionBaja.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }
}