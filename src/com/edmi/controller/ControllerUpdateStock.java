package com.edmi.controller;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.model.Product;
import com.edmi.repository.RepositoryProduct;
import com.edmi.repository.imp.ProductRepositoryImp;
import com.edmi.util.MiRenderer;
import com.edmi.view.DialogUpdateStock;
import com.edmi.view.panel.PanelShopping;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class ControllerUpdateStock implements ActionListener, KeyListener{
    private DialogUpdateStock dialogUpdateStock = null;
    private PanelShopping panelShopping = null;
    private String codProduct = null;
    
    public ControllerUpdateStock(DialogUpdateStock dialogUpdateStock, PanelShopping panelShopping){
        this.dialogUpdateStock = dialogUpdateStock;
        this.panelShopping = panelShopping;
        setListener();
    }
    
    private void setListener(){
        this.dialogUpdateStock.btnActualizar.addActionListener(this);
        this.dialogUpdateStock.txtNuevoStock.addKeyListener(this);
    }
    
    public void loadDataUpdateStock(String idProduct){
        RepositoryProduct<Product> repositoryProduct = new ProductRepositoryImp();
        Product product = repositoryProduct.findById(idProduct);
        this.dialogUpdateStock.txtCodigProducto.setText(product.getCodProducto());
        this.dialogUpdateStock.txtPrecioUnitario.setText(String.valueOf(product.getPrecioVenta()));
        this.dialogUpdateStock.txtStockAnterior.setText(String.valueOf(product.getCantidadStock()));
        this.dialogUpdateStock.txtProducto.setText(product.getNombreProducto());
        this.dialogUpdateStock.txtUnidadMedida.setText(product.getNombreUnidadMedida());
        this.codProduct = idProduct;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.dialogUpdateStock.btnActualizar){
            updateStockProduct();
        }
    }
    
    private void updateStockProduct() {
        if(this.dialogUpdateStock.txtNuevoStock.getText().isEmpty()){
            JOptionPane.showMessageDialog(dialogUpdateStock, "Debe ingresar el nuevo stock.", SYS_MSG, JOptionPane.ERROR_MESSAGE); return;
        }
        
        int option = JOptionPane.showConfirmDialog(this.dialogUpdateStock, "¿Está seguro de actualizar el stock?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if(option == JOptionPane.YES_OPTION){
            RepositoryProduct<Product> repositoryProduct = new ProductRepositoryImp();
            Double newStock = Double.parseDouble(this.dialogUpdateStock.txtNuevoStock.getText());
            Double previousStock = Double.parseDouble(this.dialogUpdateStock.txtStockAnterior.getText());
            int response = repositoryProduct.updateStock(this.codProduct, previousStock + newStock);
            if(response <= 0){
                JOptionPane.showMessageDialog(dialogUpdateStock, "Error al actualizar stock.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }else{
                loadTableProducts();
                JOptionPane.showMessageDialog(dialogUpdateStock, "Stock actualizado.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                this.dialogUpdateStock.dispose();
            }
        }
    }
    
    private void loadTableProducts(){
        RepositoryProduct<Product> repositoryProduct = new ProductRepositoryImp();
        DefaultTableModel modelProduct = (DefaultTableModel) PanelShopping.tableProducts.getModel();
        List<Product> products = repositoryProduct.findAll();
        clearTable(PanelShopping.tableProducts);
         products.forEach((p) -> {
            modelProduct.addRow(new Object[]{
                p.getCodProducto(), p.getNombreProducto(), p.getMarca(), p.getNombreCategoria(), p.getCantidadStock(),
                p.getFechaVenc(), p.getFechaReg(), "-", p.getPrecioCompra(), 
                p.getPrecioVenta(),  p.getMontoIgv(), p.getMontoIcbper(), p.getDescripcion(),
            });
        });
        PanelShopping.tableProducts.setDefaultRenderer(Object.class, new MiRenderer());
        PanelShopping.tableProducts.setModel(modelProduct);
    }

    private void clearTable(JTable table){
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }
    
    @Override
    public void keyTyped(KeyEvent e) {
        char caracter = e.getKeyChar();
        if(Character.isLetter(caracter) || caracter == ','){
            this.dialogUpdateStock.getToolkit().beep();
            e.consume();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {}

    @Override
    public void keyReleased(KeyEvent e) {}
}
