package com.edmi.controller.menu;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.ControllerNewSupplier;
import com.edmi.model.Proveedor;
import com.edmi.repository.RepositorySupplier;
import com.edmi.repository.imp.ProveedorRepositoryImp;
import com.edmi.view.panel.PanelSupplier;
import com.edmi.view.widget.MenuTableSupplier;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class ControllerTableSupplier implements ActionListener {

    private static RepositorySupplier<Proveedor> repositoryProveedor = null;
    private MenuTableSupplier menuProveedor = null;
    private PanelSupplier panelProveedor = null;
    private ControllerNewSupplier controllerProveedor = null;

    public ControllerTableSupplier(PanelSupplier panelProveedor, ControllerNewSupplier controllerProveedor) {
        this.panelProveedor = panelProveedor;
        this.controllerProveedor = controllerProveedor;
        repositoryProveedor = new ProveedorRepositoryImp();
        menuProveedor = MenuTableSupplier.getInstance();
        menuProveedor.initMenuTableClient(panelProveedor.tableSuppliers);
        setListeners();
    }

    private void setListeners() {
        menuProveedor.itemDescartado.addActionListener(this);
    }

    private void cambiarEstado(String estado) {
        int row = panelProveedor.tableSuppliers.getSelectedRow();
        int op = JOptionPane.showConfirmDialog(panelProveedor, "¿Está seguro de eliminar al proveedor?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if (op == JOptionPane.YES_OPTION) {
            if (repositoryProveedor.state(panelProveedor.tableSuppliers.getValueAt(row, 0).toString(), estado) != 0) {
                JOptionPane.showMessageDialog(panelProveedor, "Proveedor eliminado.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                controllerProveedor.loadTableSuplliers("1");
                panelProveedor.chkProveedoresDescartados.setSelected(false);
            } else {
                JOptionPane.showMessageDialog(panelProveedor, "Hubo un problema eliminar el proveedor, \ncontacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == menuProveedor.itemDescartado) {
            cambiarEstado("0");
        }
    }
}
