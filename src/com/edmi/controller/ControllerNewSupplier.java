package com.edmi.controller;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.menu.ControllerTableSupplier;
import com.edmi.model.Proveedor;
import com.edmi.repository.RepositorySupplier;
import com.edmi.repository.imp.ProveedorRepositoryImp;
import com.edmi.util.Http;
import com.edmi.util.Util;
import com.edmi.view.ModalNewSupplier;
import com.edmi.view.panel.PanelSupplier;
import com.google.gson.Gson;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class ControllerNewSupplier implements ActionListener {
    private ModalNewSupplier modalSupplier = null;
    private PanelSupplier panelSupplier = null;
    private ControllerTableSupplier controllerTable = null;

    public ControllerNewSupplier(ModalNewSupplier modalSupplier, PanelSupplier panel) {
        this.modalSupplier = modalSupplier;
        this.panelSupplier = panel;
        controllerTable = new ControllerTableSupplier(panelSupplier, this);
        setListener();
        cargarDatos();
    }

    private void setListener() {
        this.modalSupplier.btnRegistrarProveedor.addActionListener(this);
        this.modalSupplier.btnModificarProveedor.addActionListener(this);
        this.modalSupplier.btnBuscarRuc.addActionListener(this);
    }

    private void cargarDatos() {
        modalSupplier.txtCodigo.setText(getCodeSupplier());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == modalSupplier.btnRegistrarProveedor) {
            nuevoProveedor();
        }

        if (e.getSource() == modalSupplier.btnModificarProveedor) {
            modificarProveedor();
        }

        if (e.getSource() == modalSupplier.btnBuscarRuc) {
            buscarValidarNumDocu();
        }
    }

    public void mostrarDescripionDocu() {
        ProveedorRepositoryImp repositoryProveedor = new ProveedorRepositoryImp();
        List<String> lista = repositoryProveedor.findAllDocuDescripciones();
        for (String datos : lista) {
            modalSupplier.cbxTipoDoc.addItem(datos);
        }
    }

    public void loadTableSuplliers(String estado) {
        clearTable(panelSupplier.tableSuppliers);
        ProveedorRepositoryImp repositoryProveedor = new ProveedorRepositoryImp();
        DefaultTableModel modelSupplier = (DefaultTableModel) panelSupplier.tableSuppliers.getModel();
        List<Proveedor> suppliers = repositoryProveedor.findAllEstado(estado);
        suppliers.forEach((s) -> {
            if(s.getEstado().equals("1")){
                modelSupplier.addRow(new Object[]{
                    s.getCodProveedor(), s.getRazonSocial(), s.getTipoDocumento(), s.getNroDocumento(), s.getDireccion(),
                    s.getProvincia(), s.getTelefonoUno(), s.getTelefonoDos(), s.getEmail(),
                    s.getObservacion(), "REGISTRADO"
                });
            }else{
                modelSupplier.addRow(new Object[]{
                    s.getCodProveedor(), s.getRazonSocial(), s.getTipoDocumento(), s.getNroDocumento(), s.getDireccion(),
                    s.getProvincia(), s.getTelefonoUno(), s.getTelefonoDos(), s.getEmail(),
                    s.getObservacion(), "DESCARTADO"
                });
            }
            
        });
        panelSupplier.tableSuppliers.setModel(modelSupplier);
    }

    private DefaultTableModel clearTable(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        return model;
    }

    private void nuevoProveedor() {
        ProveedorRepositoryImp repositoryProveedor = new ProveedorRepositoryImp();

        if (isFieldsInvalid()) {
            JOptionPane.showMessageDialog(modalSupplier, "Debe rellanar todos los campos.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalSupplier.cbxTipoDoc.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(modalSupplier, "Seleccione el tipo de documento.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        List<String> nroDocumentos = repositoryProveedor.obtenerNroDocu();
        for (String nd : nroDocumentos) {
            if (modalSupplier.txtNroDoc.getText().equals(nd)) {
                JOptionPane.showMessageDialog(modalSupplier, "El cliente ya se encuentra registrado.", SYS_MSG, JOptionPane.WARNING_MESSAGE);
                return;
            }
        }

        Proveedor proveedor = new Proveedor();
        proveedor.setCodProveedor(modalSupplier.txtCodigo.getText());
        proveedor.setCodTipoDoc(repositoryProveedor.findAllTipeDocu(modalSupplier.cbxTipoDoc.getSelectedItem().toString()));
        proveedor.setTipoDocumento(modalSupplier.cbxTipoDoc.getSelectedItem().toString());
        proveedor.setNroDocumento(modalSupplier.txtNroDoc.getText());
        proveedor.setRazonSocial(modalSupplier.txtRazon.getText());
        proveedor.setProvincia(modalSupplier.txtProvincia.getText());
        proveedor.setDireccion(modalSupplier.txtDireccion.getText());
        proveedor.setTelefonoUno(modalSupplier.txtTelefono1.getText());
        proveedor.setTelefonoDos(modalSupplier.txtTelefono2.getText());
        proveedor.setEmail(modalSupplier.txtEmail.getText().trim());
        proveedor.setObservacion(modalSupplier.txtObservacion.getText());
        proveedor.setEstado("1");
        int op = repositoryProveedor.create(proveedor);

        if (op == 0) {
            JOptionPane.showMessageDialog(modalSupplier, "No se pudo registrar el proveedor contacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        JOptionPane.showMessageDialog(modalSupplier, "Proveedor registrado.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);

        loadTableSuplliers("1");
        clearTextField();
        modalSupplier.txtCodigo.setText(getCodeSupplier());
        this.modalSupplier.dispose();
    }

    private void modificarProveedor() {
        ProveedorRepositoryImp repositoryProveedor = new ProveedorRepositoryImp();

        int confirm = JOptionPane.showConfirmDialog(modalSupplier, "¿Desea modificar los datos?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if (confirm == JOptionPane.YES_OPTION) {
            if (modalSupplier.cbxTipoDoc.getSelectedIndex() == 0) {
                JOptionPane.showMessageDialog(modalSupplier, "Debe seleccionar el tipo de documento.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }

            Proveedor proveedor = new Proveedor();
            proveedor.setCodProveedor(modalSupplier.txtCodigo.getText());
            proveedor.setCodTipoDoc(repositoryProveedor.findAllTipeDocu(modalSupplier.cbxTipoDoc.getSelectedItem().toString()));
            proveedor.setTipoDocumento(modalSupplier.cbxTipoDoc.getSelectedItem().toString());
            proveedor.setNroDocumento(modalSupplier.txtNroDoc.getText());
            proveedor.setRazonSocial(modalSupplier.txtRazon.getText());
            proveedor.setDireccion(modalSupplier.txtDireccion.getText());
            proveedor.setProvincia(modalSupplier.txtProvincia.getText());
            proveedor.setTelefonoUno(modalSupplier.txtTelefono1.getText());
            proveedor.setTelefonoDos(modalSupplier.txtTelefono2.getText());
            proveedor.setEmail(modalSupplier.txtEmail.getText().trim());
            proveedor.setObservacion(modalSupplier.txtObservacion.getText());
            int op = repositoryProveedor.update(proveedor);

            if (op == 0) {
                JOptionPane.showMessageDialog(modalSupplier, "No se pudo actualizar el registro contacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }

            JOptionPane.showMessageDialog(modalSupplier, "Registro modificado.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);

            loadTableSuplliers("1");
            modalSupplier.dispose();
        }
    }

    private boolean isFieldsInvalid() {
        boolean invalid = modalSupplier.txtNroDoc.getText().isEmpty() || modalSupplier.txtRazon.getText().isEmpty() || modalSupplier.txtProvincia.getText().isEmpty()
                || modalSupplier.txtDireccion.getText().isEmpty() || modalSupplier.txtEmail.getText().isEmpty();
        return invalid;
    }

    public void mostrarDatosProveedor() {
        ProveedorRepositoryImp repositoryProveedor = new ProveedorRepositoryImp();
        int row = panelSupplier.tableSuppliers.getSelectedRow();
        Proveedor p = repositoryProveedor.findById(panelSupplier.tableSuppliers.getValueAt(row, 0).toString());
        modalSupplier.txtCodigo.setText(p.getCodProveedor());
        modalSupplier.txtRazon.setText(p.getRazonSocial());
        modalSupplier.cbxTipoDoc.setSelectedItem(p.getTipoDocumento());
        modalSupplier.txtNroDoc.setText(p.getNroDocumento());
        modalSupplier.txtDireccion.setText(p.getDireccion());
        modalSupplier.txtProvincia.setText(p.getProvincia());
        modalSupplier.txtTelefono1.setText(p.getTelefonoUno());
        modalSupplier.txtTelefono2.setText(p.getTelefonoDos());
        modalSupplier.txtEmail.setText(p.getEmail());
        modalSupplier.txtObservacion.setText(p.getObservacion());
    }

    private void clearTextField() {
        Component[] components = modalSupplier.modalpanelSuppierls.getComponents();
        for (Component c : components) {
            if (c instanceof JFormattedTextField) {
                ((JFormattedTextField) c).setValue("");
            }
            if (c instanceof JTextField) {
                ((JTextField) c).setText("");
            }
        }
        modalSupplier.txtObservacion.setText("");
        modalSupplier.cbxTipoDoc.setSelectedIndex(0);
    }

    private static String getCodeSupplier() {
        RepositorySupplier<Proveedor> repositoryProveedor = new ProveedorRepositoryImp();
        List<Proveedor> lista = repositoryProveedor.findAll();
        String valor;
        if (lista.size() > 0) {
            Proveedor ultimoProveedor = lista.get(lista.size() - 1);
            String cod = ultimoProveedor.getCodProveedor();
            Integer codigoProveedor = Integer.parseInt(cod.substring(4));
            valor = String.format("%5s", (codigoProveedor+1)).replace(" ", "0");
        } else {
            valor = "00001";
        }
        return "PRV-" + valor;
    }

    public void buscarValidarNumDocu() {
        String cad = modalSupplier.cbxTipoDoc.getSelectedItem().toString().toLowerCase();
        String numDocu = modalSupplier.txtNroDoc.getText().trim();
        Gson g = new Gson();

        if (modalSupplier.cbxTipoDoc.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(modalSupplier, "Debe de seleccionar el tipo de documento.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalSupplier.txtNroDoc.getText().isEmpty()) {
            JOptionPane.showMessageDialog(modalSupplier, "Ingrese el DNI o RUC del cliente para la búsqueda y validación.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (cad.equals("documento nacional de identidad") && numDocu.length() == 8) {
            Proveedor p = g.fromJson(Http.httpGetMethod("dni", numDocu), Proveedor.class);
            modalSupplier.txtRazon.setText(p.getNombre());
            Util.rellenarVacios(modalSupplier);
            return;
        }

        if (cad.equals("registro unico de contribuyentes") || cad.equals("registro único de contribuyentes") && numDocu.length() == 11) {
            Proveedor p = g.fromJson(Http.httpGetMethod("ruc", numDocu), Proveedor.class);
            modalSupplier.txtRazon.setText(p.getNombre());
            modalSupplier.txtProvincia.setText(p.getProvincia());
            modalSupplier.txtDireccion.setText(p.getDireccion());
            Util.rellenarVacios(modalSupplier);
            return;
        }
        JOptionPane.showMessageDialog(modalSupplier, "Número de documento incorrecto.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
    }
}
