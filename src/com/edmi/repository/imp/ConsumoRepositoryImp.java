package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.database.query.Query;
import com.edmi.model.Consumo;
import com.edmi.repository.RepositoryConsumo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConsumoRepositoryImp implements RepositoryConsumo<Consumo>{

    private Connection getConnection() throws SQLException {
        return getInstance();
    }
    
    @Override
    public List<Consumo> findAll() {
        List<Consumo> consumos = new ArrayList<>();
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CONSUMO_FIND)){
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    Consumo consumo = createConsumo(rs);
                    consumos.add(consumo);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsumoRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return consumos;
    }

    @Override
    public List<Consumo> findAllWhere(Object condition, Object value) {
        List<Consumo> lista = new ArrayList();
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CONSUMO_FIND_FILTRO)){
//            ps.setString(1, condition.toString());
            ps.setString(1, value.toString());
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    Consumo consumo = createConsumo(rs);
                    lista.add(consumo);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsumoRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    public List<Consumo> findAllDate(String fechaInicio, String fechaFinal) {
        List<Consumo> lista = new ArrayList();
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CONSUMO_FIND_DATE)){
            ps.setString(1, fechaInicio);
            ps.setString(2, fechaFinal);
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    Consumo consumo = createConsumo(rs);
                    lista.add(consumo);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsumoRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    @Override
    public List<Consumo> findByReservation(Object value) {
        List<Consumo> consumos = new ArrayList<>();
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CONSUMO_FIND_BY_RESERVA)){
            ps.setInt(1, Integer.parseInt(value.toString()));
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    Consumo consumo = createConsumo(rs);
                    consumos.add(consumo);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConsumoRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return consumos;
    }

    @Override
    public Consumo findById(Object id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int create(Consumo consumo) {
        int response = 0;// icbper, igv, isc, descuento
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CONSUMO_CREATE)) {
            ps.setInt(1, consumo.getCodReserva());
            ps.setString(2, consumo.getCodProducto());
            ps.setString(3, consumo.getUnidadMedida());
            ps.setDouble(4, consumo.getCantidad());
            ps.setString(5, consumo.getFechaVenta());
            ps.setString(6, consumo.getGratuito());
            ps.setDouble(7, consumo.getPrecioUnitario());
            ps.setDouble(8, consumo.getImporteTotal());
            ps.setString(9, consumo.getEstado());
            ps.setDouble(10, consumo.getTributoIcbper());
            ps.setDouble(11, consumo.getTributoIgv());
            ps.setDouble(12, consumo.getTributoIsc());
            ps.setDouble(13, consumo.getDescuento());
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    @Override
    public int update(Consumo consumo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int delete(Object id) {
        int response;
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CONSUMO_DELETE)){
            ps.setInt(1, Integer.parseInt(id.toString()));
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ConsumoRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
            response = -1;
        }
        return response;
    }
    
    private Consumo createConsumo(ResultSet rs) throws SQLException{
        Consumo consumo = new Consumo();
        consumo.setCodConsumo(rs.getInt("cod_consumo"));
        consumo.setCodReserva(rs.getInt("cod_reserva"));
        consumo.setCodProducto(rs.getString("cod_producto"));
        consumo.setCantidad(rs.getDouble("cantidad"));
        consumo.setFechaVenta(rs.getString("fecha_venta"));
        consumo.setPrecioUnitario(rs.getDouble("precio_unidad"));
        consumo.setImporteTotal(rs.getDouble("total"));
        consumo.setGratuito(rs.getString("gratuito"));
        consumo.setEstado(rs.getString("estado"));
        consumo.setNombreProducto(rs.getString("nombre_producto"));
        consumo.setUnidadMedida(rs.getString("unidad_medida"));
        consumo.setTributoIcbper(rs.getDouble("icbper"));
        consumo.setTributoIgv(rs.getDouble("igv"));
        consumo.setTributoIsc(rs.getDouble("isc"));
        consumo.setDescuento(rs.getDouble("descuento"));
        return consumo;
    }
}
