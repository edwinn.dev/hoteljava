package com.edmi.util;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

public class QR {
    public static void generarQR(String[] comprobante) {
        String[] datos = comprobante;
        String informacion = datos[0] + "|" + datos[1] + "|" + datos[2] + "|" + datos[3] + "|" + datos[4] + "|"
                + datos[5] + "|" + datos[6] + "|" + datos[7] + "|" + datos[8] + "|";
        
        try {
            QRCodeWriter qrcode = new QRCodeWriter();
            BitMatrix matrix = qrcode.encode(informacion, BarcodeFormat.QR_CODE, 1000, 1000); //Verificamos el tipo y agregamos las dimensiones.
            File f = new File("src/com/edmi/images/qr/qrhotel.png");
            BufferedImage image = new BufferedImage(matrix.getWidth(), matrix.getWidth(), BufferedImage.TYPE_INT_RGB); //Almacenamos una imagen en RGB
            image.createGraphics(); //Creacion del grafico
            
            //Diseñamos el codigo QR
            Graphics2D gd = (Graphics2D) image.getGraphics();
            gd.setColor(Color.white); //Fondo
            gd.fillRect(0, 0, 1000, 1000);
            gd.setColor(Color.black); //QR
            
            //Generar la imagen, obtenemos los valores y lo colocamos al grafico
            for (int i = 0; i < matrix.getWidth(); i++) {
                for (int j = 0; j < matrix.getWidth(); j++) {
                    if (matrix.get(i, j)) {
                        gd.fillRect(i, j, 1, 1);
                    }
                }
            }
            
            //Mostrar la imagen generada
            ImageIO.write(image, "png", f);
        } catch (WriterException | IOException ex) {
            Logger.getLogger(QR.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
