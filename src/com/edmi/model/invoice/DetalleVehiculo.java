package com.edmi.model.invoice;

public class DetalleVehiculo {
    private String idLineaVehiculo;
    private String idTramoVehiculo;
    private String cfgVehiculo;
    private String indFactorRetornoVehiculo;
    private String mtoValorReferencialporTMVehiculo;
    private String tipCargaEfectivaVehiculo;
    private String valCargaEfectivaVehiculo;
    private String tipCargaUtilVehiculo;
    private String valCargaUtilVehiculo;

    public String getIdLineaVehiculo() {
        return idLineaVehiculo;
    }

    public void setIdLineaVehiculo(String idLineaVehiculo) {
        this.idLineaVehiculo = idLineaVehiculo;
    }

    public String getIdTramoVehiculo() {
        return idTramoVehiculo;
    }

    public void setIdTramoVehiculo(String idTramoVehiculo) {
        this.idTramoVehiculo = idTramoVehiculo;
    }

    public String getCfgVehiculo() {
        return cfgVehiculo;
    }

    public void setCfgVehiculo(String cfgVehiculo) {
        this.cfgVehiculo = cfgVehiculo;
    }

    public String getIndFactorRetornoVehiculo() {
        return indFactorRetornoVehiculo;
    }

    public void setIndFactorRetornoVehiculo(String indFactorRetornoVehiculo) {
        this.indFactorRetornoVehiculo = indFactorRetornoVehiculo;
    }

    public String getMtoValorReferencialporTMVehiculo() {
        return mtoValorReferencialporTMVehiculo;
    }

    public void setMtoValorReferencialporTMVehiculo(String mtoValorReferencialporTMVehiculo) {
        this.mtoValorReferencialporTMVehiculo = mtoValorReferencialporTMVehiculo;
    }

    public String getTipCargaEfectivaVehiculo() {
        return tipCargaEfectivaVehiculo;
    }

    public void setTipCargaEfectivaVehiculo(String tipCargaEfectivaVehiculo) {
        this.tipCargaEfectivaVehiculo = tipCargaEfectivaVehiculo;
    }

    public String getValCargaEfectivaVehiculo() {
        return valCargaEfectivaVehiculo;
    }

    public void setValCargaEfectivaVehiculo(String valCargaEfectivaVehiculo) {
        this.valCargaEfectivaVehiculo = valCargaEfectivaVehiculo;
    }

    public String getTipCargaUtilVehiculo() {
        return tipCargaUtilVehiculo;
    }

    public void setTipCargaUtilVehiculo(String tipCargaUtilVehiculo) {
        this.tipCargaUtilVehiculo = tipCargaUtilVehiculo;
    }

    public String getValCargaUtilVehiculo() {
        return valCargaUtilVehiculo;
    }

    public void setValCargaUtilVehiculo(String valCargaUtilVehiculo) {
        this.valCargaUtilVehiculo = valCargaUtilVehiculo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DetalleVehiculo{idLineaVehiculo=").append(idLineaVehiculo);
        sb.append(", idTramoVehiculo=").append(idTramoVehiculo);
        sb.append(", cfgVehiculo=").append(cfgVehiculo);
        sb.append(", indFactorRetornoVehiculo=").append(indFactorRetornoVehiculo);
        sb.append(", mtoValorReferencialporTMVehiculo=").append(mtoValorReferencialporTMVehiculo);
        sb.append(", tipCargaEfectivaVehiculo=").append(tipCargaEfectivaVehiculo);
        sb.append(", valCargaEfectivaVehiculo=").append(valCargaEfectivaVehiculo);
        sb.append(", tipCargaUtilVehiculo=").append(tipCargaUtilVehiculo);
        sb.append(", valCargaUtilVehiculo=").append(valCargaUtilVehiculo);
        sb.append('}');
        return sb.toString();
    }
}
