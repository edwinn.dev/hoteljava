package com.edmi.controller.panel;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.ControllerNewProduct;
import com.edmi.controller.ControllerNewSupplier;
import com.edmi.controller.menu.ControllerTableProduct;
import com.edmi.model.Product;
import com.edmi.model.Proveedor;
import com.edmi.repository.RepositoryProduct;
import com.edmi.repository.RepositorySupplier;
import com.edmi.repository.imp.ProductRepositoryImp;
import com.edmi.repository.imp.ProveedorRepositoryImp;
import com.edmi.util.MiRenderer;
import com.edmi.view.DialogReportFiles;
import com.edmi.view.MainPanelServices;
import com.edmi.view.ModalNewProduct;
import com.edmi.view.ModalNewSupplier;
import com.edmi.view.panel.PanelReportClient;
import com.edmi.view.panel.PanelReportProduct;
import com.edmi.view.panel.PanelShopping;
import com.edmi.view.panel.PanelSupplier;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ControllerShopping implements ActionListener, KeyListener, MouseListener {
    private MainPanelServices mainFrame = null;
    private PanelShopping panelShopping = null;
    private PanelSupplier panelSupplier = null;
    private RepositoryProduct<Product> repositoryProduct = null;
    private RepositorySupplier<Proveedor> repositorySupplier = null;
    private ControllerTableProduct controllerTableProduct = null;
    private ModalNewSupplier modalSupplier = null;
    private ControllerNewSupplier controllNewSupplier = null;

    private boolean filtroProveedor = false;
    private List<Proveedor> listaProveedor;

    public ControllerShopping(MainPanelServices mainFrame) {
        this.mainFrame = mainFrame;
        this.panelShopping = new PanelShopping();
        this.panelSupplier = new PanelSupplier();
        this.repositoryProduct = new ProductRepositoryImp();
        this.controllerTableProduct = new ControllerTableProduct(this.panelShopping, mainFrame);
        this.repositorySupplier = new ProveedorRepositoryImp();
        this.modalSupplier = ModalNewSupplier.getInstance(mainFrame, true);

        this.mainFrame.panelCompras.setLayout(new GridBagLayout());

        addComponents(this.mainFrame.panelCompras, panelShopping);
        addComponents(this.mainFrame.panelCompras, panelSupplier);

        this.panelShopping.setVisible(true);
        this.panelSupplier.setVisible(false);

        setListener();
        loadTableProducts();
        loadTableSuplliers("1");
    }

    private void setListener() {
        panelShopping.btnAgregarProducto.addActionListener(this);
        panelShopping.btnImportarExcel.addActionListener(this);
        panelShopping.btnExportarExcel.addActionListener(this);
        panelShopping.btnProveedores.addActionListener(this);
        panelShopping.txtBuscarProducto.addKeyListener(this);

        panelSupplier.btnProveedorRegresar.addActionListener(this);
        panelSupplier.txtBuscarProveedor.addKeyListener(this);
        panelSupplier.tableSuppliers.addMouseListener(this);
        panelSupplier.btnAgregarProveedor.addActionListener(this);
        panelSupplier.chkProveedoresDescartados.addActionListener(this);
    }

    private void loadTableSuplliers(String estado) {
        clearTable(panelSupplier.tableSuppliers);
        DefaultTableModel modelSupplier = (DefaultTableModel) panelSupplier.tableSuppliers.getModel();
        List<Proveedor> suppliers = repositorySupplier.findAllEstado(estado);
        if (filtroProveedor) {
            listaProveedor.forEach(s -> {
                if (s.getEstado().equals("1")) {
                    modelSupplier.addRow(new Object[]{s.getCodProveedor(), s.getRazonSocial(), s.getTipoDocumento(), s.getNroDocumento(), s.getDireccion(),
                        s.getProvincia(), s.getTelefonoUno(), s.getTelefonoDos(), s.getEmail(),
                        s.getObservacion(), "REGISTRADO"});
                } else {
                    modelSupplier.addRow(new Object[]{s.getCodProveedor(), s.getRazonSocial(), s.getTipoDocumento(), s.getNroDocumento(), s.getDireccion(),
                        s.getProvincia(), s.getTelefonoUno(), s.getTelefonoDos(), s.getEmail(),
                        s.getObservacion(), "DESCARTADO"});
                }
            });
        } else {
            suppliers.forEach(s -> {
                if (s.getEstado().equals("1")) {
                    modelSupplier.addRow(new Object[]{s.getCodProveedor(), s.getRazonSocial(), s.getTipoDocumento(), s.getNroDocumento(), s.getDireccion(),
                        s.getProvincia(), s.getTelefonoUno(), s.getTelefonoDos(), s.getEmail(),
                        s.getObservacion(), "REGISTRADO"});
                } else {
                    modelSupplier.addRow(new Object[]{s.getCodProveedor(), s.getRazonSocial(), s.getTipoDocumento(), s.getNroDocumento(), s.getDireccion(),
                        s.getProvincia(), s.getTelefonoUno(), s.getTelefonoDos(), s.getEmail(),
                        s.getObservacion(), "DESCARTADO"});
                }
            });
        }
        panelSupplier.tableSuppliers.setModel(modelSupplier);
    }

    private void loadTableProducts() {
        clearTable(PanelShopping.tableProducts);
        DefaultTableModel modelProduct = (DefaultTableModel) PanelShopping.tableProducts.getModel();
        List<Product> products = repositoryProduct.findAll();
        products.forEach((p) -> {
            modelProduct.addRow(new Object[]{
                p.getCodProducto(), p.getNombreProducto(), p.getMarca(), p.getNombreCategoria(), p.getCantidadStock(),
                p.getFechaVenc(), p.getFechaReg(), "-", p.getPrecioCompra(),
                p.getPrecioVenta(), p.getMontoIgv(), p.getMontoIcbper(), p.getDescripcion()});
        });
        PanelShopping.tableProducts.setDefaultRenderer(Object.class, new MiRenderer());
        PanelShopping.tableProducts.setModel(modelProduct);
    }

    private DefaultTableModel clearTable(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        return model;
    }

    private void importarDatosExcel() {
        JFileChooser jfc = new JFileChooser();
        jfc.setFileFilter(new FileNameExtensionFilter("*.xls", "xls"));
        jfc.setFileFilter(new FileNameExtensionFilter("*.xlsx", "xlsx"));
        if (jfc.showDialog(panelShopping, "Seleccionar archivo") == JFileChooser.APPROVE_OPTION) {
            File archivo = jfc.getSelectedFile();
            if (archivo.getName().endsWith("xls") || archivo.getName().endsWith("xlsx")) { //Comprueba si la extension es correcta.
                try {
                    FileInputStream fis = new FileInputStream(archivo.getAbsolutePath());
                    XSSFWorkbook libro = new XSSFWorkbook(fis); //Extraemos el archivo
                    XSSFSheet hoja = libro.getSheetAt(0); //Recuperamos la hoja en este caso la primera hoja
                    int numeroFilas = hoja.getLastRowNum();
                    for (int i = 1; i <= numeroFilas; i++) {
                        Product p = new Product();
                        Row fila = hoja.getRow(i);
                        try {
                            p.setCodProducto(fila.getCell(0).getStringCellValue());
                            p.setCodCategoria(fila.getCell(1).getStringCellValue());
                            p.setCodUnidadMedida(fila.getCell(2).getStringCellValue());
                            p.setNombreProducto(fila.getCell(3).getStringCellValue());
                            p.setCantidadStock(fila.getCell(4).getNumericCellValue());
                            p.setPrecioCompra(fila.getCell(5).getNumericCellValue());
                            p.setPrecioVenta(fila.getCell(6).getNumericCellValue());
                            p.setFechaReg(fila.getCell(7).getStringCellValue());
                            p.setFechaVenc(fila.getCell(8).getStringCellValue());
                            p.setCantidadSalida(fila.getCell(9).getNumericCellValue());
                            p.setEstado(fila.getCell(10).getStringCellValue());
                            p.setDescripcion(fila.getCell(11).getStringCellValue());
                            p.setCodSunat(fila.getCell(12).getStringCellValue());
                            p.setTipoAfectacionIgv(fila.getCell(13).getStringCellValue());
                            p.setMontoIgv(fila.getCell(14).getNumericCellValue());
                            p.setTributoIcbper(fila.getCell(15).getStringCellValue());
                            p.setMontoIcbper(fila.getCell(16).getNumericCellValue());
                            p.setMarca(fila.getCell(17).getStringCellValue());
                            int op = repositoryProduct.create(p);
                            if (op <= 0) {
                                JOptionPane.showMessageDialog(panelShopping, "Hay duplicación de productos.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                                return;
                            }
                        } catch (HeadlessException e) {
                            loadTableProducts();
                        }
                    }
                    JOptionPane.showMessageDialog(panelShopping, "Importación exitosa.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ControllerShopping.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ControllerShopping.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                JOptionPane.showMessageDialog(panelShopping, "Elija un formato válido.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void exportarDatosExcel() {
        List<Product> productos = repositoryProduct.findAll();;
        Workbook book = new XSSFWorkbook();
        Sheet sheet = book.createSheet("PRODUCTOS");
        sheet.setColumnWidth(0, 5500);
        sheet.setColumnWidth(1, 5500);
        sheet.setColumnWidth(2, 4000);
        sheet.setColumnWidth(3, 12000);
        sheet.setColumnWidth(4, 3000);
        sheet.setColumnWidth(5, 6500);
        sheet.setColumnWidth(6, 6500);
        sheet.setColumnWidth(7, 6000);
        sheet.setColumnWidth(8, 6000);
        sheet.setColumnWidth(9, 6000);
        sheet.setColumnWidth(10, 5000);
        sheet.setColumnWidth(11, 6000);
        sheet.setColumnWidth(12, 6000);
        sheet.setColumnWidth(13, 6000);
        sheet.setColumnWidth(14, 6000);
        sheet.setColumnWidth(15, 6000);
        sheet.setColumnWidth(16, 6000);
        sheet.setColumnWidth(17, 6000);

        CellStyle estiloTablaTitulos = book.createCellStyle(); //Para crear el diseño del titulo.
        estiloTablaTitulos.setAlignment(HorizontalAlignment.CENTER); //Para centrar el titulo.
        estiloTablaTitulos.setVerticalAlignment(VerticalAlignment.CENTER);

        String cabecera[] = new String[]{"COD PRODUCTO", "COD CATEGORIA", "COD UNIDAD", "NOMBRE PRODUCTO", "STOCK", "PRECIO COMPRA(S/)",
            "PRECIO VENTA(S/)", "FECHA REGISTRO", "FECHA MOVIMIENTO", "CANTIDAD SALIDA", "ESTADO", "DESCRIPCION", "COD SUNAT", "TIPO AFECTACION",
            "MONTO IGV(S/)", "TRIBUTO ICBPER", "MONTO ICBPER", "MARCA"};

        CellStyle estiloTablaCabezera = book.createCellStyle();
        estiloTablaCabezera.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        estiloTablaCabezera.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        estiloTablaCabezera.setBorderBottom(BorderStyle.THIN);
        estiloTablaCabezera.setBorderLeft(BorderStyle.THIN);
        estiloTablaCabezera.setBorderRight(BorderStyle.THIN);
        estiloTablaCabezera.setBorderTop(BorderStyle.THIN);

        org.apache.poi.ss.usermodel.Font font = book.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setFontHeightInPoints((short) 12);
        font.setColor(IndexedColors.WHITE.getIndex());
        estiloTablaCabezera.setFont(font);

        Row row = sheet.createRow(0); //Titulo de celdas

        for (int i = 0; i < cabecera.length; i++) {
            Cell celdaCabecera = row.createCell(i);
            celdaCabecera.setCellStyle(estiloTablaCabezera);
            celdaCabecera.setCellValue(cabecera[i]);
        }

        int nroRow = 1;
        for (Product p : productos) {
            Row fila = sheet.createRow(nroRow);

            if (p.getCantidadStock() < 5) {
                CellStyle estiloAdvertenciaStock = book.createCellStyle();
                estiloAdvertenciaStock.setFillForegroundColor(IndexedColors.RED.getIndex());
                estiloAdvertenciaStock.setFillPattern(FillPatternType.SOLID_FOREGROUND);
                estiloAdvertenciaStock.setBorderBottom(BorderStyle.THIN);
                estiloAdvertenciaStock.setBorderLeft(BorderStyle.THIN);
                estiloAdvertenciaStock.setBorderRight(BorderStyle.THIN);
                estiloAdvertenciaStock.setBorderTop(BorderStyle.THIN);

                Cell c0 = fila.createCell(0);
                c0.setCellValue(p.getCodProducto());
                c0.setCellStyle(estiloAdvertenciaStock);

                Cell c1 = fila.createCell(1);
                c1.setCellValue(p.getCodCategoria());
                c1.setCellStyle(estiloAdvertenciaStock);

                Cell c2 = fila.createCell(2);
                c2.setCellValue(p.getCodUnidadMedida());
                c2.setCellStyle(estiloAdvertenciaStock);

                Cell c3 = fila.createCell(3);
                c3.setCellValue(p.getNombreProducto());
                c3.setCellStyle(estiloAdvertenciaStock);

                Cell c4 = fila.createCell(4);
                c4.setCellValue(p.getCantidadStock());
                c4.setCellStyle(estiloAdvertenciaStock);

                Cell c5 = fila.createCell(5);
                c5.setCellValue(p.getPrecioCompra());
                c5.setCellStyle(estiloAdvertenciaStock);

                Cell c6 = fila.createCell(6);
                c6.setCellValue(p.getPrecioVenta());
                c6.setCellStyle(estiloAdvertenciaStock);

                Cell c7 = fila.createCell(7);
                c7.setCellValue(p.getFechaReg());
                c7.setCellStyle(estiloAdvertenciaStock);

                Cell c8 = fila.createCell(8);
                c8.setCellValue(p.getFechaVenc());
                c8.setCellStyle(estiloAdvertenciaStock);

                Cell c9 = fila.createCell(9);
                c9.setCellValue(p.getCantidadSalida());
                c9.setCellStyle(estiloAdvertenciaStock);

                Cell c10 = fila.createCell(10);
                c10.setCellValue(p.getEstado());
                c10.setCellStyle(estiloAdvertenciaStock);

                Cell c11 = fila.createCell(11);
                c11.setCellValue(p.getDescripcion());
                c11.setCellStyle(estiloAdvertenciaStock);

                Cell c12 = fila.createCell(12);
                c12.setCellValue(p.getCodSunat());
                c12.setCellStyle(estiloAdvertenciaStock);

                Cell c13 = fila.createCell(13);
                c13.setCellValue(p.getTipoAfectacionIgv());
                c13.setCellStyle(estiloAdvertenciaStock);

                Cell c14 = fila.createCell(14);
                c14.setCellValue(p.getMontoIgv());
                c14.setCellStyle(estiloAdvertenciaStock);

                Cell c15 = fila.createCell(15);
                c15.setCellValue(p.getTributoIcbper());
                c15.setCellStyle(estiloAdvertenciaStock);

                Cell c16 = fila.createCell(16);
                c16.setCellValue(p.getMontoIcbper());
                c16.setCellStyle(estiloAdvertenciaStock);

                Cell c17 = fila.createCell(17);
                c17.setCellValue(p.getMarca());
                c17.setCellStyle(estiloAdvertenciaStock);
            } 
            else {
                CellStyle estiloTablaDatos = book.createCellStyle();
                estiloTablaDatos.setBorderBottom(BorderStyle.THIN);
                estiloTablaDatos.setBorderLeft(BorderStyle.THIN);
                estiloTablaDatos.setBorderRight(BorderStyle.THIN);
                estiloTablaDatos.setBorderTop(BorderStyle.THIN);

                Cell c0 = fila.createCell(0);
                c0.setCellValue(p.getCodProducto());
                c0.setCellStyle(estiloTablaDatos);

                Cell c1 = fila.createCell(1);
                c1.setCellValue(p.getCodCategoria());
                c1.setCellStyle(estiloTablaDatos);

                Cell c2 = fila.createCell(2);
                c2.setCellValue(p.getCodUnidadMedida());
                c2.setCellStyle(estiloTablaDatos);

                Cell c3 = fila.createCell(3);
                c3.setCellValue(p.getNombreProducto());
                c3.setCellStyle(estiloTablaDatos);

                Cell c4 = fila.createCell(4);
                c4.setCellValue(p.getCantidadStock());
                c4.setCellStyle(estiloTablaDatos);

                Cell c5 = fila.createCell(5);
                c5.setCellValue(p.getPrecioCompra());
                c5.setCellStyle(estiloTablaDatos);

                Cell c6 = fila.createCell(6);
                c6.setCellValue(p.getPrecioVenta());
                c6.setCellStyle(estiloTablaDatos);

                Cell c7 = fila.createCell(7);
                c7.setCellValue(p.getFechaReg());
                c7.setCellStyle(estiloTablaDatos);

                Cell c8 = fila.createCell(8);
                c8.setCellValue(p.getFechaVenc());
                c8.setCellStyle(estiloTablaDatos);

                Cell c9 = fila.createCell(9);
                c9.setCellValue(p.getCantidadSalida());
                c9.setCellStyle(estiloTablaDatos);

                Cell c10 = fila.createCell(10);
                c10.setCellValue(p.getEstado());
                c10.setCellStyle(estiloTablaDatos);

                Cell c11 = fila.createCell(11);
                c11.setCellValue(p.getDescripcion());
                c11.setCellStyle(estiloTablaDatos);

                Cell c12 = fila.createCell(12);
                c12.setCellValue(p.getCodSunat());
                c12.setCellStyle(estiloTablaDatos);

                Cell c13 = fila.createCell(13);
                c13.setCellValue(p.getTipoAfectacionIgv());
                c13.setCellStyle(estiloTablaDatos);

                Cell c14 = fila.createCell(14);
                c14.setCellValue(p.getMontoIgv());
                c14.setCellStyle(estiloTablaDatos);

                Cell c15 = fila.createCell(15);
                c15.setCellValue(p.getTributoIcbper());
                c15.setCellStyle(estiloTablaDatos);

                Cell c16 = fila.createCell(16);
                c16.setCellValue(p.getMontoIcbper());
                c16.setCellStyle(estiloTablaDatos);

                Cell c17 = fila.createCell(17);
                c17.setCellValue(p.getMarca());
                c17.setCellStyle(estiloTablaDatos);
            }

            nroRow++;
        }

        String path = DialogReportFiles.obtenerRutaArchiReporPdfXml()[0];
        try {
            try ( FileOutputStream out = new FileOutputStream(path + File.separator + "Productos.xlsx")) {
                book.write(out);
            }
            JOptionPane.showMessageDialog(panelShopping, "Exportación exitosa.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
            Logger.getLogger(PanelReportProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            book.close();
        } catch (IOException ex) {
            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == panelShopping.btnAgregarProducto) {
            ModalNewProduct modalForm = new ModalNewProduct(mainFrame, true);
            ControllerNewProduct controllerNewProduct = new ControllerNewProduct(modalForm, panelShopping);
            controllerNewProduct.loadDataFromNewProduct();
            modalForm.btnActualizarProducto.setEnabled(false);
            modalForm.setVisible(true);
            return;
        }

        if (e.getSource() == panelShopping.btnImportarExcel) {
            importarDatosExcel();
            loadTableProducts();
        }

        if (e.getSource() == panelShopping.btnExportarExcel) {
            int op = JOptionPane.showConfirmDialog(panelShopping, "¿Desea obtener el formato de productos?", SYS_MSG, JOptionPane.YES_NO_OPTION);
            if (op == JOptionPane.YES_OPTION) {
                exportarDatosExcel();
            }
        }

        if (e.getSource() == panelShopping.btnProveedores) {
            this.controllNewSupplier = new ControllerNewSupplier(modalSupplier, panelSupplier);
            panelShopping.setVisible(false);
            panelSupplier.setVisible(true);
            return;
        }

        if (e.getSource() == panelSupplier.btnAgregarProveedor) {
            ModalNewSupplier modal = new ModalNewSupplier(mainFrame, true);
            ControllerNewSupplier control = new ControllerNewSupplier(modal, panelSupplier);
            control.mostrarDescripionDocu();
            modal.btnRegistrarProveedor.setEnabled(true);
            modal.btnModificarProveedor.setEnabled(false);
            modal.setVisible(true);
            return;
        }

        if (e.getSource() == panelSupplier.btnProveedorRegresar) {
            returnToMenu();
            this.controllNewSupplier = null;
        }

        if (e.getSource() == panelSupplier.chkProveedoresDescartados) {
            if (panelSupplier.chkProveedoresDescartados.isSelected()) {
                loadTableSuplliers("0");
            } else {
                loadTableSuplliers("1");
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == panelSupplier.txtBuscarProveedor) {
            filterSupplier();
        }

        if (e.getSource() == panelShopping.txtBuscarProducto) {
            if (panelShopping.txtBuscarProducto.getText().trim().isEmpty()) {
                loadTableProducts();
                return;
            }
            String value = this.panelShopping.txtBuscarProducto.getText().trim();
            List<Product> productsFilters = repositoryProduct.findAllWhere("nombre_producto", value);
            filterProducts(productsFilters);
        }
    }

    private void filterProducts(List<Product> productsFilters) {
        DefaultTableModel modelProduct = (DefaultTableModel) PanelShopping.tableProducts.getModel();
        clearTable(PanelShopping.tableProducts);
        productsFilters.forEach((p) -> {
            modelProduct.addRow(new Object[]{
                p.getCodProducto(), p.getNombreProducto(), p.getMarca(), p.getNombreCategoria(), p.getCantidadStock(),
                p.getFechaVenc(), p.getFechaReg(), "-", p.getPrecioCompra(),
                p.getPrecioVenta(), p.getMontoIgv(), p.getMontoIcbper(), p.getDescripcion()
            });
        });
        PanelShopping.tableProducts.setModel(modelProduct);
    }

    private void returnToMenu() {
        panelSupplier.setVisible(false);
        panelShopping.setVisible(true);
    }

    private void addComponents(JComponent parent, JComponent child) {
        GridBagConstraints conf = new GridBagConstraints();
        conf.gridx = 0;
        conf.gridy = 0;
        conf.gridwidth = 1;
        conf.gridheight = 1;
        conf.weightx = 1.0;
        conf.weighty = 1.0;
        conf.anchor = GridBagConstraints.CENTER;
        conf.fill = GridBagConstraints.BOTH;

        parent.add(child, conf);
        child.setVisible(true);
    }

    public void filterSupplier() {
        listaProveedor = new ArrayList();
        int index = panelSupplier.cbxCriteriosProveedor.getSelectedIndex();

        filtroProveedor = true;

        if (index != 0) {
            String valueSend = panelSupplier.txtBuscarProveedor.getText().trim();
            String condition = null;
            switch (index) {
                case 1:
                    condition = "cod_proveedor";
                    break;
                case 2:
                    condition = "razon_social";
                    break;
                case 3:
                    condition = "tipo_documento";
                    break;
                case 4:
                    condition = "nro_documento";
                    break;
            }

            String estado;

            if (panelSupplier.chkProveedoresDescartados.isSelected()) {
                estado = "0";
            } else {
                estado = "1";
            }

            listaProveedor = repositorySupplier.findAllWhere(condition, valueSend, estado);
            loadTableSuplliers(estado);
            filtroProveedor = false;
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            controllNewSupplier.mostrarDescripionDocu();
            controllNewSupplier.mostrarDatosProveedor();
            modalSupplier.btnRegistrarProveedor.setEnabled(false);
            modalSupplier.btnModificarProveedor.setEnabled(true);
            modalSupplier.setVisible(true);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
