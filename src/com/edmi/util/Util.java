package com.edmi.util;

import com.edmi.database.ConnectionDb;
import com.edmi.repository.imp.CompanyRepositoryImp;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Util {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private static final SimpleDateFormat DATE_FORMAT_MACHINE = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat DATE = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss");
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.00");
    private static final String DEFAULT_PRICE = "0.00";
    private static final String CURRENCY = "S/ ";
    private static final String MAGNITUD_RESERVA = "Horas";

    public static String getMagnitudeTimeReserva() {
        return Util.MAGNITUD_RESERVA;
    }

//    public static String getDefaultPrice() {
//        String defaultPrice = CURRENCY.concat(DEFAULT_PRICE);
//        return defaultPrice;
//    }
//    public static String convertToCurrency(Double price) {
//        String priceFormat = DECIMAL_FORMAT.format(price);
//        return CURRENCY.concat(String.valueOf(priceFormat));
//    }
//    public static Double convertToPriceDecimal(String currency) {
//        Double price = Double.parseDouble(currency.substring(3).trim());
//        Double priceReturn = Double.parseDouble(DECIMAL_FORMAT.format(price));
//        return priceReturn;
//    }
    public static String getDateTimeFormat(Date date) {
        String currentTime = DATE_FORMAT.format(date);
        return currentTime;
    }
    
    public static String getDateTimeFormatMachine(Date date) {
        String currentTime = DATE_FORMAT_MACHINE.format(date);
        return currentTime;
    }

    public static Date getDateCalendar(String dateString) throws ParseException {
        Date date = DATE_FORMAT.parse(dateString);
        return date;
    }

    public static String getTimeFormat(Date date) {
        String time = TIME_FORMAT.format(date);
        return time;
    }

    public static String getDateTimeConcat(String date, String time) {
        String fechaHoraConcatenado = date.concat(" ").concat(time);
        return fechaHoraConcatenado;
    }

    public static String getDateFormat(Date date) {
        String dateString = DATE.format(date);
        return dateString;
    }

    public static String concatTime(String hour, String minute) {
        return hour.concat(":").concat(minute).concat(":").concat("00");
    }

    public static String concatDateTime(String date, String hour, String minute) {
        return date.concat(" ").concat(hour).concat(":").concat(minute).concat(":").concat("00");
    }

    public static Double getPriceFormat(Double price) {
        Double priceFormatted = Double.parseDouble(DECIMAL_FORMAT.format(price));
        return priceFormatted;
    }

    public static String getUserNamesConcatened(String name, String paternalSurname, String maternalSurname) {
        String returnValue = name.concat(" ").concat(paternalSurname).concat(" ").concat(maternalSurname);
        return returnValue;
    }

    public static String getUserEmailFormatted(String userEmail) {
        String email = userEmail.toLowerCase().trim();
        return email;
    }

    public static String getDateTimeReservation(String dateTime, long days, long hours, long minutes) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        LocalDateTime dateTimeLocal = LocalDateTime.parse(dateTime, formatter);
        dateTimeLocal = dateTimeLocal.plusDays(days);
        dateTimeLocal = dateTimeLocal.plusHours(hours);
        dateTimeLocal = dateTimeLocal.plusMinutes(minutes);
        return dateTimeLocal.format(formatter);
    }

    public static int convetHoursToMinutes(Integer hoursQuantity, Integer minutesQuantity) {
        int minutes = ((hoursQuantity * 60) + minutesQuantity);
        return minutes;
    }

    public static int getExpectedTime(String dateInput, String dateOutput) {
        Calendar c = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        try {
            Date dIngreso = Util.getDateCalendar(dateInput);
            Date dSalida = Util.getDateCalendar(dateOutput);
            c.setTime(dIngreso);
            c2.setTime(dSalida);
        } catch (ParseException ex) {
            System.out.println("Error no se puedo convertir la fecha");
        }

        int diaIngreso = c.get(Calendar.DATE);
        int mesIngreso = c.get(Calendar.MONTH) + 1;
        //int yearIngreso = c.get(Calendar.YEAR);
        int horaIngreso = c.get(Calendar.HOUR);
        int minIngreso = c.get(Calendar.MINUTE);
        //int secIngreso = c.get(Calendar.SECOND);

        int diaSalida = c2.get(Calendar.DATE);
        int mesSalida = c2.get(Calendar.MONTH) + 1;
        //int yearSalida = c2.get(Calendar.YEAR);
        int horaSalida = c2.get(Calendar.HOUR);
        int minSalida = c2.get(Calendar.MINUTE);
        //int secSalida = c2.get(Calendar.SECOND);

        //int yearRes = yearSalida - yearIngreso;
        int mesRes = mesSalida - mesIngreso;
        int diaRes = diaSalida - diaIngreso;
        int horaRes = horaSalida - horaIngreso;
        int minRes = minSalida - minIngreso;
        //int secRes = secSalida - secIngreso;

        int minutes = (minRes + (horaRes * 60) + (diaRes * 1440) + (mesRes * 43800));

        return minutes;
    }

    public static String[] separarCadena(String cadASeparar, String separarPor) {
        String[] arraySeparado = cadASeparar.split(separarPor);
//        for (int i = 0; i < arraySeparado.length; i++) {
//            System.out.println(arraySeparado[i]);
//        }
        return arraySeparado;
    }

    public static void rellenarVacios(JDialog dialogo) {
        Component[] comp = dialogo.getContentPane().getComponents();
        for (Component c : comp) {
            if (c instanceof JTextField) {
                if (((JTextField) c).getText().isEmpty()) {
                    ((JTextField) c).setText("-");
                }
            }
            if (c instanceof JFormattedTextField) {
                if (((JFormattedTextField) c).getText().isEmpty()) {
                    ((JFormattedTextField) c).setText("-");
                }
            }
        }
    }

    public static void rellenarVacios(JPanel panel) {
        Component[] comp = panel.getComponents();
        for (Component c : comp) {
            if (c instanceof JTextField) {
                if (((JTextField) c).getText().isEmpty()) {
                    ((JTextField) c).setText("-");
                }
            }
            if (c instanceof JFormattedTextField) {
                if (((JFormattedTextField) c).getText().isEmpty()) {
                    ((JFormattedTextField) c).setText("-");
                }
            }
        }
    }

    public static void restriccionLetras(JTextField text) {
        text.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char teclaSeleccionada = e.getKeyChar();
                if (!Character.isDigit(teclaSeleccionada) && teclaSeleccionada != '.' || Character.isSpaceChar(teclaSeleccionada)) {
                    e.consume();
                }
                if (teclaSeleccionada == '.' && text.getText().contains(".")) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
    }

    public static void restriccionNumeros(JTextField text) {
        text.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char teclaSeleccionada = e.getKeyChar();
                if (!Character.isLetter(teclaSeleccionada)) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
    }

    public static void limiteCaracteres(JTextField text, int limite) {
        text.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (text.getText().length() == limite) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
    }

    public static String nombreEmpresa() {
        String nombre = "";
        try {
            if (ConnectionDb.getInstance() != null) {
                CompanyRepositoryImp cri = new CompanyRepositoryImp();
                nombre = cri.findName();
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se pudo establecer la conexión con la base de datos,\ncompruebe los datos de conexión.", "Mensaje del sistema", JOptionPane.ERROR_MESSAGE);
        }
        return nombre;
    }
    
    public static String decimalRound(Double valor) {
        return String.format("%.2f", valor);
    }
}
