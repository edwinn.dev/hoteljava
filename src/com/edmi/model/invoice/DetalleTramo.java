package com.edmi.model.invoice;

public class DetalleTramo {
    private String idLineaTramo;
    private String idTramo;
    private String desTramo;
    private String mtoValorReferencialCargaEfectiva;
    private String mtoValorReferencialCargaNominal;
    private String ubiOrigenTramo;
    private String ubiDestinoTramo;

    public String getIdLineaTramo() {
        return idLineaTramo;
    }

    public void setIdLineaTramo(String idLineaTramo) {
        this.idLineaTramo = idLineaTramo;
    }

    public String getIdTramo() {
        return idTramo;
    }

    public void setIdTramo(String idTramo) {
        this.idTramo = idTramo;
    }

    public String getDesTramo() {
        return desTramo;
    }

    public void setDesTramo(String desTramo) {
        this.desTramo = desTramo;
    }

    public String getMtoValorReferencialCargaEfectiva() {
        return mtoValorReferencialCargaEfectiva;
    }

    public void setMtoValorReferencialCargaEfectiva(String mtoValorReferencialCargaEfectiva) {
        this.mtoValorReferencialCargaEfectiva = mtoValorReferencialCargaEfectiva;
    }

    public String getMtoValorReferencialCargaNominal() {
        return mtoValorReferencialCargaNominal;
    }

    public void setMtoValorReferencialCargaNominal(String mtoValorReferencialCargaNominal) {
        this.mtoValorReferencialCargaNominal = mtoValorReferencialCargaNominal;
    }

    public String getUbiOrigenTramo() {
        return ubiOrigenTramo;
    }

    public void setUbiOrigenTramo(String ubiOrigenTramo) {
        this.ubiOrigenTramo = ubiOrigenTramo;
    }

    public String getUbiDestinoTramo() {
        return ubiDestinoTramo;
    }

    public void setUbiDestinoTramo(String ubiDestinoTramo) {
        this.ubiDestinoTramo = ubiDestinoTramo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DetalleTramo{idLineaTramo=").append(idLineaTramo);
        sb.append(", idTramo=").append(idTramo);
        sb.append(", desTramo=").append(desTramo);
        sb.append(", mtoValorReferencialCargaEfectiva=").append(mtoValorReferencialCargaEfectiva);
        sb.append(", mtoValorReferencialCargaNominal=").append(mtoValorReferencialCargaNominal);
        sb.append(", ubiOrigenTramo=").append(ubiOrigenTramo);
        sb.append(", ubiDestinoTramo=").append(ubiDestinoTramo);
        sb.append('}');
        return sb.toString();
    }
}
