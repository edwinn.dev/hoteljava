package com.edmi.model;

public class Reservacion {
    private Integer codReserva;
    private String codTrabajador;
    private String codCliente;
    private String codHabitacion;
    private String fechaIngreso;
    private String fechaSalida;
    private Double importeReserva;
    private Double importeProductos;
    private Double importeTotal;
    private String tiempoReserva;
    private String magnitudReserva;
    private String estado;
    private String observacion;
    private String nombreCliente;
    private String nombreTrabajador;
    private String codTipoDoc;
    private String tipoDocumento;
    private String nroDocumento;
    
    private String auxNroRoom;
    
    public Reservacion(){}

    public Integer getCodReserva() {
        return codReserva;
    }

    public void setCodReserva(Integer codReserva) {
        this.codReserva = codReserva;
    }

    public String getCodTrabajador() {
        return codTrabajador;
    }

    public void setCodTrabajador(String codTrabajador) {
        this.codTrabajador = codTrabajador;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getCodHabitacion() {
        return codHabitacion;
    }

    public void setCodHabitacion(String codHabitacion) {
        this.codHabitacion = codHabitacion;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(String fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public Double getImporteReserva() {
        return importeReserva;
    }

    public void setImporteReserva(Double importeReserva) {
        this.importeReserva = importeReserva;
    }

    public Double getImporteProductos() {
        return importeProductos;
    }

    public void setImporteProductos(Double importeProductos) {
        this.importeProductos = importeProductos;
    }

    public Double getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(Double importeTotal) {
        this.importeTotal = importeTotal;
    }

    public String getTiempoReserva() {
        return tiempoReserva;
    }

    public void setTiempoReserva(String tiempoReserva) {
        this.tiempoReserva = tiempoReserva;
    }

    public String getMagnitudReserva() {
        return magnitudReserva;
    }

    public void setMagnitudReserva(String magnitudReserva) {
        this.magnitudReserva = magnitudReserva.toUpperCase();
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado.toUpperCase();
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion.toUpperCase();
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente.toUpperCase();
    }

    public String getNombreTrabajador() {
        return nombreTrabajador;
    }

    public void setNombreTrabajador(String nombreTrabajador) {
        this.nombreTrabajador = nombreTrabajador.toUpperCase();
    }

    public String getCodTipoDoc() {
        return codTipoDoc;
    }

    public void setCodTipoDoc(String codTipoDoc) {
        this.codTipoDoc = codTipoDoc;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento.toUpperCase();
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getAuxNroRoom() {
        return auxNroRoom;
    }

    public void setAuxNroRoom(String auxNroRoom) {
        this.auxNroRoom = auxNroRoom;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Reservacion{codReserva=").append(codReserva);
        sb.append(", codTrabajador=").append(codTrabajador);
        sb.append(", codCliente=").append(codCliente);
        sb.append(", codHabitacion=").append(codHabitacion);
        sb.append(", fechaIngreso=").append(fechaIngreso);
        sb.append(", fechaSalida=").append(fechaSalida);
        sb.append(", importeReserva=").append(importeReserva);
        sb.append(", importeProductos=").append(importeProductos);
        sb.append(", importeTotal=").append(importeTotal);
        sb.append(", tiempoReserva=").append(tiempoReserva);
        sb.append(", magnitudReserva=").append(magnitudReserva);
        sb.append(", estado=").append(estado);
        sb.append(", observacion=").append(observacion);
        sb.append(", nombreCliente=").append(nombreCliente);
        sb.append(", nombreTrabajador=").append(nombreTrabajador);
        sb.append(", codTipoDoc=").append(codTipoDoc);
        sb.append(", tipoDocumento=").append(tipoDocumento);
        sb.append(", nroDocumento=").append(nroDocumento);
        sb.append(", auxNroRoom=").append(auxNroRoom);
        sb.append('}');
        return sb.toString();
    }
}
