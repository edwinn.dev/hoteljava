package com.edmi.repository;

import java.util.List;

public interface RepositoryCompany<M> {
    List<M> findAll();
    String findName();
    M getCompany();
    int create(M company);
    int update(M company);
    int delete();
}
