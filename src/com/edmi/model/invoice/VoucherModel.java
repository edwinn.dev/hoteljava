package com.edmi.model.invoice;

import java.util.List;

public class VoucherModel {
    private Cabecera cabecera;
    private List<VariableGlobal> variablesGlobales;
    private List<Detalle> detalle;
    private List<Leyenda> leyendas;
    private List<AdicionalDetalle> adicionalDetalle;
    private List<Tributo> tributos;
    private DatoPago datoPago;

    public VoucherModel() {}

    public Cabecera getCabecera() {
        return cabecera;
    }

    public void setCabecera(Cabecera cabecera) {
        this.cabecera = cabecera;
    }

    public List<VariableGlobal> getVariablesGlobales() {
        return variablesGlobales;
    }

    public void setVariablesGlobales(List<VariableGlobal> variablesGlobales) {
        this.variablesGlobales = variablesGlobales;
    }

    public List<Detalle> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<Detalle> detalle) {
        this.detalle = detalle;
    }

    public List<Leyenda> getLeyendas() {
        return leyendas;
    }

    public void setLeyendas(List<Leyenda> leyendas) {
        this.leyendas = leyendas;
    }

    public List<AdicionalDetalle> getAdicionalDetalle() {
        return adicionalDetalle;
    }

    public void setAdicionalDetalle(List<AdicionalDetalle> adicionalDetalle) {
        this.adicionalDetalle = adicionalDetalle;
    }

    public List<Tributo> getTributos() {
        return tributos;
    }

    public void setTributos(List<Tributo> tributos) {
        this.tributos = tributos;
    }

    public DatoPago getDatoPago() {
        return datoPago;
    }

    public void setDatoPago(DatoPago datoPago) {
        this.datoPago = datoPago;
    }
}
