package com.edmi.model.catalogue;

public class TipoDocumento {
    private String codTipoDocumento;
    private String descripcion;
    
    public TipoDocumento(){}

    public TipoDocumento(String codTipoDocumento, String descripcion) {
        this.codTipoDocumento = codTipoDocumento;
        this.descripcion = descripcion;
    }

    public String getCodTipoDocumento() {
        return codTipoDocumento;
    }

    public void setCodTipoDocumento(String codTipoDocumento) {
        this.codTipoDocumento = codTipoDocumento;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TipoDocumento{codTipoDocumento=").append(codTipoDocumento);
        sb.append(", descripcion=").append(descripcion);
        sb.append('}');
        return sb.toString();
    }
}
