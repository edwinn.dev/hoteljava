package com.edmi.controller.menu;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.ControllerEmployee;
import com.edmi.model.Trabajador;
import com.edmi.repository.RepositoryEmployee;
import com.edmi.repository.imp.TrabajadorRepositoryImp;
import com.edmi.view.MainPanelServices;
import com.edmi.view.ModalNewEmployee;
import com.edmi.view.widget.MenuTableEmployee;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class ControllerTableEmployee implements ActionListener {
    private static RepositoryEmployee<Trabajador> repositoryEmployee;
    private MenuTableEmployee menuEmployee;
    private MainPanelServices mainFrame;
    private ControllerEmployee controllerEmployee;

    public ControllerTableEmployee(ModalNewEmployee modal, MainPanelServices mainFrame) {
        this.mainFrame = mainFrame;
        controllerEmployee = new ControllerEmployee(modal, mainFrame);
        repositoryEmployee = new TrabajadorRepositoryImp();
        menuEmployee = MenuTableEmployee.getInstance();
        menuEmployee.initMenuTableClient(mainFrame.employeesTableEmployees);
        setListener();
    }

    private void cambiarEstado(String estado) {
        int row = mainFrame.employeesTableEmployees.getSelectedRow();
        int response = JOptionPane.showConfirmDialog(mainFrame, "¿Está seguro de eliminar al usuario?",  SYS_MSG, JOptionPane.YES_NO_OPTION);
        if(response == JOptionPane.YES_OPTION){
            if (repositoryEmployee.state(mainFrame.employeesTableEmployees.getValueAt(row, 0), estado) != 0) {
                JOptionPane.showMessageDialog(mainFrame, "Usuario eliminado.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                controllerEmployee.loadTableEmployees("1");
                mainFrame.chkUsuariosDescartados.setSelected(false);
            } else {
                JOptionPane.showMessageDialog(mainFrame, "Hubo un problema al cambiar el estado del cliente,\ncontacte con el administrador.",  SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void setListener() {
        menuEmployee.itemDescartado.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == menuEmployee.itemDescartado) {
            cambiarEstado("0");
        }
    }
}
