package com.edmi.model;

public class ProductoTemporal {
    private String codProducto;
    private String nombreProducto;
    private String unidadMedida;
    private String gratuito;
    private Double cantidad;
    private Double precioUnitario;
    private Double subtotal;
    private Double total;
    
    public ProductoTemporal(){}

    public ProductoTemporal(String codProducto, String nombreProducto, String unidadMedida, String gratuito, Double cantidad, Double precioUnitario, Double subtotal, Double total) {
        this.codProducto = codProducto;
        this.nombreProducto = nombreProducto;
        this.unidadMedida = unidadMedida;
        this.gratuito = gratuito;
        this.cantidad = cantidad;
        this.precioUnitario = precioUnitario;
        this.subtotal = subtotal;
        this.total = total;
    }

    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto.toUpperCase();
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida.toUpperCase();
    }

    public String getGratuito() {
        return gratuito;
    }

    public void setGratuito(String gratuito) {
        this.gratuito = gratuito;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(Double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ProductoVenta{codProducto=").append(codProducto);
        sb.append(", nombreProducto=").append(nombreProducto);
        sb.append(", unidadMedida=").append(unidadMedida);
        sb.append(", gratuito=").append(gratuito);
        sb.append(", cantidad=").append(cantidad);
        sb.append(", precioUnitario=").append(precioUnitario);
        sb.append(", subtotal=").append(subtotal);
        sb.append(", total=").append(total);
        sb.append('}');
        return sb.toString();
    }
}
