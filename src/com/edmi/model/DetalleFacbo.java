
package com.edmi.model;

public class DetalleFacbo {
    private Long idCabecera;
    private String cod_producto; 
    private String nombreProducto;
    private String cantidad; 
    private String valor_unitario;
    private String total_tributo_item;
    private String unidadMedida;
    private String cod_afectacion;
    private String tipo_afectacion;
    private String porcentaje_afectacion;
    private String descuento;
    private String otros_cargos;
    private String precio_venta_item;
    private String total_importe_item;
    
    private String cod_cliente;
    private String cod_tipodocumento;
    private String numeroDocumento;
    private String tipoDocumento;
    private String RazonSocial;
    private String fecha_emision;
    private String serie;
    private String numero;
    
    public Long getIdCabecera() {
        return idCabecera;
    }

    public void setIdCabecera(Long idCabecera) {
        this.idCabecera = idCabecera;
    }
    
    public String getCod_producto() {
        return cod_producto;
    }

    public void setCod_producto(String cod_producto) {
        this.cod_producto = cod_producto;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getValor_unitario() {
        return valor_unitario;
    }

    public void setValor_unitario(String valor_unitario) {
        this.valor_unitario = valor_unitario;
    }

    public String getTotal_tributo_item() {
        return total_tributo_item;
    }

    public void setTotal_tributo_item(String total_tributo_item) {
        this.total_tributo_item = total_tributo_item;
    }

    public String getCod_afectacion() {
        return cod_afectacion;
    }

    public void setCod_afectacion(String cod_afectacion) {
        this.cod_afectacion = cod_afectacion;
    }

    public String getTipo_afectacion() {
        return tipo_afectacion;
    }

    public void setTipo_afectacion(String tipo_afectacion) {
        this.tipo_afectacion = tipo_afectacion;
    }

    public String getPorcentaje_afectacion() {
        return porcentaje_afectacion;
    }

    public void setPorcentaje_afectacion(String porcentaje_afectacion) {
        this.porcentaje_afectacion = porcentaje_afectacion;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getOtros_cargos() {
        return otros_cargos;
    }

    public void setOtros_cargos(String otros_cargos) {
        this.otros_cargos = otros_cargos;
    }

    public String getPrecio_venta_item() {
        return precio_venta_item;
    }

    public void setPrecio_venta_item(String precio_venta_item) {
        this.precio_venta_item = precio_venta_item;
    }

    public String getTotal_importe_item() {
        return total_importe_item;
    }

    public void setTotal_importe_item(String total_importe_item) {
        this.total_importe_item = total_importe_item;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public String getCod_cliente() {
        return cod_cliente;
    }

    public void setCod_cliente(String cod_cliente) {
        this.cod_cliente = cod_cliente;
    }

    public String getCod_tipodocumento() {
        return cod_tipodocumento;
    }

    public void setCod_tipodocumento(String cod_tipodocumento) {
        this.cod_tipodocumento = cod_tipodocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getRazonSocial() {
        return RazonSocial;
    }

    public void setRazonSocial(String RazonSocial) {
        this.RazonSocial = RazonSocial;
    }

    public String getFecha_emision() {
        return fecha_emision;
    }

    public void setFecha_emision(String fecha_emision) {
        this.fecha_emision = fecha_emision;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }
}
