package com.edmi.view;

import java.util.Calendar;
import java.util.Date;
import javax.swing.JDialog;

public class AddDataReserva extends javax.swing.JDialog {
    private Integer cantidadHoras1 = 23;
    private Integer cantidadHoras2 = 0;
    
    public AddDataReserva(JDialog parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setDefaultValues();
    }
    
    private void setDefaultValues(){
        this.setLocationRelativeTo(null);
        AddDataReserva.chooserIngreso.setDate(new Date());
        AddDataReserva.txtAddCantidadHoras.setValue(String.valueOf(12));
        this.chkHoras.setSelected(true);
        this.chkDias.setSelected(false);
        this.txtHoraValor.setValue(String.valueOf(12));
        this.txtDiaValor.setValue(String.valueOf(1));
        this.sliderHorasReserva.setValue(12);
        this.sliderDiasReserva.setValue(1);
        this.sliderDiasReserva.setEnabled(false);
        this.txtDiaValor.setText("0");
        AddDataReserva.txtAddCantidadMinutos.setValue("00");
        cbxMinutos.setEnabled(false);
        
        Calendar calendario = Calendar.getInstance();
        int hora, minutos;
        hora = calendario.get(Calendar.HOUR_OF_DAY);
        minutos = calendario.get(Calendar.MINUTE);
        
        if(hora < 10){
            AddDataReserva.spinnerHoras.setValue("0".concat(String.valueOf(hora)));
        }else{
            AddDataReserva.spinnerHoras.setValue(String.valueOf(hora));
        }
        
        if(minutos < 10){
            AddDataReserva.spinnerMinutos.setValue("0".concat(String.valueOf(minutos)));
        }else{
            AddDataReserva.spinnerMinutos.setValue(String.valueOf(minutos));
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        spinnerHoras = new javax.swing.JSpinner();
        spinnerMinutos = new javax.swing.JSpinner();
        jLabel8 = new javax.swing.JLabel();
        chooserIngreso = new com.toedter.calendar.JDateChooser();
        jLabel17 = new javax.swing.JLabel();
        sliderHorasReserva = new javax.swing.JSlider();
        chkHoras = new javax.swing.JCheckBox();
        chkDias = new javax.swing.JCheckBox();
        sliderDiasReserva = new javax.swing.JSlider();
        txtHoraValor = new javax.swing.JFormattedTextField();
        txtHoraMagnitud = new javax.swing.JFormattedTextField();
        txtDiaValor = new javax.swing.JFormattedTextField();
        txtDiaMagnitud = new javax.swing.JFormattedTextField();
        btnAceptar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtAddCostoTotal = new javax.swing.JFormattedTextField();
        txtAddCantidadHoras = new javax.swing.JFormattedTextField();
        jLabel2 = new javax.swing.JLabel();
        chkMinutos = new javax.swing.JCheckBox();
        cbxMinutos = new javax.swing.JComboBox<>();
        txtAddCantidadMinutos = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Personalizar Fecha y Hora");
        setResizable(false);

        spinnerHoras.setModel(new javax.swing.SpinnerListModel(new String[] {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23"}));
        spinnerHoras.setToolTipText("HORA");
        spinnerHoras.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        spinnerMinutos.setModel(new javax.swing.SpinnerListModel(new String[] {"00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"}));
        spinnerMinutos.setToolTipText("MINUTOS");

        jLabel8.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel8.setText("Fecha de ingreso:");

        chooserIngreso.setToolTipText("dd-MM-yyyy");
        chooserIngreso.setDateFormatString("dd-MM-yyyy");
        chooserIngreso.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel17.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel17.setText("Indicar el tiempo de alojamiento");

        sliderHorasReserva.setMajorTickSpacing(1);
        sliderHorasReserva.setMaximum(23);
        sliderHorasReserva.setMinimum(1);
        sliderHorasReserva.setPaintTicks(true);
        sliderHorasReserva.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliderHorasReservaStateChanged(evt);
            }
        });

        chkHoras.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        chkHoras.setSelected(true);
        chkHoras.setText("HORAS");
        chkHoras.setEnabled(false);
        chkHoras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkHorasActionPerformed(evt);
            }
        });

        chkDias.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        chkDias.setText("DIAS");
        chkDias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkDiasActionPerformed(evt);
            }
        });

        sliderDiasReserva.setMajorTickSpacing(1);
        sliderDiasReserva.setMaximum(30);
        sliderDiasReserva.setMinimum(1);
        sliderDiasReserva.setPaintTicks(true);
        sliderDiasReserva.setToolTipText("");
        sliderDiasReserva.setValue(1);
        sliderDiasReserva.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                sliderDiasReservaStateChanged(evt);
            }
        });

        txtHoraValor.setEditable(false);
        txtHoraValor.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtHoraValor.setText("23");

        txtHoraMagnitud.setEditable(false);
        txtHoraMagnitud.setText("HORAS");

        txtDiaValor.setEditable(false);
        txtDiaValor.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtDiaValor.setText("1");

        txtDiaMagnitud.setEditable(false);
        txtDiaMagnitud.setText("DIAS");

        btnAceptar.setText("ACEPTAR");

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Costo de habitación por");

        txtAddCostoTotal.setText("30.00");

        txtAddCantidadHoras.setEditable(false);
        txtAddCantidadHoras.setBorder(null);
        txtAddCantidadHoras.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("HORA(S) y");

        chkMinutos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        chkMinutos.setText("MINUTOS");
        chkMinutos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkMinutosActionPerformed(evt);
            }
        });

        cbxMinutos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00 min.", "10 min.", "20 min.", "30 min.", "40 min.", "50 min." }));
        cbxMinutos.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbxMinutosItemStateChanged(evt);
            }
        });

        txtAddCantidadMinutos.setEditable(false);
        txtAddCantidadMinutos.setBorder(null);
        txtAddCantidadMinutos.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText("MINUTOS");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(chkDias, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addComponent(sliderDiasReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDiaValor, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDiaMagnitud))
                    .addComponent(jSeparator3)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chooserIngreso, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(spinnerHoras, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(spinnerMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator2)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(chkMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(cbxMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(chkHoras, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(sliderHorasReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtHoraValor, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtHoraMagnitud, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtAddCantidadHoras, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtAddCantidadMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(46, 46, 46)
                                .addComponent(txtAddCostoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(169, 169, 169)
                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(spinnerHoras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(spinnerMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(chooserIngreso, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel17)
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chkHoras)
                    .addComponent(sliderHorasReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtHoraValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtHoraMagnitud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chkMinutos)
                    .addComponent(cbxMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(chkDias)
                    .addComponent(sliderDiasReserva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtDiaValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtDiaMagnitud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtAddCantidadHoras, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txtAddCostoTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtAddCantidadMinutos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3)
                        .addComponent(jLabel2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 463, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 355, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sliderHorasReservaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliderHorasReservaStateChanged
        int value = this.sliderHorasReserva.getValue();
        cantidadHoras1 = value;
        this.txtHoraValor.setValue(String.valueOf(cantidadHoras1));
        AddDataReserva.txtAddCantidadHoras.setValue(String.valueOf(cantidadHoras1 + cantidadHoras2));
    }//GEN-LAST:event_sliderHorasReservaStateChanged

    private void sliderDiasReservaStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_sliderDiasReservaStateChanged
        int value = this.sliderDiasReserva.getValue();
        cantidadHoras2 = (value * 24);
        this.txtDiaValor.setValue(String.valueOf(value));
        AddDataReserva.txtAddCantidadHoras.setValue(cantidadHoras1 + cantidadHoras2);
    }//GEN-LAST:event_sliderDiasReservaStateChanged

    private void chkDiasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkDiasActionPerformed
        AddDataReserva.txtAddCantidadHoras.setValue(String.valueOf(24));
        
        this.sliderDiasReserva.setEnabled(true);
        
        this.sliderDiasReserva.setMaximum(30);
        this.sliderDiasReserva.setMinimum(1);
        this.sliderDiasReserva.setValue(1);
        
        Integer value = this.sliderDiasReserva.getValue();
        this.txtDiaValor.setValue(String.valueOf(value));
        
        if(chkDias.isSelected()){
            cantidadHoras1 = 0;
            this.sliderHorasReserva.setMinimum(0);
            this.sliderHorasReserva.setMaximum(23);
            this.sliderHorasReserva.setValue(0);
            
            AddDataReserva.txtAddCantidadHoras.setValue(String.valueOf(value * 24));
        }else{
            cantidadHoras2 = 0;
            AddDataReserva.txtAddCantidadHoras.setValue(String.valueOf(cantidadHoras1 + cantidadHoras2));
            this.sliderHorasReserva.setMinimum(1);
            this.sliderHorasReserva.setMaximum(23);
            this.sliderHorasReserva.setValue(12);
            
            AddDataReserva.txtAddCantidadHoras.setValue(12);
            this.txtHoraValor.setValue(12);
            
            AddDataReserva.txtAddCantidadHoras.setValue(cantidadHoras1);
            this.sliderDiasReserva.setEnabled(false);
            this.sliderDiasReserva.setValue(1);
            
            this.sliderDiasReserva.setMaximum(30);
            this.sliderDiasReserva.setMinimum(0);
            this.sliderDiasReserva.setValue(0);
        }
    }//GEN-LAST:event_chkDiasActionPerformed
    
    private void chkHorasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkHorasActionPerformed
        this.txtHoraValor.setValue(String.valueOf(23));
        AddDataReserva.txtAddCantidadHoras.setValue(String.valueOf(this.sliderHorasReserva.getValue()));
        this.sliderHorasReserva.setValue(23);
    }//GEN-LAST:event_chkHorasActionPerformed

    private void chkMinutosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkMinutosActionPerformed
        if(chkMinutos.isSelected()){
            cbxMinutos.setEnabled(true);
        }else{
            cbxMinutos.setEnabled(false);
            cbxMinutos.setSelectedIndex(0);
            AddDataReserva.txtAddCantidadMinutos.setValue("00");
        }
    }//GEN-LAST:event_chkMinutosActionPerformed

    private void cbxMinutosItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbxMinutosItemStateChanged
        AddDataReserva.txtAddCantidadMinutos.setValue(cbxMinutos.getSelectedItem().toString().substring(0, 2));
    }//GEN-LAST:event_cbxMinutosItemStateChanged
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnAceptar;
    private javax.swing.JComboBox<String> cbxMinutos;
    private javax.swing.JCheckBox chkDias;
    private javax.swing.JCheckBox chkHoras;
    private javax.swing.JCheckBox chkMinutos;
    public static com.toedter.calendar.JDateChooser chooserIngreso;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSlider sliderDiasReserva;
    private javax.swing.JSlider sliderHorasReserva;
    public static javax.swing.JSpinner spinnerHoras;
    public static javax.swing.JSpinner spinnerMinutos;
    public static javax.swing.JFormattedTextField txtAddCantidadHoras;
    public static javax.swing.JFormattedTextField txtAddCantidadMinutos;
    public javax.swing.JFormattedTextField txtAddCostoTotal;
    private javax.swing.JFormattedTextField txtDiaMagnitud;
    private javax.swing.JFormattedTextField txtDiaValor;
    private javax.swing.JFormattedTextField txtHoraMagnitud;
    private javax.swing.JFormattedTextField txtHoraValor;
    // End of variables declaration//GEN-END:variables
}
