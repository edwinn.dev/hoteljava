package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.database.query.Query;
import com.edmi.model.Theme;
import com.edmi.repository.RepositoryTheme;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ThemeRepositoryImp implements RepositoryTheme<Theme> {

    @Override
    public Theme findById(Object id) {
        return null;
    }

    @Override
    public Theme findActiveTheme(Object value) {
        Theme theme = null;
        try (PreparedStatement ps = getInstance().prepareStatement(Query.SQL_THEME_FIND_ACTIVE)){
            ps.setString(1, value.toString());
            ResultSet rs = ps.executeQuery();
            theme = new Theme();
            theme.setIdTheme(rs.getString("id"));
            theme.setTheme(rs.getInt("theme"));
            theme.setDescription(rs.getString("theme_description"));
            theme.setState(rs.getString("state"));
        } catch (SQLException ex) {
            Logger.getLogger(ThemeRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return theme;
    }

    @Override
    public int changedTheme(Object idTheme, Object value) {
        int response = 0;
        try (PreparedStatement ps = getInstance().prepareStatement(Query.SQL_THEME_CHANGED)){
            ps.setString(1, value.toString());
            ps.setString(2, idTheme.toString());
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ThemeRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }
}
