package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.model.Reservacion;
import com.edmi.database.query.Query;
import com.edmi.repository.RepositoryReservation;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReservaRepositoryImp implements RepositoryReservation<Reservacion> {

    private Connection getConnection() throws SQLException {
        return getInstance();
    }

    @Override
    public List<Reservacion> findAll() {
        List<Reservacion> reservas = new ArrayList<>();
        try (PreparedStatement st = getConnection().prepareStatement(Query.SQL_RESERVA_FIND_ALL)) {
            try (ResultSet rs = st.executeQuery()) {
                while (rs.next()) {
                    Reservacion reserva = getReserva(rs);
                    reservas.add(reserva);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservaRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reservas;
    }

    @Override
    public List<Reservacion> findAllWhere(String condition, String value) {
        String SQL = "SELECT r.*, CONCAT(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) AS nombre_cliente, "
                .concat("CONCAT(t.nombre, ' ', t.apellido_paterno, ' ', t.apellido_materno) AS nombre_trabajador, c.cod_tipodocumento, c.tipo_documento, c.nro_documento ")
                .concat("FROM reserva AS r INNER JOIN trabajador AS t ON r.cod_trabajador = t.cod_trabajador ")
                .concat("INNER JOIN cliente AS c ON r.cod_cliente = c.cod_cliente WHERE " + condition + " LIKE " + "'" + value + "%' AND r.estado = '1' ORDER BY r.cod_reserva DESC");
        List<Reservacion> listReser = new ArrayList<>();
        try (PreparedStatement ps = getConnection().prepareStatement(SQL); ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                Reservacion reserva = getReserva(rs);
                listReser.add(reserva);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservaRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listReser;
    }

    public List<Reservacion> obtenerTodaReserva(String value) {
        String SQL = "SELECT r.*, CONCAT(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) AS nombre_cliente, "
                .concat("CONCAT(t.nombre, ' ', t.apellido_paterno, ' ', t.apellido_materno) AS nombre_trabajador, c.cod_tipodocumento, c.tipo_documento, c.nro_documento ")
                .concat("FROM reserva AS r INNER JOIN trabajador AS t ON r.cod_trabajador = t.cod_trabajador ")
                .concat("INNER JOIN cliente AS c ON r.cod_cliente = c.cod_cliente WHERE r.cod_trabajador = '" + value + "' AND r.estado = '1' ORDER BY r.cod_reserva DESC");
        List<Reservacion> listReser = new ArrayList<>();
        try (PreparedStatement ps = getConnection().prepareStatement(SQL); ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                Reservacion reserva = getReserva(rs);
                listReser.add(reserva);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservaRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listReser;
    }

    @Override
    public Reservacion findById(Object id) {
        Reservacion reserva = null;
        try (PreparedStatement st = getConnection().prepareStatement(Query.SQL_RESERVA_FIND_BY_ID)) {
            st.setString(1, id.toString());
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                reserva = obtenerReserva(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservaRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reserva;
    }

    @Override
    public Reservacion findForAddProduct(Object codRoom) {
        Reservacion reserva = null;
        try (PreparedStatement st = getConnection().prepareStatement(Query.SQL_RESERVA_FIND_FOR_ADD_PRODUCTS)) {
            st.setString(1, codRoom.toString());
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                reserva = getReserva(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ReservaRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reserva;
    }

    @Override
    public int create(Reservacion reserva) {
        int response = 0;
        try (PreparedStatement st = getConnection().prepareStatement(Query.SQL_RESERVA_CREATE)) {
            st.setString(1, reserva.getCodTrabajador());
            st.setString(2, reserva.getCodCliente());
            st.setString(3, reserva.getCodHabitacion());
            st.setString(4, reserva.getFechaIngreso());
            st.setString(5, reserva.getFechaSalida());
            st.setDouble(6, reserva.getImporteReserva());
            st.setDouble(7, reserva.getImporteProductos());
            st.setDouble(8, reserva.getImporteTotal());
            st.setString(9, reserva.getTiempoReserva());
            st.setString(10, reserva.getMagnitudReserva());
            st.setString(11, reserva.getEstado());
            st.setString(12, reserva.getObservacion());
            st.setString(13, reserva.getAuxNroRoom());
            response = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ReservaRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    @Override
    public int update(Reservacion reserva) {
        int response = 0;
        try (PreparedStatement st = getConnection().prepareStatement(Query.SQL_RESERVA_UPDATE)) {
            st.setString(1, reserva.getCodHabitacion());
            st.setString(2, reserva.getFechaIngreso());
            st.setString(3, reserva.getFechaSalida());
            st.setDouble(4, reserva.getImporteReserva());
            st.setDouble(5, reserva.getImporteProductos());
            st.setDouble(6, reserva.getImporteTotal());
            st.setString(7, reserva.getTiempoReserva());
            st.setString(8, reserva.getMagnitudReserva());
            st.setString(9, reserva.getEstado());
            st.setString(10, reserva.getObservacion());
            st.setString(11, reserva.getAuxNroRoom());
            st.setInt(12, reserva.getCodReserva());
            response = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ReservaRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    @Override
    public int updateReservaConsumo(Reservacion reserva) {
        int response = 0;
        try (PreparedStatement st = getConnection().prepareStatement(Query.SQL_RESERVA_UPDATE_FOR_CONSUMO)) {
            st.setDouble(1, reserva.getImporteProductos());
            st.setDouble(2, reserva.getImporteTotal());
            st.setInt(3, reserva.getCodReserva());
            response = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ReservaRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    @Override
    public int delete(Object id) {
        return 0;
    }

    @Override
    public int cancel(Reservacion reserva, String state) {
        int response = 0;
        try (PreparedStatement st = getConnection().prepareStatement(Query.SQL_RESERVA_CANCEL)) {
            st.setString(1, state);
            st.setInt(2, reserva.getCodReserva());
            response = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ReservaRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    @Override
    public int state(Object id, Object estado) {
        int response = 0;
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_RESERVA_STATE)) {
            ps.setString(1, estado.toString());
            ps.setString(2, id.toString());
            response = ps.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(ReservaRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }

    private Reservacion getReserva(ResultSet rs) throws SQLException {
        Reservacion reserva = new Reservacion();
        reserva.setCodReserva(rs.getInt("cod_reserva"));
        reserva.setCodCliente(rs.getString("cod_cliente"));
        reserva.setCodHabitacion(rs.getString("cod_habitacion"));
        reserva.setCodTrabajador(rs.getString("cod_trabajador"));
        reserva.setFechaIngreso(rs.getString("fecha_ingreso"));
        reserva.setFechaSalida(rs.getString("fecha_salida"));
        reserva.setImporteReserva(rs.getDouble("importe_reserva"));
        reserva.setImporteProductos(rs.getDouble("importe_productos"));
        reserva.setImporteTotal(rs.getDouble("importe_total"));
        reserva.setTiempoReserva(rs.getString("tiempo_reserva"));
        reserva.setMagnitudReserva(rs.getString("magnitud_reserva"));
        reserva.setEstado(rs.getString("estado"));
        reserva.setObservacion(rs.getString("observacion"));
        reserva.setNombreCliente(rs.getString("nombre_cliente"));
        reserva.setNombreTrabajador(rs.getString("nombre_trabajador"));
        reserva.setCodTipoDoc(rs.getString("cod_tipodocumento"));
        reserva.setTipoDocumento(rs.getString("tipo_documento"));
        reserva.setNroDocumento(rs.getString("nro_documento"));
        reserva.setAuxNroRoom(rs.getString("nro_habitacion"));
        return reserva;
    }

    private Reservacion obtenerReserva(ResultSet rs) throws SQLException {
        Reservacion reserva = new Reservacion();
        reserva.setCodReserva(rs.getInt("cod_reserva"));
        reserva.setCodCliente(rs.getString("cod_cliente"));
        reserva.setCodHabitacion(rs.getString("cod_habitacion"));
        reserva.setCodTrabajador(rs.getString("cod_trabajador"));
        reserva.setFechaIngreso(rs.getString("fecha_ingreso"));
        reserva.setFechaSalida(rs.getString("fecha_salida"));
        reserva.setImporteReserva(rs.getDouble("importe_reserva"));
        reserva.setImporteProductos(rs.getDouble("importe_productos"));
        reserva.setImporteTotal(rs.getDouble("importe_total"));
        reserva.setTiempoReserva(rs.getString("tiempo_reserva"));
        reserva.setMagnitudReserva(rs.getString("magnitud_reserva"));
        reserva.setEstado(rs.getString("estado"));
        reserva.setObservacion(rs.getString("observacion"));
        reserva.setNombreCliente(rs.getString("nombre_cliente"));
        reserva.setNombreTrabajador(rs.getString("nombre_trabajador"));
        reserva.setCodTipoDoc(rs.getString("cod_tipodocumento"));
        reserva.setTipoDocumento(rs.getString("tipo_documento"));
        reserva.setNroDocumento(rs.getString("nro_documento"));
        reserva.setAuxNroRoom(rs.getString("nro_habitacion"));
        return reserva;
    }
}
