package com.edmi.view;

import static com.edmi.util.MessageSystem.SYS_MSG;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class DialogReportFiles extends javax.swing.JDialog {

    public DialogReportFiles(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.cargarVista();
    }

    private void cargarVista() {
        this.setTitle("Ruta archivos");
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.txtRutaPdf.setEditable(false);
        this.txtRutaXML.setEditable(false);
        this.txtRutarReportes.setEditable(false);
        this.txtRutaPdf.setText(obtenerRutaArchiReporPdfXml()[1]== null ? "" : obtenerRutaArchiReporPdfXml()[1]);
        this.txtRutarReportes.setText(obtenerRutaArchiReporPdfXml()[0]== null ? "" : obtenerRutaArchiReporPdfXml()[0]);
        this.txtRutaXML.setText(obtenerRutaArchiReporPdfXml()[2]== null ? "" : obtenerRutaArchiReporPdfXml()[2]);
    }
    
    private void buscarRuta(int option) {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Seleccione la carpeta para guardar los reportes");
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setAcceptAllFileFilterUsed(false);
        if (fc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            switch (option) {
                case 1:
                    this.txtRutarReportes.setText("" + fc.getSelectedFile());
                    break;
                case 2:
                    this.txtRutaPdf.setText("" + fc.getSelectedFile());
                    break;
                case 3:
                    this.txtRutaXML.setText("" + fc.getSelectedFile());
                    break;
            }
        }
    }
    
    public static String obtenerRutaPdf() {
        File file = new File("design" + File.separator + "path" + File.separator + "ubicacionReportes.txt");
        String ruta = null;
        try {
            try (Scanner scanner = new Scanner(file)) {
                while (scanner.hasNextLine()) {
                    ruta = scanner.nextLine();
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DialogReportFiles.class.getName()).log(Level.SEVERE, null, ex);
            ruta = null;
        }
        return ruta;
    }
    
    public static String[] obtenerRutaArchiReporPdfXml() {
        File file = new File("design" + File.separator + "path" + File.separator + "ubicacionReportes.txt");
        String[] ruta = new String[3];
        int increment = 0;
        try {
            try (Scanner scanner = new Scanner(file)) {
                while (scanner.hasNextLine()) {
                    ruta[increment] = scanner.nextLine();
                    increment++;
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DialogReportFiles.class.getName()).log(Level.SEVERE, null, ex);
            ruta = null;
        }
        return ruta;
    }
    
    private void agregarRuta() {
        if (!this.txtRutarReportes.getText().isEmpty() && !this.txtRutaPdf.getText().isEmpty() && !this.txtRutaXML.getText().isEmpty()) {
            String rutaReportes = this.txtRutarReportes.getText().trim();
            String rutaPdf = this.txtRutaPdf.getText().trim();
            String rutaXML = this.txtRutaXML.getText().trim();
            File file = new File("design" + File.separator + "path" + File.separator + "ubicacionReportes.txt");
            try {
                try (PrintWriter writer = new PrintWriter(file)) {
                    writer.println(rutaReportes);
                    writer.println(rutaPdf);
                    writer.println(rutaXML);
                }
            } catch (IOException ex) {
            }
            JOptionPane.showMessageDialog(this, "Ruta guardada.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(this, "Debe de seleccionar una ruta correcta.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtRutarReportes = new javax.swing.JTextField();
        btnUbicaReporte = new javax.swing.JButton();
        btnGuardarReporte = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtRutaPdf = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtRutaXML = new javax.swing.JTextField();
        btnRutaXML = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Ruta reportes:");

        btnUbicaReporte.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnUbicaReporte.setText("ELEGIR");
        btnUbicaReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUbicaReporteActionPerformed(evt);
            }
        });

        btnGuardarReporte.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnGuardarReporte.setText("GUARDAR CAMBIOS");
        btnGuardarReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarReporteActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setText("Ruta comprobantes PDF:");

        jButton1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jButton1.setText("ELEGIR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText("Ruta documentos XML:");

        btnRutaXML.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnRutaXML.setText("ELEGIR");
        btnRutaXML.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRutaXMLActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnUbicaReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtRutarReportes)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 172, Short.MAX_VALUE)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtRutaPdf))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnRutaXML, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtRutaXML, javax.swing.GroupLayout.PREFERRED_SIZE, 431, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(145, 145, 145)
                .addComponent(btnGuardarReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(btnUbicaReporte))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtRutarReportes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtRutaPdf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(btnRutaXML))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtRutaXML, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnGuardarReporte, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnUbicaReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUbicaReporteActionPerformed
        buscarRuta(1);
    }//GEN-LAST:event_btnUbicaReporteActionPerformed

    private void btnGuardarReporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarReporteActionPerformed
        agregarRuta();
    }//GEN-LAST:event_btnGuardarReporteActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        buscarRuta(2);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnRutaXMLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRutaXMLActionPerformed
        buscarRuta(3);
    }//GEN-LAST:event_btnRutaXMLActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGuardarReporte;
    private javax.swing.JButton btnRutaXML;
    private javax.swing.JButton btnUbicaReporte;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtRutaPdf;
    private javax.swing.JTextField txtRutaXML;
    private javax.swing.JTextField txtRutarReportes;
    // End of variables declaration//GEN-END:variables
}