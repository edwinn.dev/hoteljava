package com.edmi.repository;

public interface RepositoryTheme<T> {
    T findById(Object id);
    T findActiveTheme(Object id);
    int changedTheme(Object idTheme, Object value);
}
