package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.model.CategoriaProducto;
import com.edmi.database.query.Query;
import com.edmi.repository.RepositoryCategoryProduct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CategoriaProductoRepositoryImp implements RepositoryCategoryProduct<CategoriaProducto>{

    private Connection getConnection () throws SQLException{
        return getInstance();
    }
    
    @Override
    public List<CategoriaProducto> findAll() {
        List<CategoriaProducto> list = new ArrayList<>();
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CAT_FIND_ALL);
             ResultSet rs = ps.executeQuery()) {
            while(rs.next()){
                CategoriaProducto cat = new CategoriaProducto();
                cat.setCodCategoria(rs.getString("cod_categoria"));
                cat.setNombreCategoria(rs.getString("nombre_categoria"));
                cat.setDescripcion(rs.getString("descripcion"));
                list.add(cat);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaProductoRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    @Override
    public List<CategoriaProducto> findAllWhere(String condition, String value) {
        return null;
    }

    @Override
    public CategoriaProducto findById(Object id) {
        CategoriaProducto cat = null;
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CAT_FIND_BY_ID)){
            ps.setString(1, id.toString());
            try (ResultSet rs = ps.executeQuery()) {
                if(rs.next()){
                    cat = new CategoriaProducto();
                    cat.setCodCategoria(rs.getString("cod_categoria"));
                    cat.setCodCategoria(rs.getString("nombre_categoria"));
                    cat.setCodCategoria(rs.getString("descripcion"));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaProductoRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
            cat = null;
        }
        return cat;
    }

    @Override
    public int create(CategoriaProducto cat) {
        int response = 0;
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CAT_CREATE)) {
            ps.setString(1, cat.getCodCategoria());
            ps.setString(2, cat.getNombreCategoria());
            ps.setString(3, cat.getDescripcion());
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CategoriaProductoRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    @Override
    public int update(CategoriaProducto cat) {
        return 0;
    }

    @Override
    public int delete(Object id) {
        return 0;
    }    
}
