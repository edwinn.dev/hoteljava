package com.edmi.controller;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.model.Trabajador;
import com.edmi.repository.RepositoryEmployee;
import com.edmi.repository.imp.TrabajadorRepositoryImp;
import com.edmi.util.EncriptacionPasswordAES;
import com.edmi.util.Http;
import com.edmi.util.MessageView;
import com.edmi.util.Util;
import com.edmi.view.MainPanelServices;
import com.edmi.view.ModalNewEmployee;
import com.edmi.view.panel.PanelReportEmployee;
import com.edmi.view.panel.PanelReportReserva;
import com.google.gson.Gson;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public final class ControllerEmployee implements ActionListener, KeyListener {
    private List<Trabajador> listTrabajador;
    private boolean filtroUsuario = false;
    private static TrabajadorRepositoryImp repositoryEmploye;
    private ModalNewEmployee modalEmployee;
    private final MainPanelServices mainFrame;

    public ControllerEmployee(ModalNewEmployee modal, MainPanelServices mainFrame) {
        this.modalEmployee = modal;
        this.mainFrame = mainFrame;
        loadTableEmployees("1");
        setListener();
        ocultarPassword();
    }

    private void setListener() {
        if (modalEmployee != null) {
            modalEmployee.btnRegistrarTrabajador.addActionListener(this);
            modalEmployee.btnModificar.addActionListener(this);
            modalEmployee.btnBuscarRUC.addActionListener(this);
        }
        mainFrame.chkUsuariosDescartados.addActionListener(this);
    }

    private void ocultarPassword() {
        mainFrame.employeesTableEmployees.getColumnModel().getColumn(9).setMaxWidth(0);
        mainFrame.employeesTableEmployees.getColumnModel().getColumn(9).setMinWidth(0);
        mainFrame.employeesTableEmployees.getColumnModel().getColumn(9).setPreferredWidth(0);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (modalEmployee != null) {
            if (e.getSource() == modalEmployee.btnRegistrarTrabajador) {
                createEmployee();
            }
            if (e.getSource() == modalEmployee.btnModificar) {
                updateDataEmployee();
            }
            if (e.getSource() == modalEmployee.btnBuscarRUC) {
                buscarValidarNumDocu();
            }
        } else {
            if (e.getSource() == mainFrame.checkMostrarContra) {
                if (mainFrame.checkMostrarContra.isSelected()) {
                    String pass = panelContra();
                    if (pass != null && pass.equals(mainFrame.getPassword())) {
                        mainFrame.employeesTableEmployees.getColumnModel().getColumn(9).setMaxWidth(250);
                        mainFrame.employeesTableEmployees.getColumnModel().getColumn(9).setMinWidth(250);
                        mainFrame.employeesTableEmployees.getColumnModel().getColumn(9).setPreferredWidth(250);
                    } else if (pass != null) {
                        mainFrame.checkMostrarContra.setSelected(false);
                        JOptionPane.showMessageDialog(mainFrame, "Contraseña incorrecta.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                    } else {
                        mainFrame.checkMostrarContra.setSelected(false);
                    }
                } else {
                    mainFrame.employeesTableEmployees.getColumnModel().getColumn(9).setMaxWidth(0);
                    mainFrame.employeesTableEmployees.getColumnModel().getColumn(9).setMinWidth(0);
                    mainFrame.employeesTableEmployees.getColumnModel().getColumn(9).setPreferredWidth(0);
                }
            }

            if (e.getSource() == mainFrame.chkUsuariosDescartados) {
                if (mainFrame.chkUsuariosDescartados.isSelected()) {
                    loadTableEmployees("0");
                } else {
                    loadTableEmployees("1");
                }
            }
        }
    }

    private String panelContra() {
        JPanel panel = new JPanel();
        JLabel label = new JLabel("Contraseña: ");
        JPasswordField pass = new JPasswordField(15);
        panel.add(label);
        panel.add(pass);
        String[] options = new String[]{"OK", "Cancelar"};
        int option = JOptionPane.showOptionDialog(mainFrame, panel, SYS_MSG,
                JOptionPane.NO_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, options, options[1]);
        if (option == 0) { //OK
            return pass.getText();
        } else {
            return null;
        }
    }

    public void buscarValidarNumDocu() {
        String cad = modalEmployee.cbxTipoDocEmploye.getSelectedItem().toString().toLowerCase();
        String numDocu = modalEmployee.txtNroDoc.getText().trim();
        Gson g = new Gson();

        if (modalEmployee.cbxTipoDocEmploye.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(modalEmployee, "Seleccione el tipo de documento.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalEmployee.txtNroDoc.getText().isEmpty()) {
            JOptionPane.showMessageDialog(modalEmployee, "Ingrese el DNI o RUC del cliente para la búsqueda y validación.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (cad.equals("documento nacional de identidad") && numDocu.length() == 8) {
            Trabajador t = g.fromJson(Http.httpGetMethod("dni", numDocu), Trabajador.class);

            if (t == null) {
                JOptionPane.showMessageDialog(modalEmployee, "El número de documento ingresado es inválido", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }

            String[] datos = Util.separarCadena(t.getNombre(), " ");

            switch (datos.length) {
                case 3:
                    modalEmployee.txtNombre.setText(datos[2]);
                    break;
                case 4:
                    modalEmployee.txtNombre.setText(datos[2] + " " + datos[3]);
                    break;
                case 5:
                    modalEmployee.txtNombre.setText(datos[2] + " " + datos[3] + " " + datos[4]);
                    break;
                case 6:
                    modalEmployee.txtNombre.setText(datos[2] + " " + datos[3] + " " + datos[4] + " " + datos[5]);
                    break;
            }

            modalEmployee.txtApellidoPaterno.setText(t.getApellidoPaterno());
            modalEmployee.txtApellidoMaterno.setText(t.getApellidoMaterno());
            Util.rellenarVacios(modalEmployee);
            return;
        }

        if (cad.equals("registro unico de contribuyentes") || cad.equals("registro único de contribuyentes") && numDocu.length() == 11) {
            Trabajador t = g.fromJson(Http.httpGetMethod("ruc", numDocu), Trabajador.class);

            if (t == null) {
                JOptionPane.showMessageDialog(modalEmployee, "El número de documento ingresado es inválido", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }

            modalEmployee.txtNombre.setText(t.getNombre());
            Util.rellenarVacios(modalEmployee);
            return;
        }

        JOptionPane.showMessageDialog(modalEmployee, "Número de documento incorrecto.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
    }

    private void createEmployee() {
        RepositoryEmployee<Trabajador> repositoryEmployee = new TrabajadorRepositoryImp();
        if (isFieldsInvalid()) {
            JOptionPane.showMessageDialog(modalEmployee, MessageView.MODAL_EMPLOYEE_VALIDATE, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalEmployee.cbxTipoDocEmploye.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(modalEmployee, MessageView.MODAL_EMPLOYEE_SELECTED_INDEX_ERROR, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalEmployee.cbxCargo.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(modalEmployee, MessageView.MODAL_EMPLOYEE_SELECTED_INDEX_ERROR2, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalEmployee.txtNroDoc.getText().length() < 8) {
            JOptionPane.showMessageDialog(modalEmployee, MessageView.DOCUMENT_INVALID, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        List<String> nroDocu = repositoryEmploye.obtenerNroDocu();
        for (String nd : nroDocu) {
            if (modalEmployee.txtNroDoc.getText().trim().equals(nd)) {
                JOptionPane.showMessageDialog(modalEmployee, "El usuario ya se encuentra registrado.", SYS_MSG, JOptionPane.WARNING_MESSAGE);
                return;
            }
        }

        int option = JOptionPane.showConfirmDialog(modalEmployee, "¿Está seguro de registrar al usuario?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if(option == JOptionPane.YES_OPTION){
            Trabajador tb = new Trabajador();
            tb.setCodTrabajador(generarCodUsuario());
            tb.setCodTipoDocumento(repositoryEmployee.findAllTipeDocu(modalEmployee.cbxTipoDocEmploye.getSelectedItem()));
            tb.setNombre(modalEmployee.txtNombre.getText().trim());
            tb.setApellidoPaterno(modalEmployee.txtApellidoPaterno.getText().trim());
            tb.setApellidoMaterno(modalEmployee.txtApellidoMaterno.getText().trim());
            tb.setTipoDococumento(modalEmployee.cbxTipoDocEmploye.getSelectedItem().toString());
            tb.setNroDocumento(modalEmployee.txtNroDoc.getText().trim());
            tb.setNroTelefono(modalEmployee.txtTelefono.getText().isEmpty() ? "-" : modalEmployee.txtTelefono.getText().trim());
            tb.setEmail(modalEmployee.txtEmail.getText().isEmpty() ? "-" : modalEmployee.txtEmail.getText().toLowerCase().trim());
            tb.setObervacion(modalEmployee.txtObservacion.getText().isEmpty() ? "-" : modalEmployee.txtObservacion.getText().trim());
            tb.setCargo(modalEmployee.cbxCargo.getSelectedItem().toString());
            tb.setUsername(modalEmployee.txtUsername.getText().trim());
            tb.setPassword(EncriptacionPasswordAES.encriptar(modalEmployee.txtPassword.getText().trim()));
            tb.setEstado("1");
            int response = repositoryEmployee.create(tb);
            if (response <= 0) {
                JOptionPane.showMessageDialog(modalEmployee, "Error al registrar usuario, intente nuevamente.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }
            JOptionPane.showMessageDialog(modalEmployee, "Trabajador registrado.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            clearTextField();
            loadTableEmployees("1");
            PanelReportEmployee.getInstance().loadTableEmployees();
            PanelReportReserva.getInstance().mostrarUsuarios();
            loadData();
            this.modalEmployee.dispose();
        }
    }

    private boolean isFieldsInvalid() {
        boolean invalid = modalEmployee.txtNombre.getText().isEmpty() || modalEmployee.txtApellidoPaterno.getText().isEmpty() || modalEmployee.txtApellidoMaterno.getText().isEmpty()
                || modalEmployee.txtNroDoc.getText().isEmpty();
        return invalid;
    }

    private void clearTextField() {
        Component[] components = modalEmployee.getContentPane().getComponents();
        for (Component c : components) {
            if (c instanceof JFormattedTextField) {
                ((JFormattedTextField) c).setValue("");
            }
            if (c instanceof JTextField) {
                ((JTextField) c).setText("");
            }
            modalEmployee.cbxCargo.setSelectedIndex(0);
            modalEmployee.cbxTipoDocEmploye.setSelectedIndex(0);
        }
        modalEmployee.txtObservacion.setText("");
    }

    public void eliminarUsuario() {
        int row = mainFrame.employeesTableEmployees.getSelectedRow();
        if (row >= 0) {
            int op = JOptionPane.showConfirmDialog(modalEmployee, "¿Desea eliminar al usuario?", SYS_MSG, JOptionPane.YES_NO_OPTION);
            if (op == JOptionPane.YES_OPTION) {
                String codEmployee = mainFrame.employeesTableEmployees.getValueAt(row, 0).toString();
                RepositoryEmployee<Trabajador> repositoryEmployee = new TrabajadorRepositoryImp();
                repositoryEmployee.delete(codEmployee);
                JOptionPane.showMessageDialog(modalEmployee, "El usuario ha sido eliminado.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                loadTableEmployees("1");
            }
        } else {
            JOptionPane.showMessageDialog(modalEmployee, "Debe seleccionar la fila a eliminar.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private String generarCodUsuario() {
        repositoryEmploye = new TrabajadorRepositoryImp();
        List<Trabajador> lista = repositoryEmploye.findAll();
        String codigo;
        if (lista.size() > 0) {
            Trabajador ultimoTrabajador = lista.get(lista.size() - 1);
            String cod = ultimoTrabajador.getCodTrabajador();
            Integer codigoUsuario = Integer.parseInt(cod.substring(4));
            codigo = String.format("%4s", (codigoUsuario + 1)).replace(" ", "0");
        } else {
            codigo = "0001";
        }
        return "EMP-" + codigo;
    }

    public void loadTableEmployees(String estado) {
        clearTable(mainFrame.employeesTableEmployees);
        RepositoryEmployee<Trabajador> repositoryEmployee = new TrabajadorRepositoryImp();
        DefaultTableModel modelEmployee = (DefaultTableModel) mainFrame.employeesTableEmployees.getModel();
        List<Trabajador> employees = repositoryEmployee.findAllEstado(estado);
        if (filtroUsuario) {
            listTrabajador.forEach(t -> {
                if (t.getEstado().equals("1")) {
                    modelEmployee.addRow(new Object[]{t.getCodTrabajador(), t.getNombre().concat(" ").concat(t.getApellidoPaterno()).concat(" ").concat(t.getApellidoMaterno()),
                        t.getTipoDococumento(), t.getNroDocumento(), t.getNroTelefono(), t.getEmail(), t.getObervacion(), t.getCargo(), t.getUsername(),
                        EncriptacionPasswordAES.desencriptar(t.getPassword()), "REGISTRADO"});
                } else {
                    modelEmployee.addRow(new Object[]{t.getCodTrabajador(), t.getNombre().concat(" ").concat(t.getApellidoPaterno()).concat(" ").concat(t.getApellidoMaterno()),
                        t.getTipoDococumento(), t.getNroDocumento(), t.getNroTelefono(), t.getEmail(), t.getObervacion(), t.getCargo(), t.getUsername(),
                        EncriptacionPasswordAES.desencriptar(t.getPassword()), "DESCARTADO"});
                }
            });
            mainFrame.employeesTableEmployees.setModel(modelEmployee);
        } else {
            employees.forEach(t -> {
                if (t.getEstado().equals("1")) {
                    modelEmployee.addRow(new Object[]{t.getCodTrabajador(), t.getNombre().concat(" ").concat(t.getApellidoPaterno()).concat(" ").concat(t.getApellidoMaterno()),
                        t.getTipoDococumento(), t.getNroDocumento(), t.getNroTelefono(), t.getEmail(), t.getObervacion(), t.getCargo(), t.getUsername(),
                        EncriptacionPasswordAES.desencriptar(t.getPassword()), "REGISTRADO"});
                } else {
                    modelEmployee.addRow(new Object[]{t.getCodTrabajador(), t.getNombre().concat(" ").concat(t.getApellidoPaterno()).concat(" ").concat(t.getApellidoMaterno()),
                        t.getTipoDococumento(), t.getNroDocumento(), t.getNroTelefono(), t.getEmail(), t.getObervacion(), t.getCargo(), t.getUsername(),
                        EncriptacionPasswordAES.desencriptar(t.getPassword()), "DESCARTADO"});
                }
            });
            mainFrame.employeesTableEmployees.setModel(modelEmployee);
        }
    }

    public void loadTableEmployeesId(Object id) {
        clearTable(mainFrame.employeesTableEmployees);
        RepositoryEmployee<Trabajador> repositoryEmployee = new TrabajadorRepositoryImp();
        DefaultTableModel modelEmployee = (DefaultTableModel) mainFrame.employeesTableEmployees.getModel();
        Trabajador tbj = repositoryEmployee.findById(id);
        List<Trabajador> employees = new ArrayList();
        employees.add(tbj);
        employees.forEach((t) -> {
            modelEmployee.addRow(new Object[]{
                t.getCodTrabajador(), t.getNombre().concat(" ").concat(t.getApellidoPaterno()).concat(" ").concat(t.getApellidoMaterno()),
                t.getTipoDococumento(), t.getNroDocumento(), t.getNroTelefono(), t.getEmail(), t.getObervacion(), t.getCargo(), t.getUsername(), t.getPassword(),
                t.getEstado()
            });
        });
        mainFrame.employeesTableEmployees.setModel(modelEmployee);
    }

    public void loadData() {
        modalEmployee.cbxTipoDocEmploye.requestFocus();
        modalEmployee.txtCodUsuario.setText(generarCodUsuario());
    }

    public void mostrarDatosTabla(int row, JTable tabla) {
        RepositoryEmployee<Trabajador> repositoryEmployee = new TrabajadorRepositoryImp();
        Trabajador tbj = repositoryEmployee.findById(tabla.getValueAt(row, 0).toString().trim());
        modalEmployee.txtCodUsuario.setText(tbj.getCodTrabajador());
        modalEmployee.cbxTipoDocEmploye.setSelectedItem(tbj.getTipoDococumento());
        modalEmployee.txtNroDoc.setText(tbj.getNroDocumento());
        modalEmployee.cbxCargo.setSelectedItem(tbj.getCargo());
        modalEmployee.cbxTipoDocEmploye.setSelectedItem(tbj.getTipoDococumento());
        modalEmployee.txtNombre.setText(tbj.getNombre());
        modalEmployee.txtApellidoPaterno.setText(tbj.getApellidoPaterno());
        modalEmployee.txtApellidoMaterno.setText(tbj.getApellidoMaterno());
        modalEmployee.txtTelefono.setText(tbj.getNroTelefono());
        modalEmployee.txtEmail.setText(tbj.getEmail());
        modalEmployee.txtUsername.setText(tbj.getUsername());
        modalEmployee.txtPassword.setText(EncriptacionPasswordAES.desencriptar(tbj.getPassword()));
        modalEmployee.txtObservacion.setText(tbj.getObervacion());
        modalEmployee.setVisible(true);
    }

    private void updateDataEmployee() {
        RepositoryEmployee<Trabajador> repositoryEmployee = new TrabajadorRepositoryImp();
        if (isFieldsInvalid()) {
            JOptionPane.showMessageDialog(modalEmployee, MessageView.MODAL_EMPLOYEE_VALIDATE, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalEmployee.cbxTipoDocEmploye.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(modalEmployee, MessageView.MODAL_EMPLOYEE_SELECTED_INDEX_ERROR, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalEmployee.cbxCargo.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(modalEmployee, MessageView.MODAL_EMPLOYEE_SELECTED_INDEX_ERROR2, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (modalEmployee.txtNroDoc.getText().length() < 8) {
            JOptionPane.showMessageDialog(modalEmployee, MessageView.DOCUMENT_INVALID, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        int option = JOptionPane.showConfirmDialog(modalEmployee, "¿Está seguro de modificar los datos del usuario?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if(option == JOptionPane.YES_OPTION){
            Trabajador tb = new Trabajador();
            tb.setCodTrabajador(modalEmployee.txtCodUsuario.getText());
            tb.setCodTipoDocumento(repositoryEmployee.findAllTipeDocu(modalEmployee.cbxTipoDocEmploye.getSelectedItem()));
            tb.setNombre(modalEmployee.txtNombre.getText().trim());
            tb.setApellidoPaterno(modalEmployee.txtApellidoPaterno.getText().trim());
            tb.setApellidoMaterno(modalEmployee.txtApellidoMaterno.getText().trim());
            tb.setTipoDococumento(modalEmployee.txtNroDoc.getText().trim());
            tb.setNroDocumento(modalEmployee.txtNroDoc.getText().trim());
            tb.setNroTelefono(modalEmployee.txtTelefono.getText().isEmpty() ? "-" : modalEmployee.txtTelefono.getText().trim());
            tb.setEmail(modalEmployee.txtEmail.getText().isEmpty() ? "-" : modalEmployee.txtEmail.getText().toLowerCase().trim());
            tb.setObervacion(modalEmployee.txtObservacion.getText().isEmpty() ? "-" : modalEmployee.txtObservacion.getText().trim());
            tb.setCargo(modalEmployee.cbxCargo.getSelectedItem().toString());
            tb.setUsername(modalEmployee.txtUsername.getText().trim());
            tb.setPassword(EncriptacionPasswordAES.encriptar(modalEmployee.txtPassword.getText().trim()));

            int response = repositoryEmployee.update(tb);

            if (response <= 0) {
                JOptionPane.showMessageDialog(modalEmployee, "Error al actualizar los datos del usuario, \ncontacte con el administrador", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }
            JOptionPane.showMessageDialog(modalEmployee, "Datos del usuario actualizados.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            clearTextField();
            loadTableEmployees("1");
            PanelReportEmployee.getInstance().loadTableEmployees();
            PanelReportReserva.getInstance().mostrarUsuarios();
            modalEmployee.dispose();
        }
    }

    private DefaultTableModel clearTable(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        return model;
    }

    private void filterUser() {
        filtroUsuario = true;
        repositoryEmploye = new TrabajadorRepositoryImp();
        listTrabajador = new ArrayList<>();
        int index = mainFrame.cbxCriterioUsuario.getSelectedIndex();

        if (index != 0) {
            String valueSend = mainFrame.txtFiltroUsuario.getText().trim();
            String condition = null;
            switch (index) {
                case 1:
                    condition = "cod_trabajador";
                    break;
                case 2:
                    condition = "cargo";
                    break;
                case 3:
                    condition = "nombre";
                    break;
                case 4:
                    condition = "apellido_paterno";
                    break;
                case 5:
                    condition = "apellido_materno";
                    break;
                case 6:
                    condition = "nro_documento";
                    break;
            }

            String estado;

            if (mainFrame.chkUsuariosDescartados.isSelected()) {
                estado = "0";
            } else {
                estado = "1";
            }

            listTrabajador = repositoryEmploye.findAllWhere(condition, valueSend, estado);
            loadTableEmployees(estado);
            filtroUsuario = false;
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == mainFrame.txtFiltroUsuario) {
            filterUser();
        }
    }
}