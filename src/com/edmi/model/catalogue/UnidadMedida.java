package com.edmi.model.catalogue;

public class UnidadMedida {
    private String codUnidadMedida;
    private String descripcion;
    
    public UnidadMedida(){}
    
    public UnidadMedida(String codUnidadMedida, String descripcion){
        this.codUnidadMedida = codUnidadMedida;
        this.descripcion = descripcion;
    }

    public String getCodUnidadMedida() {
        return codUnidadMedida;
    }

    public void setCodUnidadMedida(String codUnidadMedida) {
        this.codUnidadMedida = codUnidadMedida;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UnidadMedida{codUnidadMedida=").append(codUnidadMedida);
        sb.append(", descripcion=").append(descripcion);
        sb.append('}');
        return sb.toString();
    }
}
