package com.edmi.model.invoice;

public class DatoPago {
    private String formaPago;
    private String mtoNetoPendientePago;
    private String tipMonedaMtoNetoPendientePago;

    public String getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(String formaPago) {
        this.formaPago = formaPago;
    }

    public String getMtoNetoPendientePago() {
        return mtoNetoPendientePago;
    }

    public void setMtoNetoPendientePago(String mtoNetoPendientePago) {
        this.mtoNetoPendientePago = mtoNetoPendientePago;
    }

    public String getTipMonedaMtoNetoPendientePago() {
        return tipMonedaMtoNetoPendientePago;
    }

    public void setTipMonedaMtoNetoPendientePago(String tipMonedaMtoNetoPendientePago) {
        this.tipMonedaMtoNetoPendientePago = tipMonedaMtoNetoPendientePago;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DatoPago{formaPago=").append(formaPago);
        sb.append(", mtoNetoPendientePago=").append(mtoNetoPendientePago);
        sb.append(", tipMonedaMtoNetoPendientePago=").append(tipMonedaMtoNetoPendientePago);
        sb.append('}');
        return sb.toString();
    }
}
