package com.edmi.repository;

import java.util.List;

public interface RepositorySale<T> {
    List<T> findAll();
    List<T> findAllWhere(String condition, String value);
    T findById(Object id);
    int create(T sale);
    int update(T sale);
    int delete(Object id);
}
