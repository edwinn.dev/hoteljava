package com.edmi.view;

import com.edmi.repository.RepositoryClient;
import com.edmi.repository.imp.ClienteRepositoryImp;
import com.edmi.model.Cliente;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.table.DefaultTableModel;

public class DialogSearchClient extends javax.swing.JDialog {
    private RepositoryClient repositoryClient;
    private List<Cliente> clientsBySearch;
    private DefaultTableModel model;
    private Cliente client;
    private boolean stateRow = false;

    public DialogSearchClient(JDialog owner, boolean modal) {
        super(owner, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        loadTableAllClients();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtBusqueda = new javax.swing.JFormattedTextField();
        cbxCriterioBusqueda = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableClients = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Buscar Cliente");
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Criterios:");

        txtBusqueda.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBusquedaKeyReleased(evt);
            }
        });

        cbxCriterioBusqueda.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxCriterioBusqueda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --", "NOMBRE", "NRO. DOCUMENTO" }));

        tableClients = new javax.swing.JTable(){
            public boolean isCellEditable(int row, int col){
                for(int i = 0; i < tableClients.getRowCount(); i++){
                    if(row == i){
                        return false;
                    }
                }
                return true;
            }
        };
        tableClients.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tableClients.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "COD. CLIENTE", "NOMBRES", "APELLIDO PATERNO", "APELLIDO MATERNO", "TIPO DOCUMENTO", "NRO. DOCUMENTO", "TELEFONO", "EMAIL"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableClients.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tableClients.getTableHeader().setReorderingAllowed(false);
        tableClients.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableClientsMouseClicked(evt);
            }
        });
        tableClients.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tableClientsKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tableClients);
        if (tableClients.getColumnModel().getColumnCount() > 0) {
            tableClients.getColumnModel().getColumn(0).setPreferredWidth(100);
            tableClients.getColumnModel().getColumn(1).setPreferredWidth(250);
            tableClients.getColumnModel().getColumn(2).setPreferredWidth(150);
            tableClients.getColumnModel().getColumn(3).setPreferredWidth(150);
            tableClients.getColumnModel().getColumn(4).setPreferredWidth(150);
            tableClients.getColumnModel().getColumn(5).setPreferredWidth(120);
            tableClients.getColumnModel().getColumn(6).setPreferredWidth(100);
            tableClients.getColumnModel().getColumn(7).setPreferredWidth(250);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 830, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbxCriterioBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbxCriterioBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tableClientsKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableClientsKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            setClientValues();
        }
    }//GEN-LAST:event_tableClientsKeyPressed

    private void tableClientsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableClientsMouseClicked
        if (evt.getClickCount() == 2) {
            setClientValues();
        }
    }//GEN-LAST:event_tableClientsMouseClicked

    private void txtBusquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBusquedaKeyReleased
        filterClient();
    }//GEN-LAST:event_txtBusquedaKeyReleased

    private void filterClient() {
        repositoryClient = new ClienteRepositoryImp();
        clientsBySearch = new ArrayList<>();
        int index = cbxCriterioBusqueda.getSelectedIndex();
        
        if(index == 0){
            loadTableAllClients(); return;
        }

        String valueSend = txtBusqueda.getText().trim();
        String condition = null;
        switch (index) {
            case 1:
                condition = "nombre";
                break;
            case 2:
                condition = "nro_documento";
                break;
        }
        clientsBySearch = repositoryClient.findAllWhere(condition, valueSend, "1");
        loadPreferencesTableClients();
    }

    public Cliente getClient() {
        return this.client;
    }

    public boolean isSelectedRow() {
        return this.stateRow;
    }

    private void setClientValues() {
        Cliente cli = new Cliente();
        int row = tableClients.getSelectedRow();

        cli.setCodCliente(tableClients.getValueAt(row, 0).toString());
        cli.setNombre(tableClients.getValueAt(row, 1).toString());
        cli.setApellidoPaterno(tableClients.getValueAt(row, 2).toString());
        cli.setApellidoMaterno(tableClients.getValueAt(row, 3).toString());
        cli.setTipoDocumento(tableClients.getValueAt(row, 4).toString());
        cli.setNroDocumento(tableClients.getValueAt(row, 5).toString());
        cli.setNroTelefono(tableClients.getValueAt(row, 6).toString());
        cli.setEmail(tableClients.getValueAt(row, 7).toString());
        this.client = cli;
        stateRow = true; //Significa que se ha selecciondo una fila

        //Despues de haber presionado Enter o doble click sobre el registro. El formulario se cierra
        this.setVisible(false);
        this.dispose();
    }

    private void loadPreferencesTableClients() {
        clearTableRooms();
        repositoryClient = new ClienteRepositoryImp();
        model = (DefaultTableModel) tableClients.getModel();
        clientsBySearch.forEach(c -> {
            model.addRow(new Object[]{
                c.getCodCliente(), c.getNombre(), c.getApellidoPaterno(), c.getApellidoMaterno(), c.getTipoDocumento(), 
                c.getNroDocumento(), c.getNroTelefono(), c.getEmail()
            });
        });
        tableClients.setModel(model);
    }

    private void loadTableAllClients() {
        clearTableRooms();
        model = (DefaultTableModel) tableClients.getModel();
        repositoryClient = new ClienteRepositoryImp();
        List<Cliente> allClients = repositoryClient.findAllEstado("1");
        allClients.forEach(c -> {
            model.addRow(new Object[]{
                c.getCodCliente(), c.getNombre(), c.getApellidoPaterno(), c.getApellidoMaterno(), c.getTipoDocumento(), 
                c.getNroDocumento(), c.getNroTelefono(), c.getEmail()
            });
        });
        tableClients.setModel(model);
    }

    private void clearTableRooms() {
        model = (DefaultTableModel) tableClients.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cbxCriterioBusqueda;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable tableClients;
    private javax.swing.JFormattedTextField txtBusqueda;
    // End of variables declaration//GEN-END:variables
}
