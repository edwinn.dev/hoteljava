package com.edmi.controller.panel;

import com.edmi.model.Consumo;
import com.edmi.model.Reservacion;
import com.edmi.repository.RepositoryConsumo;
import com.edmi.repository.RepositoryReservation;
import com.edmi.repository.imp.ConsumoRepositoryImp;
import com.edmi.repository.imp.ReservaRepositoryImp;
import com.edmi.util.UtilTable;
import com.edmi.view.MainPanelServices;
import com.edmi.view.panel.PanelDetailReservation;
import com.edmi.view.panel.PanelReservation;
import com.edmi.view.panel.PanelVoucherTicket;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.table.DefaultTableModel;

public class ControllerDetailReserva implements ActionListener {
    private PanelDetailReservation panelDetailReserva = null;
    private MainPanelServices mainFrame = null;
    private PanelReservation panelReservation = null;
    private RepositoryConsumo<Consumo> repositoryConsumo = null;
    private RepositoryReservation<Reservacion> repositoryReserva = null;
    private PanelVoucherTicket panelVoucherTicket = null;
    private ControllerVoucherTicket controllerVoucher = null;
    //Utils
    private Reservacion reserva = null;
    private int item = 1;

    public ControllerDetailReserva(PanelDetailReservation panelDetailReservation, MainPanelServices mainFrame, PanelReservation panelReserva) {
        this.mainFrame = mainFrame;
        this.panelDetailReserva = panelDetailReservation;
        this.panelReservation = panelReserva;
        this.repositoryReserva = new ReservaRepositoryImp();
        setLitener();
    }

    private void setLitener() {
        this.panelDetailReserva.btnBoleta.addActionListener(this);
        this.panelDetailReserva.btnFactura.addActionListener(this);
        this.panelDetailReserva.btnRegresar.addActionListener(this);
    }

    public void mostrarConsumos(String codigoReserva) {
        repositoryConsumo = new ConsumoRepositoryImp();
        List<Consumo> consumos = repositoryConsumo.findByReservation(codigoReserva);
        UtilTable.clearTable(panelDetailReserva.tableServices);
        DefaultTableModel model = (DefaultTableModel) panelDetailReserva.tableServices.getModel();
        consumos.forEach(c -> {
            model.addRow(new Object[]{
                item++, c.getCodProducto(), c.getNombreProducto(), c.getCantidad(), c.getPrecioUnitario(),
                c.getImporteTotal()
            });
        });
        item = consumos.size() + 1;
        model.addRow(new Object[]{
            item++, "RSE", "Reservacion de la habitacion N°".concat(reserva.getAuxNroRoom()),
            1, reserva.getImporteReserva(), reserva.getImporteReserva()
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {        
        if (this.panelDetailReserva != null) {
            if (e.getSource() == panelDetailReserva.btnRegresar) {
                returnToPanelReservation();
                return;
            }

            if (e.getSource() == panelDetailReserva.btnBoleta) {
                irABoletaFactura();
                this.controllerVoucher.loadDataPanel(reserva, "B");
                return;
            }

            if (e.getSource() == panelDetailReserva.btnFactura) {
                irABoletaFactura();
                this.controllerVoucher.loadDataPanel(reserva, "F");
            }
        }
    }

    private void irABoletaFactura() {
        mainFrame.tabbedPanel.setSelectedComponent(mainFrame.capaFacturacion);
        this.panelVoucherTicket = PanelVoucherTicket.getInstance();
        this.controllerVoucher = ControllerVoucherTicket.getInstanceVoucher(panelVoucherTicket);
        ControllerFacura.setVisiblePanels();
        this.controllerVoucher.loadDataConsumption(panelDetailReserva.txtIdReserva.getText().trim());
    }

    public void setDataDetailReserva(String codigoReserva) {
        reserva = repositoryReserva.findById(codigoReserva);
        this.nombreCliente = reserva.getNombreCliente();
        this.idCliente = reserva.getCodCliente();
        this.tipoDocumento = reserva.getTipoDocumento();
        this.idTipoDocumento = reserva.getCodTipoDoc();
        this.nroDocumento = reserva.getNroDocumento();
        this.nroHabitacion = reserva.getAuxNroRoom();
        this.idHabitacion = reserva.getCodHabitacion();
        this.idReservaDb = reserva.getCodReserva().toString();
        this.importeReserva = reserva.getImporteReserva();
        this.tiempoAlojamiento = String.valueOf(reserva.getTiempoReserva()).concat(" ").concat(reserva.getMagnitudReserva());
        if (reserva.getImporteProductos() == null) {
            this.importeProductos = 0.0;
        } else {
            this.importeProductos = reserva.getImporteProductos();
        }

        if (idTipoDocumento.equals("1")) {
            panelDetailReserva.btnFactura.setEnabled(false);
        }

        this.panelDetailReserva.txtIdReserva.setText(codigoReserva);
        this.panelDetailReserva.txtNombreCliente.setText(this.nombreCliente);
        this.panelDetailReserva.txtIdCliente.setText(this.idCliente);
        this.panelDetailReserva.txtTipoDoc.setText(this.tipoDocumento);
        this.panelDetailReserva.txtIdTipoDoc.setText(this.idTipoDocumento);
        this.panelDetailReserva.txtNroDoc.setText(this.nroDocumento);
        this.panelDetailReserva.txtNroHabitacion.setText(this.nroHabitacion);
        this.panelDetailReserva.txtIdHabitacion.setText(this.nroHabitacion);
        this.panelDetailReserva.txtTiempoAlojamiento.setText(this.tiempoAlojamiento);
        this.panelDetailReserva.txtIdReserva.setText(this.idReservaDb);
        this.panelDetailReserva.txtCostoReserva.setText(String.valueOf(this.importeReserva));
        this.panelDetailReserva.txtCostoProductos.setText(this.importeProductos <= 0.0D ? "0.00" : String.valueOf(this.importeProductos));
    }

    private void returnToPanelReservation() {
        this.panelDetailReserva.txtIdReserva.setText("");
        this.panelDetailReserva.txtIdHabitacion.setText("");
        this.panelDetailReserva.setVisible(false);
        this.panelReservation.setVisible(true);

        this.reserva = null;
        this.panelDetailReserva.btnFactura.setEnabled(true);
        this.panelDetailReserva = null;
        this.repositoryConsumo = null;
     
        this.panelVoucherTicket = null;
        this.repositoryConsumo = null;
        this.repositoryReserva = null;
        this.controllerVoucher = null;
        this.idReservaDb = null;
        this.item = 1;

        this.nombreCliente = null;
        this.idCliente = null;
        this.tipoDocumento = null;
        this.idTipoDocumento = null;
        this.nroDocumento = null;
        this.nroHabitacion = null;
        this.idReservaDb = null;
        this.importeProductos = null;
        this.importeReserva = null;
    }

    private String nombreCliente;
    private String idCliente;
    private String tipoDocumento;
    private String idTipoDocumento;
    private String nroDocumento;
    private String tiempoAlojamiento;
    private String nroHabitacion;
    private String idHabitacion;
    private String idReservaDb;
    private Double importeProductos;
    private Double importeReserva;
}