package com.edmi.model;

public class Habitacion {
    private String codhaHabitacion;
    private String codTipoHabitacion;
    private int nroHabitacion;
    private String tipoHabitacion;
    private String estado;
    private int nroPlanta;
    private Double costoAlquiler;
    private String caracteristicas;
    private String state;
    
    public Habitacion(){}

    public Habitacion(String codhaHabitacion, String codTipoHabitacion, int nroHabitacion, String tipoHabitacion, String estado, int nroPlanta, Double costoAlquiler, String caracteristicas) {
        this.codhaHabitacion = codhaHabitacion;
        this.codTipoHabitacion = codTipoHabitacion;
        this.nroHabitacion = nroHabitacion;
        this.tipoHabitacion = tipoHabitacion;
        this.estado = estado;
        this.nroPlanta = nroPlanta;
        this.costoAlquiler = costoAlquiler;
        this.caracteristicas = caracteristicas;
    }

    public String getCodhaHabitacion() {
        return codhaHabitacion;
    }

    public void setCodhaHabitacion(String codhaHabitacion) {
        this.codhaHabitacion = codhaHabitacion;
    }

    public String getCodTipoHabitacion() {
        return codTipoHabitacion;
    }

    public void setCodTipoHabitacion(String codTipoHabitacion) {
        this.codTipoHabitacion = codTipoHabitacion;
    }
    
    public int getNroHabitacion() {
        return nroHabitacion;
    }

    public void setNroHabitacion(int nroHabitacion) {
        this.nroHabitacion = nroHabitacion;
    }

    public String getTipoHabitacion() {
        return tipoHabitacion;
    }

    public void setTipoHabitacion(String tipoHabitacion) {
        this.tipoHabitacion = tipoHabitacion.toUpperCase();
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado.toUpperCase();
    }

    public int getNroPlanta() {
        return nroPlanta;
    }

    public void setNroPlanta(int nroPlanta) {
        this.nroPlanta = nroPlanta;
    }

    public Double getCostoAlquiler() {
        return costoAlquiler;
    }

    public void setCostoAlquiler(Double costoAlquiler) {
        this.costoAlquiler = costoAlquiler;
    }

    public String getCaracteristicas() {
        return caracteristicas;
    }

    public void setCaracteristicas(String caracteristicas) {
        this.caracteristicas = caracteristicas.toUpperCase();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
