package com.edmi.model;

public class Theme {
    private String idTheme;
    private Integer theme;
    private String description;
    private String state;

    public Theme(){}
    
    public Theme(String idTheme, Integer theme, String description, String state) {
        this.idTheme = idTheme;
        this.theme = theme;
        this.description = description;
        this.state = state;
    }

    public String getIdTheme() {
        return idTheme;
    }

    public void setIdTheme(String idTheme) {
        this.idTheme = idTheme;
    }

    public Integer getTheme() {
        return theme;
    }

    public void setTheme(Integer theme) {
        this.theme = theme;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Theme{idTheme=").append(idTheme);
        sb.append(", theme=").append(theme);
        sb.append(", description=").append(description);
        sb.append(", state=").append(state);
        sb.append('}');
        return sb.toString();
    }
}
