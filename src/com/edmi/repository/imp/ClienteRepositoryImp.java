package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.model.Cliente;
import com.edmi.database.query.Query;
import com.edmi.repository.RepositoryClient;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClienteRepositoryImp implements RepositoryClient<Cliente> {

    private Connection getConnection() throws SQLException {
        return getInstance();
    }

    @Override
    public List<Cliente> findAllEstado(String estado) {
        List<Cliente> clients = new ArrayList<>();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CLI_FIND_ALL_ESTADO)) {
            ps.setString(1, estado);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Cliente client = getClient(rs);
                clients.add(client);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clients;
    }

    @Override
    public List<Cliente> findAll() {
        List<Cliente> clients = new ArrayList<>();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CLI_FIND_ALL)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Cliente client = getClient(rs);
                clients.add(client);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clients;
    }

    @Override
    public List<Cliente> findAllWhere(String condition, String value, String estado) {
        String SQL = "SELECT * FROM cliente WHERE " + condition + " LIKE " + "'" + value + "%' AND estado = '" + estado + "'";
        List<Cliente> clients = new ArrayList<>();
        try ( PreparedStatement ps = getConnection().prepareStatement(SQL);  ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                Cliente client = getClient(rs);
                clients.add(client);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return clients;
    }

    @Override
    public Cliente findById(Object id) {
        Cliente client = null;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CLI_FIND_BY_ID)) {
            ps.setString(1, id.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                client = getClient(rs);
            }
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return client;
    }

    @Override
    public int create(Cliente cliente) {
        int response = 0;
        try ( PreparedStatement st = getConnection().prepareStatement(Query.SQL_CLI_CREATE)) {
            st.setString(1, cliente.getCodCliente());
            st.setString(2, cliente.getCodTipoDocumento());
            st.setString(3, cliente.getNombre());
            st.setString(4, cliente.getApellidoPaterno());
            st.setString(5, cliente.getApellidoMaterno());
            st.setString(6, cliente.getTipoDocumento());
            st.setString(7, cliente.getNroDocumento());
            st.setString(8, cliente.getNroTelefono());
            st.setString(9, cliente.getEmail());
            st.setString(10, cliente.getObervacion());
            st.setString(11, cliente.getEstado());
            response = st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ClienteRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    @Override
    public int update(Cliente cliente) {
        int response = 0;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CLI_UPDATE)) {
            ps.setString(1, cliente.getCodTipoDocumento());
            ps.setString(2, cliente.getNombre());
            ps.setString(3, cliente.getApellidoPaterno());
            ps.setString(4, cliente.getApellidoMaterno());
            ps.setString(5, cliente.getTipoDocumento());
            ps.setString(6, cliente.getNroDocumento());
            ps.setString(7, cliente.getNroTelefono());
            ps.setString(8, cliente.getEmail());
            ps.setString(9, cliente.getObervacion());
            ps.setString(10, cliente.getCodCliente());
            response = ps.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(TrabajadorRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }

    @Override
    public int state(Object id, Object estado) {
        int response = 0;
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CLI_STATE)) {
            ps.setString(1, estado.toString());
            ps.setString(2, id.toString());
            response = ps.executeUpdate();
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return response;
    }

    @Override
    public String findAllTipeDocu(Object tipoDocu) {
        String cod = "";
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CLI_TIPODOCU)) {
            ps.setString(1, tipoDocu.toString());
            try ( ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    cod = rs.getString("cod_tipodocumento");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return cod;
    }

    @Override
    public List<String> findAllDocuDescripciones() {
        List<String> lista = new ArrayList();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CLI_FIND_AL_DOCU)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    lista.add(rs.getString("descripcion"));
                }
            }
        } catch (Exception e) {
            Logger.getLogger(HabitacionRepositoryImp.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }

    public List<String> obtenerNroDocu() {
        List<String> lista = new ArrayList();
        try ( PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CLI_FIND_AL_NRODOCU)) {
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    String nroDocu = rs.getString("nro_documento");
                    lista.add(nroDocu);
                }
            }
        } catch (Exception e) {
        }
        return lista;
    }

    private Cliente getClient(ResultSet rs) throws SQLException {
        Cliente c = new Cliente();
        c.setCodCliente(rs.getString("cod_cliente"));
        c.setCodTipoDocumento("cod_tipodocumento");
        c.setNombre(rs.getString("nombre"));
        c.setApellidoPaterno(rs.getString("apellido_paterno"));
        c.setApellidoMaterno(rs.getString("apellido_materno"));
        c.setTipoDocumento(rs.getString("tipo_documento"));
        c.setNroDocumento(rs.getString("nro_documento"));
        c.setNroTelefono(rs.getString("nro_telefono"));
        c.setEmail(rs.getString("email"));
        c.setObervacion(rs.getString("observacion"));
        c.setEstado(rs.getString("estado"));
        return c;
    }

    @Override
    public Cliente findByNroDocu(Object nro) {
        Cliente c = null;
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_CLI_NRODOCU)) {
            ps.setString(1, nro.toString());
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    c = getClient(rs);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClienteRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return c;
    }
}
