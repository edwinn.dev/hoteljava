package com.edmi.view.widget;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

public class MenuTableClient {  
    private static MenuTableClient instance;

    private JPopupMenu menu;
    public JMenu menuActualizarEstado;
    public JMenuItem itemRegistrado;
    public JMenuItem itemDescartado;
    
    public static MenuTableClient getInstance() {
        return instance == null ? new MenuTableClient() : instance;
    }
    
    public void initMenuTableClient(JTable tabla) {
        menu = new JPopupMenu();
        menuActualizarEstado = new JMenu("Acción");
        itemDescartado = new JMenuItem("Eliminar");
        
        menu.add(menuActualizarEstado);
        menuActualizarEstado.add(itemDescartado);
        
        tabla.setComponentPopupMenu(menu);
    }
}
