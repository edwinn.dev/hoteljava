package com.edmi.model.invoice;

public class Detalle {
    private String codUnidadMedida;
    private String ctdUnidadItem;
    private String codProducto;
    private String codProductoSUNAT;
    private String desItem;
    private String mtoValorUnitario;
    private String sumTotTributosItem;
    private String codTriIGV;
    private String mtoIgvItem;
    private String mtoBaseIgvItem;
    private String nomTributoIgvItem;
    private String codTipTributoIgvItem;
    private String tipAfeIGV;
    private String porIgvItem;
    private String codTriISC;
    private String mtoIscItem;
    private String mtoBaseIscItem;
    private String nomTributoIscItem;
    private String codTipTributoIscItem;
    private String tipSisISC;
    private String porIscItem;
    private String codTriOtro;
    private String mtoTriOtroItem;
    private String mtoBaseTriOtroItem;
    private String nomTributoOtroItem;
    private String codTipTributoOtroItem;
    private String porTriOtroItem; //String
    private String codTriIcbper;
    private String mtoTriIcbperItem;
    private String ctdBolsasTriIcbperItem;
    private String nomTributoIcbperItem;
    private String codTipTributoIcbperItem;
    private String mtoTriIcbperUnidad;
    private String mtoPrecioVentaUnitario;
    private String mtoValorVentaItem;
    private String mtoValorReferencialUnitario;
    
    public Detalle(){}

    public String getCodUnidadMedida() {
        return codUnidadMedida;
    }

    public void setCodUnidadMedida(String codUnidadMedida) {
        this.codUnidadMedida = codUnidadMedida;
    }
    
    public String getCtdUnidadItem() {
        return ctdUnidadItem;
    }

    public void setCtdUnidadItem(String ctdUnidadItem) {
        this.ctdUnidadItem = ctdUnidadItem;
    }

    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public String getCodProductoSUNAT() {
        return codProductoSUNAT;
    }

    public void setCodProductoSUNAT(String codProductoSUNAT) {
        this.codProductoSUNAT = codProductoSUNAT;
    }

    public String getDesItem() {
        return desItem;
    }

    public void setDesItem(String desItem) {
        this.desItem = desItem;
    }

    public String getMtoValorUnitario() {
        return mtoValorUnitario;
    }

    public void setMtoValorUnitario(String mtoValorUnitario) {
        this.mtoValorUnitario = mtoValorUnitario;
    }

    public String getSumTotTributosItem() {
        return sumTotTributosItem;
    }

    public void setSumTotTributosItem(String sumTotTributosItem) {
        this.sumTotTributosItem = sumTotTributosItem;
    }

    public String getCodTriIGV() {
        return codTriIGV;
    }

    public void setCodTriIGV(String codTriIGV) {
        this.codTriIGV = codTriIGV;
    }

    public String getMtoIgvItem() {
        return mtoIgvItem;
    }

    public void setMtoIgvItem(String mtoIgvItem) {
        this.mtoIgvItem = mtoIgvItem;
    }

    public String getMtoBaseIgvItem() {
        return mtoBaseIgvItem;
    }

    public void setMtoBaseIgvItem(String mtoBaseIgvItem) {
        this.mtoBaseIgvItem = mtoBaseIgvItem;
    }

    public String getNomTributoIgvItem() {
        return nomTributoIgvItem;
    }

    public void setNomTributoIgvItem(String nomTributoIgvItem) {
        this.nomTributoIgvItem = nomTributoIgvItem;
    }

    public String getCodTipTributoIgvItem() {
        return codTipTributoIgvItem;
    }

    public void setCodTipTributoIgvItem(String codTipTributoIgvItem) {
        this.codTipTributoIgvItem = codTipTributoIgvItem;
    }

    public String getTipAfeIGV() {
        return tipAfeIGV;
    }

    public void setTipAfeIGV(String tipAfeIGV) {
        this.tipAfeIGV = tipAfeIGV;
    }

    public String getPorIgvItem() {
        return porIgvItem;
    }

    public void setPorIgvItem(String porIgvItem) {
        this.porIgvItem = porIgvItem;
    }

    public String getCodTriISC() {
        return codTriISC;
    }

    public void setCodTriISC(String codTriISC) {
        this.codTriISC = codTriISC;
    }

    public String getMtoIscItem() {
        return mtoIscItem;
    }

    public void setMtoIscItem(String mtoIscItem) {
        this.mtoIscItem = mtoIscItem;
    }

    public String getMtoBaseIscItem() {
        return mtoBaseIscItem;
    }

    public void setMtoBaseIscItem(String mtoBaseIscItem) {
        this.mtoBaseIscItem = mtoBaseIscItem;
    }

    public String getNomTributoIscItem() {
        return nomTributoIscItem;
    }

    public void setNomTributoIscItem(String nomTributoIscItem) {
        this.nomTributoIscItem = nomTributoIscItem;
    }

    public String getCodTipTributoIscItem() {
        return codTipTributoIscItem;
    }

    public void setCodTipTributoIscItem(String codTipTributoIscItem) {
        this.codTipTributoIscItem = codTipTributoIscItem;
    }

    public String getTipSisISC() {
        return tipSisISC;
    }

    public void setTipSisISC(String tipSisISC) {
        this.tipSisISC = tipSisISC;
    }

    public String getPorIscItem() {
        return porIscItem;
    }

    public void setPorIscItem(String porIscItem) {
        this.porIscItem = porIscItem;
    }

    public String getCodTriOtro() {
        return codTriOtro;
    }

    public void setCodTriOtro(String codTriOtro) {
        this.codTriOtro = codTriOtro;
    }

    public String getMtoTriOtroItem() {
        return mtoTriOtroItem;
    }

    public void setMtoTriOtroItem(String mtoTriOtroItem) {
        this.mtoTriOtroItem = mtoTriOtroItem;
    }

    public String getMtoBaseTriOtroItem() {
        return mtoBaseTriOtroItem;
    }

    public void setMtoBaseTriOtroItem(String mtoBaseTriOtroItem) {
        this.mtoBaseTriOtroItem = mtoBaseTriOtroItem;
    }

    public String getNomTributoOtroItem() {
        return nomTributoOtroItem;
    }

    public void setNomTributoOtroItem(String nomTributoOtroItem) {
        this.nomTributoOtroItem = nomTributoOtroItem;
    }

    public String getCodTipTributoOtroItem() {
        return codTipTributoOtroItem;
    }

    public void setCodTipTributoOtroItem(String codTipTributoOtroItem) {
        this.codTipTributoOtroItem = codTipTributoOtroItem;
    }

    public String getPorTriOtroItem() {
        return porTriOtroItem;
    }

    public void setPorTriOtroItem(String porTriOtroItem) {
        this.porTriOtroItem = porTriOtroItem;
    }

    public String getCodTriIcbper() {
        return codTriIcbper;
    }

    public void setCodTriIcbper(String codTriIcbper) {
        this.codTriIcbper = codTriIcbper;
    }

    public String getMtoTriIcbperItem() {
        return mtoTriIcbperItem;
    }

    public void setMtoTriIcbperItem(String mtoTriIcbperItem) {
        this.mtoTriIcbperItem = mtoTriIcbperItem;
    }

    public String getCtdBolsasTriIcbperItem() {
        return ctdBolsasTriIcbperItem;
    }

    public void setCtdBolsasTriIcbperItem(String ctdBolsasTriIcbperItem) {
        this.ctdBolsasTriIcbperItem = ctdBolsasTriIcbperItem;
    }

    public String getNomTributoIcbperItem() {
        return nomTributoIcbperItem;
    }

    public void setNomTributoIcbperItem(String nomTributoIcbperItem) {
        this.nomTributoIcbperItem = nomTributoIcbperItem;
    }

    public String getCodTipTributoIcbperItem() {
        return codTipTributoIcbperItem;
    }

    public void setCodTipTributoIcbperItem(String codTipTributoIcbperItem) {
        this.codTipTributoIcbperItem = codTipTributoIcbperItem;
    }

    public String getMtoTriIcbperUnidad() {
        return mtoTriIcbperUnidad;
    }

    public void setMtoTriIcbperUnidad(String mtoTriIcbperUnidad) {
        this.mtoTriIcbperUnidad = mtoTriIcbperUnidad;
    }

    public String getMtoPrecioVentaUnitario() {
        return mtoPrecioVentaUnitario;
    }

    public void setMtoPrecioVentaUnitario(String mtoPrecioVentaUnitario) {
        this.mtoPrecioVentaUnitario = mtoPrecioVentaUnitario;
    }

    public String getMtoValorVentaItem() {
        return mtoValorVentaItem;
    }

    public void setMtoValorVentaItem(String mtoValorVentaItem) {
        this.mtoValorVentaItem = mtoValorVentaItem;
    }

    public String getMtoValorReferencialUnitario() {
        return mtoValorReferencialUnitario;
    }

    public void setMtoValorReferencialUnitario(String mtoValorReferencialUnitario) {
        this.mtoValorReferencialUnitario = mtoValorReferencialUnitario;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Detalle{codUnidadMedida=").append(codUnidadMedida);
        sb.append(", ctdUnidadItem=").append(ctdUnidadItem);
        sb.append(", codProducto=").append(codProducto);
        sb.append(", codProductoSUNAT=").append(codProductoSUNAT);
        sb.append(", desItem=").append(desItem);
        sb.append(", mtoValorUnitario=").append(mtoValorUnitario);
        sb.append(", sumTotTributosItem=").append(sumTotTributosItem);
        sb.append(", codTriIGV=").append(codTriIGV);
        sb.append(", mtoIgvItem=").append(mtoIgvItem);
        sb.append(", mtoBaseIgvItem=").append(mtoBaseIgvItem);
        sb.append(", nomTributoIgvItem=").append(nomTributoIgvItem);
        sb.append(", codTipTributoIgvItem=").append(codTipTributoIgvItem);
        sb.append(", tipAfeIGV=").append(tipAfeIGV);
        sb.append(", porIgvItem=").append(porIgvItem);
        sb.append(", codTriISC=").append(codTriISC);
        sb.append(", mtoIscItem=").append(mtoIscItem);
        sb.append(", mtoBaseIscItem=").append(mtoBaseIscItem);
        sb.append(", nomTributoIscItem=").append(nomTributoIscItem);
        sb.append(", codTipTributoIscItem=").append(codTipTributoIscItem);
        sb.append(", tipSisISC=").append(tipSisISC);
        sb.append(", porIscItem=").append(porIscItem);
        sb.append(", codTriOtro=").append(codTriOtro);
        sb.append(", mtoTriOtroItem=").append(mtoTriOtroItem);
        sb.append(", mtoBaseTriOtroItem=").append(mtoBaseTriOtroItem);
        sb.append(", nomTributoOtroItem=").append(nomTributoOtroItem);
        sb.append(", codTipTributoOtroItem=").append(codTipTributoOtroItem);
        sb.append(", porTriOtroItem=").append(porTriOtroItem);
        sb.append(", codTriIcbper=").append(codTriIcbper);
        sb.append(", mtoTriIcbperItem=").append(mtoTriIcbperItem);
        sb.append(", ctdBolsasTriIcbperItem=").append(ctdBolsasTriIcbperItem);
        sb.append(", nomTributoIcbperItem=").append(nomTributoIcbperItem);
        sb.append(", codTipTributoIcbperItem=").append(codTipTributoIcbperItem);
        sb.append(", mtoTriIcbperUnidad=").append(mtoTriIcbperUnidad);
        sb.append(", mtoPrecioVentaUnitario=").append(mtoPrecioVentaUnitario);
        sb.append(", mtoValorVentaItem=").append(mtoValorVentaItem);
        sb.append(", mtoValorReferencialUnitario=").append(mtoValorReferencialUnitario);
        sb.append('}');
        return sb.toString();
    }
}
