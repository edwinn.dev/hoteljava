package com.edmi.model.invoice;

public class Leyenda {
    private String codLeyenda;
    private String desLeyenda;

    public Leyenda() {
    }
    
    public Leyenda(String codLeyenda, String desLeyenda) {
        this.codLeyenda = codLeyenda;
        this.desLeyenda = desLeyenda;
    }

    public String getCodLeyenda() {
        return codLeyenda;
    }

    public void setCodLeyenda(String codLeyenda) {
        this.codLeyenda = codLeyenda;
    }

    public String getDesLeyenda() {
        return desLeyenda;
    }

    public void setDesLeyenda(String desLeyenda) {
        this.desLeyenda = desLeyenda;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Leyenda{codLeyenda=").append(codLeyenda);
        sb.append(", desLeyenda=").append(desLeyenda);
        sb.append('}');
        return sb.toString();
    }
}
