package com.edmi.model;

public class Product {
    private String codProducto;
    private String codCategoria;
    private String codUnidadMedida;
    private String codSunat;
    private String tipoAfectacionIgv;
    private Double montoIgv;
    private String tributoIcbper;
    private Double montoIcbper;
    private String marca;
    private String nombreProducto;
    private String nombreCategoria;
    private String nombreUnidadMedida;
    private Double cantidadStock;
    private Double precioCompra;
    private Double precioVenta;
    private String fechaVenc;
    private String fechaReg;
    private Double cantidadSalida;
    private String estado;
    private String descripcion;
    
    public Product(){}

    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public String getCodCategoria() {
        return codCategoria;
    }

    public void setCodCategoria(String codCategoria) {
        this.codCategoria = codCategoria;
    }

    public String getCodUnidadMedida() {
        return codUnidadMedida;
    }

    public void setCodUnidadMedida(String codUnidadMedida) {
        this.codUnidadMedida = codUnidadMedida;
    }

    public String getCodSunat() {
        return codSunat;
    }

    public void setCodSunat(String codSunat) {
        this.codSunat = codSunat;
    }

    public String getTipoAfectacionIgv() {
        return tipoAfectacionIgv;
    }

    public void setTipoAfectacionIgv(String tipoAfectacionIgv) {
        this.tipoAfectacionIgv = tipoAfectacionIgv;
    }

    public Double getMontoIgv() {
        return montoIgv;
    }

    public void setMontoIgv(Double montoIgv) {
        this.montoIgv = montoIgv;
    }

    public String getTributoIcbper() {
        return tributoIcbper;
    }

    public void setTributoIcbper(String tributoIcbper) {
        this.tributoIcbper = tributoIcbper;
    }

    public Double getMontoIcbper() {
        return montoIcbper;
    }

    public void setMontoIcbper(Double montoIcbper) {
        this.montoIcbper = montoIcbper;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto.toUpperCase();
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public String getNombreUnidadMedida() {
        return nombreUnidadMedida;
    }

    public void setNombreUnidadMedida(String nombreUnidadMedida) {
        this.nombreUnidadMedida = nombreUnidadMedida;
    }

    public Double getCantidadStock() {
        return cantidadStock;
    }

    public void setCantidadStock(Double cantidadStock) {
        this.cantidadStock = cantidadStock;
    }

    public Double getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(Double precioCompra) {
        this.precioCompra = precioCompra;
    }

    public Double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(Double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public String getFechaVenc() {
        return fechaVenc;
    }

    public void setFechaVenc(String fechaVenc) {
        this.fechaVenc = fechaVenc;
    }

    public String getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(String fechaReg) {
        this.fechaReg = fechaReg;
    }

    public Double getCantidadSalida() {
        return cantidadSalida;
    }

    public void setCantidadSalida(Double cantidadSalida) {
        this.cantidadSalida = cantidadSalida;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Product{codProducto=").append(codProducto);
        sb.append(", codCategoria=").append(codCategoria);
        sb.append(", codUnidadMedida=").append(codUnidadMedida);
        sb.append(", codSunat=").append(codSunat);
        sb.append(", tipoAfectacionIgv=").append(tipoAfectacionIgv);
        sb.append(", montoIgv=").append(montoIgv);
        sb.append(", tributoIcbper=").append(tributoIcbper);
        sb.append(", montoIcbper=").append(montoIcbper);
        sb.append(", marca=").append(marca);
        sb.append(", nombreProducto=").append(nombreProducto);
        sb.append(", nombreCategoria=").append(nombreCategoria);
        sb.append(", nombreUnidadMedida=").append(nombreUnidadMedida);
        sb.append(", cantidadStock=").append(cantidadStock);
        sb.append(", precioCompra=").append(precioCompra);
        sb.append(", precioVenta=").append(precioVenta);
        sb.append(", fechaVenc=").append(fechaVenc);
        sb.append(", fechaReg=").append(fechaReg);
        sb.append(", cantidadSalida=").append(cantidadSalida);
        sb.append(", estado=").append(estado);
        sb.append(", descripcion=").append(descripcion);
        sb.append('}');
        return sb.toString();
    }
}
