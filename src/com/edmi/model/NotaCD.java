package com.edmi.model;

public class NotaCD {
    private String cod_cabecera;
    private String fecha_emision;
    private String hora_emision; 
    private String tipo_nota;
    private String descrip_motivo;
    private String nroDocuCliente;
    private String situacionSunat;
    private String docAfectado;

    public NotaCD() {}
    
    public String getCod_cabecera() {
        return cod_cabecera;
    }

    public void setCod_cabecera(String cod_cabecera) {
        this.cod_cabecera = cod_cabecera;
    }

    public String getFecha_emision() {
        return fecha_emision;
    }

    public void setFecha_emision(String fecha_emision) {
        this.fecha_emision = fecha_emision;
    }

    public String getHora_emision() {
        return hora_emision;
    }

    public void setHora_emision(String hora_emision) {
        this.hora_emision = hora_emision;
    }

    public String getTipo_nota() {
        return tipo_nota;
    }

    public void setTipo_nota(String tipo_nota) {
        this.tipo_nota = tipo_nota;
    }

    public String getDescrip_motivo() {
        return descrip_motivo;
    }

    public void setDescrip_motivo(String descrip_motivo) {
        this.descrip_motivo = descrip_motivo;
    }

    public String getNroDocuCliente() {
        return nroDocuCliente;
    }

    public void setNroDocuCliente(String nroDocuCliente) {
        this.nroDocuCliente = nroDocuCliente;
    }

    public String getSituacionSunat() {
        return situacionSunat;
    }

    public void setSituacionSunat(String situacionSunat) {
        this.situacionSunat = situacionSunat;
    }

    public String getDocAfectado() {
        return docAfectado;
    }

    public void setDocAfectado(String docAfectado) {
        this.docAfectado = docAfectado;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("NotaCD{cod_cabecera=").append(cod_cabecera);
        sb.append(", fecha_emision=").append(fecha_emision);
        sb.append(", hora_emision=").append(hora_emision);
        sb.append(", tipo_nota=").append(tipo_nota);
        sb.append(", descrip_motivo=").append(descrip_motivo);
        sb.append(", nroDocuCliente=").append(nroDocuCliente);
        sb.append(", situacionSunat=").append(situacionSunat);
        sb.append(", docAfectado=").append(docAfectado);
        sb.append('}');
        return sb.toString();
    }
}