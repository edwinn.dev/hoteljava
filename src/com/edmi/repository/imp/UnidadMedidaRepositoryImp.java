package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.model.catalogue.UnidadMedida;
import com.edmi.database.query.Query;
import com.edmi.repository.RepositoryUnitMeasure;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UnidadMedidaRepositoryImp implements RepositoryUnitMeasure<UnidadMedida>{
    
    private Connection getConnection() throws SQLException{
        return getInstance();
    }

    @Override
    public List<UnidadMedida> findAll() {
        List<UnidadMedida> units = new ArrayList<>();
        try (PreparedStatement ps = getConnection().prepareStatement(Query.SQL_UNIT_FIND_ALL);
            ResultSet rs =  ps.executeQuery()){
            while(rs.next()){
                UnidadMedida unit = getUnitMeasure(rs);
                units.add(unit);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UnidadMedidaRepositoryImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        return units;
    }

    @Override
    public List<UnidadMedida> findAllWhere(String condition, String value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public UnidadMedida findById(Object id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int create(UnidadMedida unitMeasure) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int update(UnidadMedida unitMeasure) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int delete(Object id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    private UnidadMedida getUnitMeasure(ResultSet rs) throws SQLException {
        UnidadMedida um = new UnidadMedida();
        um.setCodUnidadMedida(rs.getString("cod_unidad"));
        um.setDescripcion(rs.getString("descripcion"));
        return um;
    }
}
