package com.edmi.view;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.panel.ControllerPanelRom;
import com.edmi.repository.imp.HabitacionRepositoryImp;
import com.edmi.repository.imp.TipoHabitacionRepositoryImp;
import com.edmi.model.Habitacion;
import com.edmi.model.TipoHabitacion;
import com.edmi.repository.RepositoryRoom;
import com.edmi.util.MessageView;
import com.edmi.util.Util;
import com.edmi.view.panel.PanelReportRoom;
import java.awt.Component;
import java.awt.Frame;
import java.util.List;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;

public class ModalNewRoom extends javax.swing.JDialog {
    private RepositoryRoom<Habitacion> roomRepository = null;
    private ControllerPanelRom control = null;

    public ModalNewRoom(Frame owner, boolean modal, ControllerPanelRom control) {
        super(owner, modal);
        this.control = control;
        initComponents();
        loadRoomType(this);
        setPreferences();
    }

    private void setPreferences() {
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.txtCodHabitacion.setText(generarCodigo());
        Util.restriccionLetras(txtCostoAlquiler);
        Util.restriccionLetras(txtNroHabitacion);
    }

    private String generarCodigo() {
        roomRepository = new HabitacionRepositoryImp();
        List<Habitacion> lista = roomRepository.findAllDesc();
        String codigo;
        if (lista.size() > 0) {
            Habitacion h = lista.get(lista.size() - 1);
            Integer cod = Integer.parseInt(h.getCodhaHabitacion().substring(4).trim());
            codigo = String.format("%3s", (cod + 1)).replace(" ", "0");
        } else {
            codigo = "001";
        }
        return "HAB-" + codigo;
    }

    public void loadRoomType(ModalNewRoom modal) {
        TipoHabitacionRepositoryImp roomTypeRepository = new TipoHabitacionRepositoryImp();
        List<TipoHabitacion> roomType = roomTypeRepository.findAll();
        modal.cbxTipoHabitacion.removeAllItems();
        modal.cbxTipoHabitacion.addItem("--SELECCIONAR--");
        if (roomType.isEmpty() || roomType.size() <= 0) {
            modal.cbxTipoHabitacion.addItem("Aun no hay tipo de habitación, registre uno.");
            return;
        }
        roomType.forEach(type -> {
            modal.cbxTipoHabitacion.addItem(type.getTipoHabitacion());
        });
    }

    private boolean isFieldsInvalid() {
        boolean invalid = txtNroHabitacion.getText().isEmpty() || txtCostoAlquiler.getText().isEmpty();
        return invalid;
    }

    private void clearTextField() {
        Component[] components = getContentPane().getComponents();
        for (Component c : components) {
            if (c instanceof JFormattedTextField) {
                ((JFormattedTextField) c).setValue("");
            }
        }
        txtCaracteristicas.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtNroHabitacion = new javax.swing.JFormattedTextField();
        cbxTipoHabitacion = new javax.swing.JComboBox<>();
        txtCostoAlquiler = new javax.swing.JFormattedTextField();
        cbxEstadoHabitacion = new javax.swing.JComboBox<>();
        spinnerNroPlanta = new javax.swing.JSpinner();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtCaracteristicas = new javax.swing.JTextArea();
        btnModificarHabitacion = new javax.swing.JButton();
        btnNuevaHabitacion = new javax.swing.JButton();
        btnNuevoTipo = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        txtCodHabitacion = new javax.swing.JTextField();

        jButton2.setText("jButton2");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Registrar Habitación");

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Número de habitación:");

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setText("Tipo de habitación:");
        jLabel2.setToolTipText("");

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText("Costo de alquiler:");

        jLabel4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel4.setText("Estado:");

        jLabel5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel5.setText("Número de planta (piso):");
        jLabel5.setToolTipText("");

        txtNroHabitacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        cbxTipoHabitacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxTipoHabitacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONE --" }));

        txtCostoAlquiler.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        cbxEstadoHabitacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxEstadoHabitacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DISPONIBLE", "MANTENIMIENTO", "OCUPADO" }));

        spinnerNroPlanta.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        spinnerNroPlanta.setModel(new javax.swing.SpinnerListModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));

        jLabel6.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel6.setText("Caracteristicas de la habitacion");

        txtCaracteristicas.setColumns(20);
        txtCaracteristicas.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtCaracteristicas.setRows(5);
        jScrollPane1.setViewportView(txtCaracteristicas);

        btnModificarHabitacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnModificarHabitacion.setText("MODIFICAR");
        btnModificarHabitacion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnModificarHabitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarHabitacionActionPerformed(evt);
            }
        });

        btnNuevaHabitacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnNuevaHabitacion.setText("REGISTRAR");
        btnNuevaHabitacion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNuevaHabitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevaHabitacionActionPerformed(evt);
            }
        });

        btnNuevoTipo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnNuevoTipo.setText("NUEVO TIPO");
        btnNuevoTipo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnNuevoTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoTipoActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel7.setText("Código:");
        jLabel7.setToolTipText("");

        txtCodHabitacion.setEditable(false);
        txtCodHabitacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtCodHabitacion.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cbxTipoHabitacion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(btnNuevoTipo))
                            .addComponent(cbxEstadoHabitacion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtNroHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtCodHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(108, 108, 108)
                        .addComponent(btnNuevaHabitacion)
                        .addGap(18, 18, 18)
                        .addComponent(btnModificarHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(spinnerNroPlanta, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txtCostoAlquiler))))
                .addGap(21, 21, 21))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtCodHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbxTipoHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(btnNuevoTipo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNroHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cbxEstadoHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(spinnerNroPlanta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtCostoAlquiler, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnModificarHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNuevaHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnModificarHabitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarHabitacionActionPerformed
        roomRepository = new HabitacionRepositoryImp();

        if (isFieldsInvalid()) {
            JOptionPane.showMessageDialog(this, MessageView.MROOM_VALIDATE);
            return;
        }

        Habitacion h = new Habitacion();
        h.setCodhaHabitacion(txtCodHabitacion.getText());
        h.setCodTipoHabitacion(roomRepository.findAllTipeRom(cbxTipoHabitacion.getSelectedItem()));
        h.setNroHabitacion(Integer.parseInt(txtNroHabitacion.getText()));
        h.setTipoHabitacion(cbxTipoHabitacion.getSelectedItem().toString());
        h.setEstado(cbxEstadoHabitacion.getSelectedItem().toString());
        h.setNroPlanta(Integer.parseInt(spinnerNroPlanta.getValue().toString()));
        h.setCostoAlquiler(Double.parseDouble(txtCostoAlquiler.getText()));
        h.setCaracteristicas(txtCaracteristicas.getText());
        int response = roomRepository.update(h);
        if (response <= 0) {
            JOptionPane.showMessageDialog(this, "Error al modificar la habitacion", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }
        JOptionPane.showMessageDialog(this, "Habitación modificada.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        ControllerPanelRom.loadTableRooms("1");
        control.showRooms();
        PanelReportRoom.getInstance().loadTableRooms();
        PanelReportRoom.getInstance().loadRoomType();
        this.dispose();
    }//GEN-LAST:event_btnModificarHabitacionActionPerformed

    private void btnNuevoTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoTipoActionPerformed
        ModalNewTypeRoom form = new ModalNewTypeRoom(this, true);
        form.setVisible(true);
    }//GEN-LAST:event_btnNuevoTipoActionPerformed

    private void btnNuevaHabitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevaHabitacionActionPerformed
        roomRepository = new HabitacionRepositoryImp();

        if (isFieldsInvalid()) {
            JOptionPane.showMessageDialog(this, MessageView.MROOM_VALIDATE, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        if (this.cbxTipoHabitacion.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(this, "El tipo de habitación es incorrecta.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        List<Habitacion> list = roomRepository.findAll();
        for (Habitacion hab : list) {
            if (this.txtNroHabitacion.getText().equals("" + hab.getNroHabitacion())) {
                JOptionPane.showMessageDialog(this, "La habitación ya se encuentra registrada.", SYS_MSG, JOptionPane.WARNING_MESSAGE);
                return;
            }
        }

        int option = JOptionPane.showConfirmDialog(this, "¿Está seguro de registrar esta habitacion?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if(option == JOptionPane.YES_OPTION){
            Habitacion h = new Habitacion();
            h.setCodhaHabitacion(txtCodHabitacion.getText());
            h.setCodTipoHabitacion(roomRepository.findAllTipeRom(cbxTipoHabitacion.getSelectedItem().toString()));
            h.setNroHabitacion(Integer.parseInt(txtNroHabitacion.getText()));
            h.setTipoHabitacion(cbxTipoHabitacion.getSelectedItem().toString());
            h.setEstado(cbxEstadoHabitacion.getSelectedItem().toString());
            h.setNroPlanta(Integer.parseInt(spinnerNroPlanta.getValue().toString()));
            h.setCostoAlquiler(Double.parseDouble(txtCostoAlquiler.getText()));
            h.setCaracteristicas(txtCaracteristicas.getText());
            h.setState("1");
            int response = roomRepository.create(h);
            if (response <= 0) {
                JOptionPane.showMessageDialog(this, "Error al registrar la habitacion", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                return;
            }
            JOptionPane.showMessageDialog(this, "Habitación registrada.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            clearTextField();
            ControllerPanelRom.loadTableRooms("1");
            control.showRooms();
            //txtCodHabitacion.setText(generarCodigo());
            cbxTipoHabitacion.setSelectedIndex(0);
            PanelReportRoom.getInstance().loadTableRooms();
            PanelReportRoom.getInstance().loadRoomType();
            this.dispose();
        }
    }//GEN-LAST:event_btnNuevaHabitacionActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnModificarHabitacion;
    public javax.swing.JButton btnNuevaHabitacion;
    private javax.swing.JButton btnNuevoTipo;
    public javax.swing.JComboBox<String> cbxEstadoHabitacion;
    public javax.swing.JComboBox<String> cbxTipoHabitacion;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JSpinner spinnerNroPlanta;
    public javax.swing.JTextArea txtCaracteristicas;
    public javax.swing.JTextField txtCodHabitacion;
    public javax.swing.JFormattedTextField txtCostoAlquiler;
    public javax.swing.JFormattedTextField txtNroHabitacion;
    // End of variables declaration//GEN-END:variables
}
