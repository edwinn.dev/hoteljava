package com.edmi.model.invoice;

public class AdicionalDetalle {
    private String idLinea;
    private String nomPropiedad;
    private String codPropiedad;
    private String valPropiedad;
    private String codBienPropiedad;
    private String fecInicioPropiedad;
    private String horInicioPropiedad;
    private String fecFinPropiedad;
    private String numDiasPropiedad; //String
    private String tipVariable;
    private String codTipoVariable;
    private String porVariable;
    private String monMontoVariable;
    private String mtoVariable;
    private String monBaseImponibleVariable;
    private String mtoBaseImpVariable;

    public String getIdLinea() {
        return idLinea;
    }

    public void setIdLinea(String idLinea) {
        this.idLinea = idLinea;
    }
    
    public String getNomPropiedad() {
        return nomPropiedad;
    }

    public void setNomPropiedad(String nomPropiedad) {
        this.nomPropiedad = nomPropiedad;
    }

    public String getCodPropiedad() {
        return codPropiedad;
    }

    public void setCodPropiedad(String codPropiedad) {
        this.codPropiedad = codPropiedad;
    }

    public String getValPropiedad() {
        return valPropiedad;
    }

    public void setValPropiedad(String valPropiedad) {
        this.valPropiedad = valPropiedad;
    }

    public String getCodBienPropiedad() {
        return codBienPropiedad;
    }

    public void setCodBienPropiedad(String codBienPropiedad) {
        this.codBienPropiedad = codBienPropiedad;
    }

    public String getFecInicioPropiedad() {
        return fecInicioPropiedad;
    }

    public void setFecInicioPropiedad(String fecInicioPropiedad) {
        this.fecInicioPropiedad = fecInicioPropiedad;
    }

    public String getHorInicioPropiedad() {
        return horInicioPropiedad;
    }

    public void setHorInicioPropiedad(String horInicioPropiedad) {
        this.horInicioPropiedad = horInicioPropiedad;
    }

    public String getFecFinPropiedad() {
        return fecFinPropiedad;
    }

    public void setFecFinPropiedad(String fecFinPropiedad) {
        this.fecFinPropiedad = fecFinPropiedad;
    }

    public String getNumDiasPropiedad() {
        return numDiasPropiedad;
    }

    public void setNumDiasPropiedad(String numDiasPropiedad) {
        this.numDiasPropiedad = numDiasPropiedad;
    }

    public String getTipVariable() {
        return tipVariable;
    }

    public void setTipVariable(String tipVariable) {
        this.tipVariable = tipVariable;
    }

    public String getCodTipoVariable() {
        return codTipoVariable;
    }

    public void setCodTipoVariable(String codTipoVariable) {
        this.codTipoVariable = codTipoVariable;
    }

    public String getPorVariable() {
        return porVariable;
    }

    public void setPorVariable(String porVariable) {
        this.porVariable = porVariable;
    }

    public String getMonMontoVariable() {
        return monMontoVariable;
    }

    public void setMonMontoVariable(String monMontoVariable) {
        this.monMontoVariable = monMontoVariable;
    }

    public String getMtoVariable() {
        return mtoVariable;
    }

    public void setMtoVariable(String mtoVariable) {
        this.mtoVariable = mtoVariable;
    }

    public String getMonBaseImponibleVariable() {
        return monBaseImponibleVariable;
    }

    public void setMonBaseImponibleVariable(String monBaseImponibleVariable) {
        this.monBaseImponibleVariable = monBaseImponibleVariable;
    }

    public String getMtoBaseImpVariable() {
        return mtoBaseImpVariable;
    }

    public void setMtoBaseImpVariable(String mtoBaseImpVariable) {
        this.mtoBaseImpVariable = mtoBaseImpVariable;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AdicionalDetalle{nomPropiedad=").append(nomPropiedad);
        sb.append(", codPropiedad=").append(codPropiedad);
        sb.append(", valPropiedad=").append(valPropiedad);
        sb.append(", codBienPropiedad=").append(codBienPropiedad);
        sb.append(", fecInicioPropiedad=").append(fecInicioPropiedad);
        sb.append(", horInicioPropiedad=").append(horInicioPropiedad);
        sb.append(", fecFinPropiedad=").append(fecFinPropiedad);
        sb.append(", numDiasPropiedad=").append(numDiasPropiedad);
        sb.append(", tipVariable=").append(tipVariable);
        sb.append(", codTipoVariable=").append(codTipoVariable);
        sb.append(", porVariable=").append(porVariable);
        sb.append(", monMontoVariable=").append(monMontoVariable);
        sb.append(", mtoVariable=").append(mtoVariable);
        sb.append(", monBaseImponibleVariable=").append(monBaseImponibleVariable);
        sb.append(", mtoBaseImpVariable=").append(mtoBaseImpVariable);
        sb.append('}');
        return sb.toString();
    }
}
