package com.edmi.model;

public class Proveedor {
    private String codProveedor;
    private String codTipoDoc;
    private String nroDocumento;
    private String razonSocial;
    private String nombre;
    private String pais;
    private String provincia;
    private String direccion;
    private String telefonoUno;
    private String telefonoDos;
    private String prodSuministrado;
    private String email;
    private String observacion;
    private String tipoDocumento;
    private String estado;
    
    public Proveedor(){}

    public Proveedor(String codProveedor, String codTipoDoc, String nroDocumento, String razonSocial, String pais, String provincia, String direccion, String telefonoUno, String telefonoDos, String prodSuministrado, String email, String observacion, String tipoDocumento, String estado) {
        this.codProveedor = codProveedor;
        this.codTipoDoc = codTipoDoc;
        this.nroDocumento = nroDocumento;
        this.razonSocial = razonSocial;
        this.pais = pais;
        this.provincia = provincia;
        this.direccion = direccion;
        this.telefonoUno = telefonoUno;
        this.telefonoDos = telefonoDos;
        this.prodSuministrado = prodSuministrado;
        this.email = email;
        this.observacion = observacion;
        this.tipoDocumento = tipoDocumento;
        this.estado = estado;
    }

    public String getCodProveedor() {
        return codProveedor;
    }

    public void setCodProveedor(String codProveedor) {
        this.codProveedor = codProveedor;
    }

    public String getCodTipoDoc() {
        return codTipoDoc;
    }

    public void setCodTipoDoc(String codTipoDoc) {
        this.codTipoDoc = codTipoDoc;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial.toUpperCase();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre.toUpperCase();
    }
    
    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais.toUpperCase();
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia.toUpperCase();
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion.toUpperCase();
    }

    public String getTelefonoUno() {
        return telefonoUno;
    }

    public void setTelefonoUno(String telefonoUno) {
        this.telefonoUno = telefonoUno;
    }

    public String getTelefonoDos() {
        return telefonoDos;
    }

    public void setTelefonoDos(String telefonoDos) {
        this.telefonoDos = telefonoDos;
    }

    public String getProdSuministrado() {
        return prodSuministrado;
    }

    public void setProdSuministrado(String prodSuministrado) {
        this.prodSuministrado = prodSuministrado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion.toUpperCase();
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento.toUpperCase();
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Proveedor{codProveedor=").append(codProveedor);
        sb.append(", codTipoDoc=").append(codTipoDoc);
        sb.append(", nroDocumento=").append(nroDocumento);
        sb.append(", razonSocial=").append(razonSocial);
        sb.append(", pais=").append(pais);
        sb.append(", provincia=").append(provincia);
        sb.append(", direccion=").append(direccion);
        sb.append(", telefonoUno=").append(telefonoUno);
        sb.append(", telefonoDos=").append(telefonoDos);
        sb.append(", prodSuministrado=").append(prodSuministrado);
        sb.append(", email=").append(email);
        sb.append(", observacion=").append(observacion);
        sb.append(", tipoDocumento=").append(tipoDocumento);
        sb.append(", estado=").append(estado);
        sb.append('}');
        return sb.toString();
    }
}
