package com.edmi.view.panel;

public class PanelSupplier extends javax.swing.JPanel {

    public PanelSupplier() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnAgregarProveedor = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtBuscarProveedor = new javax.swing.JFormattedTextField();
        btnProveedorRegresar = new javax.swing.JButton();
        cbxCriteriosProveedor = new javax.swing.JComboBox<>();
        chkProveedoresDescartados = new javax.swing.JCheckBox();
        jScrollPane11 = new javax.swing.JScrollPane();
        tableSuppliers = new javax.swing.JTable();

        btnAgregarProveedor.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnAgregarProveedor.setText("NUEVO PROVEEDOR");
        btnAgregarProveedor.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Criterios:");

        txtBuscarProveedor.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        btnProveedorRegresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icon_left.png"))); // NOI18N
        btnProveedorRegresar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnProveedorRegresar.setFocusPainted(false);

        cbxCriteriosProveedor.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxCriteriosProveedor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --", "CODIGO", "NOMBRE - RAZON", "TIPO DOCUMENTO", "NRO. DOCUMENTO" }));

        chkProveedoresDescartados.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        chkProveedoresDescartados.setText("Proveedores descartados");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnProveedorRegresar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnAgregarProveedor)
                .addGap(18, 18, 18)
                .addComponent(chkProveedoresDescartados, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbxCriteriosProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtBuscarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnProveedorRegresar, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnAgregarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtBuscarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)
                        .addComponent(cbxCriteriosProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(chkProveedoresDescartados)))
                .addContainerGap())
        );

        tableSuppliers.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tableSuppliers.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "NOMBRE - RAZON SOCIAL", "TIPO DOCUMENTO", "NRO. DOCUMENTO", "DIRECCION", "PROVINCIA - DISTRITO", "TELEFONO 1", "TELEFONO 2", "EMAIL", "OBSERVACION", "ESTADO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableSuppliers.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tableSuppliers.setAutoscrolls(false);
        tableSuppliers.getTableHeader().setReorderingAllowed(false);
        jScrollPane11.setViewportView(tableSuppliers);
        if (tableSuppliers.getColumnModel().getColumnCount() > 0) {
            tableSuppliers.getColumnModel().getColumn(0).setPreferredWidth(150);
            tableSuppliers.getColumnModel().getColumn(1).setPreferredWidth(250);
            tableSuppliers.getColumnModel().getColumn(2).setPreferredWidth(200);
            tableSuppliers.getColumnModel().getColumn(3).setPreferredWidth(150);
            tableSuppliers.getColumnModel().getColumn(4).setPreferredWidth(300);
            tableSuppliers.getColumnModel().getColumn(5).setPreferredWidth(150);
            tableSuppliers.getColumnModel().getColumn(6).setPreferredWidth(100);
            tableSuppliers.getColumnModel().getColumn(7).setPreferredWidth(100);
            tableSuppliers.getColumnModel().getColumn(8).setPreferredWidth(200);
            tableSuppliers.getColumnModel().getColumn(9).setPreferredWidth(250);
            tableSuppliers.getColumnModel().getColumn(10).setPreferredWidth(100);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 980, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 473, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnAgregarProveedor;
    public javax.swing.JButton btnProveedorRegresar;
    public javax.swing.JComboBox<String> cbxCriteriosProveedor;
    public javax.swing.JCheckBox chkProveedoresDescartados;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane11;
    public javax.swing.JTable tableSuppliers;
    public javax.swing.JFormattedTextField txtBuscarProveedor;
    // End of variables declaration//GEN-END:variables
}
