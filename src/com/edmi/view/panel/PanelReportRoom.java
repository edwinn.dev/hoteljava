package com.edmi.view.panel;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.model.Habitacion;
import com.edmi.model.TipoHabitacion;
import com.edmi.repository.RepositoryRoom;
import com.edmi.repository.imp.HabitacionRepositoryImp;
import com.edmi.repository.imp.TipoHabitacionRepositoryImp;
import java.util.Date;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import com.edmi.util.Util;
import com.edmi.view.DialogReportFiles;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public final class PanelReportRoom extends javax.swing.JPanel {
    private static PanelReportRoom instance = null;
    private RepositoryRoom<Habitacion> repositoryRoom = null;
    private String opcion = "1";
    private String hojaExcel = "TODAS LAS HABITACIONES";
    private String nombreArchivo = "";
    
    private PanelReportRoom() {
        initComponents();
        repositoryRoom = new HabitacionRepositoryImp();
        loadTableRooms();
        loadRoomType();
        rbTodo.setSelected(true);
        cboEstado.setEnabled(false);
        cboTipoHabitacion.setEnabled(false);
        nombreArchivo = "TH_".concat(Util.getDateFormat(new Date()));
    }

    public static PanelReportRoom getInstance(){
        if(instance == null){
            instance = new PanelReportRoom();
        }
        return instance;
    }
    
    private void almacenarReporte() {
        String path = DialogReportFiles.obtenerRutaArchiReporPdfXml()[0];
        boolean res = generareReporte(path + File.separator + nombreArchivo + ".xlsx");
        if (res) {
            JOptionPane.showMessageDialog(this, "Exportacion generada con exito\n\n"
                    + "RUTA: " + path + File.separator + nombreArchivo, SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(this, "No se pudo generar el reporte,\n\ncontacte con el administrador.", SYS_MSG, JOptionPane.WARNING_MESSAGE);
        }
    }

    private boolean generareReporte(String sheetName) {
        List<Habitacion> habitaciones = null;
        switch (opcion) {
            case "1":
                habitaciones = repositoryRoom.findAllEstado("1");
                break;

            case "2":
                habitaciones = repositoryRoom.findAllWhere("estado",cboEstado.getSelectedItem().toString(),"1");
                break;

            case "3":
                habitaciones = repositoryRoom.findAllWhere("tipo_habitacion",cboTipoHabitacion.getSelectedItem().toString(),"1");
                break;
        }
        
        boolean response = false;
        Workbook book = new XSSFWorkbook();
        Sheet sheet = book.createSheet(this.hojaExcel);
        
        try {
            File ruta = new File("design" + File.separator + "logo inicio");
            String[] img = ruta.list();
            String logo = ruta.getAbsolutePath() + File.separator + img[0];

            InputStream is = new FileInputStream(logo);
            byte[] bytes = IOUtils.toByteArray(is);
            int imgIndex = book.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG); //Agrega la imagen y su tipo de imagen.
            is.close();

            //Agregamos la imagen al archivo.
            CreationHelper help = book.getCreationHelper();
            Drawing draw = sheet.createDrawingPatriarch(); //Para crear la imagen.

            ClientAnchor anchor = help.createClientAnchor(); //Sacamos el ancho de la imagen para colocarlo de forma correcta.
            anchor.setCol1(0); //Posicion columna.
            anchor.setRow1(1); //Posicion fila.
            Picture pict = draw.createPicture(anchor, imgIndex); //Aparece la imagen.
            pict.resize(1, 5); //Indicamos el tamaño, de donde comenzara y cuanto espacio utilizara.

        } catch (IOException e) {
            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, e);
        }

        CellStyle estiloTitulo = book.createCellStyle(); //Para crear el diseño del titulo.
        estiloTitulo.setAlignment(HorizontalAlignment.CENTER); //Para centrar el titulo.
        estiloTitulo.setVerticalAlignment(VerticalAlignment.CENTER);

        org.apache.poi.ss.usermodel.Font fontTitulo = book.createFont(); //Problemas de compatibilidad con el itextPdf no se podia importar.

        fontTitulo.setFontName("Arial");
        fontTitulo.setBold(true);
        fontTitulo.setFontHeightInPoints((short) 22); //tamaño de letra
        estiloTitulo.setFont(fontTitulo); //agregamos los cambios.
        Row filaTitulo = sheet.createRow(2); //Creamos la fila para el titulo
        Cell celdaTitulo = filaTitulo.createCell(1); //Posicion columna
        celdaTitulo.setCellStyle(estiloTitulo); //Agregamos el estilo
        celdaTitulo.setCellValue("REPORTE DE HABITACIONES");

        sheet.addMergedRegion(new CellRangeAddress(2, 2, 1, 3));
        
        sheet.setColumnWidth(0, 7000);
        sheet.setColumnWidth(1, 8000);
        sheet.setColumnWidth(2, 7000);
        sheet.setColumnWidth(3, 5000);
        sheet.setColumnWidth(4, 7000);
        sheet.setColumnWidth(5, 7000);
        sheet.setColumnWidth(6, 8000);

        CellStyle estiloTablaTitulos = book.createCellStyle(); //Para crear el diseño del titulo.
        estiloTablaTitulos.setAlignment(HorizontalAlignment.CENTER); //Para centrar el titulo.
        estiloTablaTitulos.setVerticalAlignment(VerticalAlignment.CENTER);

        String cabecera[] = new String[]{"CODIGO HABITACION", "NUMERO DE HABITACION", "TIPO DE HABITACION", "ESTADO", "COSTO ALQUILER (S/)", "NUMERO DE PLANTA",
            "CARACTERISTICAS"};

        CellStyle estiloTablaCabezera = book.createCellStyle();
        estiloTablaCabezera.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        estiloTablaCabezera.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        estiloTablaCabezera.setBorderBottom(BorderStyle.THIN);
        estiloTablaCabezera.setBorderLeft(BorderStyle.THIN);
        estiloTablaCabezera.setBorderRight(BorderStyle.THIN);
        estiloTablaCabezera.setBorderTop(BorderStyle.THIN);

        org.apache.poi.ss.usermodel.Font font = book.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setFontHeightInPoints((short) 12);
        font.setColor(IndexedColors.WHITE.getIndex());
        estiloTablaCabezera.setFont(font);

        Row row = sheet.createRow(7);

        for (int i = 0; i < cabecera.length; i++) {
            Cell celdaCabecera = row.createCell(i);
            celdaCabecera.setCellStyle(estiloTablaCabezera);
            celdaCabecera.setCellValue(cabecera[i]);
        }
        
        CellStyle estiloTablaDatos = book.createCellStyle();
        estiloTablaDatos.setBorderBottom(BorderStyle.THIN);
        estiloTablaDatos.setBorderLeft(BorderStyle.THIN);
        estiloTablaDatos.setBorderRight(BorderStyle.THIN);
        estiloTablaDatos.setBorderTop(BorderStyle.THIN);        

        int nroRow = 8;
        for (Habitacion h : habitaciones) {
            Row fila = sheet.createRow(nroRow);
            
            Cell celdaInformacion0 = fila.createCell(0);
            celdaInformacion0.setCellValue(h.getCodhaHabitacion());
            celdaInformacion0.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion1 = fila.createCell(1);
            celdaInformacion1.setCellValue("" + h.getNroHabitacion());
            celdaInformacion1.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion2 = fila.createCell(2);
            celdaInformacion2.setCellValue(h.getTipoHabitacion());
            celdaInformacion2.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion3 = fila.createCell(3);
            celdaInformacion3.setCellValue(h.getEstado());
            celdaInformacion3.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion4 = fila.createCell(4);
            celdaInformacion4.setCellValue("" + h.getCostoAlquiler());
            celdaInformacion4.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion5 = fila.createCell(5);
            celdaInformacion5.setCellValue("" + h.getNroPlanta());
            celdaInformacion5.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion6 = fila.createCell(6);
            celdaInformacion6.setCellValue(h.getCaracteristicas());
            celdaInformacion6.setCellStyle(estiloTablaDatos);
            
            nroRow++;
        }

        try {
            try ( FileOutputStream out = new FileOutputStream(sheetName)) {
                book.write(out);
                response = true;
            }
        } catch (IOException ex) {
            Logger.getLogger(PanelReportProduct.class.getName()).log(Level.SEVERE, null, ex);
            response = false;
        }
        
        try {
            book.close();
        } catch (IOException ex) {
            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    private void generarPDF() {
        Document documento = new Document();
        documento.setMargins(24f, 24f, 24f, 24f);

        String path = DialogReportFiles.obtenerRutaArchiReporPdfXml()[0];

        try {
            PdfWriter.getInstance(documento, new FileOutputStream(path + File.separator + nombreArchivo + ".pdf"));
            documento.open();
        } catch (DocumentException | HeadlessException | FileNotFoundException e) {
            JOptionPane.showMessageDialog(this, "No se pudo generar el reporte,\n\ncontacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        List<Habitacion> habitaciones = null;
        switch (opcion) {
            case "1":
                habitaciones = repositoryRoom.findAllEstado("1");
                break;

            case "2":
                habitaciones = repositoryRoom.findAllWhere("estado",cboEstado.getSelectedItem().toString(),"1");
                break;

            case "3":
                habitaciones = repositoryRoom.findAllWhere("tipo_habitacion",cboTipoHabitacion.getSelectedItem().toString(),"1");
                break;
        }

        try {
            documento.setPageSize(PageSize.A2.rotate());
            documento.open();

            BaseFont fuente = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, true);
            Font tituloHab = new Font(fuente, 20f, Font.BOLD, BaseColor.BLACK);

            BaseFont fuente1 = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, true);
            Font celdaTitulo = new Font(fuente1, 15f, Font.BOLD, BaseColor.RED);

            var titulo = new Paragraph(hojaExcel + "\n\n", tituloHab);
            titulo.setAlignment(Paragraph.ALIGN_CENTER);
            documento.add(titulo);

            PdfPTable tablapdf = new PdfPTable(new float[]{75f, 75f, 75f, 75f, 75f, 75f, 75f});
            tablapdf.setWidthPercentage(100f);
            tablapdf.addCell(new Phrase("CODIGO HABITACION", celdaTitulo));
            tablapdf.addCell(new Phrase("NUMERO DE HABITACION", celdaTitulo));
            tablapdf.addCell(new Phrase("TIPO DE HABITACION", celdaTitulo));
            tablapdf.addCell(new Phrase("ESTADO", celdaTitulo));
            tablapdf.addCell(new Phrase("COSTO ALQUILER (S/)", celdaTitulo));
            tablapdf.addCell(new Phrase("NUMERO DE PLANTA", celdaTitulo));
            tablapdf.addCell(new Phrase("CARACTERISTICAS", celdaTitulo));

            for (Habitacion h : habitaciones) {
                tablapdf.addCell(h.getCodhaHabitacion());
                tablapdf.addCell("" + h.getNroHabitacion());
                tablapdf.addCell(h.getTipoHabitacion());
                tablapdf.addCell(h.getEstado());
                tablapdf.addCell("" + h.getCostoAlquiler());
                tablapdf.addCell("" + h.getNroPlanta());
                tablapdf.addCell(h.getCaracteristicas());
            }
            documento.add(tablapdf);
            documento.close();

            JOptionPane.showMessageDialog(this, "Exportación generada con exito\n\n"
                    + "RUTA: " + path + File.separator + nombreArchivo, SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } catch (DocumentException | IOException ex) {
            JOptionPane.showMessageDialog(this, "Error al exportar archivo, intente nuevamente.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void loadRoomType() {
        TipoHabitacionRepositoryImp roomTypeRepository = new TipoHabitacionRepositoryImp();
        List<TipoHabitacion> roomType = roomTypeRepository.findAll();
        cboTipoHabitacion.removeAllItems();
        cboTipoHabitacion.addItem("-- SELECCIONAR --");
        roomType.forEach(type -> {
            cboTipoHabitacion.addItem(type.getTipoHabitacion());
        });
    }
    
    public void loadTableRooms() {
        clearTable(tablaRoomReportes);
        
        List<Habitacion> habitaciones = null;
        switch (opcion) {
            case "1":
                habitaciones = repositoryRoom.findAllEstado("1");
                break;

            case "2":
                String estado = cboEstado.getSelectedIndex() == 0 ? "" : cboEstado.getSelectedItem().toString();
                habitaciones = repositoryRoom.findAllWhere("estado",estado,"1");
                break;

            case "3":
                String tipo = cboTipoHabitacion.getSelectedIndex() == 0 ? "" : cboTipoHabitacion.getSelectedItem().toString();
                habitaciones = repositoryRoom.findAllWhere("tipo_habitacion",tipo,"1");
                break;
        }
        
        if(habitaciones != null){
            DefaultTableModel modelRoom = (DefaultTableModel) tablaRoomReportes.getModel();
            habitaciones.forEach((h) -> {
                modelRoom.addRow(new Object[]{
                    h.getCodhaHabitacion(), h.getNroHabitacion(), h.getTipoHabitacion(), h.getEstado(),
                    h.getCostoAlquiler(), h.getNroPlanta(), h.getCaracteristicas()
                });
            });
            tablaRoomReportes.setModel(modelRoom);
        }
    }
    
    private DefaultTableModel clearTable(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        return model;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane4 = new javax.swing.JScrollPane();
        tablaRoomReportes = new javax.swing.JTable();
        rbTodo = new javax.swing.JRadioButton();
        rbTipoHabitacion = new javax.swing.JRadioButton();
        rbEstado = new javax.swing.JRadioButton();
        cboEstado = new javax.swing.JComboBox<>();
        cboTipoHabitacion = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        btnExportarExcel = new javax.swing.JButton();
        btnExportarPDF = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        jScrollPane4.setPreferredSize(new java.awt.Dimension(500, 500));

        tablaRoomReportes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tablaRoomReportes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "NRO. HABITACION", "TIPO DE HABITACION", "ESTADO", "COSTO ALQUILER (S/)", "NRO. PLANTA", "CARACTERISTICAS"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaRoomReportes.getTableHeader().setReorderingAllowed(false);
        jScrollPane4.setViewportView(tablaRoomReportes);

        rbTodo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbTodo.setText("TODO");
        rbTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbTodoActionPerformed(evt);
            }
        });

        rbTipoHabitacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbTipoHabitacion.setText("TIPO HABITACION");
        rbTipoHabitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbTipoHabitacionActionPerformed(evt);
            }
        });

        rbEstado.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbEstado.setText("ESTADO");
        rbEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbEstadoActionPerformed(evt);
            }
        });

        cboEstado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --", "DISPONIBLE", "MANTENIMIENTO", "OCUPADO" }));
        cboEstado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboEstadoActionPerformed(evt);
            }
        });

        cboTipoHabitacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --" }));
        cboTipoHabitacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTipoHabitacionActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        btnExportarExcel.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnExportarExcel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_microsoft_excel_26px.png"))); // NOI18N
        btnExportarExcel.setText("EXPORTAR EXCEL");
        btnExportarExcel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExportarExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarExcelActionPerformed(evt);
            }
        });

        btnExportarPDF.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnExportarPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_pdf_30px.png"))); // NOI18N
        btnExportarPDF.setText("EXPORTAR PDF");
        btnExportarPDF.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExportarPDF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarPDFActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel1.setText("REPORTE DE HABITACIONES");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 892, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbTodo)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(rbTipoHabitacion)
                                    .addComponent(rbEstado))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cboTipoHabitacion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cboEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(31, 31, 31)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnExportarExcel)
                            .addComponent(btnExportarPDF, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(rbTodo)
                                .addGap(20, 20, 20)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cboEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(rbEstado))
                                .addGap(20, 20, 20)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(rbTipoHabitacion)
                                    .addComponent(cboTipoHabitacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(btnExportarExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnExportarPDF, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void rbTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbTodoActionPerformed
        opcion = "1";
        hojaExcel = "TODAS LAS HABITACIONES";
        nombreArchivo = "TR_".concat(Util.getDateFormat(new Date()));

        this.rbTodo.setSelected(true);
        this.rbEstado.setSelected(false);
        this.rbTipoHabitacion.setSelected(false);
        this.cboEstado.setEnabled(false);
        this.cboTipoHabitacion.setEnabled(false);
        this.cboEstado.setSelectedIndex(0);
        this.cboTipoHabitacion.setSelectedIndex(0);

        loadTableRooms();
    }//GEN-LAST:event_rbTodoActionPerformed

    private void rbEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbEstadoActionPerformed
        opcion = "2";
        hojaExcel = "HABITACIONES POR ESTADO";
        nombreArchivo = "HE_".concat(Util.getDateFormat(new Date()));

        this.rbEstado.setSelected(true);
        this.rbTodo.setSelected(false);
        this.rbTipoHabitacion.setSelected(false);
        this.cboEstado.setEnabled(true);
        this.cboTipoHabitacion.setEnabled(false);
        this.cboTipoHabitacion.setSelectedIndex(0);
    }//GEN-LAST:event_rbEstadoActionPerformed

    private void rbTipoHabitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbTipoHabitacionActionPerformed
        opcion = "3";
        hojaExcel = "HABITACIONES POR TIPO";
        nombreArchivo = "HT_".concat(Util.getDateFormat(new Date()));

        this.rbTipoHabitacion.setSelected(true);
        this.rbTodo.setSelected(false);
        this.rbEstado.setSelected(false);
        this.cboEstado.setEnabled(false);
        this.cboTipoHabitacion.setEnabled(true);
        this.cboEstado.setSelectedIndex(0);
    }//GEN-LAST:event_rbTipoHabitacionActionPerformed

    private void btnExportarExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarExcelActionPerformed
        almacenarReporte();
    }//GEN-LAST:event_btnExportarExcelActionPerformed

    private void btnExportarPDFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarPDFActionPerformed
        generarPDF();
    }//GEN-LAST:event_btnExportarPDFActionPerformed

    private void cboEstadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboEstadoActionPerformed
        loadTableRooms();
    }//GEN-LAST:event_cboEstadoActionPerformed

    private void cboTipoHabitacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTipoHabitacionActionPerformed
        loadTableRooms();
    }//GEN-LAST:event_cboTipoHabitacionActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExportarExcel;
    private javax.swing.JButton btnExportarPDF;
    private javax.swing.JComboBox<String> cboEstado;
    private javax.swing.JComboBox<String> cboTipoHabitacion;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JRadioButton rbEstado;
    private javax.swing.JRadioButton rbTipoHabitacion;
    private javax.swing.JRadioButton rbTodo;
    public javax.swing.JTable tablaRoomReportes;
    // End of variables declaration//GEN-END:variables
}
