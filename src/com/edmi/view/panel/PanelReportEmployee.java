package com.edmi.view.panel;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.model.Trabajador;
import com.edmi.repository.RepositoryEmployee;
import com.edmi.repository.imp.TrabajadorRepositoryImp;
import com.edmi.util.Util;
import com.edmi.view.DialogReportFiles;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.HeadlessException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class PanelReportEmployee extends javax.swing.JPanel {
    private static PanelReportEmployee instance = null;
    private RepositoryEmployee<Trabajador> repositoryEmploye;
    private String opcion = "1";
    private String hojaExcel = "TODOS LOS USUARIOS";
    private String nombreArchivo = "";

    private PanelReportEmployee() {
        initComponents();
        repositoryEmploye = new TrabajadorRepositoryImp();
        loadTableEmployees();
        mostrarDescripionDocu();
        this.rbTodo.setSelected(true);
        this.cboTipoDocumento.setEnabled(false);
        this.cboCargo.setEnabled(false);
        nombreArchivo = "TU_".concat(Util.getDateFormat(new Date()));
    }

    public static PanelReportEmployee getInstance() {
        if (instance == null) {
            instance = new PanelReportEmployee();
        }
        return instance;
    }

    private void almacenarReporte() {
        String path = DialogReportFiles.obtenerRutaArchiReporPdfXml()[0];
        boolean res = generareReporte(path + File.separator + nombreArchivo + ".xlsx");
        if (res) {
            JOptionPane.showMessageDialog(this, "Exportacion generada con exito\n\n"
                    + "RUTA: " + path + File.separator + nombreArchivo, SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(this, "No se pudo generar el reporte,\n\ncontacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }

    private boolean generareReporte(String sheetName) {
        List<Trabajador> usuarios = null;
        switch (opcion) {
            case "1":
                usuarios = repositoryEmploye.findAllEstado("1");
                break;

            case "2":
                String tipoDocumento = cboTipoDocumento.getSelectedIndex() == 0 ? "" : cboTipoDocumento.getSelectedItem().toString();
                usuarios = repositoryEmploye.findAllWhere("tipo_documento", tipoDocumento, "1");
                break;
            case "3":
                String cargo = cboCargo.getSelectedIndex() == 0 ? "" : cboCargo.getSelectedItem().toString();
                usuarios = repositoryEmploye.findAllWhere("cargo", cargo, "1");
                break;
        }

        boolean response = false;
        Workbook book = new XSSFWorkbook();
        Sheet sheet = book.createSheet(this.hojaExcel);

        try {
            File ruta = new File("design" + File.separator + "logo inicio");
            String[] img = ruta.list();
            String logo = ruta.getAbsolutePath() + File.separator + img[0];

            int imgIndex;
            try (InputStream is = new FileInputStream(logo)) {
                byte[] bytes = IOUtils.toByteArray(is);
                imgIndex = book.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG); //Agrega la imagen y su tipo de imagen.
            }

            //Agregamos la imagen al archivo.
            CreationHelper help = book.getCreationHelper();
            Drawing draw = sheet.createDrawingPatriarch(); //Para crear la imagen.

            ClientAnchor anchor = help.createClientAnchor(); //Sacamos el ancho de la imagen para colocarlo de forma correcta.
            anchor.setCol1(0); //Posicion columna.
            anchor.setRow1(1); //Posicion fila.
            Picture pict = draw.createPicture(anchor, imgIndex); //Aparece la imagen.
            pict.resize(1, 5); //Indicamos el tamaño, de donde comenzara y cuanto espacio utilizara.

        } catch (IOException e) {
            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, e);
        }

        CellStyle estiloTitulo = book.createCellStyle(); //Para crear el diseño del titulo.
        estiloTitulo.setAlignment(HorizontalAlignment.CENTER); //Para centrar el titulo.
        estiloTitulo.setVerticalAlignment(VerticalAlignment.CENTER);

        org.apache.poi.ss.usermodel.Font fontTitulo = book.createFont(); //Problemas de compatibilidad con el itextPdf no se podia importar.

        fontTitulo.setFontName("Arial");
        fontTitulo.setBold(true);
        fontTitulo.setFontHeightInPoints((short) 22); //tamaño de letra
        estiloTitulo.setFont(fontTitulo); //agregamos los cambios.
        Row filaTitulo = sheet.createRow(2); //Creamos la fila para el titulo
        Cell celdaTitulo = filaTitulo.createCell(1); //Posicion columna
        celdaTitulo.setCellStyle(estiloTitulo); //Agregamos el estilo
        celdaTitulo.setCellValue("REPORTE DE USUARIOS");

        sheet.addMergedRegion(new CellRangeAddress(2, 2, 1, 3));

        sheet.setColumnWidth(0, 7000);
        sheet.setColumnWidth(1, 12000);
        sheet.setColumnWidth(2, 11000);
        sheet.setColumnWidth(3, 9000);
        sheet.setColumnWidth(4, 5000);
        sheet.setColumnWidth(5, 10000);
        sheet.setColumnWidth(6, 6000);
        sheet.setColumnWidth(7, 10000);

        CellStyle estiloTablaTitulos = book.createCellStyle(); //Para crear el diseño del titulo.
        estiloTablaTitulos.setAlignment(HorizontalAlignment.CENTER); //Para centrar el titulo.
        estiloTablaTitulos.setVerticalAlignment(VerticalAlignment.CENTER);

        String cabecera[] = new String[]{"CODIGO USUARIO", "NOMBRE Y APELLIDOS", "TIPO DOCUMENTO", "NUMERO DE DOCUMENTO", "TELEFONO", "EMAIL", "CARGO", "OBSERVACION"};

        CellStyle estiloTablaCabezera = book.createCellStyle();
        estiloTablaCabezera.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
        estiloTablaCabezera.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        estiloTablaCabezera.setBorderBottom(BorderStyle.THIN);
        estiloTablaCabezera.setBorderLeft(BorderStyle.THIN);
        estiloTablaCabezera.setBorderRight(BorderStyle.THIN);
        estiloTablaCabezera.setBorderTop(BorderStyle.THIN);

        org.apache.poi.ss.usermodel.Font font = book.createFont();
        font.setFontName("Arial");
        font.setBold(true);
        font.setFontHeightInPoints((short) 12);
        font.setColor(IndexedColors.WHITE.getIndex());
        estiloTablaCabezera.setFont(font);

        Row row = sheet.createRow(7);

        for (int i = 0; i < cabecera.length; i++) {
            Cell celdaCabecera = row.createCell(i);
            celdaCabecera.setCellStyle(estiloTablaCabezera);
            celdaCabecera.setCellValue(cabecera[i]);
        }
        
        CellStyle estiloTablaDatos = book.createCellStyle();
        estiloTablaDatos.setBorderBottom(BorderStyle.THIN);
        estiloTablaDatos.setBorderLeft(BorderStyle.THIN);
        estiloTablaDatos.setBorderRight(BorderStyle.THIN);
        estiloTablaDatos.setBorderTop(BorderStyle.THIN);

        int nroRow = 8;
        for (Trabajador t : usuarios) {
            Row fila = sheet.createRow(nroRow);
            
            Cell celdaInformacion0 = fila.createCell(0);
            celdaInformacion0.setCellValue(t.getCodTrabajador());
            celdaInformacion0.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion1 = fila.createCell(1);
            celdaInformacion1.setCellValue(t.getNombre() + " " + t.getApellidoPaterno() + " " + t.getApellidoMaterno());
            celdaInformacion1.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion2 = fila.createCell(2);
            celdaInformacion2.setCellValue(t.getTipoDococumento());
            celdaInformacion2.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion3 = fila.createCell(3);
            celdaInformacion3.setCellValue(t.getNroDocumento());
            celdaInformacion3.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion4 = fila.createCell(4);
            celdaInformacion4.setCellValue(t.getNroTelefono());
            celdaInformacion4.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion5 = fila.createCell(5);
            celdaInformacion5.setCellValue(t.getEmail());
            celdaInformacion5.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion6 = fila.createCell(6);
            celdaInformacion6.setCellValue(t.getCargo());
            celdaInformacion6.setCellStyle(estiloTablaDatos);
            
            Cell celdaInformacion7 = fila.createCell(7);
            celdaInformacion7.setCellValue(t.getObervacion());
            celdaInformacion7.setCellStyle(estiloTablaDatos);
            
            nroRow++;
        }

        try {
            try (FileOutputStream out = new FileOutputStream(sheetName)) {
                book.write(out);
                response = true;
            }
        } catch (IOException ex) {
            Logger.getLogger(PanelReportProduct.class.getName()).log(Level.SEVERE, null, ex);
            response = false;
        }

        try {
            book.close();
        } catch (IOException ex) {
            Logger.getLogger(PanelReportClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }

    private void generarPDF() {
        Document documento = new Document();
        documento.setMargins(24f, 24f, 24f, 24f);

        String path = DialogReportFiles.obtenerRutaArchiReporPdfXml()[0];

        try {
            PdfWriter.getInstance(documento, new FileOutputStream(path + File.separator + nombreArchivo + ".pdf"));
            documento.open();
        } catch (DocumentException | HeadlessException | FileNotFoundException e) {
            JOptionPane.showMessageDialog(this, "No se pudo generar el reporte,\n\ncontacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        List<Trabajador> usuarios = null;
        switch (opcion) {
            case "1":
                usuarios = repositoryEmploye.findAllEstado("1");
                break;

            case "2":
                String tipoDocumento = cboTipoDocumento.getSelectedIndex() == 0 ? "" : cboTipoDocumento.getSelectedItem().toString();
                usuarios = repositoryEmploye.findAllWhere("tipo_documento", tipoDocumento, "1");
                break;
            case "3":
                String cargo = cboCargo.getSelectedIndex() == 0 ? "" : cboCargo.getSelectedItem().toString();
                usuarios = repositoryEmploye.findAllWhere("cargo", cargo, "1");
                break;
        }

        try {
            documento.setPageSize(PageSize.A2.rotate());
            documento.open();

            BaseFont fuenteUser = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, true);
            Font tituloUser = new Font(fuenteUser, 20f, Font.BOLD, BaseColor.BLACK);

            BaseFont fuenteUser1 = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1250, true);
            Font celdaTitulo = new Font(fuenteUser1, 15f, Font.BOLD, BaseColor.RED);

            var titulo = new Paragraph(hojaExcel + "\n\n", tituloUser);
            titulo.setAlignment(Paragraph.ALIGN_CENTER);
            documento.add(titulo);

            PdfPTable tablapdf = new PdfPTable(new float[]{75f, 75f, 75f, 75f, 75f, 75f, 75f, 75f});
            tablapdf.setWidthPercentage(100f);
            tablapdf.addCell(new Phrase("CODIGO CLIENTE", celdaTitulo));
            tablapdf.addCell(new Phrase("NOMBRE Y APELLIDOS", celdaTitulo));
            tablapdf.addCell(new Phrase("TIPO DOCUMENTO", celdaTitulo));
            tablapdf.addCell(new Phrase("NUMERO DE DOCUMENTO", celdaTitulo));
            tablapdf.addCell(new Phrase("TELEFONO", celdaTitulo));
            tablapdf.addCell(new Phrase("EMAIL", celdaTitulo));
            tablapdf.addCell(new Phrase("CARGO", celdaTitulo));
            tablapdf.addCell(new Phrase("OBSERVACION", celdaTitulo));

            for (Trabajador t : usuarios) {
                tablapdf.addCell(t.getCodTrabajador());
                tablapdf.addCell(t.getNombre() + " " + t.getApellidoPaterno() + " " + t.getApellidoMaterno());
                tablapdf.addCell(t.getTipoDococumento());
                tablapdf.addCell(t.getNroDocumento());
                tablapdf.addCell(t.getNroTelefono());
                tablapdf.addCell(t.getEmail());
                tablapdf.addCell(t.getCargo());
                tablapdf.addCell(t.getObervacion());
            }
            documento.add(tablapdf);
            documento.close();

            JOptionPane.showMessageDialog(this, "Exportación generada con exito\n\n"
                    + "RUTA: " + path + File.separator + nombreArchivo, SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        } catch (DocumentException | IOException ex) {
            JOptionPane.showMessageDialog(this, "Error al exportar archivo, intente nuevamente.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        }
    }

    public void loadTableEmployees() {
        clearTable(tablaUsusariosReportes);
        DefaultTableModel modelEmployee = (DefaultTableModel) tablaUsusariosReportes.getModel();

        List<Trabajador> usuarios = null;
        switch (opcion) {
            case "1":
                usuarios = repositoryEmploye.findAllEstado("1");
                break;

            case "2":
                String tipoDocumento = cboTipoDocumento.getSelectedIndex() == 0 ? "" : cboTipoDocumento.getSelectedItem().toString();
                usuarios = repositoryEmploye.findAllWhere("tipo_documento", tipoDocumento, "1");
                break;
            case "3":
                String cargo = cboCargo.getSelectedIndex() == 0 ? "" : cboCargo.getSelectedItem().toString();
                usuarios = repositoryEmploye.findAllWhere("cargo", cargo, "1");
                break;
        }

        if(usuarios != null){
            usuarios.forEach(t -> {
            modelEmployee.addRow(new Object[]{t.getCodTrabajador(), t.getNombre().concat(" ").concat(t.getApellidoPaterno()).concat(" ").concat(t.getApellidoMaterno()),
                t.getTipoDococumento(), t.getNroDocumento(), t.getNroTelefono(), t.getEmail(), t.getObervacion(), t.getCargo()});
            });
            tablaUsusariosReportes.setModel(modelEmployee);
        }
    }

    private DefaultTableModel clearTable(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        for (int i = model.getRowCount() - 1; i >= 0; i--) {
            model.removeRow(i);
        }
        return model;
    }

    private void mostrarDescripionDocu() {
        List<String> lista = repositoryEmploye.findAllDocuDescripciones();
        cboTipoDocumento.addItem("-- SELECCIONAR --");
        for (String datos : lista) {
            cboTipoDocumento.addItem(datos);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane14 = new javax.swing.JScrollPane();
        tablaUsusariosReportes = new javax.swing.JTable();
        rbTodo = new javax.swing.JRadioButton();
        rbTipoDocumento = new javax.swing.JRadioButton();
        cboTipoDocumento = new javax.swing.JComboBox<>();
        rbCargoUsuario = new javax.swing.JRadioButton();
        cboCargo = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        btnExportarExcel = new javax.swing.JButton();
        btnExportarPDF = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        tablaUsusariosReportes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tablaUsusariosReportes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "NOMBRES", "TIPO DOCUMENTO", "N° DOCUMENTO", "N° TELEFONO", "CORREO ELECTRONICO", "OBSERVACION", "ROL"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaUsusariosReportes.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaUsusariosReportes.setShowVerticalLines(false);
        tablaUsusariosReportes.getTableHeader().setReorderingAllowed(false);
        jScrollPane14.setViewportView(tablaUsusariosReportes);
        if (tablaUsusariosReportes.getColumnModel().getColumnCount() > 0) {
            tablaUsusariosReportes.getColumnModel().getColumn(0).setPreferredWidth(200);
            tablaUsusariosReportes.getColumnModel().getColumn(0).setMaxWidth(100);
            tablaUsusariosReportes.getColumnModel().getColumn(1).setPreferredWidth(300);
            tablaUsusariosReportes.getColumnModel().getColumn(1).setMaxWidth(300);
            tablaUsusariosReportes.getColumnModel().getColumn(2).setPreferredWidth(250);
            tablaUsusariosReportes.getColumnModel().getColumn(2).setMaxWidth(250);
            tablaUsusariosReportes.getColumnModel().getColumn(3).setPreferredWidth(150);
            tablaUsusariosReportes.getColumnModel().getColumn(3).setMaxWidth(150);
            tablaUsusariosReportes.getColumnModel().getColumn(4).setPreferredWidth(150);
            tablaUsusariosReportes.getColumnModel().getColumn(4).setMaxWidth(120);
            tablaUsusariosReportes.getColumnModel().getColumn(5).setPreferredWidth(250);
            tablaUsusariosReportes.getColumnModel().getColumn(5).setMaxWidth(250);
            tablaUsusariosReportes.getColumnModel().getColumn(6).setPreferredWidth(250);
            tablaUsusariosReportes.getColumnModel().getColumn(6).setMaxWidth(250);
            tablaUsusariosReportes.getColumnModel().getColumn(7).setPreferredWidth(200);
            tablaUsusariosReportes.getColumnModel().getColumn(7).setMaxWidth(200);
        }

        rbTodo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbTodo.setText("TODO");
        rbTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbTodoActionPerformed(evt);
            }
        });

        rbTipoDocumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbTipoDocumento.setText("TIPO DE DOCUMENTO");
        rbTipoDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbTipoDocumentoActionPerformed(evt);
            }
        });

        cboTipoDocumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cboTipoDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboTipoDocumentoActionPerformed(evt);
            }
        });

        rbCargoUsuario.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        rbCargoUsuario.setText("CARGO DE USUARIO");
        rbCargoUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbCargoUsuarioActionPerformed(evt);
            }
        });

        cboCargo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cboCargo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --", "ADMINISTRADOR", "EJECUTIVO DE VENTAS" }));
        cboCargo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboCargoActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        btnExportarExcel.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnExportarExcel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_microsoft_excel_26px.png"))); // NOI18N
        btnExportarExcel.setText("EXPORTAR EXCEL");
        btnExportarExcel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExportarExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarExcelActionPerformed(evt);
            }
        });

        btnExportarPDF.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnExportarPDF.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icons8_pdf_30px.png"))); // NOI18N
        btnExportarPDF.setText("EXPORTAR PDF");
        btnExportarPDF.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExportarPDF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarPDFActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 1, 18)); // NOI18N
        jLabel1.setText("REPORTE DE USUARIOS");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(rbCargoUsuario)
                                    .addComponent(rbTipoDocumento))
                                .addGap(8, 8, 8)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cboCargo, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cboTipoDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(rbTodo))
                        .addGap(25, 25, 25)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnExportarExcel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnExportarPDF, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 892, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(rbTodo)
                                .addGap(20, 20, 20)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(rbTipoDocumento)
                                    .addComponent(cboTipoDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(20, 20, 20)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(rbCargoUsuario)
                                    .addComponent(cboCargo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(31, 31, 31))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addComponent(btnExportarExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnExportarPDF, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(jScrollPane14, javax.swing.GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void rbTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbTodoActionPerformed
        opcion = "1";
        hojaExcel = "TODOS LOS USUARIOS";
        nombreArchivo = "TU_".concat(Util.getDateFormat(new Date()));

        this.rbTodo.setSelected(true);
        this.rbTipoDocumento.setSelected(false);
        this.rbCargoUsuario.setSelected(false);
        this.cboTipoDocumento.setEnabled(false);
        this.cboCargo.setEnabled(false);
        this.cboTipoDocumento.setSelectedIndex(0);
        this.cboCargo.setSelectedIndex(0);

    }//GEN-LAST:event_rbTodoActionPerformed

    private void rbTipoDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbTipoDocumentoActionPerformed
        opcion = "2";
        hojaExcel = "USUARIOS POR TIPO DOCUMENTO";
        nombreArchivo = "UTD_".concat(Util.getDateFormat(new Date()));

        this.rbTipoDocumento.setSelected(true);
        this.rbTodo.setSelected(false);
        this.rbCargoUsuario.setSelected(false);
        this.cboTipoDocumento.setEnabled(true);
        this.cboCargo.setEnabled(false);
        this.cboCargo.setSelectedIndex(0);
    }//GEN-LAST:event_rbTipoDocumentoActionPerformed

    private void rbCargoUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbCargoUsuarioActionPerformed
        opcion = "3";
        hojaExcel = "USUARIOS POR CARGO";
        nombreArchivo = "UC_".concat(Util.getDateFormat(new Date()));

        this.rbCargoUsuario.setSelected(true);
        this.rbTodo.setSelected(false);
        this.rbTipoDocumento.setSelected(false);
        this.cboTipoDocumento.setEnabled(false);
        this.cboCargo.setEnabled(true);
        this.cboTipoDocumento.setSelectedIndex(0);
    }//GEN-LAST:event_rbCargoUsuarioActionPerformed

    private void cboTipoDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboTipoDocumentoActionPerformed
        loadTableEmployees();
    }//GEN-LAST:event_cboTipoDocumentoActionPerformed

    private void cboCargoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboCargoActionPerformed
        loadTableEmployees();
    }//GEN-LAST:event_cboCargoActionPerformed

    private void btnExportarExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarExcelActionPerformed
        almacenarReporte();
    }//GEN-LAST:event_btnExportarExcelActionPerformed

    private void btnExportarPDFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarPDFActionPerformed
        generarPDF();
    }//GEN-LAST:event_btnExportarPDFActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExportarExcel;
    private javax.swing.JButton btnExportarPDF;
    private javax.swing.JComboBox<String> cboCargo;
    private javax.swing.JComboBox<String> cboTipoDocumento;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JRadioButton rbCargoUsuario;
    private javax.swing.JRadioButton rbTipoDocumento;
    private javax.swing.JRadioButton rbTodo;
    public javax.swing.JTable tablaUsusariosReportes;
    // End of variables declaration//GEN-END:variables
}
