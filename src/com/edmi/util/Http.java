package com.edmi.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Http {

    public static String httpGetMethod(String tipoDocu, String numDocu) {
        //tipoDocu = ruc o dni
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL("https://api.apis.net.pe/v1/" + tipoDocu + "?numero=" + numDocu);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            Logger.getLogger(Http.class.getName()).log(Level.SEVERE, null, e);
        }
        return response.toString();
    }
}
