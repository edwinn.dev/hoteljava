package com.edmi.model.invoice;

public class Relacionado {
    private String indDocRelacionado;
    private String numIdeAnticipo;
    private String tipDocRelacionado;
    private String numDocRelacionado;
    private String tipDocEmisor;
    private String numDocEmisor;
    private String mtoDocRelacionado;

    public String getIndDocRelacionado() {
        return indDocRelacionado;
    }

    public void setIndDocRelacionado(String indDocRelacionado) {
        this.indDocRelacionado = indDocRelacionado;
    }

    public String getNumIdeAnticipo() {
        return numIdeAnticipo;
    }

    public void setNumIdeAnticipo(String numIdeAnticipo) {
        this.numIdeAnticipo = numIdeAnticipo;
    }

    public String getTipDocRelacionado() {
        return tipDocRelacionado;
    }

    public void setTipDocRelacionado(String tipDocRelacionado) {
        this.tipDocRelacionado = tipDocRelacionado;
    }

    public String getNumDocRelacionado() {
        return numDocRelacionado;
    }

    public void setNumDocRelacionado(String numDocRelacionado) {
        this.numDocRelacionado = numDocRelacionado;
    }

    public String getTipDocEmisor() {
        return tipDocEmisor;
    }

    public void setTipDocEmisor(String tipDocEmisor) {
        this.tipDocEmisor = tipDocEmisor;
    }

    public String getNumDocEmisor() {
        return numDocEmisor;
    }

    public void setNumDocEmisor(String numDocEmisor) {
        this.numDocEmisor = numDocEmisor;
    }

    public String getMtoDocRelacionado() {
        return mtoDocRelacionado;
    }

    public void setMtoDocRelacionado(String mtoDocRelacionado) {
        this.mtoDocRelacionado = mtoDocRelacionado;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Relacionado{indDocRelacionado=").append(indDocRelacionado);
        sb.append(", numIdeAnticipo=").append(numIdeAnticipo);
        sb.append(", tipDocRelacionado=").append(tipDocRelacionado);
        sb.append(", numDocRelacionado=").append(numDocRelacionado);
        sb.append(", tipDocEmisor=").append(tipDocEmisor);
        sb.append(", numDocEmisor=").append(numDocEmisor);
        sb.append(", mtoDocRelacionado=").append(mtoDocRelacionado);
        sb.append('}');
        return sb.toString();
    }
}
