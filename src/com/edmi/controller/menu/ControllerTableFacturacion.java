package com.edmi.controller.menu;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.model.DetalleFacbo;
import com.edmi.reports.ReportComunicacionBaja;
import com.edmi.reports.ReportResumenDiario;
import com.edmi.repository.imp.NotaCreditoRepository;
import com.edmi.util.Constant;
import com.edmi.util.NombresArchivosJSON;
import com.edmi.view.DialogBaja;
import com.edmi.view.DialogResumenDiario;
import com.edmi.util.Util;
import com.edmi.view.DialogNotaCreditoDebito;
import com.edmi.view.DialogNotaDebito;
import com.edmi.view.MainPanelServices;
import com.edmi.view.panel.PanelMainVoucher;
import com.edmi.view.widget.MenuTableFacturacion;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControllerTableFacturacion implements ActionListener, MouseListener {
    private MenuTableFacturacion menuFactura;
    private MainPanelServices mainFrame;
    private List<DetalleFacbo> detallesBoleta;
    private int row;
    private static String idCabecera;

    public ControllerTableFacturacion(MainPanelServices mainFrame) {
        this.mainFrame = mainFrame;
        this.menuFactura = MenuTableFacturacion.getInstance();
        this.menuFactura.initMenuTableFacturacion(PanelMainVoucher.tableVouchers);
    }
    
    public void setListener() {
        this.menuFactura.itemNotaCredito.addActionListener(this);
        this.menuFactura.itemNotaDebito.addActionListener(this);
        this.menuFactura.itemBaja.addActionListener(this);
        this.menuFactura.itemResumen.addActionListener(this);
        PanelMainVoucher.tableVouchers.addMouseListener(this);
    }

    private void mostrarDatosNotaCredito(DialogNotaCreditoDebito dialog) {
        List<DetalleFacbo> lista = NotaCreditoRepository.findAllProductNotaCredito(Long.parseLong(idCabecera), "");
        DetalleFacbo dfb = lista.get(lista.size() - 1);
        double sumaTotal = 0.00D;
        for (DetalleFacbo df : lista) {
            sumaTotal += Double.parseDouble(df.getTotal_importe_item());
        }

        dialog.txtCodigoNota.setText(PanelMainVoucher.tableVouchers.getValueAt(row, 8).toString().substring(0, 1).equals("F")
                ? "FC" + NombresArchivosJSON.getCodigoNotaCredito()
                : "BC" + NombresArchivosJSON.getCodigoNotaCredito());

        dialog.txtCodigoCliente.setText(dfb.getCod_cliente());

        switch (dfb.getCod_tipodocumento()) {
            case "1":
                dialog.txtTipoDocumento.setText("DNI");
                break;
            case "4":
                dialog.txtTipoDocumento.setText("CEX");
                break;
            case "6":
                dialog.txtTipoDocumento.setText("RUC");
                break;
            case "7":
                dialog.txtTipoDocumento.setText("PAS");
                break;
            default:
                dialog.txtTipoDocumento.setText("-");
        }

        dialog.txtIdCabecera.setText(idCabecera);
        dialog.txtNumeroDocumento.setText(dfb.getNumeroDocumento());
        dialog.txtRazonSocial.setText(dfb.getRazonSocial());
        dialog.txtFechaComprobante.setText(dfb.getFecha_emision());
        dialog.txtSerieComprobante.setText(dfb.getSerie());
        dialog.txtNumeroComprobante.setText(dfb.getNumero());
        dialog.txtSumaTotal.setText(Util.decimalRound(sumaTotal));
        dialog.txtIGV.setText(dfb.getPorcentaje_afectacion());
    }

    private void mostrarDatosNotaDebito(DialogNotaDebito dialog) {
        List<DetalleFacbo> lista = NotaCreditoRepository.findAllProductNotaCredito(Long.parseLong(idCabecera), "");
        DetalleFacbo dfb = lista.get(lista.size() - 1);

        dialog.txtCodigoNota.setText(PanelMainVoucher.tableVouchers.getValueAt(row, 8).toString().substring(0, 1).equals("F")
                ? "FD" + NombresArchivosJSON.getCodigoNotaDebito()
                : "BD" + NombresArchivosJSON.getCodigoNotaDebito());

        dialog.txtCodigoCliente.setText(dfb.getCod_cliente());

        switch (dfb.getCod_tipodocumento()) {
            case "1":
                dialog.txtTipoDocumento.setText("DNI");
                break;
            case "4":
                dialog.txtTipoDocumento.setText("CEX");
                break;
            case "6":
                dialog.txtTipoDocumento.setText("RUC");
                break;
            case "7":
                dialog.txtTipoDocumento.setText("PAS");
                break;
            default:
                dialog.txtTipoDocumento.setText("-");
        }

        dialog.txtIdCabecera.setText(idCabecera);
        dialog.txtNumeroDocumento.setText(dfb.getNumeroDocumento());
        dialog.txtRazonSocial.setText(dfb.getRazonSocial());
        dialog.txtFechaComprobante.setText(dfb.getFecha_emision());
        dialog.txtSerieComprobante.setText(dfb.getSerie());
        dialog.txtNumeroComprobante.setText(dfb.getNumero());
    }

    private void mostrarDatosTablaNota(DialogNotaCreditoDebito dialog) {
        DefaultTableModel model = (DefaultTableModel) dialog.tablaProductosNotaCredito.getModel();

        List<DetalleFacbo> lista = NotaCreditoRepository.findAllProductNotaCredito(Long.parseLong(idCabecera), "");

        lista.forEach(dfb -> {
            model.addRow(new Object[]{dfb.getCod_producto(), dfb.getNombreProducto(), dfb.getCantidad(), dfb.getValor_unitario(),
                dfb.getTotal_importe_item(), dfb.getUnidadMedida()});
        });
        dialog.tablaProductosNotaCredito.setModel(model);
    }

    public static String getIdCabecera() {
        return ControllerTableFacturacion.idCabecera;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.menuFactura.itemNotaCredito) {
            if (row >= 0) {
                String exists = PanelMainVoucher.tableVouchers.getValueAt(row, 11).toString().trim(); 
                if(! exists.equalsIgnoreCase(Constant.ENVIADO_ACEPTADO)) {
                    JOptionPane.showMessageDialog(mainFrame, Constant.MSG_NO_ENVIADO, SYS_MSG, JOptionPane.WARNING_MESSAGE);
                }else {
                    DialogNotaCreditoDebito dialog = new DialogNotaCreditoDebito(mainFrame, true, 1);
                    mostrarDatosNotaCredito(dialog);
                    mostrarDatosTablaNota(dialog);
                    dialog.setLocationRelativeTo(mainFrame);
                    dialog.setVisible(true);
                }
            } else {
                JOptionPane.showMessageDialog(mainFrame, "Seleccione un comprobante.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        }else if (e.getSource() == this.menuFactura.itemNotaDebito) {
            if (row >= 0) {
                String exists = PanelMainVoucher.tableVouchers.getValueAt(row, 11).toString().trim(); 
                if(! exists.equalsIgnoreCase(Constant.ENVIADO_ACEPTADO)) {
                    JOptionPane.showMessageDialog(mainFrame, Constant.MSG_NO_ENVIADO, SYS_MSG, JOptionPane.WARNING_MESSAGE);
                }else {
                    DialogNotaDebito dialog = new DialogNotaDebito(mainFrame, true);
                    dialog.setTitle("Nota de Débito");
                    mostrarDatosNotaDebito(dialog);
                    dialog.setLocationRelativeTo(mainFrame);
                    dialog.setVisible(true);
                }
            } else {
                JOptionPane.showMessageDialog(mainFrame, "Seleccione un comprobante.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(e.getSource() == this.menuFactura.itemBaja){
            if (row >= 0) {
                String exists = PanelMainVoucher.tableVouchers.getValueAt(row, 11).toString().trim(); 
                if(! exists.equalsIgnoreCase(Constant.ENVIADO_ACEPTADO)) {
                    JOptionPane.showMessageDialog(mainFrame, Constant.MSG_NO_ENVIADO, SYS_MSG, JOptionPane.WARNING_MESSAGE);
                }else {
                    String serie = PanelMainVoucher.tableVouchers.getValueAt(row, 8).toString().trim(); 
                    if(serie.substring(0,1).equals("B")){
                        JOptionPane.showMessageDialog(mainFrame, "La comunicacion de baja solo se \n"
                                + "aplica para Facturas, Notas de debito de Factura\n"
                                + "o para Notas de credito de Factura", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                    } else {
                        String numero = PanelMainVoucher.tableVouchers.getValueAt(row, 9).toString().trim(); 

                        String tipoBaja = this.obtenerTipoDocumentoBaja(PanelMainVoucher.tableVouchers.getValueAt(row, 8).toString().trim());
                        DialogBaja dialogBaja = new DialogBaja(mainFrame, true);
                        dialogBaja.txtIdCabecera.setText(PanelMainVoucher.tableVouchers.getValueAt(row, 0).toString().trim());
                        dialogBaja.txtFechaDocumento.setText(PanelMainVoucher.tableVouchers.getValueAt(row, 6).toString().substring(0, 10).trim());
                        dialogBaja.txtTipoDocumento.setText(tipoBaja);
                        dialogBaja.txtNroDoc.setText(serie+"-"+numero);
                        dialogBaja.fechaGeneracionBaja.setDate(new Date());
                        ReportComunicacionBaja comBaja = new ReportComunicacionBaja(dialogBaja);
                        comBaja.initView(mainFrame);
                    }
                }
            } else {
                JOptionPane.showMessageDialog(mainFrame, "Seleccione un comprobante.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        }
        
        if(e.getSource() == this.menuFactura.itemResumen){
            if (row >= 0 || idCabecera == null) {
                String exists = PanelMainVoucher.tableVouchers.getValueAt(row, 11).toString().trim(); 
                if(! exists.equalsIgnoreCase(Constant.ENVIADO_ACEPTADO)) {
                    JOptionPane.showMessageDialog(mainFrame, Constant.MSG_NO_ENVIADO, SYS_MSG, JOptionPane.WARNING_MESSAGE);
                }else {
                    //JOptionPane.showMessageDialog(mainFrame, Constant.MSG_ENVIADO_ACEPTADO, SYS_MSG, JOptionPane.WARNING_MESSAGE);
                    String serie = PanelMainVoucher.tableVouchers.getValueAt(row, 8).toString().trim(); 
                    if(serie.substring(0,1).equals("F")){
                        JOptionPane.showMessageDialog(mainFrame, "El resumen diario solo se\naplica para Boletas, Notas de debito de Boleta\no para Notas de credito de Boleta", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                    } else {
                        mostrarResumenDiario(Long.parseLong(idCabecera));
                    }
                }
            } else {
                JOptionPane.showMessageDialog(mainFrame, "Seleccione un comprobante.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    private String obtenerTipoDocumentoBaja(String serie){
        String tipo = null;
        switch(serie.substring(0,1)){
            case "F" :
                tipo = "01";
                break;
            case "B":
                tipo = "03";
                break;
        }
        return tipo;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        this.row = PanelMainVoucher.tableVouchers.getSelectedRow();
        ControllerTableFacturacion.idCabecera = PanelMainVoucher.tableVouchers.getValueAt(row, 0).toString();
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    private String obtenerTipoDocumentoAfectado(String tipoDocResumen) {
        String value;
        switch(tipoDocResumen.substring(0,1).toUpperCase().trim()){
            case "B0": value = "03"; break;
            case "BC": value = "07"; break;
            case "BD": value = "08"; break;
            default: value = "03"; break;
        }
        return value;
    }

    private void mostrarResumenDiario(Long idCabecera) {
        detallesBoleta = NotaCreditoRepository.findAllProductNotaCredito(idCabecera, "");
        Double opGrav=0.0, opExo=0.0, opIna=0.0, opGrat=0.0, importeTotal=0.0;
        
        for(DetalleFacbo d : detallesBoleta){
            if(Double.parseDouble(d.getPorcentaje_afectacion()) > 0.0){
                opGrav += Double.parseDouble(d.getTotal_importe_item());
            }else{
                opExo = 0.0;
                opIna += Double.parseDouble(d.getTotal_importe_item());
            }
            importeTotal += Double.parseDouble(d.getTotal_importe_item());
        }
        
        String tipoDocResumen = this.obtenerTipoDocumentoBaja(PanelMainVoucher.tableVouchers.getValueAt(row, 8).toString().trim());
        String serie = PanelMainVoucher.tableVouchers.getValueAt(row, 8).toString().trim();
        String numero = PanelMainVoucher.tableVouchers.getValueAt(row, 9).toString().trim();
        String nroDocCliente = PanelMainVoucher.tableVouchers.getValueAt(row, 2).toString().trim();

        DialogResumenDiario dResumen = new DialogResumenDiario(mainFrame, true);
        dResumen.txtIdCabecera.setText(ControllerTableFacturacion.idCabecera);
        dResumen.txtFechaDocAfectado.setText(PanelMainVoucher.tableVouchers.getValueAt(row, 6).toString().substring(0, 10).trim());
        dResumen.txtTipoDocResumen.setText(tipoDocResumen);
        dResumen.txtSerieNroResumen.setText(serie+"-"+numero);
        dResumen.txtNroDocUsuario.setText(nroDocCliente);
        dResumen.txtTipoDocUsuario.setText(nroDocCliente.length() > 8 ? "6" : "1");
        dResumen.txtTipoMoneda.setText(PanelMainVoucher.tableVouchers.getValueAt(row, 3).toString());
        dResumen.txtOpExoneradas.setText("0.00");
        dResumen.txtOpInafectas.setText("0.00");
        dResumen.txtOpGratuitas.setText("0.00");
        dResumen.txtOpExportacion.setText("0.00");

        dResumen.txtTipoDocAfectado.setText(obtenerTipoDocumentoAfectado(tipoDocResumen));
        dResumen.txtSerieBoleta.setText(PanelMainVoucher.tableVouchers.getValueAt(row, 8).toString());
        dResumen.txtCorrelativoBoleta.setText(PanelMainVoucher.tableVouchers.getValueAt(row, 9).toString());
       // dResumen.txtTipoDocAfectado.setText();
        
        dResumen.txtOpExoneradas.setText(opExo.toString());
        dResumen.txtOpExportacion.setText("0.0");
        dResumen.txtOpGratuitas.setText(opGrat.toString());
        dResumen.txtOpInafectas.setText(opIna.toString());
        dResumen.txtOpGravadas.setText(opGrav.toString());
        dResumen.txtImporteTotal.setText(importeTotal.toString());

        ReportResumenDiario reportResumen = new ReportResumenDiario(dResumen);
        reportResumen.initView(mainFrame, detallesBoleta);
    }
}