package com.edmi.view.widget;

import com.edmi.controller.ControllerCardRoom;
import com.edmi.view.MainPanelServices;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

public class CardRoom extends JPanel {

    private ControllerCardRoom controllerCard;
    // private ControllerCardRoom.ItemMenuListener itemListener;
    private final MainPanelServices frame;
//    private Color colorPrimario = new Color(0x93ABD3);
////    private Color colorContorno = new Color(0x4B778D);
//    private Color colorPrimario = new Color(0x93ABD3);
//    private Color colorContorno = new Color(0x4B778D);
    private int arcw = 20;
    private int arch = 20;

    public CardRoom(MainPanelServices frame) {
        this.frame = frame;
        initComponents();
//        initMenuItem();
        setPreferences();
//        setListenerMenuItem();
    }

    private void setPreferences() {
        controllerCard = new ControllerCardRoom(this, frame);
        this.setOpaque(false);
        this.addMouseListener(controllerCard);

    }

//    private void setListenerMenuItem(){
//        itemListener = controllerCard.new ItemMenuListener();
//        this.itemReservar.addActionListener(itemListener);
//        this.itemDetalle.addActionListener(itemListener);
//        this.itemLimpieza.addActionListener(itemListener);
//        this.itemMantenimiento.addActionListener(itemListener);
//        this.itemChx.addActionListener(itemListener);
//    }
    private void initMenuItem() {
        menu = new JPopupMenu();
        itemReservar = new JMenuItem("Reservar");
        itemDetalle = new JMenuItem("Ver detalle");
        itemLimpieza = new JMenuItem("Limpieza");
        itemMantenimiento = new JMenuItem("Limpieza");
        itemChx = new JCheckBoxMenuItem("Terminar reserva");

        menu.add(itemReservar);
        menu.add(itemDetalle);
        menu.add(itemLimpieza);
        menu.add(itemMantenimiento);
        menu.add(itemChx);
        this.setComponentPopupMenu(menu);
    }

    private Color cambiarColor() {
        Color color = null;
        if (txtEstadoHabitacion.getText().toLowerCase().equals("disponible")) {
            color = new Color(84, 236, 90);
        } else if ((txtEstadoHabitacion.getText().toLowerCase().equals("mantenimiento"))) {
            color = new Color(251, 243, 102);
        } else if ((txtEstadoHabitacion.getText().toLowerCase().equals("ocupado"))) {
            color = new Color(255,82,82);
//            jLabel10.setForeground(color.WHITE);
//            jLabel13.setForeground(Color.WHITE);
//            jLabel2.setForeground(Color.WHITE);
//            jLabel4.setForeground(Color.WHITE);
//            jLabel6.setForeground(Color.WHITE);
//            jLabel8.setForeground(Color.WHITE);
//            jLabel9.setForeground(Color.WHITE);
//            txtImagenHabitacion.setForeground(Color.WHITE);
//            txtEstadoHabitacion.setForeground(Color.WHITE);
//            txtCodigoHabitacion.setForeground(Color.WHITE);
//            txtCostoHabitacion.setForeground(Color.WHITE);
//            txtEstadoHabitacion.setForeground(Color.WHITE);
//            txtNroPlanta.setForeground(Color.WHITE);
//            txtTipoHabitacion.setForeground(Color.WHITE);
//            txtNumeroHabitacion.setForeground(Color.WHITE);
//            txtWifi.setForeground(Color.WHITE);
        }
        return color;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtImagenHabitacion = new javax.swing.JLabel();
        txtNumeroHabitacion = new javax.swing.JLabel();
        txtCodigoHabitacion = new javax.swing.JLabel();
        txtEstadoHabitacion = new javax.swing.JLabel();
        txtTipoHabitacion = new javax.swing.JLabel();
        txtNroPlanta = new javax.swing.JLabel();
        txtCostoHabitacion = new javax.swing.JLabel();

        setOpaque(false);
        setPreferredSize(new java.awt.Dimension(288, 250));

        jPanel1.setOpaque(false);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Número:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Codigo :");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Estado :");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Tipo :");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Planta :");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Costo x noche S/.");

        txtImagenHabitacion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        txtNumeroHabitacion.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtNumeroHabitacion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txtNumeroHabitacion.setText("100");

        txtCodigoHabitacion.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtCodigoHabitacion.setText("00000");

        txtEstadoHabitacion.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtEstadoHabitacion.setText("Disponible");

        txtTipoHabitacion.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtTipoHabitacion.setText("Individual");

        txtNroPlanta.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtNroPlanta.setText("2");

        txtCostoHabitacion.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtCostoHabitacion.setText("S/ 60.00");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtImagenHabitacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE))
                                .addGap(21, 21, 21)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtNroPlanta, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtTipoHabitacion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtEstadoHabitacion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 95, Short.MAX_VALUE)
                                    .addComponent(txtCodigoHabitacion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtNumeroHabitacion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCostoHabitacion)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtImagenHabitacion, javax.swing.GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtNumeroHabitacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtCodigoHabitacion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtEstadoHabitacion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtTipoHabitacion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtNroPlanta))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtCostoHabitacion))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        Paint oldPaint = g2.getPaint();
        RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, getWidth(), getHeight(), arcw, arch);
        g2.clip(r2d);

        g2.setPaint(new GradientPaint(0.0f, 0.0f, cambiarColor(), 0.0f, getHeight(), cambiarColor()));
        g2.fillRect(0, 0, getWidth(), getHeight());

        g2.setStroke(new BasicStroke(3f));
        g2.setPaint(new GradientPaint(0.0f, 0.0f, cambiarColor(), 0.0f, getHeight(), cambiarColor()));
        g2.drawRoundRect(0, 0, getWidth(), getHeight(), 20, 20);

        g2.setPaint(oldPaint);
        super.paintComponent(g);
    }

//    public Color getColorPrimario() {
//        return colorPrimario;
//    }
//
//    public void setColorPrimario(Color colorPrimario) {
//        this.colorPrimario = colorPrimario;
//    }
//
//    public Color getColorContorno() {
//        return colorContorno;
//    }
//
//    public void setColorContorno(Color colorContorno) {
//        this.colorContorno = colorContorno;
//    }
    public int getArcw() {
        return this.arcw;
    }

    public void setArcw(int arcw) {
        this.arcw = arcw;
    }

    public int getArch() {
        return this.arch;
    }

    public void setArch(int arch) {
        this.arch = arch;
    }

    private JPopupMenu menu;
    public JMenuItem itemReservar;
    public JMenuItem itemDetalle;
    public JMenuItem itemLimpieza;
    public JMenuItem itemMantenimiento;
    public JCheckBoxMenuItem itemChx;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    public javax.swing.JLabel txtCodigoHabitacion;
    public javax.swing.JLabel txtCostoHabitacion;
    public javax.swing.JLabel txtEstadoHabitacion;
    public javax.swing.JLabel txtImagenHabitacion;
    public javax.swing.JLabel txtNroPlanta;
    public javax.swing.JLabel txtNumeroHabitacion;
    public javax.swing.JLabel txtTipoHabitacion;
    // End of variables declaration//GEN-END:variables
}
