package com.edmi.controller;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.view.DialogReportFiles;
import com.edmi.view.RutaSFS1;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class ControllerUbicacionSFS implements ActionListener {
    private RutaSFS1 rutaSfs = null;
    
    public ControllerUbicacionSFS(RutaSFS1 rutaSfs) {
        this.rutaSfs = rutaSfs;
        setListeners();
    }
    
    public void iniciarVista() {
        rutaSfs.setLocationRelativeTo(null);
        rutaSfs.setResizable(false);
        rutaSfs.setVisible(true);
    }
    
    private void setListeners() {
        rutaSfs.btnFileChooser.addActionListener(this);
        rutaSfs.btnGuardarRuta.addActionListener(this);
        rutaSfs.btnFileChooserSqlite.addActionListener(this);
    }
    
    private void buscarRuta() {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Seleccione la carpeta para guardar el SFS...");
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fc.setAcceptAllFileFilterUsed(false);
        if (fc.showOpenDialog(rutaSfs) == JFileChooser.APPROVE_OPTION) {
            rutaSfs.txtRutaArhivosSfs.setText("" + fc.getSelectedFile());
        }
    }
    
    private void buscarRutaSqlite() {
        JFileChooser fc = new JFileChooser();
        fc.setDialogTitle("Seleccione la base de datos del Facturador.");
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fc.setAcceptAllFileFilterUsed(false);
        if (fc.showOpenDialog(rutaSfs) == JFileChooser.APPROVE_OPTION) {
            rutaSfs.txtRutaSqlite.setText("" + fc.getSelectedFile());
        }
    }
    
    public static String[] obtenerRutaSFS() {
        File file = new File("design" + File.separator + "path" + File.separator + "ubicacionSFS.txt");
        String[] ruta = new String[3];
        int increment = 0;
        try {
            try (Scanner scanner = new Scanner(file)) {
                while (scanner.hasNextLine()) {
                    ruta[increment] = scanner.nextLine();
                    increment++;
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DialogReportFiles.class.getName()).log(Level.SEVERE, null, ex);
            ruta = null;
        }
        return ruta;
    }
    
    private void agregarRuta() {
        if (rutaSfs.txtRutaArhivosSfs.getText().trim().isEmpty() || rutaSfs.txtRutaSqlite.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Ambas rutas son obligatorias.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
        } else {
            String ruta = rutaSfs.txtRutaArhivosSfs.getText().trim();
            String rutaSqlite = rutaSfs.txtRutaSqlite.getText().trim();
            File file = new File("design" + File.separator + "path" + File.separator + "ubicacionSFS.txt");
            try {
                try (PrintWriter writer = new PrintWriter(file)) {
                    writer.println(ruta);
                    writer.println(rutaSqlite);
                }
            } catch (IOException ex) {
            }
            JOptionPane.showMessageDialog(rutaSfs, "Cambios guardados.\n"
                    + "Es necesario reiniciar el programa para ver los cambios.", SYS_MSG, JOptionPane.WARNING_MESSAGE);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == rutaSfs.btnFileChooser) {
            buscarRuta();
        }
        
        if(e.getSource() == rutaSfs.btnFileChooserSqlite){
            buscarRutaSqlite();
        }
        
        if (e.getSource() == rutaSfs.btnGuardarRuta) {
            agregarRuta();
        }
    }
}
