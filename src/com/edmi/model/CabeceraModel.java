package com.edmi.model;

public class CabeceraModel {
    private long idCabecera;
    private String codCliente;
    private String fechaEmision; 
    private String horaEmision; 
    private String fechaVencimiento ;
    private String tipoMoneda;
    private String totalTributos; 
    private String totalDescuentos;
    private String importeVenta;
    private String totalVenta; 
    private String precioVenta;
    private String otrosCargos;
    private String tipoComprobante; 
    private String serie;
    private String numero;
    private String situacionSunat;
    
    private String auxNombreCliente;
    private String auxNumDoc;
    private String auxEstado;

    public long getIdCabecera() {
        return idCabecera;
    }

    public void setIdCabecera(long idCabecera) {
        this.idCabecera = idCabecera;
    }
    
    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String cod_cliente) {
        this.codCliente = cod_cliente;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fecha_emsion) {
        this.fechaEmision = fecha_emsion;
    }

    public String getHoraEmision() {
        return horaEmision;
    }

    public void setHoraEmision(String hora_emsion) {
        this.horaEmision = hora_emsion;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fecha_vencimiento) {
        this.fechaVencimiento = fecha_vencimiento;
    }

    public String getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(String tipo_moneda) {
        this.tipoMoneda = tipo_moneda;
    }

    public String getTotalTributos() {
        return totalTributos;
    }

    public void setTotalTributos(String total_tributos) {
        this.totalTributos = total_tributos;
    }

    public String getTotalDescuento() {
        return totalDescuentos;
    }

    public void setTotalDescuento(String total_descuento) {
        this.totalDescuentos = total_descuento;
    }

    public String getImporteVenta() {
        return importeVenta;
    }

    public void setImporteVenta(String importe_venta) {
        this.importeVenta = importe_venta;
    }

    public String getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(String total_venta) {
        this.totalVenta = total_venta;
    }

    public String getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(String precio_venta) {
        this.precioVenta = precio_venta;
    }

    public String getOtrosCargos() {
        return otrosCargos;
    }

    public void setOtrosCargos(String otros_cargos) {
        this.otrosCargos = otros_cargos;
    }

    public String getTipoComprobante() {
        return tipoComprobante;
    }

    public void setTipoComprobante(String tipo_comprobante) {
        this.tipoComprobante = tipo_comprobante;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTotalDescuentos() {
        return totalDescuentos;
    }

    public void setTotalDescuentos(String totalDescuentos) {
        this.totalDescuentos = totalDescuentos;
    }

    public String getAuxNombreCliente() {
        return auxNombreCliente;
    }

    public void setAuxNombreCliente(String auxNombreCliente) {
        this.auxNombreCliente = auxNombreCliente;
    }

    public String getAuxNumDoc() {
        return auxNumDoc;
    }

    public void setAuxNumDoc(String auxNumDoc) {
        this.auxNumDoc = auxNumDoc;
    }

    public String getAuxEstado() {
        return auxEstado;
    }

    public void setAuxEstado(String auxEstado) {
        this.auxEstado = auxEstado;
    }

    public String getSituacionSunat() {
        return situacionSunat;
    }

    public void setSituacionSunat(String situacionSunat) {
        this.situacionSunat = situacionSunat;
    }
}