package com.edmi.controller;

import com.edmi.view.MainPanelServices;
import com.edmi.view.ModalNewClient;
import com.edmi.view.ModalNewEmployee;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControllerMainPanel implements ActionListener {
    private final MainPanelServices mainFrame;
    private ControllerEmployee controllerEmploye;
    
    public ControllerMainPanel(MainPanelServices mainPanel){
        this.mainFrame = mainPanel;
        controllerEmploye = new ControllerEmployee(null, mainPanel);
        setListeners();
    }
    
    private void setListeners() {
        mainFrame.btnNuevoCliente.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == mainFrame.btnNuevoTrabajador){
            ModalNewEmployee form = new ModalNewEmployee(mainFrame,true);
            ControllerEmployee controllerEmployee = new ControllerEmployee(form, mainFrame);
            form.btnModificar.setEnabled(false);
            controllerEmployee.loadData();
            form.setVisible(true);
        }
        
        if (e.getSource() == mainFrame.btnNuevoCliente) {
            ModalNewClient modal = new ModalNewClient(mainFrame,true);
            ControllerCliente control = new ControllerCliente(mainFrame, modal);
            modal.btnModificarCliente.setEnabled(false);
            modal.setVisible(true);
        }
    }
}
