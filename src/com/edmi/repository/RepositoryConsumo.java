package com.edmi.repository;

import java.util.List;

public interface RepositoryConsumo<T> {
    List<T> findAll();
    List<T> findAllWhere(Object condition, Object value);
    List<T> findByReservation(Object value);
    T findById(Object id);
    int create(T consumo);
    int update(T consumo);
    int delete(Object id);    
}
