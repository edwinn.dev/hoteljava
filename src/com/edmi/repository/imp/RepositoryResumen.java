package com.edmi.repository.imp;

import static com.edmi.database.ConnectionDb.getInstance;
import com.edmi.database.query.Query;
import com.edmi.model.invoice.ResumenDiario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RepositoryResumen {
    
    public ResumenDiario findById(String numberDoc){
        ResumenDiario resumen = null;
        try (PreparedStatement st = getInstance().prepareStatement(Query.SQL_RESU_FIND_BY_ID)){
            st.setString(1, numberDoc);
            try (ResultSet rs = st.executeQuery()) {
                if(rs.next()){
                    resumen = new ResumenDiario();
                    resumen.setCodResumen(rs.getString("cod_resumen"));
                    resumen.setFecEmision(rs.getString("fecEmision"));
                    resumen.setFecResumen(rs.getString("fecResumen"));
                    resumen.setTipDocResumen(rs.getString("tipDocResumen"));
                    resumen.setIdDocResumen(rs.getString("idDocResumen"));
                    resumen.setSituacionSunat(rs.getString("situacion_sunat"));
                }
            }
        } catch (SQLException ex) {
            resumen = null;
            Logger.getLogger(RepositoryResumen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resumen;
    }
    
    public List<ResumenDiario> findAll(){
        List<ResumenDiario> lista = new ArrayList();
        try (PreparedStatement st = getInstance().prepareStatement(Query.SQL_RESU_FIND_ALL)){
            try (ResultSet rs = st.executeQuery()) {
                while(rs.next()){
                    ResumenDiario resumen = new ResumenDiario();
                    resumen.setCodResumen(rs.getString("cod_resumen"));
                    resumen.setFecEmision(rs.getString("fecEmision"));
                    resumen.setFecResumen(rs.getString("fecResumen"));
                    resumen.setTipDocResumen(rs.getString("tipDocResumen"));
                    resumen.setIdDocResumen(rs.getString("idDocResumen"));
                    resumen.setSituacionSunat(rs.getString("situacion_sunat"));
                    lista.add(resumen);
                }
            }
        } catch (SQLException ex) {
            lista = null;
            Logger.getLogger(RepositoryResumen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    public int guardarResumenDiario(ResumenDiario resumen){
        int response;
        try (PreparedStatement ps = getInstance().prepareStatement(Query.SQL_RESU_SAVE)){
            ps.setString(1, resumen.getCodResumen());
            ps.setString(2, resumen.getFecEmision());
            ps.setString(3, resumen.getFecResumen());
            ps.setString(4, resumen.getTipDocResumen());
            ps.setString(5, resumen.getIdDocResumen());
            ps.setString(6, "-");
            response = ps.executeUpdate();
        } catch (SQLException ex) {
            response = -1;
            Logger.getLogger(RepositoryResumen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return response;
    }
}
