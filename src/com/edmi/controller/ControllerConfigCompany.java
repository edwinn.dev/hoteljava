package com.edmi.controller;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.model.Company;
import com.edmi.repository.imp.CompanyRepositoryImp;
import com.edmi.util.CapturaException;
import com.edmi.view.ConfigCompany;
import java.awt.Component;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.apache.log4j.Logger;

public class ControllerConfigCompany implements ActionListener {
    private final ConfigCompany config;
    private JFileChooser fc;
    private FileNameExtensionFilter filtro;
    private CompanyRepositoryImp companyRepository;
    private static final Logger LOGGER = Logger.getLogger(ControllerConfigCompany.class);

    public ControllerConfigCompany(ConfigCompany config) {
        this.config = config;
        setListener();
    }
    
    public void iniciarVista(){
        config.setLocationRelativeTo(null);
        config.setVisible(true);
    }
    
    public static String getRutaIconSistema() {
        File ruta = new File("design" + File.separator + "icono del sistema");
        String[] img = ruta.list();
        String icon = ruta.getAbsolutePath() + File.separator + img[0];
        return icon;
    }

    private void modificarCompany() {
        companyRepository = new CompanyRepositoryImp();

        if (isFieldsInvalid()) {
            JOptionPane.showMessageDialog(config, "Debe de llenar todos los datos.", "Mensaje del Sistema", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        Company c = new Company();
        c.setIdCompanny("0");
        c.setNombre(config.txtNombreEmpresa.getText());
        c.setRuc(config.txtRUC.getText());
        c.setDireccion(config.txtDireccion.getText());
        c.setCiudad(config.txtCiudad.getText());
        c.setPais(config.txtPais.getText());
        int op = companyRepository.update(c);

        if (op == 0) {
            JOptionPane.showMessageDialog(config, "No se puedo registrar los datos, contacte con el administrador.", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }

        JOptionPane.showMessageDialog(config, "Modificación exitosa.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
        clearTextField(config.panelDatosCompany);
    }

    private void mostrarDatosCompany() {
        clearTextField(config.panelDatosCompany);
        companyRepository = new CompanyRepositoryImp();
        List<Company> lista = companyRepository.findAll();
        for (Company c : lista) {
            config.txtNombreEmpresa.setText(c.getNombre());
            config.txtRUC.setText(c.getRuc());
            config.txtDireccion.setText(c.getDireccion());
            config.txtCiudad.setText(c.getCiudad());
            config.txtPais.setText(c.getPais());
        }
    }

    private void clearTextField(JPanel panel) {
        Component[] componentes = panel.getComponents();
        for (Component c : componentes) {
            if (c instanceof JTextField) {
                ((JTextField) c).setText("");
            }
        }
    }

    private boolean isFieldsInvalid() {
        boolean invalid = config.txtNombreEmpresa.getText().isEmpty() || config.txtRUC.getText().isEmpty() || config.txtDireccion.getText().isEmpty() || config.txtCiudad.getText().isEmpty()
                || config.txtPais.getText().isEmpty();
        return invalid;
    }

    private void agregarImagen(int op) {
        fc = new JFileChooser();
        filtro = new FileNameExtensionFilter("JPG y PNG y JPEG", "jpg", "JPG", "png", "PNG", "jpeg", "JPEG");
        fc.setFileFilter(filtro);
        int seleccion = fc.showOpenDialog(config);
        File ruta = fc.getSelectedFile();
        if (seleccion == JFileChooser.APPROVE_OPTION) {
            switch (op) {
                case 1:
                    File rutaSplash = fc.getSelectedFile();
                    config.txtLogoSplash.setText(rutaSplash.getAbsolutePath());
                    break;
                case 2:
                    File rutaFondoIncio = fc.getSelectedFile();
                    config.txtFondoLogin.setText(rutaFondoIncio.getAbsolutePath());
                    break;
                case 3:
                    File rutaLogoInicio = fc.getSelectedFile();
                    config.txtLogoInicio.setText(rutaLogoInicio.getAbsolutePath());
                    break;
                case 4:
                    File rutaIconSistema = fc.getSelectedFile();
                    config.txtIconSistema.setText(rutaIconSistema.getAbsolutePath());
                    break;
            }

            ImageIcon icon = new ImageIcon(ruta.toString());
            Icon mostrar = new ImageIcon(icon.getImage().getScaledInstance(config.lblVistaPrevia.getWidth(), config.lblVistaPrevia.getHeight(), Image.SCALE_DEFAULT));
            config.lblVistaPrevia.setIcon(mostrar);
        }
    }

    private void validacionImagenes() {
        int cont = 0;

        if (!config.txtLogoSplash.getText().isEmpty()) {
            File origen = new File(config.txtLogoSplash.getText());
            File destino = new File("design" + File.separator + "logo splash" + File.separator + origen.getName());
            File imagen = new File("design" + File.separator + "logo splash");
            nuevasImagenes(origen, destino, imagen);
            config.txtLogoSplash.setText("");
            cont++;
        }

        if (!config.txtFondoLogin.getText().isEmpty()) {
            File origen = new File(config.txtFondoLogin.getText());
            File destino = new File("design" + File.separator + "imagen login" + File.separator + origen.getName());
            File imagen = new File("design" + File.separator + "imagen login");
            nuevasImagenes(origen, destino, imagen);
            config.txtFondoLogin.setText("");
            cont++;
        }

        if (!config.txtLogoInicio.getText().isEmpty()) {
            File origen = new File(config.txtLogoInicio.getText());
            File destino = new File("design" + File.separator + "logo inicio" + File.separator + origen.getName());
            File imagen = new File("design" + File.separator + "logo inicio");
            nuevasImagenes(origen, destino, imagen);
            config.txtLogoInicio.setText("");
            cont++;
        }
        
        if (!config.txtIconSistema.getText().isEmpty()) {
            File origen = new File(config.txtIconSistema.getText());
            File destino = new File("design" + File.separator + "icono del sistema" + File.separator + origen.getName());
            File imagen = new File("design" + File.separator + "icono del sistema");
            nuevasImagenes(origen, destino, imagen);
            config.txtIconSistema.setText("");
            cont++;
        }

        if (cont == 0) {
            JOptionPane.showMessageDialog(config, "Debe agregar una ruta de imagen.", SYS_MSG, JOptionPane.WARNING_MESSAGE);
        } else {
            int op = JOptionPane.showConfirmDialog(config, "¿Está seguro que desea agregar una nueva imagen?", SYS_MSG, JOptionPane.YES_NO_OPTION);
            if (op == JOptionPane.YES_OPTION) {
                JOptionPane.showMessageDialog(config, "Imagen agregada, los cambios se mostrarán cuando cierre sesión o reinicie el sistema.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    private void nuevasImagenes(File origen, File destino, File limpiar) {
        String[] archivos = limpiar.list();
        for (String a : archivos) {
            File documento = new File(limpiar.getAbsolutePath(), a);
            documento.delete();
        }

        try {
            OutputStream out;
            try (InputStream in = new FileInputStream(origen)) {
                out = new FileOutputStream(destino);
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            }
            out.close();
        } catch (IOException e) {
            LOGGER.error("Error: \n"+CapturaException.exceptionStacktraceToString(e));
        }
    }

    private void setListener() {
        config.guardarCambios.addActionListener(this);
        config.btnAgregarImagenes.addActionListener(this);
        config.btnLogoSplash.addActionListener(this);
        config.btnFondoLogin.addActionListener(this);
        config.btnLogoInicio.addActionListener(this);
        config.btnBuscar.addActionListener(this);
        config.btnIconSistema.addActionListener(this);
        config.btnLimpiar.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == config.guardarCambios) {
            modificarCompany();
        }

        if (e.getSource() == config.btnBuscar) {
            mostrarDatosCompany();
        }

        if (e.getSource() == config.btnLimpiar) {
            clearTextField(config.panelDatosCompany);
            clearTextField(config.panelImagenesLogos);
        }

        if (e.getSource() == config.btnLogoSplash) {
            agregarImagen(1);
        }

        if (e.getSource() == config.btnFondoLogin) {
            agregarImagen(2);
        }

        if (e.getSource() == config.btnLogoInicio) {
            agregarImagen(3);
        }
        
        if (e.getSource() == config.btnIconSistema) {
            agregarImagen(4);
        }

        if (e.getSource() == config.btnAgregarImagenes) {
            validacionImagenes();
        }
    }
}
