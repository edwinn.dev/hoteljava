package com.edmi.model;

public class Trabajador {
    private String codTrabajador;
    private String codTipoDocumento;
    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String tipoDococumento;
    private String nroDocumento;
    private String nroTelefono;
    private String email;
    private String obervacion;
    private String username;
    private String password;
    private String cargo;
    private String actividad;
    private String estado;
    
    public Trabajador(){}

    public Trabajador(String codTrabajador, String codTipoDocumento, String nombre, String apellidoPaterno, String apellidoMaterno, String tipoDococumento, String nroDocumento, String nroTelefono, String email, String obervacion, String username, String password, String cargo, String actividad, String estado) {
        this.codTrabajador = codTrabajador;
        this.codTipoDocumento = codTipoDocumento;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
        this.tipoDococumento = tipoDococumento;
        this.nroDocumento = nroDocumento;
        this.nroTelefono = nroTelefono;
        this.email = email;
        this.obervacion = obervacion;
        this.username = username;
        this.password = password;
        this.cargo = cargo;
        this.actividad = actividad;
        this.estado = estado;
    }

    public String getCodTrabajador() {
        return codTrabajador;
    }

    public void setCodTrabajador(String codTrabajador) {
        this.codTrabajador = codTrabajador;
    }

    public String getCodTipoDocumento() {
        return codTipoDocumento;
    }

    public void setCodTipoDocumento(String codTipoDocumento) {
        this.codTipoDocumento = codTipoDocumento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre.toUpperCase();
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno.toUpperCase();
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno.toUpperCase();
    }

    public String getTipoDococumento() {
        return tipoDococumento;
    }

    public void setTipoDococumento(String tipoDococumento) {
        this.tipoDococumento = tipoDococumento.toUpperCase();
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getNroTelefono() {
        return nroTelefono;
    }

    public void setNroTelefono(String nroTelefono) {
        this.nroTelefono = nroTelefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObervacion() {
        return obervacion;
    }

    public void setObervacion(String obervacion) {
        this.obervacion = obervacion.toUpperCase();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo.toUpperCase();
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
