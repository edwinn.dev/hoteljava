package com.edmi.repository;

import java.util.List;

public interface RepositoryCategoryProduct<T> {
    List<T> findAll();
    List<T> findAllWhere(String condition, String value);
    T findById(Object id);
    int create(T room);
    int update(T room);
    int delete(Object id);    
}
