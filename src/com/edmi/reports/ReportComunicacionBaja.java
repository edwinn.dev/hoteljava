package com.edmi.reports;

import static com.edmi.util.MessageSystem.SYS_MSG;
import com.edmi.controller.invoice.JsonFiles;
import com.edmi.controller.panel.ControllerFacura;
import com.edmi.model.Company;
import com.edmi.model.invoice.ComunicacionBaja;
import com.edmi.repository.imp.CabeceraRepositoryImp;
import com.edmi.repository.imp.CompanyRepositoryImp;
import com.edmi.repository.imp.RepositoryComunicacionBaja;
import com.edmi.util.Constant;
import com.edmi.util.NombresArchivosJSON;
import com.edmi.util.Util;
import com.edmi.view.DialogBaja;
import com.edmi.view.MainPanelServices;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class ReportComunicacionBaja implements ActionListener {
    private DialogBaja dialogBaja = null;

    public ReportComunicacionBaja(DialogBaja dialogBaja) {
        this.dialogBaja = dialogBaja;
        this.setListener();
    }

    private void setListener() {
        this.dialogBaja.btnProcesar.addActionListener(this);
    }

    public void initView(MainPanelServices frame) {
        this.dialogBaja.setLocationRelativeTo(frame);
        this.dialogBaja.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.dialogBaja.btnProcesar) {
            this.generarComunicacionBaja();
        }
    }

    private void generarComunicacionBaja() {
        String nroDocComunicacion = dialogBaja.txtNroDoc.getText().trim();
        RepositoryComunicacionBaja repoCom = new RepositoryComunicacionBaja();
        
        ComunicacionBaja comDatabase = repoCom.findById(nroDocComunicacion);
        if(comDatabase != null){
            JOptionPane.showMessageDialog(dialogBaja, Constant.MSG_ENVIADO_ACEPTADO, SYS_MSG, JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        if (dialogBaja.txtMotivobaja.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(dialogBaja, "Ingrese un motivo para generar\nesta comunicacion de baja.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        
        int option = JOptionPane.showConfirmDialog(dialogBaja, "¿Desea anular este documento?", SYS_MSG, JOptionPane.YES_NO_OPTION);
        if (option == JOptionPane.YES_OPTION) {
            String fechaBaja = Util.getDateFormat(dialogBaja.fechaGeneracionBaja.getDate());
            List<ComunicacionBaja> coms = new ArrayList<>();
            ComunicacionBaja com = new ComunicacionBaja();
            com.setNumDocBaja(nroDocComunicacion);
            com.setTipDocBaja(dialogBaja.txtTipoDocumento.getText().trim());
            com.setFecGeneracion(dialogBaja.txtFechaDocumento.getText().trim());
            com.setDesMotivoBaja(dialogBaja.txtMotivobaja.getText().trim().toUpperCase());
            com.setFecComunicacion(fechaBaja);
            coms.add(com);

            JsonFiles jsonFiles = new JsonFiles();

            ResumenBaja resumenBajas = new ResumenBaja();
            resumenBajas.setResumenBajas(coms);

            CompanyRepositoryImp repositoryCompany = new CompanyRepositoryImp();
            Company company = repositoryCompany.getCompany();

            String correlativo = NombresArchivosJSON.generarNombreComunicacionBaja();
            String nroBaja = company.getRuc() + "-RA-" + fechaBaja.replace("-", "") + "-" + correlativo;
            boolean response = jsonFiles.createFileJson(resumenBajas, nroBaja);
            if (response) {
                JOptionPane.showMessageDialog(dialogBaja, "Comunicacion de baja generada.", SYS_MSG, JOptionPane.INFORMATION_MESSAGE);
                //Guardar en la base de datos
                int responseUpdate = CabeceraRepositoryImp.actualizarEstadoComprobante(dialogBaja.txtIdCabecera.getText().trim(), "COMUNICACION DE BAJA GENERADO");
                if(responseUpdate <= 0){
                    JOptionPane.showMessageDialog(dialogBaja, "Error al catualizar cabecera", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                }
                
                com.setCodBaja("RA-" + fechaBaja.replace("-", "") + "-" + correlativo);
                int responseDatabase = repoCom.saveComunication(com);
                if(responseDatabase <= 0){
                    JOptionPane.showMessageDialog(dialogBaja, "Error al registrar en la base de datos", SYS_MSG, JOptionPane.ERROR_MESSAGE);
                }
                this.dialogBaja.dispose();
                ControllerFacura.loadTableVouchers();
                ControllerFacura.loadTableComunicacionBaja();
            } else {
                JOptionPane.showMessageDialog(dialogBaja, "Error al generar comunicacion de baja.\n"
                        + "contacte con el administrador", SYS_MSG, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private class ResumenBaja {
        private List<ComunicacionBaja> resumenBajas;

        private ResumenBaja() {
        }

        public List<ComunicacionBaja> getResumenBajas() {
            return resumenBajas;
        }

        public void setResumenBajas(List<ComunicacionBaja> resumenBajas) {
            this.resumenBajas = resumenBajas;
        }
    }
}
