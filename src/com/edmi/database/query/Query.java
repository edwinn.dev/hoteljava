package com.edmi.database.query;

public interface Query {
    /******************* Productos ********************/
    String SQL_PROD_CREATE = "INSERT INTO producto (cod_producto, cod_categoria, cod_unidadmedida, nombre_producto, cantidad_stock, precio_compra, precio_venta, fecha_registro, fecha_vencimiento, cantidad_salida, estado, descripcion, cod_sunat, tipo_igv, mto_igv, icbper, mto_icbper, marca) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    String SQL_PROD_FIND_BY_ID = "SELECT p.*, cat.nombre_categoria, um.descripcion as unidad_medida "
            .concat("FROM producto p INNER JOIN categoria_producto cat ON p.cod_categoria = cat.cod_categoria ")
            .concat("INNER JOIN unidad_medida um on p.cod_unidadmedida = um.cod_unidad WHERE p.cod_producto = ?");
    String SQL_PROD_FIND_ALL = "SELECT p.*, cat.nombre_categoria, um.descripcion as unidad_medida "
            .concat("FROM producto p INNER JOIN categoria_producto cat ON p.cod_categoria = cat.cod_categoria ")
            .concat("INNER JOIN unidad_medida um on p.cod_unidadmedida = um.cod_unidad WHERE p.estado != '-1' ORDER BY p.cod_producto");
    String SQL_PROD_FIND_ALL_CODE = "SELECT p.*, cat.nombre_categoria, um.descripcion as unidad_medida "
            .concat("FROM producto p INNER JOIN categoria_producto cat ON p.cod_categoria = cat.cod_categoria ")
            .concat("INNER JOIN unidad_medida um on p.cod_unidadmedida = um.cod_unidad ORDER BY p.cod_producto");
    String SQL_PROD_FIND_ALL_WHERE = "SELECT p.*, cat.nombre_categoria, um.descripcion as unidad_medida "
            .concat("FROM producto p INNER JOIN categoria_producto cat ON p.cod_categoria = cat.cod_categoria ")
            .concat("INNER JOIN unidad_medida um on p.cod_unidadmedida = um.cod_unidad WHERE p.nombre_producto LIKE CONCAT( ?,'%') AND p.estado != '-1'  ORDER BY p.cod_producto");
    String SQL_PROD_DELETE = "UPDATE producto SET estado = ? WHERE cod_producto = ?"; 
    String SQL_PROD_UPDATE = "UPDATE producto SET cod_categoria=?, cod_unidadmedida=?, nombre_producto=?, cantidad_stock=?, precio_compra=?, precio_venta=?, fecha_registro=?, fecha_vencimiento=?, estado=?, descripcion=?, cod_sunat=?, tipo_igv=?, mto_igv=?, icbper=?, mto_icbper=?, marca=? WHERE cod_producto=?"; 
    String SQL_PROD_UPDATE_STOCK = "UPDATE producto SET cantidad_stock = ? WHERE cod_producto = ?"; 
    String SQL_PROD_CATEGORIAS = "SELECT * FROM categoria_producto";
            
    /************** Categoria de Productos ************/
    String SQL_CAT_CREATE = "INSERT INTO categoria_producto (cod_categoria, nombre_categoria, descripcion) VALUES (?, ?, ?)";
    String SQL_CAT_FIND_BY_ID = "SELECT * FROM categoria_producto WHERE cod_categoria = ?";
    String SQL_CAT_FIND_ALL = "SELECT * FROM categoria_producto ORDER BY cod_categoria ASC";
            
    /**************** Habitaciones ********************/
    String SQL_HAB_CREATE = "INSERT INTO habitacion (cod_habitacion, cod_tipohabitacion, nro_habitacion, tipo_habitacion, estado, nro_planta, costo_alquiler, caracteristicas, state) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
    String SQL_HAB_UPDATE = "UPDATE habitacion SET nro_habitacion=?, tipo_habitacion=?, estado=?, nro_planta=?, costo_alquiler=?, caracteristicas=? WHERE cod_habitacion = ?";
    String SQL_HAB_UPDATE_STATE = "UPDATE habitacion SET estado = ? WHERE nro_habitacion = ?";
    String SQL_HAB_FIND_BY_ID = "SELECT * FROM habitacion WHERE cod_habitacion = ?";
    String SQL_HAB_FIND_ALL = "SELECT * FROM habitacion AS room ORDER BY room.nro_habitacion ASC";
    String SQL_HAB_FIND_DESC = "SELECT * FROM habitacion AS room ORDER BY room.cod_habitacion ASC";
    String SQL_HAB_FIND_ALL_STATE = "SELECT * FROM habitacion AS room WHERE room.state = ? ORDER BY room.nro_habitacion ASC";
    String SQL_HAB_FIND_ALL_STATE_TWO = "SELECT * FROM habitacion as room WHERE room.estado = ? AND room.state = ? ORDER BY room.nro_habitacion ASC";
    String SQL_HAB_FIND_ALL_AVAILABLE = "SELECT * FROM habitacion AS room WHERE room.estado = 'Disponible' AND room.state = 1 ORDER BY room.nro_habitacion ASC";
    String SQL_HAB_STATE = "UPDATE habitacion SET state=? WHERE cod_habitacion=?";
    String SQL_HAB_DELETE = "DELETE FROM habitacion WHERE cod_habitacion=?";
    String SQL_HAB_TIPEHAB = "SELECT * FROM tipo_habitacion WHERE tipo_habitacion=?";
    
    /*************** Tipo de habitacion ***************/
    String SQL_TYPE_ROOM_CREATE = "INSERT INTO tipo_habitacion (cod_tipohabitacion, capacidad, tipo_habitacion, descripcion) VALUES(?, ?, ?, ?)";
    String SQL_TYPE_ROOM_FIND_ALL = "SELECT * FROM tipo_habitacion";
    
    /******************* Clientes *********************/
    String SQL_CLI_CREATE = "INSERT INTO cliente (cod_cliente, cod_tipodocumento, nombre, apellido_paterno, apellido_materno, tipo_documento, nro_documento, nro_telefono, email, observacion, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    String SQL_CLI_FIND_ALL_ESTADO = "SELECT * FROM cliente WHERE estado = ?";
    String SQL_CLI_FIND_ALL = "SELECT * FROM cliente";
    String SQL_CLI_FIND_BY_ID = "SELECT * FROM cliente WHERE cod_cliente=?";
    String SQL_CLI_UPDATE = "UPDATE cliente SET cod_tipodocumento=?, nombre=?, apellido_paterno=?, apellido_materno=?, tipo_documento=?, nro_documento=?, nro_telefono=?, email=?, observacion=? WHERE cod_cliente=?";
    String SQL_CLI_STATE = "UPDATE cliente SET estado=? WHERE cod_cliente=?";
    String SQL_CLI_TIPODOCU = "SELECT * FROM tipo_documento WHERE descripcion=?";
    String SQL_CLI_FIND_AL_DOCU = "SELECT * FROM tipo_documento";
    String SQL_CLI_FIND_AL_NRODOCU = "SELECT nro_documento FROM cliente WHERE estado = '1'";
    String SQL_CLI_NRODOCU = "SELECT * FROM cliente WHERE nro_documento = ?";

    /****************** Trabajador ********************/
    String SQL_TBJ_CREATE = "INSERT INTO trabajador (cod_trabajador, cod_tipodocumento, nombre, apellido_paterno, apellido_materno, tipo_documento, nro_documento, nro_telefono, email, observacion, cargo, username, password, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    String SQL_TBJ_FIND_ALL = "SELECT * FROM trabajador";
    String SQL_TBJ_FIND_ALL_STATE = "SELECT * FROM trabajador WHERE estado = ?";
    String SQL_TBJ_FIND_LOGIN = "SELECT * FROM trabajador WHERE username = ? AND password = ?"; 
    String SQL_TBJ_UPDATE = "UPDATE trabajador SET nombre=?, apellido_paterno=?, apellido_materno=?, tipo_documento=?, nro_documento=?, nro_telefono=?, email=?, observacion=?, cargo=?, username=?, password=? WHERE cod_trabajador=?";
    String SQL_TBJ_FIND_BY_ID = "SELECT * FROM trabajador WHERE cod_trabajador=?";
    String SQL_TBJ_FIND_USER_SYSTEM = "SELECT * FROM trabajador WHERE estado = ?";
    String SQL_TBJ_DELETE = "DELETE FROM trabajador WHERE cod_trabajador=?";
    String SQL_TBJ_FIND_AL_DOCU = "SELECT * FROM tipo_documento";
    String SQL_TBJ_TIPODOCU = "SELECT * FROM tipo_documento WHERE descripcion=?";
    String SQL_TBJ_STATE = "UPDATE trabajador SET estado=? WHERE cod_trabajador=?";
    String SQL_TBJ_FIND_AL_NRODOCU = "SELECT nro_documento FROM trabajador WHERE estado = '1'";
    
    /***************** Proveedores *******************/
    String SQL_PROV_CREATE = "INSERT INTO proveedor (cod_proveedor, cod_tipodocumento, tipo_documento, nro_documento, razon_social, provincia, direccion, telefono_uno, telefono_dos, email, observacion, estado) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    String SQL_PROV_FIND_ALL_STATE = "SELECT * FROM proveedor WHERE estado = ?";
    String SQL_PROV_FIND_ALL = "SELECT * FROM proveedor";
    String SQL_PROV_TIPODOCU = "SELECT * FROM tipo_documento WHERE descripcion=?";
    String SQL_PROV_FIND_BY_ID = "SELECT * FROM proveedor WHERE cod_proveedor=?";
    String SQL_PROV_UPDATE = "UPDATE proveedor SET cod_tipodocumento=?, tipo_documento=?, nro_documento=?, razon_social=?, provincia=?, direccion=?, telefono_uno=?, telefono_dos=?, email=?, observacion=? WHERE cod_proveedor=?";
    String SQL_PROV_STATE = "UPDATE proveedor SET estado=? WHERE cod_proveedor=?";
    String SQL_PROV_FIND_AL_NRODOCU = "SELECT nro_documento FROM proveedor WHERE estado = '1'";
    
    /**************** Reservaciones *****************/
    String SQL_RESERVA_CREATE = "INSERT INTO reserva (cod_trabajador, cod_cliente, cod_habitacion, fecha_ingreso, fecha_salida, importe_reserva, importe_productos, importe_total, tiempo_reserva, magnitud_reserva, estado, observacion, nro_habitacion) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    String SQL_RESERVA_UPDATE = "UPDATE reserva SET cod_habitacion=?, fecha_ingreso=?, fecha_salida=?, importe_reserva=?, importe_productos=?, importe_total=?, tiempo_reserva=?, magnitud_reserva=?, estado=?, observacion=?, nro_habitacion=? WHERE cod_reserva = ?";
    String SQL_RESERVA_UPDATE_FOR_CONSUMO = "UPDATE reserva SET importe_productos = ?, importe_total = ? WHERE cod_reserva = ?";
    String SQL_RESERVA_FIND_ALL = "SELECT  r.cod_reserva, r.cod_trabajador, r.cod_cliente, r.cod_habitacion, r.nro_habitacion, r.fecha_ingreso, r.fecha_salida, r.importe_reserva, "
            .concat("r.importe_productos, r.importe_total, r.tiempo_reserva, r.magnitud_reserva, r.estado, r.observacion, ")
            .concat("concat(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) as nombre_cliente, c.tipo_documento, c.cod_tipodocumento, c.nro_documento, concat(t.nombre, ' ', t.apellido_paterno, ' ', t.apellido_materno) as nombre_trabajador ")
            .concat("from reserva r inner join cliente c on r.cod_cliente = c.cod_cliente inner join trabajador t on r.cod_trabajador = t.cod_trabajador WHERE r.estado = '1' ORDER BY r.cod_reserva DESC");
    String SQL_RESERVA_CANCEL = "UPDATE reserva SET estado = ? WHERE cod_reserva = ?";
    String SQL_RESERVA_FIND_FOR_ADD_PRODUCTS = "SELECT r.cod_reserva, r.cod_trabajador, r.cod_cliente, r.cod_habitacion, r.nro_habitacion, r.fecha_ingreso, r.fecha_salida, r.importe_reserva, "
            .concat("r.importe_productos, r.importe_total, r.tiempo_reserva, r.magnitud_reserva, r.estado, r.observacion, ")
            .concat("concat(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) as nombre_cliente, c.tipo_documento, c.cod_tipodocumento, c.nro_documento, concat(t.nombre, ' ', t.apellido_paterno, ' ', t.apellido_materno) as nombre_trabajador ")
            .concat("from reserva r inner join cliente c on r.cod_cliente = c.cod_cliente inner join trabajador t on r.cod_trabajador = t.cod_trabajador WHERE cod_habitacion = ? AND r.estado = '1' order by fecha_ingreso desc LIMIT 1");
    String SQL_RESERVA_FIND_BY_ID = "SELECT  r.cod_reserva, r.cod_trabajador, r.cod_cliente, r.cod_habitacion, r.nro_habitacion, r.fecha_ingreso, r.fecha_salida, r.importe_reserva, "
            .concat("r.importe_productos, r.importe_total, r.tiempo_reserva, r.magnitud_reserva, r.estado, r.observacion, ")
            .concat("concat(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) as nombre_cliente, c.tipo_documento, c.cod_tipodocumento, c.nro_documento, concat(t.nombre, ' ', t.apellido_paterno, ' ', t.apellido_materno) as nombre_trabajador, h.nro_habitacion as nroHabitacion ")
            .concat("from reserva r inner join cliente c on r.cod_cliente = c.cod_cliente inner join trabajador t on r.cod_trabajador = t.cod_trabajador inner join habitacion h on r.cod_habitacion = h.cod_habitacion WHERE cod_reserva = ? AND r.estado = '1' ORDER BY r.cod_reserva DESC");
    String SQL_RESERVA_STATE = "UPDATE reserva SET estado=? WHERE cod_reserva=?";
    
    /**************** Consumo *****************/
    String SQL_CONSUMO_CREATE = "INSERT INTO consumo (cod_reserva, cod_producto, unidad_medida, cantidad, fecha_venta, gratuito, precio_unidad, total, estado, icbper, igv, isc, descuento) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    String SQL_CONSUMO_DELETE = "DELETE FROM consumo WHERE cod_consumo = ?";
    String SQL_CONSUMO_FIND_BY_RESERVA = "SELECT c.cod_consumo, c.cod_reserva, c.cod_producto, c.unidad_medida, c.cantidad, c.fecha_venta, c.precio_unidad, c.total, c.gratuito, c.estado, c.icbper, c.igv, c.isc, c.descuento, "
            .concat("p.nombre_producto FROM consumo c INNER JOIN producto p ON c.cod_producto = p.cod_producto WHERE c.cod_reserva=?");
    String SQL_CONSUMO_FIND = "SELECT c.cod_consumo, c.cod_reserva, c.cod_producto, c.unidad_medida, c.cantidad, c.fecha_venta, c.precio_unidad, c.total, c.gratuito, c.estado, c.icbper, c.igv, c.isc, c.descuento, "
            .concat("p.nombre_producto FROM consumo c INNER JOIN producto p ON c.cod_producto = p.cod_producto");
    String SQL_CONSUMO_FIND_DATE = "SELECT c.cod_consumo, c.cod_reserva, c.cod_producto, c.unidad_medida, c.cantidad, c.fecha_venta, c.precio_unidad, c.total, c.gratuito, c.estado, c.icbper, c.igv, c.isc, c.descuento, "
            .concat("p.nombre_producto FROM consumo c INNER JOIN producto p ON c.cod_producto = p.cod_producto WHERE c.fecha_venta BETWEEN ? AND ?");
    String SQL_CONSUMO_FIND_FILTRO = "SELECT c.cod_consumo, c.cod_reserva, c.cod_producto, c.unidad_medida, c.cantidad, c.fecha_venta, c.precio_unidad, c.total, c.gratuito, c.estado, c.icbper, c.igv, c.isc, c.descuento, "
            .concat("p.nombre_producto FROM consumo c INNER JOIN producto p ON c.cod_producto = p.cod_producto WHERE gratuito = ?");
    
    /**************** Comprobantes *****************/
    String SQL_VOUCHER_CREATE = "INSERT INTO comprobante (cod_consumo, tipo_comprobante, tipo_documento, nro_documento, importe_reserva, importe_productos, importe_total, igv, estado, fecha_emision, fecha_pago, monto_pagado, monto_restante) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    /**************** Catalogo Unnida de Medida *****************/
    String SQL_UNIT_FIND_ALL = "SELECT * FROM unidad_medida";
    
    /**************** Theme *****************/
    String SQL_THEME_FIND_ACTIVE = "SELECT * FROM tehme WHERE state = ?";
    String SQL_THEME_CHANGED = "UPDATE theme SET state = ? WHERE id = ?";
    
    /**************** Company *****************/
    String SQL_COMPANY_FIND_ALL = "SELECT * FROM company";
    String SQL_COMPANY_FIND = "SELECT * FROM company WHERE company_id = '0'";
    String SQL_COMPANY_CREATE = "INSERT INTO company (company_id,company_name,company_ruc,company_address,company_city,company_country) VALUES (?,?,?,?,?,?)";
    String SQL_COMPANY_UPDATE = "UPDATE company SET company_id = ?, company_name = ?, company_ruc =?, company_address = ?, company_city = ?, company_country = ?";
    String SQL_COMPANY_FIND_NAME = "SELECT company_name FROM company";
    
    /**************** Cabecera Facbo *****************/
    String SQL_CAB_FACBO_FIND_ALL = "SELECT h.*, concat(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) as cliente, c.nro_documento FROM cabec_facbo h inner join cliente c on h.cod_cliente = c.cod_cliente ORDER BY h.id_cabecera DESC";
    String SQL_CAB_FACBO_FIND_ALL_SN = "SELECT h.*, concat(c.nombre, ' ', c.apellido_paterno, ' ', c.apellido_materno) as cliente, c.nro_documento FROM cabec_facbo h inner join cliente c on h.cod_cliente = c.cod_cliente WHERE serie = ? AND numero = ? ORDER BY h.id_cabecera DESC";
    String SQL_CAB_FACBO_CREATE = "INSERT INTO cabec_facbo (cod_cliente,fecha_emision,hora_emision,fecha_vencimiento,tipo_moneda,total_tributos,total_descuento,importe_venta,total_venta,precio_venta,otros_cargos,tipo_comprobante,serie,numero,estado,situacion_sunat) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    /**************** Detalle Facbo *****************/
    String SQL_DETALLE_FACBO_FIND_ALL = "SELECT * FROM detalle_facbo";
    String SQL_DETALLE_FACBO_FIND_ALL_ID_CAB = "SELECT * FROM detalle_facbo WHERE id_cabecera = ?";
    String SQL_DETALLE_FACBO_CREATE = "INSERT INTO detalle_facbo (id_cabecera, cod_producto,cantidad,valor_unitario,total_tributo_item,cod_afectacion,tipo_afectacion,porcentaje_afectacion,descuento,otros_cargos,precio_venta_item,total_importe_item) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

    /**************** NotaCD *****************/
    String SQL_NOTACD_FIND_ALL = "SELECT * FROM nota_cd ORDER BY id_nota DESC";
    String SQL_NOTACD_FIND_ALL_CODCABEC = "SELECT * FROM nota_cd WHERE cod_cabecera = ? ORDER BY fecha_emision DESC";
    String SQL_NOTACD_CREATE = "INSERT INTO nota_cd (cod_cabecera, doc_afectado, fecha_emision, hora_emision, tipo_nota, nro_doc_cliente, descrip_motivo, situacion_sunat) VALUES (?,?,?,?,?,?,?,?)";

    
    /**************** Comunciacion de baja *****************/
    String SQL_COM_FIND_BY_ID = "SELECT * FROM COMUNICACION_BAJA WHERE numDocBaja = ?";
    String SQL_COM_FIND_BY_CODBAJA = "SELECT * FROM COMUNICACION_BAJA WHERE cod_baja = ?";
    String SQL_COM_FIND_ALL = "SELECT * FROM COMUNICACION_BAJA ORDER BY fecGeneracion DESC";
    String SQL_COM_SAVE = "INSERT INTO COMUNICACION_BAJA (cod_baja, fecGeneracion, fecComunicacion, tipDocBaja, numDocBaja, desMotivoBaja, situacion_sunat) VALUES (?,?,?,?,?,?,?)";

    /**************** Resumen diario *****************/
    String SQL_RESU_FIND_BY_ID = "SELECT * FROM RESUMEN_DIARIO WHERE idDocResumen = ?";
    String SQL_RESU_FIND_BY_CODRESUMEN = "SELECT * FROM RESUMEN_DIARIO WHERE cod_resumen = ?";
    String SQL_RESU_FIND_ALL = "SELECT * FROM RESUMEN_DIARIO ORDER BY fecEmision DESC";
    String SQL_RESU_SAVE = "INSERT INTO RESUMEN_DIARIO (cod_resumen, fecEmision, fecResumen, tipDocResumen, idDocResumen, situacion_sunat) VALUES (?,?,?,?,?,?)";
    
    /**************** Config email *****************/
    String SQL_CONFIGEMAIL_FIND_ALL = "SELECT * FROM config_email";
    String SQL_CONFIGEMAIL_UPDATE = "UPDATE config_email SET idconfig_email = ?, host = ?, puerto = ?, correo_empresa = ?, password = ?, asunto = ?, mensaje = ?";
    String SQL_CONFIGEMAIL_INSERT = "INSERT INTO config_email VALUES(?,?,?,?,?,?,?)";
}