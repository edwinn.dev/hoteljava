package com.edmi.model.invoice;

public class Tributo {
    private String ideTributo;
    private String nomTributo;
    private String codTipTributo;
    private String mtoBaseImponible;
    private String mtoTributo;
    
    public Tributo(){}

    public String getIdeTributo() {
        return ideTributo;
    }

    public void setIdeTributo(String ideTributo) {
        this.ideTributo = ideTributo;
    }

    public String getNomTributo() {
        return nomTributo;
    }

    public void setNomTributo(String nomTributo) {
        this.nomTributo = nomTributo;
    }

    public String getCodTipTributo() {
        return codTipTributo;
    }

    public void setCodTipTributo(String codTipTributo) {
        this.codTipTributo = codTipTributo;
    }

    public String getMtoBaseImponible() {
        return mtoBaseImponible;
    }

    public void setMtoBaseImponible(String mtoBaseImponible) {
        this.mtoBaseImponible = mtoBaseImponible;
    }

    public String getMtoTributo() {
        return mtoTributo;
    }

    public void setMtoTributo(String mtoTributo) {
        this.mtoTributo = mtoTributo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Tributo{ideTributo=").append(ideTributo);
        sb.append(", nomTributo=").append(nomTributo);
        sb.append(", codTipTributo=").append(codTipTributo);
        sb.append(", mtoBaseImponible=").append(mtoBaseImponible);
        sb.append(", mtoTributo=").append(mtoTributo);
        sb.append('}');
        return sb.toString();
    }
}
