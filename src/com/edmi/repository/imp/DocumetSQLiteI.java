package com.edmi.repository.imp;

import com.edmi.database.ConnectionSQLite;
import com.edmi.database.DocumentSQLite;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DocumetSQLiteI {
    public List<DocumentSQLite> getAllDocuments(){
        List<DocumentSQLite> docs = new ArrayList<>();
        DocumentSQLite doc;
        String sql = "SELECT * FROM DOCUMENTO";
        try {
            PreparedStatement ps = ConnectionSQLite.getInstanceSQLite().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                doc = new DocumentSQLite();
                doc.setNumeroRuc(rs.getString("NUM_RUC"));
                doc.setTipoDocumento(rs.getString("TIP_DOCU"));
                doc.setNroDocumento(rs.getString("NUM_DOCU"));
                doc.setFechaCarga(rs.getString("FEC_CARG"));
                doc.setFechaGeneracion(rs.getString("FEC_GENE"));
                doc.setFechaEnvio(rs.getString("FEC_ENVI"));
                doc.setObservacion(rs.getString("DES_OBSE"));
                doc.setNombreArcivo(rs.getString("NOM_ARCH"));
                doc.setSituacion(rs.getString("IND_SITU"));
                doc.setTipoArchivo(rs.getString("TIP_ARCH"));
                doc.setFirmaDigital(rs.getString("FIRM_DIGITAL"));
                docs.add(doc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DocumetSQLiteI.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return docs;
    }
    
    public static DocumentSQLite obtenerComprobantesPorNroDoc(String numDocu){
        DocumentSQLite document = null;
        try (PreparedStatement ps = ConnectionSQLite.getInstanceSQLite().prepareStatement("SELECT * FROM documento WHERE num_docu = ?")){
            ps.setString(1, numDocu);
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()){
                    document = new DocumentSQLite();
                    document.setNumeroRuc(rs.getString("NUM_RUC"));
                    document.setTipoDocumento(rs.getString("TIP_DOCU"));
                    document.setNroDocumento(rs.getString("NUM_DOCU"));
                    document.setFechaCarga(rs.getString("FEC_CARG"));
                    document.setFechaGeneracion(rs.getString("FEC_GENE"));
                    document.setFechaEnvio(rs.getString("FEC_ENVI"));
                    document.setObservacion(rs.getString("DES_OBSE"));
                    document.setNombreArcivo(rs.getString("NOM_ARCH"));
                    document.setSituacion(rs.getString("IND_SITU"));
                    document.setTipoArchivo(rs.getString("TIP_ARCH"));
                    document.setFirmaDigital(rs.getString("FIRM_DIGITAL"));
                }
            }
        } catch (SQLException ex) {
            //Logger.getLogger(DocumetSQLiteI.class.getName()).log(Level.SEVERE, null, ex);
            //System.err.println("La ruta de la base de datos del Facturador es incorrecta.");
            return null;
        }
        return document;
    }
}
