package com.edmi.database;

public class DocumentSQLite {
    private String numeroRuc;
    private String tipoDocumento;
    private String nroDocumento;
    private String fechaCarga;
    private String fechaGeneracion;
    private String fechaEnvio;
    private String observacion;
    private String nombreArcivo;
    private String situacion;
    private String tipoArchivo;
    private String firmaDigital;
    
    public DocumentSQLite(){}

    public String getNumeroRuc() {
        return numeroRuc;
    }

    public void setNumeroRuc(String numeroRuc) {
        this.numeroRuc = numeroRuc;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNroDocumento() {
        return nroDocumento;
    }

    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }

    public String getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(String fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public String getFechaGeneracion() {
        return fechaGeneracion;
    }

    public void setFechaGeneracion(String fechaGeneracion) {
        this.fechaGeneracion = fechaGeneracion;
    }

    public String getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(String fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getNombreArcivo() {
        return nombreArcivo;
    }

    public void setNombreArcivo(String nombreArcivo) {
        this.nombreArcivo = nombreArcivo;
    }

    public String getSituacion() {
        return situacion;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public String getFirmaDigital() {
        return firmaDigital;
    }

    public void setFirmaDigital(String firmaDigital) {
        this.firmaDigital = firmaDigital;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DocumentSQLite{numeroRuc=").append(numeroRuc);
        sb.append(", tipoDocumento=").append(tipoDocumento);
        sb.append(", nroDocumento=").append(nroDocumento);
        sb.append(", fechaCarga=").append(fechaCarga);
        sb.append(", fechaGeneracion=").append(fechaGeneracion);
        sb.append(", fechaEnvio=").append(fechaEnvio);
        sb.append(", observacion=").append(observacion);
        sb.append(", nombreArcivo=").append(nombreArcivo);
        sb.append(", situacion=").append(situacion);
        sb.append(", tipoArchivo=").append(tipoArchivo);
        sb.append(", firmaDigital=").append(firmaDigital);
        sb.append('}');
        return sb.toString();
    }
}
