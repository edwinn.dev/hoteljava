package com.edmi.view;

import static java.awt.Frame.MAXIMIZED_BOTH;
import com.edmi.controller.ControllerCliente;
import com.edmi.controller.ControllerEmployee;
import com.edmi.controller.ControllerMainPanel;
import com.edmi.controller.ControllerReport;
import com.edmi.controller.menu.ControllerArchivos;
import com.edmi.controller.menu.ControllerTableClient;
import com.edmi.controller.menu.ControllerTableEmployee;
import com.edmi.controller.panel.ControllerFacura;
import com.edmi.controller.panel.ControllerPanelRom;
import com.edmi.controller.panel.ControllerReservation;
import com.edmi.controller.panel.ControllerShopping;
import com.edmi.database.ConnectionDb;
import java.sql.Connection;
import com.edmi.model.Trabajador;
import com.edmi.repository.imp.TrabajadorRepositoryImp;
import com.edmi.util.Util;
import com.edmi.view.panel.PanelMainVoucher;
import com.edmi.view.panel.PanelVoucherTicket;
import java.awt.Dimension;
import java.awt.Image;
import java.io.File;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MainPanelServices extends JFrame {
    private String codigoUsuario, password;
    private ControllerMainPanel controllerMainPanel;
    private ControllerCliente controllerCliente;
    private ControllerArchivos controllerArchivos;
    private ControllerTableClient tableClient;
    private ControllerEmployee controllerEmploye;
    private ControllerReservation controllerReserva;
    private ControllerTableEmployee controllerTablTableEmpl;
    private ControllerFacura controllerFacura;

    public MainPanelServices() {
        initComponents();
        setPreferences();
        logo();
        setListeners();
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setMinimumSize(new Dimension(1270, 600));
    }

    private void logo() {
        File ruta = new File("design" + File.separator + "logo inicio");
        String[] img = ruta.list();
        String logo = ruta.getAbsolutePath() + File.separator + img[0];
        ImageIcon mostrar = new ImageIcon(new ImageIcon(logo).getImage().getScaledInstance(lblLogo.getWidth(), lblLogo.getHeight(), Image.SCALE_DEFAULT));
        lblLogo.setIcon(mostrar);
    }

    private void setListeners() {
        controllerMainPanel = new ControllerMainPanel(this);

        controllerEmploye = new ControllerEmployee(null, this);
        controllerTablTableEmpl = new ControllerTableEmployee(null, this);
        this.btnNuevoTrabajador.addActionListener(controllerMainPanel);
        this.txtFiltroUsuario.addKeyListener(controllerEmploye);
        this.checkMostrarContra.addActionListener(controllerEmploye);

        //Cliente
        controllerCliente = new ControllerCliente(this, null);
        this.btnNuevoCliente.addActionListener(controllerCliente);
        this.txtBuscarCliente.addKeyListener(controllerCliente);
        this.tableClients.addMouseListener(controllerCliente);
        tableClient = new ControllerTableClient(this);

        //Reservaciones
        controllerReserva = new ControllerReservation(this);
        controllerReserva.showReservas();
        controllerReserva.showRooms(cboEstadosFiltro.getSelectedIndex());

        //Menus
        controllerArchivos = new ControllerArchivos(this);

        //Facturacion
        controllerFacura = new ControllerFacura(this, -2, PanelVoucherTicket.getInstance(), PanelMainVoucher.getInstance());
        //controllerFacura.loadTableVouchers();

        //Inventario
//        ControllerInventary controllerInventary = new ControllerInventary(this);

        //Compras
        ControllerShopping controllerShopping = new ControllerShopping(this);

        //Habitaciones
        ControllerPanelRom controllerRom = new ControllerPanelRom(this);

//        //Reportes
        ControllerReport controllerReport = new ControllerReport(this);
    }

    private void setPreferences() {
        this.setTitle(Util.nombreEmpresa());
        this.setLocationRelativeTo(null);
    }

    public void mostrarNombreUsuario() {
        TrabajadorRepositoryImp repository = new TrabajadorRepositoryImp();
        Trabajador t = repository.findById(this.codigoUsuario);
        lblUsuario.setText(Util.getUserNamesConcatened(t.getNombre(), t.getApellidoPaterno(), t.getApellidoMaterno()));
    }

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabbedPanel = new javax.swing.JTabbedPane();
        capaInicio = new javax.swing.JLayeredPane();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btnFacturacion = new javax.swing.JButton();
        btnReservaciones = new javax.swing.JButton();
        btnHabitaciones = new javax.swing.JButton();
        btnControlClientes = new javax.swing.JButton();
        btnCompras = new javax.swing.JButton();
        btnTrabajadores = new javax.swing.JButton();
        btnReportes = new javax.swing.JButton();
        jScrollPane13 = new javax.swing.JScrollPane();
        panelRooms = new javax.swing.JPanel();
        lblLogo = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cboEstadosFiltro = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        capaReservas = new javax.swing.JLayeredPane();
        panelReservacion = new javax.swing.JPanel();
        capaFacturacion = new javax.swing.JLayeredPane();
        panelParentVoucher = new javax.swing.JPanel();
        capaHabitaciones = new javax.swing.JLayeredPane();
        panelHabitaciones = new javax.swing.JPanel();
        capaClientes = new javax.swing.JLayeredPane();
        btnNuevoCliente = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableClients = new javax.swing.JTable();
        txtBuscarCliente = new javax.swing.JTextField();
        cbxCriterioBusqueda = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        chkClienteDescartados = new javax.swing.JCheckBox();
        capaCompras = new javax.swing.JLayeredPane();
        panelCompras = new javax.swing.JPanel();
        capaTrabajadores = new javax.swing.JLayeredPane();
        jScrollPane14 = new javax.swing.JScrollPane();
        employeesTableEmployees = new javax.swing.JTable();
        txtFiltroUsuario = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        cbxCriterioUsuario = new javax.swing.JComboBox<>();
        checkMostrarContra = new javax.swing.JCheckBox();
        btnNuevoTrabajador = new javax.swing.JButton();
        chkUsuariosDescartados = new javax.swing.JCheckBox();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        panelReports = new javax.swing.JPanel();
        btnReturnReport = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        btnReportProducts = new javax.swing.JButton();
        btnReportReserva = new javax.swing.JButton();
        btnReportClients = new javax.swing.JButton();
        btnReportEmployee = new javax.swing.JButton();
        btnReportRoom = new javax.swing.JButton();
        btnReportVentas = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        menuArchivos = new javax.swing.JMenu();
        itemConfiguracion = new javax.swing.JMenuItem();
        itemRutaSFS = new javax.swing.JMenuItem();
        itemRutasArchivos = new javax.swing.JMenuItem();
        itemEmail = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        setMinimumSize(new java.awt.Dimension(1270, 600));
        setSize(new java.awt.Dimension(900, 600));
        setType(java.awt.Window.Type.POPUP);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        tabbedPanel.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tabbedPanel.setPreferredSize(new java.awt.Dimension(1122, 600));

        capaInicio.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Acceso rápido     ", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N
        jPanel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        btnFacturacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnFacturacion.setText("FACTURACION");
        btnFacturacion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFacturacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFacturacionActionPerformed(evt);
            }
        });

        btnReservaciones.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnReservaciones.setText("RESERVACIONES");
        btnReservaciones.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnReservaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReservacionesActionPerformed(evt);
            }
        });

        btnHabitaciones.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnHabitaciones.setText("HABITACIONES");
        btnHabitaciones.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnHabitaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHabitacionesActionPerformed(evt);
            }
        });

        btnControlClientes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnControlClientes.setText("CLIENTES");
        btnControlClientes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnControlClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnControlClientesActionPerformed(evt);
            }
        });

        btnCompras.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnCompras.setText("INVENTARIO");
        btnCompras.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCompras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnComprasActionPerformed(evt);
            }
        });

        btnTrabajadores.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnTrabajadores.setText("USUARIOS");
        btnTrabajadores.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnTrabajadores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTrabajadoresActionPerformed(evt);
            }
        });

        btnReportes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnReportes.setText("REPORTES");
        btnReportes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnReportes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReportesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnReportes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnFacturacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnReservaciones, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                    .addComponent(btnHabitaciones, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnControlClientes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCompras, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnTrabajadores, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnReservaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnFacturacion, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnHabitaciones, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnControlClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnReportes, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnCompras, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnTrabajadores, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(120, Short.MAX_VALUE))
        );

        jScrollPane13.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jScrollPane13.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        jScrollPane13.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        jScrollPane13.setOpaque(false);

        panelRooms.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        panelRooms.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout panelRoomsLayout = new javax.swing.GroupLayout(panelRooms);
        panelRooms.setLayout(panelRoomsLayout);
        panelRoomsLayout.setHorizontalGroup(
            panelRoomsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1052, Short.MAX_VALUE)
        );
        panelRoomsLayout.setVerticalGroup(
            panelRoomsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 685, Short.MAX_VALUE)
        );

        jScrollPane13.setViewportView(panelRooms);

        lblUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/perfil (1).png"))); // NOI18N

        cboEstadosFiltro.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cboEstadosFiltro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "TODOS", "DISPONIBLE", "MANTENIMIENTO", "OCUPADO" }));
        cboEstadosFiltro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboEstadosFiltroActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel1.setText("Estado:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lblLogo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane13, javax.swing.GroupLayout.DEFAULT_SIZE, 820, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cboEstadosFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addComponent(lblUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addGap(8, 8, 8)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(lblLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(lblUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cboEstadosFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel1)))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        capaInicio.setLayer(jPanel2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout capaInicioLayout = new javax.swing.GroupLayout(capaInicio);
        capaInicio.setLayout(capaInicioLayout);
        capaInicioLayout.setHorizontalGroup(
            capaInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        capaInicioLayout.setVerticalGroup(
            capaInicioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        tabbedPanel.addTab("INICIO", capaInicio);

        capaReservas.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        capaReservas.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        javax.swing.GroupLayout panelReservacionLayout = new javax.swing.GroupLayout(panelReservacion);
        panelReservacion.setLayout(panelReservacionLayout);
        panelReservacionLayout.setHorizontalGroup(
            panelReservacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1116, Short.MAX_VALUE)
        );
        panelReservacionLayout.setVerticalGroup(
            panelReservacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 732, Short.MAX_VALUE)
        );

        capaReservas.setLayer(panelReservacion, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout capaReservasLayout = new javax.swing.GroupLayout(capaReservas);
        capaReservas.setLayout(capaReservasLayout);
        capaReservasLayout.setHorizontalGroup(
            capaReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(capaReservasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelReservacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        capaReservasLayout.setVerticalGroup(
            capaReservasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(capaReservasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelReservacion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPanel.addTab("RESERVACIONES", capaReservas);

        capaFacturacion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        capaFacturacion.setLayer(panelParentVoucher, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout capaFacturacionLayout = new javax.swing.GroupLayout(capaFacturacion);
        capaFacturacion.setLayout(capaFacturacionLayout);
        capaFacturacionLayout.setHorizontalGroup(
            capaFacturacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(capaFacturacionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelParentVoucher, javax.swing.GroupLayout.DEFAULT_SIZE, 1116, Short.MAX_VALUE)
                .addContainerGap())
        );
        capaFacturacionLayout.setVerticalGroup(
            capaFacturacionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(capaFacturacionLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelParentVoucher, javax.swing.GroupLayout.DEFAULT_SIZE, 732, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPanel.addTab("FACTURACION", capaFacturacion);

        capaHabitaciones.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        javax.swing.GroupLayout panelHabitacionesLayout = new javax.swing.GroupLayout(panelHabitaciones);
        panelHabitaciones.setLayout(panelHabitacionesLayout);
        panelHabitacionesLayout.setHorizontalGroup(
            panelHabitacionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1116, Short.MAX_VALUE)
        );
        panelHabitacionesLayout.setVerticalGroup(
            panelHabitacionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 732, Short.MAX_VALUE)
        );

        capaHabitaciones.setLayer(panelHabitaciones, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout capaHabitacionesLayout = new javax.swing.GroupLayout(capaHabitaciones);
        capaHabitaciones.setLayout(capaHabitacionesLayout);
        capaHabitacionesLayout.setHorizontalGroup(
            capaHabitacionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(capaHabitacionesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelHabitaciones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        capaHabitacionesLayout.setVerticalGroup(
            capaHabitacionesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(capaHabitacionesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelHabitaciones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPanel.addTab("HABITACIONES", capaHabitaciones);

        capaClientes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        btnNuevoCliente.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnNuevoCliente.setText("NUEVO CLIENTE");
        btnNuevoCliente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        tableClients.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        tableClients.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "NOMBRES", "TIPO DE DOCUMENTO", "NRO. DE DOCUMENTO", "TELEFONO", "EMAIL", "OBSERVACIONES", "ESTADO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableClients.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tableClients.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tableClients);
        if (tableClients.getColumnModel().getColumnCount() > 0) {
            tableClients.getColumnModel().getColumn(0).setPreferredWidth(100);
            tableClients.getColumnModel().getColumn(1).setPreferredWidth(350);
            tableClients.getColumnModel().getColumn(2).setPreferredWidth(150);
            tableClients.getColumnModel().getColumn(3).setPreferredWidth(120);
            tableClients.getColumnModel().getColumn(4).setPreferredWidth(100);
            tableClients.getColumnModel().getColumn(5).setPreferredWidth(300);
            tableClients.getColumnModel().getColumn(6).setPreferredWidth(300);
            tableClients.getColumnModel().getColumn(7).setPreferredWidth(100);
        }

        txtBuscarCliente.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        cbxCriterioBusqueda.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxCriterioBusqueda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --", "CODIGO", "NOMBRE", "APE. PATERNO", "APE. MATERNO", "NRO. DOCUMENTO" }));

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Criterios:");

        chkClienteDescartados.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        chkClienteDescartados.setText("Clientes descartados");

        capaClientes.setLayer(btnNuevoCliente, javax.swing.JLayeredPane.DEFAULT_LAYER);
        capaClientes.setLayer(jScrollPane1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        capaClientes.setLayer(txtBuscarCliente, javax.swing.JLayeredPane.DEFAULT_LAYER);
        capaClientes.setLayer(cbxCriterioBusqueda, javax.swing.JLayeredPane.DEFAULT_LAYER);
        capaClientes.setLayer(jLabel2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        capaClientes.setLayer(chkClienteDescartados, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout capaClientesLayout = new javax.swing.GroupLayout(capaClientes);
        capaClientes.setLayout(capaClientesLayout);
        capaClientesLayout.setHorizontalGroup(
            capaClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(capaClientesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(capaClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(capaClientesLayout.createSequentialGroup()
                        .addComponent(btnNuevoCliente)
                        .addGap(18, 18, 18)
                        .addComponent(chkClienteDescartados)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 375, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbxCriterioBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtBuscarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        capaClientesLayout.setVerticalGroup(
            capaClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(capaClientesLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(capaClientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNuevoCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(chkClienteDescartados)
                    .addComponent(jLabel2)
                    .addComponent(cbxCriterioBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 669, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPanel.addTab("CONTROL CLIENTES", capaClientes);

        capaCompras.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        javax.swing.GroupLayout panelComprasLayout = new javax.swing.GroupLayout(panelCompras);
        panelCompras.setLayout(panelComprasLayout);
        panelComprasLayout.setHorizontalGroup(
            panelComprasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1116, Short.MAX_VALUE)
        );
        panelComprasLayout.setVerticalGroup(
            panelComprasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 732, Short.MAX_VALUE)
        );

        capaCompras.setLayer(panelCompras, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout capaComprasLayout = new javax.swing.GroupLayout(capaCompras);
        capaCompras.setLayout(capaComprasLayout);
        capaComprasLayout.setHorizontalGroup(
            capaComprasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(capaComprasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelCompras, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        capaComprasLayout.setVerticalGroup(
            capaComprasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(capaComprasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelCompras, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPanel.addTab("INVENTARIO", capaCompras);

        capaTrabajadores.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        employeesTableEmployees.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        employeesTableEmployees.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CODIGO", "NOMBRES", "TIPO DOCUMENTO", "N° DOCUMENTO", "N° TELEFONO", "CORREO ELECTRONICO", "OBSERVACION", "ROL", "USERNAME", "PASSWORD", "Estado"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        employeesTableEmployees.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        employeesTableEmployees.setAutoscrolls(false);
        employeesTableEmployees.setShowVerticalLines(false);
        employeesTableEmployees.getTableHeader().setReorderingAllowed(false);
        employeesTableEmployees.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                employeesTableEmployeesMouseClicked(evt);
            }
        });
        jScrollPane14.setViewportView(employeesTableEmployees);
        if (employeesTableEmployees.getColumnModel().getColumnCount() > 0) {
            employeesTableEmployees.getColumnModel().getColumn(0).setPreferredWidth(150);
            employeesTableEmployees.getColumnModel().getColumn(1).setPreferredWidth(300);
            employeesTableEmployees.getColumnModel().getColumn(2).setPreferredWidth(300);
            employeesTableEmployees.getColumnModel().getColumn(3).setPreferredWidth(110);
            employeesTableEmployees.getColumnModel().getColumn(4).setPreferredWidth(100);
            employeesTableEmployees.getColumnModel().getColumn(5).setPreferredWidth(300);
            employeesTableEmployees.getColumnModel().getColumn(6).setPreferredWidth(250);
            employeesTableEmployees.getColumnModel().getColumn(7).setPreferredWidth(150);
            employeesTableEmployees.getColumnModel().getColumn(8).setPreferredWidth(150);
            employeesTableEmployees.getColumnModel().getColumn(9).setPreferredWidth(200);
            employeesTableEmployees.getColumnModel().getColumn(10).setPreferredWidth(100);
        }

        txtFiltroUsuario.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        jLabel12.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Criterios:");

        cbxCriterioUsuario.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cbxCriterioUsuario.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "-- SELECCIONAR --", "CODIGO", "ROL", "NOMBRE", "APE. PATERNO", "APE. MATERNO", "NRO. DOCUMENTO" }));

        checkMostrarContra.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        checkMostrarContra.setText("Mostrar contraseña");

        btnNuevoTrabajador.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnNuevoTrabajador.setText("NUEVO USUARIO");

        chkUsuariosDescartados.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        chkUsuariosDescartados.setText("Usuarios descartados");

        capaTrabajadores.setLayer(jScrollPane14, javax.swing.JLayeredPane.DEFAULT_LAYER);
        capaTrabajadores.setLayer(txtFiltroUsuario, javax.swing.JLayeredPane.DEFAULT_LAYER);
        capaTrabajadores.setLayer(jLabel12, javax.swing.JLayeredPane.DEFAULT_LAYER);
        capaTrabajadores.setLayer(cbxCriterioUsuario, javax.swing.JLayeredPane.DEFAULT_LAYER);
        capaTrabajadores.setLayer(checkMostrarContra, javax.swing.JLayeredPane.DEFAULT_LAYER);
        capaTrabajadores.setLayer(btnNuevoTrabajador, javax.swing.JLayeredPane.DEFAULT_LAYER);
        capaTrabajadores.setLayer(chkUsuariosDescartados, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout capaTrabajadoresLayout = new javax.swing.GroupLayout(capaTrabajadores);
        capaTrabajadores.setLayout(capaTrabajadoresLayout);
        capaTrabajadoresLayout.setHorizontalGroup(
            capaTrabajadoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(capaTrabajadoresLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(capaTrabajadoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane14)
                    .addGroup(capaTrabajadoresLayout.createSequentialGroup()
                        .addComponent(btnNuevoTrabajador)
                        .addGap(18, 18, 18)
                        .addGroup(capaTrabajadoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(checkMostrarContra, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(chkUsuariosDescartados))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 355, Short.MAX_VALUE)
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbxCriterioUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtFiltroUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        capaTrabajadoresLayout.setVerticalGroup(
            capaTrabajadoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(capaTrabajadoresLayout.createSequentialGroup()
                .addGroup(capaTrabajadoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(capaTrabajadoresLayout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(capaTrabajadoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbxCriterioUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12)
                            .addComponent(txtFiltroUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(capaTrabajadoresLayout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(capaTrabajadoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, capaTrabajadoresLayout.createSequentialGroup()
                                .addComponent(chkUsuariosDescartados, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(checkMostrarContra))
                            .addComponent(btnNuevoTrabajador, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane14, javax.swing.GroupLayout.DEFAULT_SIZE, 666, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPanel.addTab("USUARIOS", capaTrabajadores);

        jLayeredPane1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        javax.swing.GroupLayout panelReportsLayout = new javax.swing.GroupLayout(panelReports);
        panelReports.setLayout(panelReportsLayout);
        panelReportsLayout.setHorizontalGroup(
            panelReportsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelReportsLayout.setVerticalGroup(
            panelReportsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 660, Short.MAX_VALUE)
        );

        btnReturnReport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/edmi/images/icon_left.png"))); // NOI18N
        btnReturnReport.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnReportProducts.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnReportProducts.setText("PRODUCTOS");
        btnReportProducts.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnReportReserva.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnReportReserva.setText("RESERVACIONES");
        btnReportReserva.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnReportClients.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnReportClients.setText("CLIENTES");
        btnReportClients.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnReportEmployee.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnReportEmployee.setText("USUARIOS");
        btnReportEmployee.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnReportRoom.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnReportRoom.setText("HABITACIONES");
        btnReportRoom.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        btnReportVentas.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        btnReportVentas.setText("VENTAS");
        btnReportVentas.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnReportProducts, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnReportReserva)
                .addGap(18, 18, 18)
                .addComponent(btnReportClients, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnReportEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnReportRoom)
                .addGap(18, 18, 18)
                .addComponent(btnReportVentas, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(238, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnReportProducts, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReportReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReportClients, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReportEmployee, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReportRoom, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReportVentas, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLayeredPane1.setLayer(panelReports, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(btnReturnReport, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane1.setLayer(jPanel3, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jLayeredPane1Layout = new javax.swing.GroupLayout(jLayeredPane1);
        jLayeredPane1.setLayout(jLayeredPane1Layout);
        jLayeredPane1Layout.setHorizontalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelReports, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jLayeredPane1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(btnReturnReport, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jLayeredPane1Layout.setVerticalGroup(
            jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jLayeredPane1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jLayeredPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReturnReport))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelReports, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        tabbedPanel.addTab("REPORTES", jLayeredPane1);

        jMenuBar1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        menuArchivos.setText("Configuracion");
        menuArchivos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        itemConfiguracion.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemConfiguracion.setText("Empresa");
        menuArchivos.add(itemConfiguracion);

        itemRutaSFS.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemRutaSFS.setText("Ruta SFS");
        menuArchivos.add(itemRutaSFS);

        itemRutasArchivos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemRutasArchivos.setText("Rutas archivos");
        menuArchivos.add(itemRutasArchivos);

        itemEmail.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        itemEmail.setText("Email");
        menuArchivos.add(itemEmail);

        jMenuBar1.add(menuArchivos);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabbedPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 1141, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabbedPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 782, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        int op = JOptionPane.showConfirmDialog(this, "¿Desea salir del sistema?", "Mensaje del Sistema", JOptionPane.YES_NO_OPTION);
        if (op == JOptionPane.YES_OPTION) {
            try {
                Connection connection = ConnectionDb.getInstance();
                connection.close();
                System.exit(0);
            } catch (SQLException ex) {
                Logger.getLogger(MainPanelServices.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_formWindowClosing

    private void employeesTableEmployeesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_employeesTableEmployeesMouseClicked
        if (evt.getClickCount() == 2) {
            int row = employeesTableEmployees.getSelectedRow();
            ModalNewEmployee modal = new ModalNewEmployee(this, true);
            ControllerEmployee controllerEmployee = new ControllerEmployee(modal, this);
            modal.btnRegistrarTrabajador.setEnabled(false);
            modal.setTitle("Modificar datos de usuario");
            controllerEmployee.loadData();
            controllerEmployee.mostrarDatosTabla(row, employeesTableEmployees);
        }
    }//GEN-LAST:event_employeesTableEmployeesMouseClicked

    private void cboEstadosFiltroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboEstadosFiltroActionPerformed
        controllerReserva.showRooms(cboEstadosFiltro.getSelectedIndex());
    }//GEN-LAST:event_cboEstadosFiltroActionPerformed

    private void btnReportesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReportesActionPerformed
        tabbedPanel.setSelectedComponent(jLayeredPane1);
    }//GEN-LAST:event_btnReportesActionPerformed

    private void btnTrabajadoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTrabajadoresActionPerformed
        tabbedPanel.setSelectedComponent(capaTrabajadores);
    }//GEN-LAST:event_btnTrabajadoresActionPerformed

    private void btnComprasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnComprasActionPerformed
        tabbedPanel.setSelectedComponent(capaCompras);
    }//GEN-LAST:event_btnComprasActionPerformed

    private void btnControlClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnControlClientesActionPerformed
        tabbedPanel.setSelectedComponent(capaClientes);
    }//GEN-LAST:event_btnControlClientesActionPerformed

    private void btnHabitacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHabitacionesActionPerformed
        tabbedPanel.setSelectedComponent(capaHabitaciones);
    }//GEN-LAST:event_btnHabitacionesActionPerformed

    private void btnReservacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReservacionesActionPerformed
        tabbedPanel.setSelectedComponent(capaReservas);
    }//GEN-LAST:event_btnReservacionesActionPerformed

    private void btnFacturacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFacturacionActionPerformed
        tabbedPanel.setSelectedComponent(capaFacturacion);
    }//GEN-LAST:event_btnFacturacionActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnCompras;
    public javax.swing.JButton btnControlClientes;
    public javax.swing.JButton btnFacturacion;
    public javax.swing.JButton btnHabitaciones;
    public javax.swing.JButton btnNuevoCliente;
    public javax.swing.JButton btnNuevoTrabajador;
    public javax.swing.JButton btnReportClients;
    public javax.swing.JButton btnReportEmployee;
    public javax.swing.JButton btnReportProducts;
    public javax.swing.JButton btnReportReserva;
    public javax.swing.JButton btnReportRoom;
    public javax.swing.JButton btnReportVentas;
    public javax.swing.JButton btnReportes;
    private javax.swing.JButton btnReservaciones;
    public javax.swing.JButton btnReturnReport;
    public javax.swing.JButton btnTrabajadores;
    public javax.swing.JLayeredPane capaClientes;
    public javax.swing.JLayeredPane capaCompras;
    public javax.swing.JLayeredPane capaFacturacion;
    public javax.swing.JLayeredPane capaHabitaciones;
    public javax.swing.JLayeredPane capaInicio;
    public javax.swing.JLayeredPane capaReservas;
    public javax.swing.JLayeredPane capaTrabajadores;
    private javax.swing.JComboBox<String> cboEstadosFiltro;
    public javax.swing.JComboBox<String> cbxCriterioBusqueda;
    public javax.swing.JComboBox<String> cbxCriterioUsuario;
    public javax.swing.JCheckBox checkMostrarContra;
    public javax.swing.JCheckBox chkClienteDescartados;
    public javax.swing.JCheckBox chkUsuariosDescartados;
    public javax.swing.JTable employeesTableEmployees;
    public javax.swing.JMenuItem itemConfiguracion;
    public javax.swing.JMenuItem itemEmail;
    public javax.swing.JMenuItem itemRutaSFS;
    public javax.swing.JMenuItem itemRutasArchivos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JLabel lblLogo;
    public javax.swing.JLabel lblUsuario;
    public javax.swing.JMenu menuArchivos;
    public javax.swing.JPanel panelCompras;
    public javax.swing.JPanel panelHabitaciones;
    public javax.swing.JPanel panelParentVoucher;
    public javax.swing.JPanel panelReports;
    public javax.swing.JPanel panelReservacion;
    public javax.swing.JPanel panelRooms;
    public javax.swing.JTabbedPane tabbedPanel;
    public javax.swing.JTable tableClients;
    public javax.swing.JTextField txtBuscarCliente;
    public javax.swing.JTextField txtFiltroUsuario;
    // End of variables declaration//GEN-END:variables
}
