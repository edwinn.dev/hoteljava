package com.edmi.util;

public interface MessageView {
    public static final String MODAL_PRODUCT_VALIDATE = "Los siguientes campos son requeridos: \n* Nombre de producto\n* Cantidad\n* Precio de compra\n* Precio de venta\n* Fecha de vencimiento";
    public static final String MODAL_PRODUCT_INSERT_OK = "Producto guardado";
    public static final String MODAL_PRODUCT_INSERT_FAILED = "Error al registrar producto";
    public static final String MODAL_PRODUCT_UPDATE_OK = "Producto actualizado";
    public static final String MODAL_PRODUCT_UPDATE_FAILED = "Error al actualizar producto";
    
    
    /**** Formulario Categoria productos *****/
    public static final String MODAL_CATEGORY_VALIDATE = "El siguiente campo es requerido:\n* Nombre de categoria";
    
    /**** Formulario Clientes*****/
    String MODAL_CLIENT_VALIDATE = "Los siguientes campos son requeridos:\n* Nombre\n* Apellido paterno\n* Apellido materno\n* Numero de documento";
    String MODAL_CLIENT_SELECTED_INDEX_ERROR = "Seleccione un tipo de documento valido";
    
    /*** Formulario Trabajadores ***/
    public static final String MODAL_EMPLOYEE_VALIDATE = "Los siguientes campos son requeridos:\n* Nombre\n* Apellido paterno\n* Apellido materno\n* Numero de documento";
    public static final String MODAL_EMPLOYEE_SELECTED_INDEX_ERROR = "Seleccione un tipo de documento valido";
    public static final String MODAL_EMPLOYEE_SELECTED_INDEX_ERROR2 = "Seleccione un estado.";
    
    /*** Formulario Habitaciones ***/
    public static final String MROOM_VALIDATE = "Los campos:\n* Numero de habitacion\n* Costo de alquiler\nson requeridos.";
    public static final String MROOM_UPDATE_STATE_ERROR = "Error, la habitacion ya existe";
    public static final String MROOM_UPDATE_STATE_OK = "Se ha cambiado el estado de la habitacion";
    public static final String MROOM_NO_SELECTED = "Debe seleccionar una habitacion";
    
    /*** Formulario Tipo de habitaciones ***/
    public static final String MODAL_TYPE_ROOM_VALIDATE = "La capacidad y el nombre de tipo de habitacion son requeridos";

    /*** Formulario Proveedores ***/
    public static final String MODAL_SUPLIER_VALIDATE = "Los siguientes campos son requeridos:\n* Nombre - razon social\n* Provincia\n* Numero de documento\n* Direccion\n* Producto(s) sumninistrado(s)";
    
    /*** Formulario Reservas ***/
    String MODAL_RESERVA_INSERT_ERROR = "Error al guardar reserva, verifique los datos";
    String MODAL_RESERVA_INSERT_EXIT = "Reserva registrada";
    String MODAL_RESERVA_QUESTION_CREATE = "¿Está seguro de registrar esta reserva?";
    String MODAL_RESERVA_QUESTION_CANCEL = "Esta seguro dar de baja esta reservacion?";
    String MODAL_RESERVA_QUESTION_RETURN = "Esta seguro retomar esta reservacion?";
    String MODAL_RESERVA_NO_OPTION = "Ha cancelado esta operacion";
    String MODAL_RESERVA_CANCEL_OK = "Esta reserva se ha dado de baja";
    String MODAL_RESERVA_RETURN_OK = "Esta reserva se ha retomado";
    String MODAL_RESERVA_CANCEL_FAILED = "No se pudo dar de baja, revise si la reserva existe";
    String MODAL_RESERVA_OPERATION = "No se pudo completar esta operacion";
    
    public static final String DOCUMENT_INVALID = "El numero de documeto es incorrecto";
    
    /*** Formulario - Agregar Porductos a una Reserva ***/
    public static final String DIALOG_ADD_PRODUCT_QUANTITY = "Es necesario ingresar una cantidad\npara cada producto";
    public static final String DIALOG_ADD_PRODUCT_OUT = "¿Desea salir sin guardar cambios?";
    public static final String DIALOG_ADD_PRODUCT_EXIST = "El producto ya existe.";

    /*** Tarjeta - Habitacion (Card) ***/
    public static final String CARD_ROOM_ADD_PRODUCTS = "Habitacion Ocupada!!! Presione [SI] para\n"
            + "realizar cualquiera de las dos operaciones\n\n"
            + "1. Agregar productos a la reservacion.\n2. Ver detalle de la reservacion.";
    
    /*** Consumo servicio - reserva ***/
    public static final String DIALOG_CONSUMO_CREATE_ERROR = "Error al agregar producto";
    public static final String DIALOG_CONSUMO_CREATE_OKs = "Producto agregado al consumo";
}
