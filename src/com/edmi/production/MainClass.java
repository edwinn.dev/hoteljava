package com.edmi.production;

import com.edmi.controller.ControllerConexionBD;
import com.edmi.util.SplashWindow;
import com.edmi.view.Login;
import com.formdev.flatlaf.FlatLightLaf;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class MainClass {    
    public static void main(String[] args) {        
        SplashWindow splash = new SplashWindow(3000);
        
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
        }
        
        try { 
            UIManager.setLookAndFeel ( new FlatLightLaf()); 
        } catch (UnsupportedLookAndFeelException ex) { 
            System.err.println( "No se pudo inicializar LaF" ); 
        }
        
        java.awt.EventQueue.invokeLater(() -> {
            ControllerConexionBD cn = new ControllerConexionBD(null);
            cn.conectarDatosBD();
            Login loginForm = new Login();
            loginForm.setVisible(true);
        });
    }
}
